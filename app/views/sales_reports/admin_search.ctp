<?php
echo $this->element('program/program_default_js');
?>
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){
    $('#sales_reports').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true});
    $(window).resize(function(){});
    $(window).scroll(function(){});
    $('.delete').live('click',function(){});
    $("#q").focus();
});
/* ]]> */
</script>
<?php
echo $this->Html->css(array(
        'jquery.autocomplete',
        'fcbkselection'
    )
);

echo $this->Html->script(array(
        'jquery/jquery.highlight'
    )
);
?>
<script type="text/javascript">
/* <![CDATA[ */
if(!String.prototype.startsWith){
    String.prototype.startsWith = function (str) {
        return !this.indexOf(str);
    }
}

$(document).ready(function(){
<?php
$qValue = null;
$sUrl   = '/';
if (isset($this->params['url']['q']) && strlen($this->params['url']['q'])> 0) {
    $qValue = $this->params['url']['q'];
}

if (isset($this->params['url']['division']) && strlen($this->params['url']['division'])> 0) {
    $qValue = $this->params['url']['division'];
}

if( !is_null($qValue) ){
    $aKeywordsSearch = explode(" ",$qValue);
    $i = 0;
    foreach( $aKeywordsSearch as $keyword ){
        if( strlen(trim($keyword)) >= 3 ){
            if( $i < 1 ){
                echo "$('.listing').removeHighlight().highlight('".addslashes($keyword)."');";
            }else{
                echo "$('.listing').highlight('".addslashes($keyword)."');";
            }
            $i++;
        }
    }
}

if( isset($program_type) ){
    ?>
    $('.listing').highlight(<?php echo "'".addslashes($program_type)."'";?>);
    <?php
}
?>

    $("#cancel").live("click",function(){
        $("#dialog").dialog('close');
    });

    $("#q").focus();
});

/* ]]> */
</script>
<style type="text/css">
    .listing .box1 {
        float:left !important;
        padding:0 0 0 6px !important;
        width:270px !important;
    }
    .listing .box2 {
        float: left !important;
        text-align: left !important;
        width: 170px !important;
    }

    .listing .box3 {
        float: left !important;
        text-align: left !important;
        width: 190px !important;
    }

    .listing .box5 {
        float: left !important;
        padding: 6px 0 0 !important;
        text-align: left !important;
    }

    .listing .box6 {
        float: left !important;
        padding: 6px 0 0 !important;
        text-align: left !important;
        width: 210px !important;
    }

    .listing .box7 {
        float: left !important;
        padding: 6px 0 0 !important;
        text-align: left !important;
        width: 111px !important;
    }
</style>
<?php
if( isset($this->params['url']['q']) ){
    $this->Html->addCrumb('Sales Reports', '/admin/sales_reports');
    $this->Html->addCrumb('Search Sales Reports', '/admin/sales_reports/search?q='.$this->params['url']['q']);
    $this->Html->addCrumb('<strong>'.trim($this->params['url']['q']).'</strong>');
}else{
    $this->Html->addCrumb('Sales Reports', '/admin/sales_reports');
    $this->Html->addCrumb('<strong>Search Sales Reports</strong>');
}
?>
<div class="sales_reports index">
    <h2><?php __('Search Sales Reports');?></h2>
    <?php echo $this->element('sales_reports/search_advance_salesreports'); ?>
    <div>
        <div id="sales_reports">
            <ul>
                <?php
                if(empty($sales_reports)){
                    ?>
                    <li><a href="#all_salesreports"><span>Empty result</span></a></li>
                    <?php
                }else{
                    $qValue .= (empty($qValue) && isset($schedule_date)) ? '&nbsp;&nbsp;Schedule Date:&nbsp;'.$schedule_date: null;
                    if( is_null($qValue) || strlen(trim($qValue)) <1 || empty($qValue) ){
                        ?>
                        <li><a href="#all_salesreports"><span>Search Sales Reports</span></a></li>
                        <?php
                    }else{
                        ?>
                        <li><a href="#all_salesreports"><span>Search results for <?php echo '<strong>'.$qValue.'</strong>';?></span></a></li>
                        <?php
                    }
                }
                ?>
            </ul>
            <?php
            if(!empty($sales_reports)  && count($sales_reports) ){
            ?>
            <div id="all_salesreports">

                    <table cellspacing="0" cellpadding="0" style="background-color:#FFFFFF;border:1px solid #DDDDDD;clear:both;width:100%;">
                    <?php
                    if(!empty($sales_reports)  && count($sales_reports) ){
                    ?>
                    <tr>
                        <td align="center"  colspan="5">
                            <div class="paging">
                        <?php
                        $this->Paginator->options(array('url' => '/'));
                        $sPaginator = $this->Paginator->first('<< First').' '.
                        $this->Paginator->prev('< Previous') .' '.
                        $this->Paginator->numbers().' '.
                        $this->Paginator->next('Next >'). ' '.
                        $this->Paginator->last('Last >>');
                        echo $sPaginator."<br />";
                        echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));
                        ?>
                            </div>
                        </td>
                    </tr>
                    <?php
                    }
                    ?>
                    <tr>
                        <th style="border-right: 1px solid rgb(223, 223, 223);width:250px;">Program</th>
                        <th style="border-right: 1px solid rgb(223, 223, 223);width:150px;">Reference Code</th>
                        <th style="border-right: 1px solid rgb(223, 223, 223);">Month Year </th>
                        <th style="border-right: 1px solid rgb(223, 223, 223);">Date Created</th>
                        <th style="border-right: 1px solid rgb(223, 223, 223);">Who Created</th>
                    </tr>
                    <tr>
                        <td colspan="5">
                        <?php
                        if( isset($sales_reports) && is_array($sales_reports) ){
                            $i = 0;
                            foreach( $sales_reports as $sales_reports_key => $sales_reports_info ){

                                $this->SalesReports->setSalesReportsData($sales_reports_info);
                                $salesReportsDirectory = $this->SalesReports->getSalesReportsDirectory();
                                $salesReportsDetailsInfo = $this->SalesReportsDetails->loadSalesReportsDetailsById(intval($sales_reports_info['SalesReports']['id']));
                                $this->SalesReportsDetails->setSalesReportsDetailsData($salesReportsDetailsInfo);
                                $sCss = ( ($i % 2) ==0 )? 'listing_accomm':'listing_tr';
                        ?>
                            <div class="listing" id="<?php echo $sCss;?>">
                                <div class="box1" id="all_programs_<?php echo $salesReportsDetailsInfo['SalesReportsDetails']['id'];?>">
                                    <div><?php echo str_replace('"','',$salesReportsDetailsInfo['SalesReports']['source_file']);?></div>
                                </div><!--end box1-->
                                <div class="box2"><?php echo $salesReportsDetailsInfo['SalesReports']['reference_code'];?></div>
                                <div class="box3"><?php echo $salesReportsDetailsInfo['SalesReports']['report_date'];?></div>
                                <div class="box6">
                                    <?php echo date("D j",strtotime(trim($salesReportsDetailsInfo['SalesReports']['date_created']))).'<sup>'.date("S",strtotime(trim($salesReportsDetailsInfo['SalesReports']['date_created']))).'</sup>&nbsp;'.date("F Y h:i:s A",strtotime(trim($salesReportsDetailsInfo['SalesReports']['date_created'])));?>
                                </div>
                                <div class="box7"><?php echo isset($users[$salesReportsDetailsInfo['SalesReports']['who_created']]) ? $users[$salesReportsDetailsInfo['SalesReports']['who_created']]:null;?></div>
                                <div class="box4"></div>
                                <div class="clearboth"></div>
                            </div><!--end listing-->
                        <?php
                                $i++;
                            }//end of foreach
                        }//end of programs attendance sheet
                        ?>
                        </td>
                    </tr>
                    <?php
                    if(!empty($sales_reports)){
                    ?>
                        <tr><td align="center" colspan="5">
                            <div class="paging">
                            <?php
                            $this->Paginator->options(array('url' => '/'));
                            $sPaginator = $this->Paginator->first('<< First').' '.
                            $this->Paginator->prev('< Previous') .' '.
                            $this->Paginator->numbers().' '.
                            $this->Paginator->next('Next >'). ' '.
                            $this->Paginator->last('Last >>');
                            echo $sPaginator."<br />";
                            echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));
                            ?>
                            </div>
                        </td></tr>
                    <?php
                    }
                    ?>
                    </table>

            </div><!-- end of search -->
            <?php
            }//end of isset and count
            ?>
        </div>
    </div>
</div>
