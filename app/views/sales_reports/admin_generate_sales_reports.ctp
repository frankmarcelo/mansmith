<?php
echo $this->Html->css(array(
        'jquery.autocomplete',
        'fcbkselection'
    )
);
?>
<script type="text/javascript">
    /* <![CDATA[ */
    $(document).ready(function(){
        $('#sales_reports').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true});
        $(window).resize(function(){});
        $(window).scroll(function(){});
    });
    /* ]]> */
</script>
<script type="text/javascript">
    /* <![CDATA[ */
    if(!String.prototype.startsWith){
        String.prototype.startsWith = function (str) {
            return !this.indexOf(str);
        }
    }
    /* ]]> */
</script>
<style type="text/css">
    .listing
    {
        width:1040px !important;
    }
    .listing .box1 {
        float:left !important;
        padding:0 0 0 6px !important;
        width:270px !important;
    }
    .listing .box2 {
        float: left !important;
        text-align: left !important;
        width: 170px !important;
    }

    .listing .box3 {
        float: left !important;
        text-align: left !important;
        width: 190px !important;
    }

    .listing .box5 {
        float: left !important;
        padding: 6px 0 0 !important;
        text-align: left !important;
    }

    .listing .box6 {
        float: left !important;
        padding: 6px 0 0 !important;
        text-align: left !important;
        width: 210px !important;
    }

    .listing .box7 {
        float: left !important;
        padding: 6px 0 0 !important;
        text-align: left !important;
        width: 111px !important;
    }

    .add_info {
        -moz-border-bottom-colors: #49E20E !important;
        -moz-border-image: #49E20E !important;
        -moz-border-left-colors: #49E20E !important;
        -moz-border-right-colors: #49E20E !important;
        -moz-border-top-colors: #49E20E !important;
        background-color: #49E20E !important;
        border-style: solid !important;
        border-width: 1px !important;
        font-weight: bold !important;
        margin-left: 5px !important;
        padding: 2px 4px !important;
        text-decoration: none !important;
        width: 180px !important;
    }
</style>
<?php  $this->Layout->sessionFlash();?>
<div class="sales_reports index">
    <h2>Generate Sales Reports</h2>
    <div id="sales_reports">
        <ul><li><a href="#all_salesreports"><span>Generate Sales Reports</span></a></li></ul>
        <div id="all_salesreports">
            <form name="SearchSalesReports" method="get" accept-charset="utf-8" action="<?php echo $this->Html->url(array("admin"=>true, "controller" => "sales_reports" ,"action"=>"generate_sales_reports"));?>">
                <table>
                    <tr><td>
                        <?php
                            $invoice_date_month= null;
                            $invoice_date_year = null;
                            if( isset($year) && $year ){
                                $invoice_date_year = $year;
                            }

                            if( isset($month) && $month ){
                                $invoice_date_month = $month;
                            }

                            $invoice_date_year = ($invoice_date_year==null) ? date("Y"): $invoice_date_year;
                            $invoice_date_month = ($invoice_date_month==null) ? date("m"):$invoice_date_month;
                            ?>Select Date:
                            <select id="month" name="month">
                                <?php
                                for( $i = 1; $i < 13; $i++){
                                    $month_name = date('F', mktime(0, 0, 0, $i, 1, date("Y")));
                                    $month_id = date('m', mktime(0, 0, 0, $i, 1, date("Y")));

                                    $selected_month = ($invoice_date_month==$month_id) ?'selected="selected" ':' ';
                                    ?><option <?php echo $selected_month;?>value="<?php echo $month_id;?>"><?php echo $month_name;?></option><?php
                                }
                                ?>
                            </select>
                            <select id="year" name="year">
                                <?php
                                $from = 2001;
                                $until = $from+20;//20 years plus start 2001
                                for( $i = $from ; $i < $until; $i++){
                                    $selected_year = ($invoice_date_year==$i) ?'selected="selected" ':' ';
                                    ?><option <?php echo $selected_year;?>value="<?php echo $i;?>"><?php echo $i;?></option>
                                    <?php
                                }
                                ?>
                            </select>&nbsp;<input type="submit" name="submit" value="submit" />
                    </td></tr>
                </table>
            </form>
            <br>
            <?php
            if( isset($programs) && $programs ){
            ?>
            <form name="SalesReports" id="SalesReports" method="post" accept-charset="utf-8" action="<?php echo $this->Html->url(array("admin"=>true, "controller" => "sales_reports" ,"action"=>"add_generate_sales_reports"));?>">
                <input type="hidden" name="data[SalesReports][month]" value="<?php echo ( isset($month) && $month ) ? $month:'';?>" />
                <input type="hidden" name="data[SalesReports][year]" value="<?php echo ( isset($year) && $year ) ? $year:'';?>" />
            <table cellspacing="0" cellpadding="0" style="background-color:#FFFFFF;border:1px solid #DDDDDD;clear:both;width:100%;">
                <tr>
                    <th style="border-right: 1px solid rgb(223, 223, 223);width:25px;">&nbsp;</th>
                    <th style="border-right: 1px solid rgb(223, 223, 223);width:40%;">Program</th>
                    <th style="border-right: 1px solid rgb(223, 223, 223);width:15%;">Schedule</th>
                    <th style="border-right: 1px solid rgb(223, 223, 223);width:40%" align="left">Venue</th>
                </tr>
                <?php
                $i = 0;
                foreach( $programs as $program_key => $program_info ){
                    $this->Program->setProgramData( $program_info['Program'] );
                    $this->ProgramExtraInfo->setExtraInfo( $program_info );
                    $sCss = ( ($i % 2) ==0 )? 'listing_accomm':'listing_tr';
                ?>
                <tr>
                    <td><input type="checkbox" id="report_<?php echo $this->Program->id;?>" name="data[SalesReports][id][]" value="<?php echo $this->Program->id;?>" /></td>
                    <td><a href="<?php echo $this->Html->url(array('controller'=>'programs','action'=>'info',$this->Program->getSeoUri()));?>"><?php echo $this->Program->getTitle();?></a></td>
                    <td><?php echo $this->ProgramExtraInfo->getSchedule($shortcut=false);?></td>
                    <td><?php echo $this->Program->getFullAddress();?></td>
                </tr>
                <?php
                }
                ?>
            </table>
            <br>
            <input class="add_info" type="submit" name="submit" value="Generate Sales Report" />&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" name="reset" value="Reset" />
            </form>
            <?php
            }
            ?>
            <br>
            <table cellspacing="0" cellpadding="0" style="background-color:#FFFFFF;border:1px solid #DDDDDD;clear:both;width:100%;">

                <tr>
                    <th style="border-right: 1px solid rgb(223, 223, 223);width:250px;">Program</th>
                    <th style="border-right: 1px solid rgb(223, 223, 223);width:150px;">Reference Code</th>
                    <th style="border-right: 1px solid rgb(223, 223, 223);">Month Year </th>
                    <th style="border-right: 1px solid rgb(223, 223, 223);">Date Created</th>
                    <th style="border-right: 1px solid rgb(223, 223, 223);">Who Created</th>
                </tr>
                <tr>
                    <td colspan="5">
                        <?php
                        if( isset($sales_reports) && is_array($sales_reports) ){
                            $i = 0;
                            foreach( $sales_reports as $sales_reports_key => $sales_reports_info ){

                                $this->SalesReports->setSalesReportsData($sales_reports_info);
                                $salesReportsDirectory = $this->SalesReports->getSalesReportsDirectory();
                                $salesReportsDetailsInfo = $this->SalesReportsDetails->loadSalesReportsDetailsById(intval($sales_reports_info['SalesReports']['id']));
                                $this->SalesReportsDetails->setSalesReportsDetailsData($salesReportsDetailsInfo);
                                $file = $salesReportsDirectory . $salesReportsDetailsInfo['SalesReports']['source_file'];
                                $sCss = ( ($i % 2) ==0 )? 'listing_accomm':'listing_tr';


                        ?>
                                    <div class="listing" id="<?php echo $sCss;?>">
                                        <div class="box1" id="all_programs_<?php echo $salesReportsDetailsInfo['SalesReports']['id'];?>">
                                            <div><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'sales_reports','action'=>'download',intval($salesReportsDetailsInfo['SalesReports']['id'])));?>"><?php echo $salesReportsDetailsInfo['SalesReports']['source_file'];?></a></div>
                                        </div><!--end box1-->
                                        <div class="box2"><?php echo $salesReportsDetailsInfo['SalesReports']['reference_code'];?></div>
                                        <div class="box3"><?php echo $salesReportsDetailsInfo['SalesReports']['report_date'];?></div>
                                        <div class="box6">
                                            <?php echo date("D j",strtotime(trim($salesReportsDetailsInfo['SalesReports']['date_created']))).'<sup>'.date("S",strtotime(trim($salesReportsDetailsInfo['SalesReports']['date_created']))).'</sup>&nbsp;'.date("F Y h:i:s A",strtotime(trim($salesReportsDetailsInfo['SalesReports']['date_created'])));?>
                                        </div>
                                        <div class="box7"><?php echo (isset($users[$salesReportsDetailsInfo['SalesReports']['who_created']]))? $users[$salesReportsDetailsInfo['SalesReports']['who_created']]:null;?></div>
                                        <div class="box4"></div>
                                        <div class="clearboth"></div>
                                    </div><!--end listing-->
                                    <?php

                                    $i++;

                            }//end of foreach
                        }//end of programs attendance sheet
                        ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>