<?php echo $this->element('program/program_default_js'); ?>
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){
	$('#billing_eb').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true});
	$(window).resize(function(){});
	$(window).scroll(function(){});
	$('.delete').live('click',function(){});

    $('.listing a').live('click',function(){
        var string_match = new String( $(this).attr('id') );

		var string_found = string_match.search(/map/i);
        var string_match_data = string_match.split('_');
		if( string_found > 0 ){
           if( !$(this).attr('show_div') ){
              $(this).attr('show_div','show_div');
              var geocodes = new String( $(this).attr('alt') );
              var xy = geocodes.split('_');

              if( string_match.startsWith('all') ){
                  $('#all_programs_map_canvas_'+parseInt(string_match_data[4],10)).fadeIn("fast", function(){
                      $(this).show();
                  });
                  invokeMap( 'all_programs_map_canvas_'+ parseInt(string_match_data[4]) , xy[0], xy[1] );

              }else{
				  $('#'+string_match_data[0]+'_programs_map_canvas_'+parseInt(string_match_data[4],10)).fadeIn("fast", function(){
                      $(this).show();
                  });
                  invokeMap(string_match_data[0]+'_programs_map_canvas_'+parseInt(string_match_data[4],10), xy[0], xy[1] );
			  }
		   }else{
              $(this).removeAttr('show_div');

              if( string_match.startsWith('all') ){
			  	  $('#all_programs_map_canvas_'+parseInt(string_match_data[4],10)).fadeOut("slow", function(){
                  	$(this).hide();
                  });
              }else{
            	  $('#'+string_match_data[0]+'_programs_map_canvas_'+parseInt(string_match_data[4],10)).fadeOut("slow", function(){
                    	$(this).hide();
                  });
              }
		   }
        }

        var rateplan_found = string_match.search(/rate/i);
        if( rateplan_found > 0 ){
           if( !$(this).attr('show_div') ){
              $(this).attr('show_div','show_div');
			  if( string_match.startsWith('all') ){
              	 $('#all_programs_rate_plans_'+parseInt(string_match_data[5],10)).fadeIn("fast", function(){
                    $(this).show();
                 });
              }else{
                 $('#'+string_match_data[0]+'_programs_rate_plans_'+parseInt(string_match_data[5],10)).fadeIn("fast", function(){
                      $(this).show();
                 });
              }
           }else{
              $(this).removeAttr('show_div');
              if( string_match.startsWith('all') ){
				 $('#all_programs_rate_plans_'+parseInt(string_match_data[5],10)).fadeOut("slow", function(){
                    $(this).hide();
                 });
              }else{
            	 $('#'+string_match_data[0]+'_programs_rate_plans_'+parseInt(string_match_data[5],10)).fadeOut("slow", function(){
                      $(this).hide();
                 });
              }
           }
        }


        var attendees_found = string_match.search(/companies/i);
        if( attendees_found> 0 ){
           if( !$(this).attr('show_div') ){
              $(this).attr('show_div','show_div');
			  if( string_match.startsWith('all') ){
              	 $('#all_programs_attending_companies_'+parseInt(string_match_data[3],10)).fadeIn("fast", function(){
                    $(this).show();
                 });
              }else{
            	 $('#'+string_match_data[0]+'_programs_attending_companies_'+parseInt(string_match_data[3],10)).fadeIn("fast", function(){
                      $(this).show();
                 });
              }
           }else{
              $(this).removeAttr('show_div');
              if( string_match.startsWith('all') ){
				 $('#all_programs_attending_companies_'+parseInt(string_match_data[3],10)).fadeOut("slow", function(){
                    $(this).hide();
                 });
              }else{
            	 $('#'+string_match_data[0]+'_programs_attending_companies_'+parseInt(string_match_data[3],10)).fadeOut("slow", function(){
                      $(this).hide();
                 });
              }
           }
        }
    });
});
/* ]]> */
</script>
<style type="text/css">
    .listing .box1 {
        float: left !important;
        padding: 0 0 0 6px !important;
        width:200px !important;
    }
    .listing .box2 {
        float: left !important;
        text-align: left !important;
        width: 165px !important;
    }

    .listing .box3 {
        float: left !important;
        text-align: left !important;
        width: 128px !important;
    }

    .listing .box5 {
        float: left !important;
        padding-left: 10px !important;
        text-align: left !important;
        width: 675px !important;
    }

    .listing .box6 {
        float: left !important;
        padding: 6px 0 0 !important;
        text-align: left !important;
        width: 226px !important;
    }

    .listing .box7 {
        float: left !important;
        padding: 6px 0 0 !important;
        text-align: left !important;
        width: 107px !important;
    }
    .listing .box4 {
        clear: both !important;
        margin: 0 !important;
        height: -7px !important;
        padding: 10px 0 0 10px !important;
        width: 950px !important;
    }
</style>
<?php 
echo $this->Html->css(array(
    'jquery.autocomplete',
    'fcbkselection'
    )
);
?>
<script type="text/javascript">
/* <![CDATA[ */
if(!String.prototype.startsWith){
    String.prototype.startsWith = function (str) {
        return !this.indexOf(str);
    }
}

$(document).ready(function(){
	$("#q").focus();
});
/* ]]> */
</script>
<?php
$this->Html->addCrumb('Billing Eb', '/admin/billing_eb');
$this->Html->addCrumb('<strong>'.ucfirst($division).' Billing Eb</strong>');
?>
<div class="billing_eb index">
	<h2><?php __(''.ucwords($division).' Billing Eb');?></h2>
    <?php echo $this->element('billing_eb/search_simple_billing_eb'); ?>
	<div>
       	<div id="billing_eb">
       		<ul>
             <?php 
             if( $this->Session->read('Auth.User.role_id')== Configure::read('administrator') ){
             	echo '<li><a href="#all_billing_eb"><span>Billing Eb</span></a></li>';
             }else{
                foreach( $divisions as $division_key => $division ){
                   if( $division_key == $this->Session->read('Auth.User.role_id') ){
                      echo '<li><a href="#all_billing_eb"><span>Search '.$division.' Billing Eb</span></a></li>';
                   }
                }                
             }?>  
    		</ul>
    		<?php
    		if( isset($billing_eb) && count($billing_eb)>0 ){
    		?>
            <div id="all_billing_eb">
            	<ul>
                    <table cellspacing="0" cellpadding="0" style="background-color:#FFFFFF;border:1px solid #DDDDDD;clear:both;width:100%;">
    				<?php
    				if(!empty($billing_eb)){
    				?>
                    <tr><td align="center" colspan="5"><div class="paging">
                    <?php
                    $this->Paginator->options(array('url' => '/'));
                    $sPaginator = $this->Paginator->first('<< First').' '.
                    $this->Paginator->prev('< Previous') .' '.
                    $this->Paginator->numbers().' '.
                    $this->Paginator->next('Next >'). ' '.
                    $this->Paginator->last('Last >>');
                    echo $sPaginator."<br />";
                    echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));
                    ?>
                    </div></td></tr>
	                <?php
    				}
	                ?>
                    <tr>
                        <th style="border-right: 1px solid rgb(223, 223, 223); width:180px;">Program</th>
                        <th style="border-right: 1px solid rgb(223, 223, 223); width:110px;" colspan="4">Division/Type</th>
                    </tr>
                    <tr><td colspan="5">
                        <?php

                        $i = 0;
                        if( isset($billing_eb) && is_array($billing_eb) ){
                            foreach( $billing_eb as $billing_eb_key => $billing_eb_info ){
                                $billingEbFileInfo = $this->BillingEb->loadBillingEbByProgramId($billing_eb_info['Program']['id']);
                                $oProgram = $this->Program->loadProgramDataById($billing_eb_info['Program']['id'],$recursive=false);
                                $this->Program->setProgramData($billing_eb_info['Program']);
                                $this->ProgramExtraInfo->setExtraInfo($oProgram);
                                $this->BillingEb->setBillingEbData($billing_eb_info);
                                $sCss = ( ($i % 2) ==0 )? 'listing_accomm':'listing_tr';
                        ?>
                                <div class="listing" id="<?php echo $sCss;?>">
                                    <div class="box1" id="all_programs_<?php echo $this->BillingEb->getBillingEbId();?>">
                                        <div>
                                            <h3><a href="<?php echo $this->Html->url(array('controller'=>'programs','action'=>'info',$this->BillingEb->getSeoUri()));?>"><?php echo $this->BillingEb->getProgramTitle();?></a></h3>
                                            <div><schedule><?php echo $this->ProgramExtraInfo->getSchedule($shortcut=true);?></schedule><address><?php echo $billing_eb_info['BillingEb']['venue'];?></address></div>
                                        </div>
                                    </div><!--end box1-->
                                    <div class="box2"><div></div></div>
                                    <div class="box3"><div>
                                    <?php
                                    if( $this->Session->read('Auth.User.role_id') == Configure::read('administrator')){
                                    ?><a href="<?php echo $this->Html->url(array('controller'=>'billing_eb','action'=>'list',$this->ProgramExtraInfo->getProgramDivision()));?>">
                                    <?php echo $this->ProgramExtraInfo->getProgramDivision();?></a><?php
                                    }else{ echo $this->ProgramExtraInfo->getProgramDivision();}?> | <span class="tiny"><?php echo $this->ProgramExtraInfo->getProgramType();?></span></div><!--end box3--></div>
                                    <div class="box5">
                                        <table width="100%">
                                            <tr>
                                                <th width="25%">File</th>
                                                <th width="25%">Company</th>
                                                <th width="20%">Recipient</th>
                                                <!--<th>Reference Code</th>-->
                                                <th width="15%">Billing Date</th>
                                                <th width="5%">No.<br />Pax</th>
                                                <th width="10%">Created</th>
                                            </tr>
                                            <?php
                                            if( count($billingEbFileInfo) > 0 ){

                                            $billingEbDirectory = $this->BillingEb->getBillingEbDirectory(intval($billing_eb_info['Program']['id']));
                                            foreach( $billingEbFileInfo as $key => $billingEbFile ){
                                                if( intval($billing_eb_info['Program']['id']) >0 && isset($billingEbFile['BillingEb']['source_file']) ){
                                                    $file = trim($billingEbDirectory.$billingEbFile['BillingEb']['source_file']);
                                                    if( $this->BillingEb->billingEbExist($billing_eb_info['Program']['id'],$file)){
                                            ?>
                                           <tr>
                                                <td><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'billing_eb','action'=>'download',intval($billingEbFile['BillingEb']['id'])));?>"><?php echo trim($billingEbFile['BillingEb']['source_file']);?></a></td>
                                                <td><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'companies','admin'=>true,'action'=>'info',intval($billingEbFile['BillingEb']['company_id']).'/'.trim($billingEbFile['Company']['seo_name'])));?>"><?php echo trim($billingEbFile['Company']['name']);?></a></td>
                                                <td><?php echo trim($billingEbFile['BillingEb']['recipient']);?></td>
                                                <!--<td><?php echo trim($billingEbFile['BillingEb']['billing_reference_code']);?></td>-->
                                                <td><?php echo trim($billingEbFile['BillingEb']['billing_date']);?></td>
                                                <td><?php echo trim($billingEbFile['BillingEb']['number_of_participant']);?></td>
                                                <td><?php echo $this->BillingEb->createdBy($users,$billingEbFile);?></td>
                                            </tr>
                                           <?php		}else{
                                                            echo '&nbsp;';
                                                        }
                                                    }
                                                }
                                            }
                                           ?>
                                        </table>
                                    </div>
                                    <div class="box5"><div>

                                    </div></div>
                                    <div class="box6"><div align="left"><?php //echo $this->ParticipantDirectory->getAttendanceCompanies();?></div></div>
                                    <div class="box4">
                                        <a class="edit" style="cursor:pointer;" id="all_programs_view_rate_plans_<?php echo $this->Program->get('id');?>">View RatePlans</a>&nbsp;
                                	</div><!--end box4-->
                                    <div class="clearboth"></div>
                                    <div id="all_programs_rate_plans_<?php echo $this->Program->get('id');?>" style="display:none;">
                                	<?php echo $this->BillingEb->getBillingRatePlans();?>
                                    </div>
                                </div><!--end listing-->
                        <?php
                                $i++;
                            }//end of foreach
                        }//end of programs attendance sheet
                        ?>
                    </td></tr>
                  	<?php
                  	if(!empty($billing_eb)){
                  	?>
					<tr><td align="center" colspan="5" class="striped"><div class="paging">
    	            <?php
    	            	$this->Paginator->options(array('url' => '/'));
                        $sPaginator = $this->Paginator->first('<< First').' '.
                        $this->Paginator->prev('< Previous') .' '.
                        $this->Paginator->numbers().' '.
                        $this->Paginator->next('Next >'). ' '.
                        $this->Paginator->last('Last >>');
                        echo $sPaginator."<br />";
                        echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));
                 	?>
                 	</div></td></tr>
                 	<?php
                  	}
                 	?>
                  	</table>
               	</ul>
            </div><!-- end of name tags -->
            <?php
    		}//end of isset and count
    		?>
    	</div>
	</div>