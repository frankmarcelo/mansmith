<?php echo $this->element('program/program_default_js'); ?>
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){
	$('#billing_non_eb').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true});
	$(window).resize(function(){});
	$(window).scroll(function(){});
	$('.delete').live('click',function(){});
});
/* ]]> */
</script>
<style type="text/css">
.listing .box1 {
    float: left !important;
    padding: 0 0 0 6px !important;
    width:200px !important;
}
.listing .box2 {
    float: left !important;
    text-align: left !important;
    width: 165px !important;
}

.listing .box3 {
    float: left !important;
    text-align: left !important;
    width: 128px !important;
}

.listing .box5 {
    float: left !important;
    padding-left: 10px !important;
    text-align: left !important;
    width: 675px !important;
}

.listing .box6 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 226px !important;
}

.listing .box7 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 107px !important;
}
.listing .box4 {
    clear: both !important;
    margin: 0 !important;
    height: -7px !important;
    padding: 10px 0 0 10px !important;
    width: 950px !important;
}
</style>
<?php 
echo $this->Html->css(array(
    'jquery.autocomplete',
    'fcbkselection'
    )
);
?>
<script type="text/javascript">
/* <![CDATA[ */
if(!String.prototype.startsWith){
    String.prototype.startsWith = function (str) {
        return !this.indexOf(str);
    }
}

$(document).ready(function(){
	$("#q").focus();
});
/* ]]> */
</script>
<?php
$this->Html->addCrumb('Billing Non Eb', '/admin/billing_non_eb');
$this->Html->addCrumb('<strong>'.ucfirst($division).' Billing Non Eb</strong>');
?>
<div class="billing_non_eb index">
	<h2><?php __(''.ucwords($division).' Billing Non Eb');?></h2>
	<?php echo $this->element('billing_non_eb/search_simple_billing_non_eb'); ?>
	<div>
       	<div id="billing_non_eb">
       		<ul>
             <?php 
             if( $this->Session->read('Auth.User.role_id')== Configure::read('administrator') ){
             	echo '<li><a href="#all_billing_non_eb"><span>Billing Non Eb</span></a></li>';
             }else{
                foreach( $divisions as $division_key => $division ){
                   if( $division_key == $this->Session->read('Auth.User.role_id') ){
                      echo '<li><a href="#all_billing_non_eb"><span>Search '.$division.' Billing Non Eb</span></a></li>';
                   }
                }                
             }?>  
    		</ul>
    		<?php
            if( isset($billing_non_eb) && count($billing_non_eb)>0 ){
            ?>
            <div id="all_billing_non_eb">
            		<table cellspacing="0" cellpadding="0" style="background-color:#FFFFFF;border:1px solid #DDDDDD;clear:both;width:100%;">
    				<?php
    				if(!empty($billing_non_eb)){
    				?>
                    <tr><td align="center" colspan="5"><div class="paging">
                    <?php
                    $this->Paginator->options(array('url' => '/'));
                    $sPaginator = $this->Paginator->first('<< First').' '.
                    $this->Paginator->prev('< Previous') .' '.
                    $this->Paginator->numbers().' '.
                    $this->Paginator->next('Next >'). ' '.
                    $this->Paginator->last('Last >>');
                    echo $sPaginator."<br />";
                    echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));
                    ?>
                    </div></td></tr>
	                <?php
    				}
	                ?>
                    <tr>
                        <th style="border-right: 1px solid rgb(223, 223, 223); width:180px;">Program</th>
                        <th style="border-right: 1px solid rgb(223, 223, 223); width:110px;" colspan="4">Division/Type</th>
                    </tr>
                    <tr><td colspan="5">
                        <?php

                        $i = 0;
                        if( isset($billing_non_eb) && is_array($billing_non_eb) ){

                             foreach( $billing_non_eb as $billing_non_eb_key => $billing_non_eb_info ){

                                $billingNonEbFileInfo = $this->BillingNonEb->loadBillingNonEbByProgramId($billing_non_eb_info['Program']['id']);

                                $oProgram = $this->Program->loadProgramDataById($billing_non_eb_info['Program']['id'],$recursive=false);
                                $this->Program->setProgramData($billing_non_eb_info['Program']);
                                $this->ProgramExtraInfo->setExtraInfo($oProgram);
                                $this->BillingNonEb->setBillingNonEbData($billing_non_eb_info);
                                $sCss = ( ($i % 2) ==0 )? 'listing_accomm':'listing_tr';
                        ?>
                                <div class="listing" id="<?php echo $sCss;?>">
                                    <div class="box1" id="all_programs_<?php echo $this->BillingNonEb->getBillingNonEbId();?>">
                                        <div>
                                            <h3><a href="<?php echo $this->Html->url(array('controller'=>'programs','action'=>'info',$this->BillingNonEb->getSeoUri()));?>"><?php echo $this->BillingNonEb->getProgramTitle();?></a></h3>
                                            <div><schedule><?php echo $this->ProgramExtraInfo->getSchedule($shortcut=true);?></schedule><address><?php echo $billing_non_eb_info['BillingNonEb']['venue'];?></address></div>
                                        </div>
                                    </div><!--end box1-->
                                    <div class="box2"><div></div></div>
                                    <div class="box3"><div>
                                    <?php
                                    if( $this->Session->read('Auth.User.role_id') == Configure::read('administrator')){
                                    ?><a href="<?php echo $this->Html->url(array('controller'=>'billing_non_eb','action'=>'list',$this->ProgramExtraInfo->getProgramDivision()));?>">
                                    <?php echo $this->ProgramExtraInfo->getProgramDivision();?></a><?php
                                    }else{ echo $this->ProgramExtraInfo->getProgramDivision();}?> | <span class="tiny"><?php echo $this->ProgramExtraInfo->getProgramType();?></span></div><!--end box3--></div>
                                    <div class="box5">
                                        <table width="100%">
                                            <tr>
                                                <th width="25%">File</th>
                                                <th width="25%">Company</th>
                                                <th width="20%">Recipient</th>
                                                <!--<th>Reference Code</th>-->
                                                <th width="15%">Billing Date</th>
                                                <th width="5%">No.<br />Pax</th>
                                                <th width="10%">Created</th>
                                            </tr>
                                            <?php
                                            if( count($billingNonEbFileInfo) > 0 ){

                                                $billingNonEbDirectory = $this->BillingNonEb->getBillingNonEbDirectory(intval($billing_non_eb_info['Program']['id']));
                                                foreach( $billingNonEbFileInfo as $key => $billingNonEbFile ){
                                                    if( intval($billing_non_eb_info['Program']['id']) >0 && isset($billingNonEbFile['BillingNonEb']['source_file']) ){
                                                        $file = trim($billingNonEbDirectory.$billingNonEbFile['BillingNonEb']['source_file']);
                                                        if( $this->BillingNonEb->billingNonEbExist($billing_non_eb_info['Program']['id'],$file)){
                                            ?>
                                           <tr>
                                                <td><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'billing_non_eb','action'=>'download',intval($billingNonEbFile['BillingNonEb']['id'])));?>"><?php echo trim($billingNonEbFile['BillingNonEb']['source_file']);?></a></td>
                                                <td><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'companies','admin'=>true,'action'=>'info',intval($billingNonEbFile['BillingNonEb']['company_id']).'/'.trim($billingNonEbFile['Company']['seo_name'])));?>"><?php echo trim($billingNonEbFile['Company']['name']);?></a></td>
                                                <td><?php echo trim($billingNonEbFile['BillingNonEb']['recipient']);?></td>
                                                <!--<td><?php echo trim($billingNonEbFile['BillingNonEb']['billing_reference_code']);?></td>-->
                                                <td><?php echo trim($billingNonEbFile['BillingNonEb']['billing_date']);?></td>
                                                <td><?php echo trim($billingNonEbFile['BillingNonEb']['number_of_participant']);?></td>
                                                <td><?php echo $this->BillingNonEb->createdBy($users,$billingNonEbFile);?></td>
                                            </tr>
                                           <?php		}else{
                                                            echo '&nbsp;';
                                                        }
                                                    }
                                                }
                                            }
                                           ?>
                                        </table>
                                    </div>
                                    <div class="box5"><div>

                                    </div></div>
                                    <div class="box6"><div align="left"><?php //echo $this->ParticipantDirectory->getAttendanceCompanies();?></div></div>
                                    <div class="box4">
                                        <a class="edit" style="cursor:pointer;" id="all_programs_view_rate_plans_<?php echo $this->Program->get('id');?>">View RatePlans</a>&nbsp;
                                	</div><!--end box4-->
                                    <div class="clearboth"></div>
                                    <div id="all_programs_rate_plans_<?php echo $this->Program->get('id');?>" style="display:none;">
                                	<?php echo $this->BillingNonEb->getBillingRatePlans();?>
                                    </div>
                                </div><!--end listing-->
                        <?php
                                $i++;
                            }//end of foreach
                        }//end of programs attendance sheet
                        ?>
                    </td></tr>
                  	<?php
                  	if(!empty($billing_non_eb)){
                  	?>
					<tr><td align="center" colspan="5" class="striped"><div class="paging">
    	            <?php
    	            	$this->Paginator->options(array('url' => '/'));
                        $sPaginator = $this->Paginator->first('<< First').' '.
                        $this->Paginator->prev('< Previous') .' '.
                        $this->Paginator->numbers().' '.
                        $this->Paginator->next('Next >'). ' '.
                        $this->Paginator->last('Last >>');
                        echo $sPaginator."<br />";
                        echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));
                 	?>
                 	</div></td></tr>
                 	<?php
                  	}
                 	?>
                  	</table>
            </div><!-- end of name tags -->
            <?php
    		}//end of isset and count
    		?>
    	</div>
	</div>