<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="/js/jquery/development-bundle/themes/base/jquery.ui.all.css?1302644960" />
	<link rel="stylesheet" type="text/css" href="/css/reset.css?1307407479" />
	<link rel="stylesheet" type="text/css" href="/css/960.css?1307213340" />
	<link rel="stylesheet" type="text/css" href="/css/admin_footer_panel.css?1303754635" />
	<link rel="stylesheet" type="text/css" href="/css/admin.css?1311031305" />
    <script type="text/javascript" src="/js/jquery/jquery.min.js?1333743415"></script>
	<script type="text/javascript" src="/js/jquery/jquery-ui.min.js?1333743414"></script>
	<script type="text/javascript" src="/js/jquery/jquery.slug.js?1333743415"></script>
	<script type="text/javascript" src="/js/jquery/jquery.uuid.js?1333743415"></script>
	<script type="text/javascript" src="/js/jquery/jquery.cookie.js?1333743414"></script>
	<script type="text/javascript" src="/js/jquery/jquery.hoverIntent.minified.js?1333743414"></script>
	<script type="text/javascript" src="/js/jquery/superfish.js?1333743415"></script>
	<script type="text/javascript" src="/js/jquery/supersubs.js?1333743415"></script>
	<script type="text/javascript" src="/js/jquery/jquery.tipsy.js?1333743415"></script>
	<script type="text/javascript" src="/js/jquery/jquery.elastic-1.6.1.js?1333743414"></script>
	<script type="text/javascript" src="/js/admin.js?1345326200"></script>
</head>
<body>
     <div><div id="content">
     <?php

     echo $content_for_layout;
     ?>
     </div></div>
</body>
</html>
