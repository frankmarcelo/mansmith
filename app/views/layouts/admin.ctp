<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $title_for_layout; ?> - <?php __('Mansmith Management System'); ?></title>
    <?php
        echo $this->Html->css('/js/jquery/development-bundle/themes/base/jquery.ui.all.css');
        echo $this->Html->css(array(
            'reset',
            '960',
            'admin_footer_panel',
            'admin'
        ));
        echo $this->Layout->js();
        echo $this->Html->script(array(
            'jquery/jquery.min',
            'jquery/jquery-ui.min',
            'jquery/jquery.slug',
            'jquery/jquery.uuid',
            'jquery/jquery.cookie',
            'jquery/jquery.hoverIntent.minified',
            'jquery/superfish',
            'jquery/supersubs',
            'jquery/jquery.tipsy',
            'jquery/jquery.elastic-1.6.1.js',
            'admin'
        ));
        echo $scripts_for_layout;
    ?>
</head>
<body>
	<div id="fb-root"></div>
    <div id="wrapper">
        <?php echo $this->element('admin/header'); ?>

        <div id="nav-container">
            <div class="container_16">
                <?php echo $this->element("admin/navigation"); ?>
            </div>
        </div>
        <div id="main" class="container_16">
            <div class="grid_16">
                <div id="content">
                <div class="breadcrumb"><?php	echo $this->Html->getCrumbs(' > ','Dashboard');?></div>
				<noscript><strong class="ui-state-error">Your browser does not support javascript. The application will not run as expected it should be.</strong></noscript>

                    <?php
                        $this->Layout->sessionFlash();
                        echo $content_for_layout;
                    ?>
                </div>
            </div>
            <div class="clear">&nbsp;</div>
        </div>
        <div class="push"></div>
    </div>
    <script type="text/javascript">
	/* <![CDATA[ */
	var _gaq = _gaq || [];
  	_gaq.push(['_setAccount', 'UA-24124142-1']);
  	_gaq.push(['_setDomainName', '.mansmith.net']);
  	_gaq.push(['_setAllowHash', 'false']);
  	_gaq.push(['_trackPageview']);
	(function() {
    	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  	})();
	/* ]]> */
	</script> 
    </body>
</html>
