<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $title_for_layout; ?> - <?php __('Croogo'); ?></title>
    <?php
        echo $this->Html->css(array(
            'reset',
            '960',
            '/ui-themes/smoothness/jquery-ui.css',
            'admin',
            'thickbox',
        ));
        echo $this->Layout->js();
        echo $this->Html->script(array(
            'jquery/jquery.min',
            'jquery/jquery-ui.min',
            'jquery/jquery.uuid',
            'jquery/jquery.cookie',
            'jquery/jquery.hoverIntent.minified',
            'jquery/superfish',
            'jquery/supersubs',
            'jquery/jquery.tipsy',
            'jquery/jquery.elastic-1.6.1.js',
            'jquery/thickbox-compressed',
            'admin',
        ));
        echo $scripts_for_layout;
    ?>
</head>

<body>

    <div id="wrapper">
        <?php echo $this->element('admin/header'); ?>

        <div id="main" class="container_16">
            <div class="grid_16">
                <div id="content">
                    <?php
                        $this->Layout->sessionFlash();
                        echo $content_for_layout;
                    ?>
                </div>
            </div>
            <div class="clear">&nbsp;</div>
        </div>

        <div class="push"></div>
    </div>

    <?php echo $this->element('admin/footer'); ?>
	<script type="text/javascript">
	/* <![CDATA[ */
	var _gaq = _gaq || [];
  	_gaq.push(['_setAccount', 'UA-24124142-1']);
  	_gaq.push(['_setDomainName', '.mansmith.net']);
  	_gaq.push(['_setAllowHash', 'false']);
  	_gaq.push(['_trackPageview']);
	(function() {
    	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  	})();
	/* ]]> */
	</script>
    </body>
</html>
