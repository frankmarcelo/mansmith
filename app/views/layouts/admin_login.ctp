<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $title_for_layout; ?> - <?php __('Mansmith Management System'); ?></title>
    <?php
        echo $this->Html->css(array(
            'reset',
            '960',
            'admin',
        ));
        echo $scripts_for_layout;
    ?>
</head>

<body>

    <div id="wrapper" class="login">
        <div id="header">
            <p id="backtosite">
            <?php echo 'Go to '.Configure::read('Site.title'), '<a href="http://www.mansmith.net">http://www.mansmith.net</a>';?>
            </p>
        </div>

        <div id="main">
            <div id="login">
            <?php
                $this->Layout->sessionFlash();
                echo $content_for_layout;
            ?>
            </div>
        </div>

        <?php echo $this->element('admin/footer'); ?>

    </div>
	<script type="text/javascript">
	/* <![CDATA[ */
	var _gaq = _gaq || [];
  	_gaq.push(['_setAccount', 'UA-24124142-1']);
  	_gaq.push(['_setDomainName', '.mansmith.net']);
  	_gaq.push(['_setAllowHash', 'false']);
  	_gaq.push(['_trackPageview']);
	(function() {
    	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  	})();
	/* ]]> */
	</script>
	</body>
</html>
