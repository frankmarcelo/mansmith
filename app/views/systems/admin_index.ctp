<?php
echo $this->element('program/program_default_js');
?>
<script type="text/javascript">
    /* <![CDATA[ */
    $(document).ready(function(){
        $('#manage_lookups').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true});
        $(window).resize(function(){});
        $(window).scroll(function(){});
        $('.delete').live('click',function(){});
        $("#q").focus();
    });
    /* ]]> */
</script>
<?php
echo $this->Html->css(array(
        'jquery.autocomplete',
        'fcbkselection'
    )
);
?>
<script type="text/javascript">
/* <![CDATA[ */
if(!String.prototype.startsWith){
    String.prototype.startsWith = function (str) {
        return !this.indexOf(str);
    }
}

$(document).ready(function(){
    $('#submit_participants_grouping').live("click",function(){
   		$.ajax({
          cache: false  ,
          type:  "POST" ,
          url:   "<?php echo Configure::read('js_directory');?>ajax_create_participants_grouping.php",
          data:  $("#ParticipantsGroupingForm :input").serializeArray(),
          dataType: "json" ,
          success: function(data){
             alert('Success');
             location.reload();
          }
       });
    });

    $('#submit_company_affiliation').live("click",function(){
        $.ajax({
            cache: false  ,
            type:  "POST" ,
            url:   "<?php echo Configure::read('js_directory');?>ajax_create_company_affiliation.php",
            data:  $("#CompanyAffiliationForm :input").serializeArray(),
            dataType: "json" ,
            success: function(data){
                alert('Success');
                location.reload();
            }
        });
    });

    $('#submit_company_industry').live("click",function(){
        $.ajax({
            cache: false  ,
            type:  "POST" ,
            url:   "<?php echo Configure::read('js_directory');?>ajax_create_company_industry.php",
            data:  $("#CompanyIndustryForm :input").serializeArray(),
            dataType: "json" ,
            success: function(data){
                alert('Success');
                location.reload();
            }
        });
    });
});
/* ]]> */
</script>
<?php
$this->Html->addCrumb('<strong>Manage Look Ups</strong>');
?>
<div class="manage_lookups index">
    <h2>All Look Ups</h2>
    <div id="manage_lookups">
        <ul><li><a href="#all_manage_lookups"><span>Look Ups</span></a></li></ul>
        <div id="all_manage_lookups">
            <table cellspacing="0" cellpadding="0" style="background-color:#FFFFFF;border:1px solid #DDDDDD;clear:both;width:100%;">
                <tr>
                    <td width="20%">Participants Grouping</td>
                    <td width="80%">
                        <form id="ParticipantsGroupingForm" method="post" accept-charset="utf-8" onSubmit="return false;">
                       <input type="hidden" name="data[ParticipantsGrouping][who_modified]" value="<?php echo $this->Session->read('Auth.User.id');?>" />      
                        <table>
                            <tr>
                                <td width="40%">
                                    <select id="participants_grouping" name="participants_grouping">
                                        <?php
                                        if (count($participantsGrouping) >0) {
                                            foreach ($participantsGrouping as $key => $value) {
                                                ?>
                                                <option value="<?php echo $key?>"><?php echo $value;?></option>
                                            <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td width="40%">Add:&nbsp;<input type="text" name="data[ParticipantsGrouping][add_participants_grouping]" id="add_participants_grouping" /></td>
                                <td width="20%"><input type="submit" name="submit_participants_grouping" id="submit_participants_grouping" value="Add Participant Grouping"/></td>
                            </tr>
                        </table>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td width="20%">Company Affiliation</td>
                    <td width="80%">
                        <form id="CompanyAffiliationForm" method="post" accept-charset="utf-8" onSubmit="return false;">
                       <input type="hidden" name="data[CompanyAffiliation][who_modified]" value="<?php echo $this->Session->read('Auth.User.id');?>" />      
                        <table>
                            <tr>
                                <td width="40%">
                                    <select id="company_affiliation" name="company_affiliation">
                                    <?php
                                    if (count($companiesAffiliation) >0) {
                                        foreach ($companiesAffiliation as $key => $value) {
                                    ?>
                                    <option value="<?php echo $key?>"><?php echo $value;?></option>
                                    <?php
                                        }
                                    }
                                    ?>
                                    </select>
                                </td>
                                <td width="40%">Add:&nbsp;<input type="text" name="data[CompanyAffiliation][add_company_affiliation]" id="add_company_affiliation" /></td>
                                <td width="20%"><input type="submit" name="submit_company_affiliation" id="submit_company_affiliation" value="Add Company Affiliation" /></td>
                            </tr>
                        </table>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td width="20%">Company Industry</td>
                    <td width="80%">
                        <form id="CompanyIndustryForm" method="post" accept-charset="utf-8" onSubmit="return false;">
                       <input type="hidden" name="data[CompanyIndustry][who_modified]" value="<?php echo $this->Session->read('Auth.User.id');?>" />      
                        <table>
                            <tr>
                                <td width="40%">
                                    <select id="company_industry" name="company_industry">
                                    <?php
                                    if (count($companiesIndustry) >0) {
                                        foreach ($companiesIndustry as $key => $value) {
                                            ?>
                                            <option value="<?php echo $key?>"><?php echo $value;?></option>
                                        <?php
                                        }
                                    }
                                    ?>
                                    </select>
                                </td>
                                <td width="40%">Add:&nbsp;<input type="text" name="data[CompanyIndustry][add_company_industry]" id="add_company_industry" /></td>
                                <td width="20%"><input type="submit" name="submit_company_industry" id="submit_company_industry" value="Add Company Industry"/></td>
                            </tr>
                        </table>
                        </form>
                    </td>
                </tr>
            </table>
        </div><!-- end of search -->
    </div>
</div>
