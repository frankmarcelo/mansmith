<?php
echo $this->element('program/program_default_js');
?>
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){
    $('#back_ups').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true});
    $(window).resize(function(){});
    $(window).scroll(function(){});
    $('.delete').live('click',function(){});
    $("#q").focus();
});
/* ]]> */
</script>
<?php
echo $this->Html->css(array(
    'jquery.autocomplete',
    'fcbkselection'
    )
);
?>
<script type="text/javascript">
/* <![CDATA[ */
if(!String.prototype.startsWith){
    String.prototype.startsWith = function (str) {
        return !this.indexOf(str);
    }
}
/* ]]> */
</script>
<style type="text/css">
.listing
{
    width:1040px !important;
}
.listing .box1 {
    float:left !important;
    padding:0 0 0 6px !important;
    width:270px !important;
}
.listing .box2 {
    float: left !important;
    text-align: left !important;
    width: 170px !important;
}

.listing .box3 {
    float: left !important;
    text-align: left !important;
    width: 190px !important;
}

.listing .box5 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
}

.listing .box6 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 210px !important;
}

.listing .box7 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 111px !important;
}
</style>
<?php
$this->Html->addCrumb('<strong>All Back Ups</strong>');
?>
<div class="back_ups index">
    <h2>All Back Ups</h2>
    <div id="back_ups">
        <ul><li><a href="#all_back_ups"><span>Back Ups</span></a></li></ul>
        <?php
        if( isset($back_ups) && count($back_ups)>0 ){
        ?>
        <div id="all_back_ups">

            <table cellspacing="0" cellpadding="0" style="background-color:#FFFFFF;border:1px solid #DDDDDD;clear:both;width:100%;">
            <?php
            if(!empty($back_ups)){
            ?>
                <tr>
                    <td align="center"  colspan="3">
                        <div class="paging">
                    <?php
                    $this->Paginator->options(array('url' => '/'));
                    $sPaginator = $this->Paginator->first('<< First').' '.
                    $this->Paginator->prev('< Previous') .' '.
                    $this->Paginator->numbers().' '.
                    $this->Paginator->next('Next >'). ' '.
                    $this->Paginator->last('Last >>');
                    echo $sPaginator."<br />";
                    echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));
                    ?>
                        </div>
                    </td>
                </tr>
            <?php
                }
            ?>
                <tr>
                    <th style="border-right: 1px solid rgb(223, 223, 223);width:250px;">Back Up</th>
                    <th style="border-right: 1px solid rgb(223, 223, 223);">Date Created</th>
                    <th style="border-right: 1px solid rgb(223, 223, 223);">Who Created</th>
                </tr>
                <tr>
                    <td colspan="3">
                    <?php
                    if( isset($back_ups) && is_array($back_ups) ){
                        $i = 0;
                        foreach( $back_ups as $back_ups_key => $back_up ){

                            $file = $back_up['BackUps']['source_file'];
                            $sCss = ( ($i % 2) ==0 )? 'listing_accomm':'listing_tr';

                            //if( file_exists($file) ){
                    ?>
                        <div class="listing" id="<?php echo $sCss;?>">
                            <div class="box1" id="all_programs_<?php echo $back_up['BackUps']['id'];?>">
                                <div><?php echo $back_up['BackUps']['source_file'];?></div>
                            </div><!--end box1-->
                            <div class="box6">
                                <?php echo date("D j",strtotime(trim($back_up['BackUps']['date_created']))).'<sup>'.date("S",strtotime(trim($back_up['BackUps']['date_created']))).'</sup>&nbsp;'.date("F Y h:i:s A",strtotime(trim($back_up['BackUps']['date_created'])));?>
                            </div>
                            <div class="box4"></div>
                            <div class="clearboth"></div>
                        </div><!--end listing-->
                    <?php
                            $i++;
                            //}
                        }//end of foreach
                    }//end of programs attendance sheet
                    ?>
                    </td>
                </tr>
                <?php
                if(!empty($back_ups)){
                ?>
                    <tr><td align="center" colspan="3">
                        <div class="paging">
                        <?php
                        $this->Paginator->options(array('url' => '/'));
                        $sPaginator = $this->Paginator->first('<< First').' '.
                        $this->Paginator->prev('< Previous') .' '.
                        $this->Paginator->numbers().' '.
                        $this->Paginator->next('Next >'). ' '.
                        $this->Paginator->last('Last >>');
                        echo $sPaginator."<br />";
                        echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));
                        ?>
                        </div>
                    </td></tr>
                <?php
                }
                ?>
            </table>

        </div><!-- end of search -->
        <?php
        }//end of isset and count
        ?>
    </div>
</div>