<?php
echo $this->element('program/program_default_js');
?>
<script type="text/javascript">
    /* <![CDATA[ */
    $(document).ready(function(){
        $('#data_dumps').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true});
        $(window).resize(function(){});
        $(window).scroll(function(){});
        $('.delete').live('click',function(){});
        $("#q").focus();
    });
    /* ]]> */
</script>
<?php
echo $this->Html->css(array(
        'jquery.autocomplete',
        'fcbkselection'
    )
);
?>
<script type="text/javascript">
    /* <![CDATA[ */
    if(!String.prototype.startsWith){
        String.prototype.startsWith = function (str) {
            return !this.indexOf(str);
        }
    }
    /* ]]> */
</script>
<style type="text/css">
    .listing
    {
        width:1040px !important;
    }
    .listing .box1 {
        float:left !important;
        padding:0 0 0 6px !important;
        width:270px !important;
    }
    .listing .box2 {
        float: left !important;
        text-align: left !important;
        width: 170px !important;
    }

    .listing .box3 {
        float: left !important;
        text-align: left !important;
        width: 190px !important;
    }

    .listing .box5 {
        float: left !important;
        padding: 6px 0 0 !important;
        text-align: left !important;
    }

    .listing .box6 {
        float: left !important;
        padding: 6px 0 0 !important;
        text-align: left !important;
        width: 320px !important;
    }

    .listing .box7 {
        float: left !important;
        padding: 6px 0 0 !important;
        text-align: left !important;
        width: 111px !important;
    }
</style>
<?php
$this->Html->addCrumb('<strong>All Data Dumps</strong>');
$oDirectoryProperty = new DirectoryProperty();
$oDirectoryProperty->documentDirectory = Configure::read('docs_directory').DIRECTORY_SEPARATOR.'data_dumps'.DIRECTORY_SEPARATOR.'groupings'.DIRECTORY_SEPARATOR;
?>
<div class="back_ups index">
    <h2>All Data Dumps</h2>
    <div id="data_dumps">
        <ul><li><a href="#all_data_dumps"><span>Data Dumps</span></a></li></ul>
        <?php
        if( isset($participantsGroupingDumps) && count($participantsGroupingDumps)>0 ){
            ?>
            <div id="all_data_dumps">
                <?php
                if( $participantsGrouping ){
                ?>
                <form name="SearchParticipantsGrouping" method="get" accept-charset="utf-8" action="<?php echo $this->Html->url(array("admin"=>true, "controller" => "systems" ,"action"=>"generate_participants_grouping"));?>">
                    <table>
                        <tr><td>
                            Select Participant Grouping:
                            <select id="grouping" name="grouping_id">
                                <?php
                                foreach( $participantsGrouping as $grouping){
                                ?><option value="<?php echo $grouping['ParticipantsGrouping']['id'];?>"><?php echo $grouping['ParticipantsGrouping']['name'];?></option><?php
                                }
                                ?>
                            </select>
                            &nbsp;<input type="submit" name="submit" value="Generate Participant Groupings" />
                        </td></tr>
                    </table>
                </form>
                <?php
                }
                ?>
                <table cellspacing="0" cellpadding="0" style="background-color:#FFFFFF;border:1px solid #DDDDDD;clear:both;width:100%;">
                    <?php
                    if(!empty($participantsGroupingDumps)){
                        ?>
                        <tr>
                            <td align="center"  colspan="4">
                                <div class="paging">
                                    <?php
                                    $this->Paginator->options(array('url' => '/'));
                                    $sPaginator = $this->Paginator->first('<< First').' '.
                                        $this->Paginator->prev('< Previous') .' '.
                                        $this->Paginator->numbers().' '.
                                        $this->Paginator->next('Next >'). ' '.
                                        $this->Paginator->last('Last >>');
                                    echo $sPaginator."<br />";
                                    echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));
                                    ?>
                                </div>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    <tr>
                        <th style="border-right: 1px solid rgb(223, 223, 223);width:250px;">Source File</th>
                        <th style="border-right: 1px solid rgb(223, 223, 223);width:150px;">Participants Grouping</th>
                        <th style="border-right: 1px solid rgb(223, 223, 223);">Date Created</th>
                        <th style="border-right: 1px solid rgb(223, 223, 223);">Who Created</th>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <?php
                            if( isset($participantsGroupingDumps) && is_array($participantsGroupingDumps) ){
                                foreach( $participantsGroupingDumps as $i => $participantsGroupingDump ){



                                    $file = $oDirectoryProperty->documentDirectory . $participantsGroupingDump['ParticipantsGroupingDumps']['source_file'];
                                    $sCss = ( ($i % 2) ==0 )? 'listing_accomm':'listing_tr';

                                    if( file_exists($file) || true ){
                                        ?>
                                        <div class="listing" id="<?php echo $sCss;?>">
                                            <div class="box1" id="all_programs_<?php echo $participantsGroupingDump['ParticipantsGroupingDumps']['id'];?>">
                                                <div><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'systems','action'=>'download_data_dumps',intval($participantsGroupingDump['ParticipantsGroupingDumps']['id'])));?>"><?php echo $participantsGroupingDump['ParticipantsGroupingDumps']['source_file'];?></a></div>
                                            </div><!--end box1-->
                                            <div class="box2"><?php echo $participantsGroupingDump['ParticipantsGrouping']['name'];?></div>
                                            <div class="box6">
                                                <?php echo date("D j",strtotime(trim($participantsGroupingDump['ParticipantsGroupingDumps']['date_created']))).'<sup>'.date("S",strtotime(trim($participantsGroupingDump['ParticipantsGroupingDumps']['date_created']))).'</sup>&nbsp;'.date("F Y h:i:s A",strtotime(trim($participantsGroupingDump['ParticipantsGroupingDumps']['date_created'])));?>
                                            </div>
                                            <div class="box7"><?php echo $users[$participantsGroupingDump['ParticipantsGroupingDumps']['who_created']];?></div>
                                            <div class="box4"></div>
                                            <div class="clearboth"></div>
                                        </div><!--end listing-->
                                        <?php

                                        $i++;
                                    }
                                }//end of foreach
                            }//end of programs attendance sheet
                            ?>
                        </td>
                    </tr>
                    <?php
                    if(!empty($participantsGroupingDumps)){
                        ?>
                        <tr><td align="center" colspan="4">
                            <div class="paging">
                                <?php
                                $this->Paginator->options(array('url' => '/'));
                                $sPaginator = $this->Paginator->first('<< First').' '.
                                    $this->Paginator->prev('< Previous') .' '.
                                    $this->Paginator->numbers().' '.
                                    $this->Paginator->next('Next >'). ' '.
                                    $this->Paginator->last('Last >>');
                                echo $sPaginator."<br />";
                                echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));
                                ?>
                            </div>
                        </td></tr>
                        <?php
                    }
                    ?>
                </table>

            </div><!-- end of search -->
            <?php
        }//end of isset and count
        ?>
    </div>
</div>
