<?php

//print_r($latest_companies);
echo $this->Html->css(array(
    'jquery.autocomplete',
    'fcbkselection'
    )
);
?>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&region=PH"></script>
<script type="text/javascript">
/* <![CDATA[ */
function invokeLatestProgramsMap() {
	<?php
	if(is_array($latest_programs) && count($latest_programs)>0 ){

        $isAdmin = false;
        if( $this->Session->read('Auth.User.role_id') == Configure::read('administrator')){
            $isAdmin = true;
        }
    ?>
    var myLatlng = new google.maps.LatLng(<?php echo Configure::read('latitude').','.Configure::read('longitude');?> );
    var myOptions = {
      zoom: 14,
      center: myLatlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      navigationControl: true,
      mapTypeControl: true,
      disableDefaultUI: true,
      navigationControl: true,
      mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
        position: google.maps.ControlPosition.TOP_LEFT
      },
      streetViewControl: false
    }
    var map = new google.maps.Map(document.getElementById("map_program_canvas"), myOptions);
	    <?php
	    foreach( $latest_programs as $key => $latest ){
	    	$this->Program->setProgramData($latest['Program']);
	    	if( intval($latest['Program']['status']) == Configure::read('status_live') ){
                if( $isAdmin == true || intval($this->Session->read('Auth.User.role_id')) == intval($latest['Program']['programs_division_id']) ){
                ?>
			    var programContent<?php echo $key;?> = '<div style="width:200px;!important;"><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'programs','action'=>'info',$this->Program->getSeoUri()));?>"><?php echo utf8_encode(stripslashes(trim(str_replace("'","",$latest['Program']['title']))));?></a></div>';
				var programInfowindow<?php echo $key;?> = new google.maps.InfoWindow({
				    content: programContent<?php echo $key;?>
				});
			    var programlatlng<?php echo $key;?> = new google.maps.LatLng(<?php echo $latest['Program']['latitude'].','.$latest['Program']['longitude'];?> );
			    var programmarker<?php echo $key;?> = new google.maps.Marker({
			        position: programlatlng<?php echo $key;?>,
			        map: map,
			        title: '<?php echo utf8_encode(stripslashes(trim(str_replace("'","",$latest['Program']['title']))));?>',
			        maxWidth: 200
			    });
			    google.maps.event.addListener(programmarker<?php echo $key;?>, 'click', function() {
			    	programInfowindow<?php echo $key;?>.open(map,programmarker<?php echo $key;?>);
			    });
			    map.setCenter(programlatlng<?php echo $key;?>);
	    <?php
                }
            }
	    }
    ?>
    <?php 
    }
    ?>
    map.getCenter();
}

function invokeCompanyMap() {
	<?php
	if(is_array($latest_companies) && count($latest_companies)>0 ){
	?>
    var myLatlng = new google.maps.LatLng(<?php echo Configure::read('latitude').','.Configure::read('longitude');?> );
    var myOptions = {
      zoom: 14,
      center: myLatlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      navigationControl: true,
      mapTypeControl: true,
      disableDefaultUI: true,
      navigationControl: true,
      mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
          position: google.maps.ControlPosition.TOP_LEFT
      },
      streetViewControl: false
    }

    var map = new google.maps.Map(document.getElementById("map_company_canvas"), myOptions);
	    <?php
	    foreach( $latest_companies as $key => $companies ){
	    	 $this->Company->setCompanyData($companies['Company']);
	    	 if( intval($companies['Company']['status']) == Configure::read('status_live') ){
	    ?>
			    var companyContent<?php echo $key;?> = '<div style="width:200px;!important;"><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'companies','action'=>'info',$this->Company->getSeoUri()));?>"><?php echo utf8_encode(stripslashes(trim(str_replace("'","",$companies['Company']['name']))));?></a></div>';
				var companyInfowindow<?php echo $key;?> = new google.maps.InfoWindow({
				    content: companyContent<?php echo $key;?>
				});
			    var companylatlng<?php echo $key;?> = new google.maps.LatLng(<?php echo $companies['Company']['latitude'].','.$companies['Company']['longitude'];?> );
			    var companymarker<?php echo $key;?> = new google.maps.Marker({
			        position: companylatlng<?php echo $key;?>,
			        map: map,
			        title: '<?php echo utf8_encode(addslashes(trim($companies['Company']['name'])));?>',
			        maxWidth: 200
			    });
			    google.maps.event.addListener(companymarker<?php echo $key;?>, 'click', function() {
			    	companyInfowindow<?php echo $key;?>.open(map,companymarker<?php echo $key;?>);
			    });
			    map.setCenter(companylatlng<?php echo $key;?>);
	    <?php
	    	}
	    }
    ?>
        map.getCenter();
        <?php
    }
    ?>
}



$(document).ready(function(){
	invokeLatestProgramsMap();
    invokeCompanyMap();
    $('#programs').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true});
    $('#companies').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true});
    $('#participants').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true});
    $('#upcoming_programs').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true});
});
/* ]]> */
</script>
<style type="text/css">
  #map_program_canvas { height: 250px;width: 475px; }
  #map_company_canvas { height: 250px;width: 475px; }
</style>
<?php $this->Html->addCrumb('<strong>Dashboard</strong>');?>
<div class="users index">
    <h2>Welcome to Dashboard <?php echo $this->Session->read('Auth.User.name');?>!</h2>
    <div class="clearfix hasRightCol" id="contentCol">
        <div id="headerArea">
            <table>
                <tr valign="top">
                    <td width="500px;" style="vertical-align: top;">
                        <div id="programs" style="width:500px;">
                            <h2>Latest Added Programs</h2>
                            <ul>
                                <li><a href="#program_map_data"><span>Map View</span></a></li>
                                <li><a href="#program_data"><span>Standard View</span></a></li>
                            </ul>
                            <div id="program_map_data">
                                <div id="map_program_canvas"></div>
                            </div>
                            <div class="profileHeaderMain" id="program_data">
                                <div>
                                    <table class="uiInfoTable">
                                        <tr><th width="60%">Program Name</th><th width="40%">Schedule</th></tr>
                                        <?php
                                        foreach( $latest_programs as $key => $latest_program ){
                                            $css = ( $key % 2 == 0 ) ? 'striped':''; 
                                            $this->Program->setProgramData($latest_program['Program']);
                                            if( intval($latest_program['Program']['status']) == Configure::read('status_live') ){
                                                if( $isAdmin == true || intval($this->Session->read('Auth.User.role_id')) == intval($latest_program['Program']['programs_division_id']) ){
                                        ?>
                                        <tr class="<?php echo $css;?>">
                                          <td><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'programs','action'=>'info',$this->Program->getSeoUri()));?>"><?php echo $this->Text->truncate($latest_program['Program']['title'],50);?></a></td>
                                          <td><?php echo date("D j",strtotime(trim($latest_program['LatestProgram']['start_date']))).'<sup>'.date("S",strtotime(trim($latest_program['LatestProgram']['start_date']))).'</sup>&nbsp;'.date("F Y",strtotime(trim($latest_program['LatestProgram']['start_date'])));?></td>
                                        </tr>
                                        <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </table>
                                </div>
                            </div>    
                        </div>    
                    </td>

                    <td width="500px" style="vertical-align: top;">
                        <div id="upcoming_programs">
                            <h2>Upcoming Programs</h2>
                            <ul>
                                <li><a href="#upcoming_calendar_view"><span>Calendar View</span></a></li>
                                <li><a href="#upcoming_program_data"><span>Standard View</span></a></li>
                            </ul>
                            <div id="upcoming_calendar_view" style="width:400px;">
                                <iframe src="https://www.google.com/calendar/embed?src=mansmith.net%40gmail.com&ctz=Asia/Manila&showTitle=1;&width=420" style="border: 0" width="420" height="300" frameborder="0" scrolling="no"></iframe>
                                <!--<iframe src="https://www.google.com/calendar/embed?title=Mansmith%20Calendar%20of%20Events&amp;showTitle=0&amp;showTz=0&amp;width=400&amp;height=400&amp;wkst=2&amp;bgcolor=%23cccccc&amp;src=mansmith.net%40gmail.com&amp;color=%23A32929&amp;ctz=Asia%2FManila" style="border:solid 1px #777;width:400px;" height="400px" frameborder="0" scrolling="no"></iframe>-->
                            </div>
                            <div class="profileHeaderMain" id="upcoming_program_data">
                                <div>
                                    <table class="uiInfoTable">
                                        <tr><th width="60%">Upcoming Program</th><th width="40%">Schedule</th></tr>
                                        <?php
                                        if( isset($upcoming_programs) && count($upcoming_programs)>0 ){
                                            foreach( $upcoming_programs as $key => $latest_program ){
                                                $css = ( $key % 2 == 0 ) ? 'striped':'';
                                                $this->Program->setProgramData($latest_program['Program']);
                                                if( intval($latest_program['Program']['status']) == Configure::read('status_live') ){
                                                    if( $isAdmin == true || intval($this->Session->read('Auth.User.role_id')) == intval($latest_program['Program']['programs_division_id']) ){
                                                    ?>
                                                    <tr class="<?php echo $css;?>">
                                                        <td><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'programs','action'=>'info',$this->Program->getSeoUri()));?>"><?php echo $this->Text->truncate($latest_program['Program']['title'],50);?></a></td>
                                                        <td><?php echo date("D j",strtotime(trim($latest_program['UpcomingProgram']['schedule']))).'<sup>'.date("S",strtotime(trim($latest_program['UpcomingProgram']['schedule']))).'</sup>&nbsp;'.date("F Y",strtotime(trim($latest_program['UpcomingProgram']['schedule'])));?></td>
                                                    </tr>
                                                    <?php
                                                    }
                                                }
                                            }
                                        }
                                        ?>
                                    </table>
                                </div>
                            </div>
                        </div>   
                    </td>

                </tr>
                <tr valign="top">
                    <td width="500px;" style="vertical-align: top;">
                        <div id="companies" style="width:500px;">
                            <h2>Latest Added Companies</h2>
                            <ul>
                                <li><a href="#company_calendar_view"><span>Map View</span></a></li>
                                <li><a href="#company_data"><span>Standard View</span></a></li>
                            </ul>
                            <div id="company_calendar_view">
                                <div id="map_company_canvas"></div>
                            </div>
                            <div class="profileHeaderMain" id="company_data">
                                <div>
                                    <table class="uiInfoTable">
                                        <tr><th width="60%">Company Name</th><th width="40%">Date Added</th></tr>
                                        <?php
                                        foreach( $latest_companies as $key => $latest_company ){
                                            $css = ( $key % 2 == 0 ) ? 'striped':'';
                                            $this->Company->setCompanyData($latest_company['Company']);
                                            if( intval($latest_company['Company']['status']) == Configure::read('status_live') ){

                                        ?>
                                        <tr class="<?php echo $css;?>">
                                          <td><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'companies','action'=>'info',$this->Company->getSeoUri()));?>"><?php echo $this->Text->truncate($latest_company['Company']['name'],50);?></a></td>
                                          <td><?php echo date("D j",strtotime(trim($latest_company['LatestCompany']['date_created']))).'<sup>'.date("S",strtotime(trim($latest_company['LatestCompany']['date_created']))).'</sup>&nbsp;'.date("F Y",strtotime(trim($latest_company['LatestCompany']['date_created'])));?></td>
                                        </tr>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </table>
                                </div>
                            </div>    
                        </div>    
                    </td>
                    <?php
                    if(isset($latest_participants) && count($latest_participants)>0){
                    ?>
                    <td style="vertical-align: top;">
                        <div id="participants">
                             <h2>Latest Added Participants</h2>
                            <ul>
                                <li><a href="#participant_data"><span>Standard View</span></a></li>
                            </ul>
                            <div class="profileHeaderMain" id="participant_data">
                                <div id="program">
                                    <table class="uiInfoTable">
                                        <tr><th width="60%">Participants Name</th><th width="40%">Date Added</th></tr>
                                        <?php
                                        foreach( $latest_participants as $key => $latest_participant ){
                                            $css = ( $key % 2 == 0 ) ? 'striped':'';
                                            $this->Participant->setParticipantData($latest_participant['Participant']);
                                            if( intval($latest_participant['Participant']['status']) == Configure::read('status_live') ){
                                        ?>
                                        <tr class="<?php echo $css;?>">
                                          <td><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'participants','action'=>'info',$this->Participant->getSeoUri()));?>"><?php echo $this->Text->truncate($latest_participant['Participant']['full_name'],50);?></a></td>
                                            <td><?php echo date("D j",strtotime(trim($latest_participant['Participant']['date_created']))).'<sup>'.date("S",strtotime(trim($latest_participant['Participant']['date_created']))).'</sup>&nbsp;'.date("F Y",strtotime(trim($latest_participant['Participant']['date_created'])));?></td>
                                        </tr>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </table>
                                </div>
                            </div>
                        </div>   
                    </td>
                    <?php
                    }
                    ?>
                </tr>
            </table>
        </div>
    </div>
</div>
