<div class="users index">
    <?php
    $this->Html->addCrumb('<strong>All Speakers</strong>');
    ?>
    <h2><?php __('Speakers');?></h2>

    <div class="actions">
        <ul>
            <li><?php echo $this->Html->link(__('New Speaker', true), array('action'=>'add')); ?></li>
        </ul>
    </div>

    <table cellpadding="0" cellspacing="0">
    <?php
        $tableHeaders =  $this->Html->tableHeaders(array(
            $paginator->sort('id'),
            __('Role', true),
            $paginator->sort('first_name'),
            $paginator->sort('last_name'),
            $paginator->sort('email'),
            $paginator->sort('mobile'),
            __('Actions', true),
        ));
        echo $tableHeaders;

        $rows = array();
        foreach ($speakers AS $speaker) {
           
            $actions  = $this->Html->link(__('Edit', true), array('controller' => 'speakers', 'action' => 'edit', $speaker['Speaker']['id']));
            $actions .= ' ' . $this->Layout->adminRowActions($speaker['Speaker']['id']);
            $actions .= ' ' . $this->Html->link(__('Delete', true), array(
                'controller' => 'speakers',
                'action' => 'delete',
                $speaker['Speaker']['id'],
                'token' => $this->params['_Token']['key'],
            ), null, __('Are you sure?', true));

            $rows[] = array(
                $speaker['Speaker']['id'],
                $speaker['ProgramsDivision']['title'],
                $speaker['Speaker']['first_name'],
                $speaker['Speaker']['last_name'],
                $speaker['Speaker']['email'],
                $speaker['Speaker']['mobile'],
                $actions,
            );
        }
        echo $this->Html->tableCells($rows);
        //echo $tableHeaders;
    ?>
    </table>
</div>

<div class="paging"><?php echo $paginator->numbers(); ?></div>
<div class="counter"><?php echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true))); ?></div>
