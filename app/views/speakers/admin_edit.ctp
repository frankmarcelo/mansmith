<div class="users form">
    <?php
    $this->Html->addCrumb('<strong>Edit Speaker</strong>');
    ?>
    <h2><?php __('Edit Speaker'); ?></h2>
    <?php echo $this->Form->create('Speaker');?>
    <fieldset>
        <div class="tabs">
            <ul>
                <li><a href="#user-main"><?php __('Speaker'); ?></a></li>
                <?php echo $this->Layout->adminTabs(); ?>
            </ul>
            <div id="user-main">
            <div class="input select"><label for="UserRoleId">Role</label>
                <input type="hidden" name="data[Speaker][id]" value="<?php echo $data['Speaker']['id'];?>" />
                <select name="data[Speaker][programs_division_id]" id="UserRoleId">
                <?php
                foreach( $roles as $role_id => $role ){
                   $selected = ($role_id == $data['Speaker']['programs_division_id']) ? 'selected="selected"':'';
                   echo '<option value="'.$role_id.'" '.$selected.'>'.$role.'</option>';
                }
                ?>
                </select>
            </div>
            <?php
                echo $this->Form->input('first_name');
                echo $this->Form->input('last_name');
                echo $this->Form->input('email');
                echo $this->Form->input('mobile');
                echo $this->Form->input('status');
            ?>
            </div>
            <?php echo $this->Layout->adminTabs(); ?>
        </div>
    </fieldset>

    <div class="buttons">
    <?php
        echo $this->Form->end(__('Save', true));
        echo $this->Html->link(__('Cancel', true), array(
            'action' => 'index',
        ), array(
            'class' => 'cancel',
        ));
    ?>
    </div>
</div>
