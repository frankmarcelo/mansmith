<div class="users form">
    <h2><?php __('Add Speaker'); ?></h2>
    <?php echo $this->Form->create('Speaker');?>
    <fieldset>
        <div class="tabs">
            <ul>
                <li><a href="#user-main"><?php __('Speaker'); ?></a></li>
                <?php echo $this->Layout->adminTabs(); ?>
            </ul>
            <div id="user-main">
            <div class="input select"><label for="UserRoleId">Role</label>
                <select name="data[Speaker][programs_division_id]" id="UserRoleId">
                <?php
                foreach( $roles as $role_id => $role ){ 
                   echo '<option value="'.$role_id.'">'.$role.'</option>';
                }
                ?> 
                </select>
            </div>
            <?php
                echo $this->Form->input('first_name');
                echo $this->Form->input('last_name');
                echo $this->Form->input('email');
                echo $this->Form->input('mobile');
                echo $this->Form->input('status');
            ?>
            </div>
            <?php echo $this->Layout->adminTabs(); ?>
        </div>
    </fieldset>

    <div class="buttons">
    <?php
        echo $this->Form->end(__('Save', true));
        echo $this->Html->link(__('Cancel', true), array(
            'action' => 'index',
        ), array(
            'class' => 'cancel',
        ));
    ?>
    </div>
</div>
