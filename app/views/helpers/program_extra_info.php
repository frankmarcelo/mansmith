<?php

class ProgramExtraInfoHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html');
    public $uses = array('Company','Participant');
    public $program = array();

    public function setExtraInfo($options = array()) {
       	$this->program = $options;
	}

    public function getProgramType(){
       	return ucwords($this->program['ProgramsType']['name']);
    }

    public function getProgramDivision(){
    	return ucwords($this->program['ProgramsDivision']['title']);
    }
    
    public function getAttendingCompanies(){
		$sHtmlUrl = null;
		if( isset($this->program['ProgramsParticipant']) && isset($this->program['ProgramsParticipant']) && sizeof($this->program['ProgramsParticipant'])>0 ){
			$companyId = array();
			
			foreach( $this->program['ProgramsParticipant'] as $key => $value ){
				$companyId[] = $value['company_id'];
			}
			
			$this->Company = &ClassRegistry::init('Company');
			$this->Company->recursive = -1;
			$this->Company->Behaviors->attach('Containable');
			$companies = $this->Company->find("all",array(
				'fields' => array('Company.id','Company.name','Company.seo_name'),
				'conditions'=> array(
					'Company.id IN ('. implode(",",$companyId). ')',
					'Company.status' => Configure::read('status_live')
				),
				'group'=> array('Company.id'),
				'order'=> array('Company.name')
			));
			
			if( is_array($companies) && sizeof($companies)>0){
                $sHtmlUrl  ='<table style="width:200px;">';
                $sHtmlUrl .='<tr class="striped"><th>Company</th></tr>';
				foreach( $companies as $key => $company ){
					$sHtmlUrl .= '<tr><td><a href="'.$this->Html->url(array('controller'=>'companies','action'=>'info',$company['Company']['id'].DIRECTORY_SEPARATOR.$company['Company']['seo_name']));
					$sHtmlUrl .= '">'.ucwords($company['Company']['name']).'</a>';
					$sHtmlUrl .= '</td></tr>';
				}
                $sHtmlUrl .= '</table>';
			}
		}
		return $sHtmlUrl;
	}

    public function getSchedule($shortcut=false){
       $sSchedule = null;
       if( isset($this->program['ProgramsSchedule']) && is_array($this->program['ProgramsSchedule']) && count($this->program['ProgramsSchedule']) > 0 ){
       	  if( $shortcut == true ){
       	  	  if( count($this->program['ProgramsSchedule'])>1 ){
       	  	   	  return '<strong>'.date("d M",strtotime($this->program['ProgramsSchedule'][0]['start_date'])).' - '.date("d M Y",strtotime($this->program['ProgramsSchedule'][count($this->program['ProgramsSchedule'])-1]['start_date'])).'</strong>'; 	 
       	  	  }else{
       	  	  	  return '<strong>'.date("d M Y",strtotime($this->program['ProgramsSchedule'][0]['start_date'])).'</strong>';
       	  	  }	
       	  }else{	
	       	  foreach( $this->program['ProgramsSchedule'] as $program_schedule_key  => $program_schedule ){
	       	  	  if( is_array($program_schedule) && $program_schedule){	
					  $sSchedule .= '<span id="date_'.$program_schedule['program_id'].'">
			                             <span class="date_info"><strong>'.date("D",strtotime($program_schedule['start_date'])).' '.
			                             date("d",strtotime($program_schedule['start_date'])).' '.
			                             date("M Y",strtotime($program_schedule['start_date'])).'</strong></span></span><br />'; 
			      }
	       	  }
       	  }       
       }
       return $sSchedule;
    }

    public function getProgramSpeakers($speakers=array()){
       $sSpeakers = null;       
       if( isset($this->program['ProgramsSpeaker']) && is_array($this->program['ProgramsSpeaker']) && count($this->program['ProgramsSpeaker']) > 0 ){
          foreach( $speakers  as $speaker_key => $speaker_data ){
             foreach( $this->program['ProgramsSpeaker'] as $program_key => $program_speaker ){
                if( $speaker_data['Speaker']['id'] == $program_speaker['speaker_id'] ){
                   $sSpeakers .= '<div><a href="mailto:'.$speaker_data['Speaker']['email'].'">'.$speaker_data['Speaker']['first_name'].' '.$speaker_data['Speaker']['last_name'].'</a></div>';
                }
             }
          }
          return $sSpeakers; 
       }else{
       	  return 'Unassigned';	
       } 
       
    }
    
    public function getProgramRatePlans($rateplans=array()){
       $html = null;
       $aPackageRatePlan = array();

        if( isset($this->program['ProgramsRatePlan']) && is_array($this->program['ProgramsRatePlan']) && count($this->program['ProgramsRatePlan']) > 0 ){
           $html  ='<table style="width:600px;" class="resengine">';
           $html .='<tr class="striped"><th>Rate Plan No.</th><th>Cut Off Date</th><th>Early Bird</th><th>Regular</th><th>On Site</th><th>No. Adults</th></tr>';
           foreach( $this->program['ProgramsRatePlan'] as $key => $program_rate_plan ){
              $css = ( $key % 2 == 1 )? "":"";
              $html .= '<tr class="'.$css.'"><th>Plan '.$program_rate_plan['rate_plan_id'].'</th>';
              $html .= '<th>'.date("d M Y",strtotime($program_rate_plan['cut_off_date'])).'</th>';
              $html .= '<th>PHP '.number_format($program_rate_plan['eb_cost'],2, '.',',').'</th>';
              $html .= '<th>PHP '.number_format($program_rate_plan['reg_cost'],2, '.',',').'</th>';
              $html .= '<th>PHP '.number_format($program_rate_plan['osp_cost'],2, '.',',').'</th>';
              $html .= '<th>'.$program_rate_plan['num_of_adults'].'</th>';
              $html .= '</tr>';
           }
           $html .= '</table>';
        }
        return $html;
    }
    
	public function getLatestCompany(){
		$sHtmlUrl = null;
		if( isset($this->program['ProgramsParticipant']) && isset($this->program['ProgramsParticipant']) && sizeof($this->program['ProgramsParticipant'])>0 ){
			$companyId = $this->program['ProgramsParticipant'][0]['company_id'];
			$this->Company = &ClassRegistry::init('Company');
			$this->Company->recursive = -1;
			$this->Company->Behaviors->attach('Containable');
			$companies = $this->Company->find("all",array(
				'fields' => array('Company.id','Company.name','Company.seo_name'),
				'conditions'=> array(
					'Company.id' => $companyId,
					'Company.status' => Configure::read('status_live')
				),
				'limit'=> 1
			));
			
			if( is_array($companies) && sizeof($companies)>0){
				$sHtmlUrl .= '<span><strong>Latest Attending Company:</strong>&nbsp;';
				$sHtmlUrl .= '<a href="'.$this->Html->url(array('controller'=>'companies','action'=>'info',$companyId.DIRECTORY_SEPARATOR.$companies[0]['Company']['seo_name']));
				$sHtmlUrl .= '">'.ucwords($companies[0]['Company']['name']).'</a></span>';
			}
		}
		return $sHtmlUrl;
	}
	
	public function getLatestParticipant(){
		$sHtmlUrl = null;
		if( isset($this->program['ProgramsParticipant']) && sizeof($this->program['ProgramsParticipant'])>0 ){
			$companyId = $this->program['ProgramsParticipant'][0]['company_id'];
			$participantId = $this->program['ProgramsParticipant'][0]['participant_id'];
			
			$this->Participant = &ClassRegistry::init('Participant');
			$this->Participant->recursive = -1;
			$this->Participant->Behaviors->attach('Containable');
			$participants = $this->Participant->find("all",array(
				'fields' => array('Participant.id','Participant.email','Participant.first_name','Participant.last_name','Participant.seo_name'),
				'conditions'=> array(
						'Participant.id' => $participantId,
						'Participant.company_id' => $companyId,
						'Participant.status' => Configure::read('status_live')
					),
				'limit' => 1	
				)
			);
			
			if( is_array($participants) && sizeof($participants)>0){
				$sHtmlUrl .= '<span><strong>Latest Attending Participant:</strong>&nbsp;';
				$sHtmlUrl .= '<a href="'.$this->Html->url(array('controller'=>'participants','action'=>'info',$participantId.DIRECTORY_SEPARATOR.$participants[0]['Participant']['seo_name']));
				//$sHtmlUrl .= '">'.ucwords($participants['Participant']['first_name'].' '.$participants['Participant']['last_name']).'</a>&nbsp;&nbsp;Email:'.$participants['Participant']['email'].'</a>';
				$sHtmlUrl .= '">'.ucwords($participants[0]['Participant']['first_name'].' '.$participants[0]['Participant']['last_name']).'</a></span>';
			}
			
		}
		return $sHtmlUrl;
	}
	
	public function getProgramParticipantInfoById(){
	}

	public function getProgramParticipantPrimaryContact(){
		
	}

    public function createdBy($users=array(),$roles=array()){
        $division = null;
        if(!empty($roles) && count($roles)>0 ){
            $division = $roles[intval($this->program['ProgramsDivision']['id'])];
        }
        $html = null;
        if( intval($this->program[$division.'Program']['who_created']) > 0 && isset($users[intval($this->program[$division.'Program']['who_created'])]) ){
            $html = '&nbsp;Created By:&nbsp;<strong>'.$users[intval($this->program[$division.'Program']['who_created'])].'</strong>';
        }
        if( intval($this->program[$division.'Program']['who_modified']) > 0 && !is_null($this->program[$division.'Program']['who_modified']) ){
            $html .='&nbsp;Modified By:&nbsp;<strong>'.$users[intval($this->program[$division.'Program']['who_modified'])].'</strong>';
        }
        return $html;
    }
}