<?php

class CompanyHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
	public $helpers = array('Html');
    
	public function loadCompanyDataById( $companyId = null ){
    	$oCompany = null;
    	if( isset($companyId) > 0){
    		$this->Company->recursive = -1;
			$this->Company = &ClassRegistry::init('Company');
			$oCompany = $this->Company->findById($companyId);
		}
		return $oCompany;
	}
	
	public function loadCompanyProgramsById( $programs = array()){
		$programIds = array();
		foreach( $programs as $program ){
			$programIds[] = $program['program_id'];
		}
		$programIds = array_unique($programIds);
		
		$this->Program = &ClassRegistry::init('Program');
		$programs = $this->Program->find('all',array(
				'conditions'=> array('Program.id IN ('.implode(",",$programIds).')' ),
				'group' => array('Program.id'),
				'order' => array('Program.title')
			)
		);
		$html = null;
		if( is_array($programs) && count($programs) > 0){
            $html = '<table><tr><th>Program</th></tr>';
			foreach( $programs as $programInfo ){
                $html .= '<tr>';
				$seo_name = $programInfo['Program']['seo_name'];
				$program_name = $programInfo['Program']['title'];	
				$html .= '<td><a href="'.$this->Html->url(array('controller'=>'programs','action'=>'info',$programInfo['Program']['id'].DIRECTORY_SEPARATOR.$seo_name));
				$html .= '">'.ucwords(strtolower($program_name)).'</a></td></tr>';
			}
            $html .= '</table>';
		}
		return $html;
	}
    
	public function setCompanyData($options = array()) {

        if( $options && is_array($options) ){
            foreach( $options as $key => $value ){
               unset($this->{$key});
               if( strlen(trim($value)) > 0 ){
                  $key = strtolower($key);
                  $this->{$key} = trim(html_entity_decode($value));
               }
            }
        }
    }

    public function getCompanyName(){
        return ( isset($this->name) ) ? ucwords(html_entity_decode($this->name)): null;
    }

    public function getSeoUri(){
        return ( isset($this->seo_name) ) ? $this->id.DIRECTORY_SEPARATOR.$this->seo_name: null;
    }
    
    public function getTotalAttendedProgram(){
    	return (isset($this->total_attended) && $this->total_attended > 0) ? '<strong>Programs:</strong>&nbsp;'.intval($this->total_attended):null; 
    }
    
    public function getTotalEmployees(){
    	return (isset($this->total_employee) && $this->total_employee > 0) ?'<strong>Employees:</strong>&nbsp;'.intval($this->total_employee):null;
    }
    
    public function getTotalEmployee(){
    	return (isset($this->total_employee) && $this->total_employee > 0) ? intval($this->total_employee):0;
    }
    
    public function getTotalPrograms(){
    	return (isset($this->total_attended) && $this->total_attended > 0) ? intval($this->total_attended):0;	
    }

    public function getEmail(){
    	return (!empty($this->email) ) ? trim($this->email):null;
    }
    
    public function getPrimaryPhone(){
    	return (!empty($this->primary_phone) ) ? trim($this->primary_phone):null;
    }

    public function getFax(){
    	return (!empty($this->fax) ) ? trim($this->fax):null;
    }

    public function getMobile(){
    	return (!empty($this->mobile) ) ? trim($this->mobile):null;
    }

    public function getWebsite(){
    	return (!empty($this->website) ) ? trim($this->website):null;
    }
 
    public function getPrimaryAddress(){
        $sAddress = null;
        
        if( isset($this->primary_bldg_name) ){
            $sAddress .= '<strong>'.$this->primary_bldg_name.'</strong>';
        }
		if( isset($this->primary_street) ){
            $sAddress .= ' '. ucwords($this->primary_street);
        }
    	if( isset($this->primary_suburb) ){
            $sAddress .= ', '. ucwords($this->primary_suburb);
        }

        if( isset($this->primary_city) ){
            $sAddress .= ' '.ucwords($this->primary_city);
        }
        
		if( isset($this->country) ){
            $sAddress .= ' '.ucwords($this->country);
        }

        if( isset($this->primary_zip_code) ){
            $sAddress .= ' '.$this->primary_zip_code;
        }

        if( preg_match( '/^(\,)/',$sAddress) ){
            $sAddress = substr($sAddress,1);
        }
        
        return $sAddress;
    }
    
	public function getSecondaryAddress(){
		$sAddress = null;
        if( isset($this->secondary_bldg_name) ){
            $sAddress .= '<strong>'.$this->secondary_bldg_name.'</strong>';
        }
		if( isset($this->secondary_street) ){
            $sAddress .= ' '. ucwords($this->secondary_street);
        }
    	if( isset($this->secondary_suburb) ){
            $sAddress .= ', '. ucwords($this->secondary_suburb);
        }

        if( isset($this->secondary_city) ){
            $sAddress .= ' '.ucwords($this->secondary_city);
        }
        
		if( isset($this->country) && strlen(trim($sAddress)) > 0){
            $sAddress .= ' '.ucwords($this->country);
        }

        if( isset($this->secondary_zip_code) ){
            $sAddress .= ' '.$this->secondary_zip_code;
        }

        if( preg_match( '/^(\,)/',$sAddress) ){
            $sAddress = substr($sAddress,1);
        }
        return $sAddress;
    }
    
    public function getLatitude(){
        return ( isset($this->latitude) ) ? $this->latitude: null;
    }
    
    public function getLongitude(){
        return ( isset($this->longitude) ) ? $this->longitude: null;
    }

    public function get( $key='' ){
        return ( isset($this->{$key}) ) ? $this->{$key} : null; 
    }
    
	public function getDisplayDelete(){
        if( !$this->get('total_attended') ){
            $url = $this->Html->url(array(
                	 	"controller" => "companies",
                	 	"admin"=>'true',
                	 	"action"=>"info" ,
                	 	$this->getSeoUri()
                	));
        	return '<a class="delete" title="'.$this->getCompanyName().'" seo="'.$this->getSeoUri().'" seouri="'.$url.'" style="cursor:pointer;" id="all_companies_'.$this->id.'">Delete</a>';
        }
        return null;
    }
    
	public function getDisplayEdit(){
        $url = $this->Html->url(array(
                	 	"controller" => "companies",
                	 	"admin"=>'true',
                	 	"action"=>"edit" ,
                	 	$this->getSeoUri()
                	));
        return '<a class="edit_info" href="'.$url.'">Edit</a>';
    }
    
    public function getAddEmployee(){
        $url = $this->Html->url(array(
                	 	"controller" => "participants",
                	 	"admin"=>'true',
                	 	"action" => "create?company=".$this->getSeoUri()
                ));
    	return '<a target="_blank" class="add_info" href="'.$url.'">Add Employee</a>';
    }
    
 	public function getNotes(){
		$sNotes = (isset($this->notes)) ? html_entity_decode($this->notes):null;
		$sNotes = strip_tags($sNotes);
	    if(strlen($sNotes) > 0 ){
	    	$sNotes = substr($sNotes,0,150).'...';
	    }	
	    return $sNotes;
    }
}