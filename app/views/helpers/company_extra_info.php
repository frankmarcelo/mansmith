<?php

class CompanyExtraInfoHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html');
    public $uses = array('Company','Participant','Program');
    public $company = array();

    public function setExtraInfo($options = array()) {
       $this->company = $options;
    }

    public function getCompanyAffiliationType(){
       return ucwords($this->company['CompaniesAffiliation']['name']);
    }

    public function getCompanyIndustryType(){
       return ucwords(trim(strtolower($this->company['CompaniesIndustry']['name'])));
    }

    public function getCompanyEmployees(){
    	$html = '<table><tr><th>Name</th><th>Email</th><th>Phone</th></tr>';
        foreach( $this->company['Participant'] as $participantInfo ){
            $html .='<tr>';
			$seo_name = $participantInfo['seo_name'];
			$participant_name = '<a href="'.$this->Html->url(array('controller'=>'participants','action'=>'info',$participantInfo['id'].DIRECTORY_SEPARATOR.$seo_name)).'">'.$participantInfo['full_name'].'</a>';
            $html .= '<td>'.trim($participant_name).'</td>';
			$html .= '<td>'.trim($participantInfo['email']).'</td>';
            $html .= '<td>'.$participantInfo['phone'].'</td>';
			$html .= '</tr>';
		}
        $html .= '</table>';
		return $html;
	}
    
	public function getLatestParticipant(){
            $sHtmlUrl = null;
            if( isset($this->company['Participant']) && sizeof($this->company['Participant']) > 0){
                $participantId = null;
                $seo_name = null;
                $first_name = null;
                $last_name = null;

		foreach( $this->company['Participant'] as $participantInfo ){
                    $participantInfo['first_name'] = trim(str_replace(array("-","."),array("",""),$participantInfo['first_name']));
                    $participantInfo['last_name'] = trim(str_replace(array("-","."),array("",""),$participantInfo['last_name']));
                    $participantInfo['email'] = trim($participantInfo['email']);
                    $participantInfo['seo_name'] = trim($participantInfo['seo_name']);
                    if( $participantInfo['seo_name'] != "-" ){
                        $participantId = $participantInfo['id'];
                        $seo_name = $participantInfo['seo_name'];
                        $first_name = $participantInfo['first_name'];
                        $last_name = $participantInfo['last_name'];
                        break;
                    }
                }
                $sHtmlUrl .= '<a href="'.$this->Html->url(array('controller'=>'participants','action'=>'info',$participantId.DIRECTORY_SEPARATOR.$seo_name));
                $sHtmlUrl .= '">'.ucfirst(strtolower($first_name)).' '.ucfirst(strtolower($last_name)).'</a>';
            }else{
                $sHtmlUrl .= '-';
            }
            return $sHtmlUrl;
	}
	
	public function getLatestProgramAttended(){
		$sHtmlUrl = null;
		if( isset($this->company['ProgramsParticipant']) && sizeof($this->company['ProgramsParticipant']) > 0){
			$programId =$this->company['ProgramsParticipant'][0]['program_id'];
			$this->Program = &ClassRegistry::init('Program');
			$this->Program->recursive = -1;
			$oProgram = $this->Program->findById($programId);
			if( is_array($oProgram) && $oProgram['Program']['id'] > 0){
				$seo_name = $oProgram['Program']['seo_name'];
				$program_name = $oProgram['Program']['title'];	
				$sHtmlUrl .= '<a href="'.$this->Html->url(array('controller'=>'programs','action'=>'info',$programId.DIRECTORY_SEPARATOR.$seo_name));
				$sHtmlUrl .= '">'.ucwords(strtolower($program_name)).'</a>';	
			}
		}else{
			$sHtmlUrl .= '&nbsp;';
		}
		return $sHtmlUrl;
		
	}

    public function createdBy($users=array()){
        $html = null;
        if( isset( $this->company['Company']['who_created'] ) && $this->company['Company']['who_created'] > 0){
            $html = '&nbsp;Created By:&nbsp;<strong>'.$users[intval($this->company['Company']['who_created'])].'</strong>';
        }
        if( intval($this->company['Company']['who_modified']) > 0 && !is_null($this->company['Company']['who_modified']) ){
            $html .='&nbsp;Modified By:&nbsp;<strong>'.$users[intval($this->company['Company']['who_modified'])].'</strong>';
        }
        return $html;
    }
}