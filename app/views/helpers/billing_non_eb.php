<?php

class BillingNonEbHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html');
    public $components = array('Session');
    public $oOptions = null;
    const billing_type_id = 2;
    
	public function loadBillingNonEbByProgramId( $programId = null,$company_id=0  ){
		$oBillingNonEbs = null;
		if( isset($programId) && intval($programId) > 0){
    		$this->BillingNonEb = &ClassRegistry::init('BillingNonEb');
			$this->BillingNonEb->contain(array('Company.name','Company.seo_name'));
            if( intval($company_id) < 1 ){
                $oBillingNonEbs = $this->BillingNonEb->findAllByProgramId($programId);
            }else{
                $oBillingNonEbs = $this->BillingNonEb->find('all',array(
                    'conditions' => array('BillingNonEb.company_id IN ('.intval($company_id).')')
                ));
            }

			
			if( isset($oBillingNonEbs) && count($oBillingNonEbs)>0 ){
                foreach( $oBillingNonEbs as $key => $oBillingNonEb ){
    				if( $oBillingNonEb['BillingNonEb']['status'] != Configure::read('status_live') && $oBillingNonEb['BillingNonEb']['billing_type_id'] == constant('self::billing_type_id') ){
                        unset($oBillingNonEbs[$key]);
					}		
				}		
			}
		}
        return $oBillingNonEbs;
	}

    public function setBillingNonEbData($options = array()) {
		if( $options && is_array($options) ){
            $this->oOptions = $options;
    	}
    }

    public function getBillingNonEbDirectory($programId=0){
    	$oDirectoryProperty = new DirectoryProperty();
        $oDirectoryProperty->documentDirectory = Configure::read('docs_directory');
        $oDocumentDirectory = new DocumentDirectory( $oDirectoryProperty, $programId );
        return $oDocumentDirectory->getBillingNonEbDirectory();
    }
    
    public function billingNonEbExist($programId=0,$sourceFile = null){
        if(file_exists($sourceFile)){
        	return true;
        }else{
        	return false;
        }
    }

    public function getBillingRatePlans(){
        $html  = '<table class="resengine" style="width: 600px;">';
        $html .= '<tr class="striped"><th>Regular Rate</th><th>On Site Rate</th></tr>';
        //$html .= '<tr><td>PHP '.number_format($this->oOptions['BillingNonEb']['early_bird_charges'],2, '.',',').'</td>';
        $html .= '<tr>';
        $html .= '<td>PHP '.number_format($this->oOptions['BillingNonEb']['regular_rate_charges'],2, '.',',').'</td>';
        $html .= '<td>PHP '.number_format($this->oOptions['BillingNonEb']['on_site_rate_charges'],2, '.',',').'</td></tr></table>';
        return $html;
    }
    
    public function getProgramTitle(){
    	return wordwrap(trim($this->oOptions['Program']['title']),40,'<br/>');	
    }
    
    public function getBillingNonEbId(){
 		return intval($this->oOptions['BillingNonEb']['id']);
    }
    
	public function getSeoUri(){
        return ( isset($this->oOptions['Program']['title']) ) ? intval($this->oOptions['Program']['id']).DIRECTORY_SEPARATOR.trim($this->oOptions['Program']['seo_name']):null;
    }

    public function createdBy($users=array(),$billingNonEb){

        $html = (isset($users[intval($billingNonEb['BillingNonEb']['who_created'])])) ? '<strong>'.$users[intval($billingNonEb['BillingNonEb']['who_created'])].'</strong>':null;
        return $html;
    }
}