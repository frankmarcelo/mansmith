<?php

class InvoiceHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html');
    public $components = array('Session');
    public $oOptions = null;

    
    public function loadInvoiceByProgramId($programId = null,$company_id=0 ){
		$oInvoices = null;
		if( isset($programId) && intval($programId) > 0){

            $this->Invoice = &ClassRegistry::init('Invoice');
            $this->Invoice->contain();

            if( intval($company_id) < 1 ){
                $invoiceIds = $this->Invoice->findAllByProgramId($programId);
            }else{
                $invoiceIds = $this->Invoice->find('all',array(
                    'conditions' => array('Invoice.company_id IN ('.intval($company_id).')')
                ));
            }

            if( count($invoiceIds)>0 ){
                $aInvoices = array();
                foreach( $invoiceIds as $invoice_info ){
                    $aInvoices[] = $invoice_info['Invoice']['id'];
                }
                
                $this->InvoiceDetails = &ClassRegistry::init('InvoiceDetails');
                $this->InvoiceDetails->bindModel(array(
                        'hasOne' => array(
                            'Company' => array(
                                'fields' => array('Company.id','Company.name','Company.seo_name'),
                                'foreignKey' => false,
                                'conditions' => array('Invoice.company_id = Company.id')
                            )
                        )
                    )
                );
                $oInvoices = $this->InvoiceDetails->find('all',array(
                        'conditions' => array('InvoiceDetails.invoice_id IN ('.implode(",",$aInvoices).')' ),
                        'order'=> array('InvoiceDetails.invoice_id','InvoiceDetails.billing_type_id')
                    )
                );
            }
		}
        return $oInvoices;
	}

    public function setInvoiceData($options = array()) {
		if( $options && is_array($options) ){
            $this->oOptions = $options;
    	}
    }

    public function getInvoiceType($billingTypeId=1){
        $extraDirectory = null;
        if( $billingTypeId == 1){
            $extraDirectory = 'eb';
        }elseif( $billingTypeId == 2){
            $extraDirectory = 'non-eb';
        }elseif( $billingTypeId == 3){
            $extraDirectory = 'osp';
        }elseif( $billingTypeId == 4){
            $extraDirectory = 'regular';
        }
        return $extraDirectory.DIRECTORY_SEPARATOR;
    }

    public function getInvoiceDirectory($programId=0){
        $oDirectoryProperty = new DirectoryProperty();
        $oDirectoryProperty->documentDirectory = Configure::read('docs_directory');
        $oDocumentDirectory = new DocumentDirectory( $oDirectoryProperty, $programId );
        return $oDocumentDirectory->getInvoiceDirectory();
    }
    
    public function invoiceExist($sourceFile = null){
        if(file_exists($sourceFile)){
        	return true;
        }else{
        	return false;
        }
    }
    
    public function getProgramTitle(){
    	return wordwrap(trim($this->oOptions['Program']['title']),40,'<br/>');	
    }
    
    public function getInvoiceId(){
 		return intval($this->oOptions['Invoice']['id']);
    }
    
	public function getSeoUri(){
        return ( isset($this->oOptions['Program']['title']) ) ? intval($this->oOptions['Program']['id']).DIRECTORY_SEPARATOR.trim($this->oOptions['Program']['seo_name']):null;
    }

    public function createdBy($users=array(),$invoiceFile){
        $html = (isset($users[intval($invoiceFile['InvoiceDetails']['who_created'])])) ? '<strong>'.$users[intval($invoiceFile['InvoiceDetails']['who_created'])].'</strong>':null;
        return $html;
    }
}
