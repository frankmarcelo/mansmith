<?php

class ProgramExtraInfoHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html');
    public $program = array();

    public function setProgramData($options = array()) {

        if( $options && is_array($options) ){
            foreach( $options as $key => $value ){
               if( strlen(trim($value)) > 0 ){
                  $key = strtolower($key);
                  unset($this->{$key});
                  $this->{$key} = trim($value);
               }
            }
        }
    }
}