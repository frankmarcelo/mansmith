<?php

class CertificatesHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html','Text');
    public $components = array('Session');
    public $oOptions = null;
    
	public function loadCertificatesByProgramId( $programId = null ){
		$oCertificates = null;
		if( isset($programId) && intval($programId) > 0){
    		$this->ProgramsCertificate = &ClassRegistry::init('ProgramsCertificate');
			$this->ProgramsCertificate->contain();
			$oCertificates = $this->ProgramsCertificate->findAllByProgramId($programId);
			if( isset($oCertificates) && count($oCertificates)>0 ){
				foreach( $oCertificates as $key => $oCertificate ){
					if( $oCertificate['ProgramsCertificate']['status'] != Configure::read('status_live') ){
						unset($oCertificates[$key]);
					}		
				}		
			}
		}
		return $oCertificates;
	}

     public function getTotalAttendingCompanies(){
        if( intval($this->oOptions['ProgramsCertificate']['program_id']) >0 && count($this->oOptions['ProgramsCertificate'])>0 ){
            $companyId = array();
            if(isset($this->oOptions['CertificatesParticipants']) && count($this->oOptions['ProgramsCertificate'])>0 ){
                foreach( $this->oOptions['CertificatesParticipants'] as $participants ){
                    $companyId[] = $participants['company_id'];
                }
                $companyId = array_unique($companyId);
            }
            return count($companyId);
        }
    }

    public function getAttendanceCompanies(){
        $html = null;
        if( intval($this->oOptions['ProgramsCertificate']['program_id']) >0 && count($this->oOptions['ProgramsCertificate'])>0 ){
            $companyId = array();
            if(isset($this->oOptions['CertificatesParticipants']) && count($this->oOptions['ProgramsCertificate'])>0 ){
                foreach( $this->oOptions['CertificatesParticipants'] as $participants ){
                    $companyId[] = $participants['company_id'];
                }
                $companyId = array_unique($companyId);
            }

            $this->Company = &ClassRegistry::init('Company');
            $this->Company->contain();
			$aCompanies = $this->Company->find('all',array(
                'fields' => array('Company.id','Company.name','Company.seo_name'),
                'conditions' => array('Company.id IN ('.implode(",",$companyId).')'),
                'group' => array('Company.id'),
                'order' => array('Company.name')
            ));

            if( count($aCompanies) >0 && is_array($aCompanies) ){
                foreach($aCompanies as $company_key => $company ){
                    $link = $this->Html->url(array(
                	 	"controller" => "companies",
                	 	"admin"=>'true',
                	 	"action"=>"info" ,
                	 	intval($company['Company']['id']).'/'.trim($company['Company']['seo_name'])
                	));
                    $html .= '<a href="'.$link.'">'.$this->Text->truncate($company['Company']['name'],44,array(
                    'ending' => '...',
                    'exact' => false
                    )).'</a><br/>';
                }
            }
        }
        return $html;
    }

    public function getTotalAttendanceParticipants(){
        return count($this->oOptions['CertificatesParticipants']);
    }

    public function setCertificateData($options = array()) {
		if( $options && is_array($options) ){
            $this->oOptions = $options;
    	}
    }
    
    public function getCertificatesDirectory($programId=0){
    	$oDirectoryProperty = new DirectoryProperty();
        $oDirectoryProperty->documentDirectory = Configure::read('docs_directory');
        $oDocumentDirectory = new DocumentDirectory( $oDirectoryProperty, $programId );
        return $oDocumentDirectory->getCertificatesDirectory();	
    }
    
    public function certificatesExist($programId=0,$sourceFile = null){
        if(file_exists($sourceFile)){
        	return true;
        }else{
        	return false;
        }
    }
    
    public function getProgramTitle(){
    	return wordwrap(trim($this->oOptions['Program']['title']),40,'<br/>');	
    }
    
    public function getCertificateId(){
 		return intval($this->oOptions['ProgramsCertificate']['id']);   	
    }
    
	public function getSeoUri(){
        return ( isset($this->oOptions['Program']['title']) ) ? intval($this->oOptions['Program']['id']).DIRECTORY_SEPARATOR.trim($this->oOptions['Program']['seo_name']):null;
    }

    public function createdBy($users=array()){
        $html = isset($users[$this->oOptions['ProgramsCertificate']['who_created']]) ?'Created By:&nbsp;<strong>'.$users[intval($this->oOptions['ProgramsCertificate']['who_created'])].'</strong>':null;
        if( intval($this->oOptions['ProgramsCertificate']['who_modified']) > 0 && !is_null($this->oOptions['ProgramsCertificate']['who_modified']) ){
            $html .='<br/>Modified By:&nbsp;<strong>'.$users[intval($this->oOptions['ProgramsCertificate']['who_modified'])].'</strong>';
        }
        return $html;
    }
}