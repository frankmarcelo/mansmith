<?php
class NameTagsHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html','Text');
    public $components = array('Session');
    public $oOptions = null;
    
	public function loadNameTagDataById( $nametagId = null ){
    	$oNameTags = null;
    	if( isset($nametagId) && intval($nametagId)> 0){
    		$conditions[] = array( 
	        	'AND' => array (
	    			'NameTags.id' => $nametagId,
        			'NameTags.status = '.Configure::read('status_live')
        		)
	    	);
	    	$this->NameTags->recursive = -1;
			$this->NameTags = &ClassRegistry::init('NameTags');
			$oNameTags = $this->NameTags->find($conditions);
		}
		return $oNameTags;
	}

    public function getTotalAttendingCompanies(){
        if( intval($this->oOptions['ProgramsNameTag']['program_id']) >0 && count($this->oOptions['NameTagsParticipants'])>0 ){
            $companyId = array();
            if(isset($this->oOptions['NameTagsParticipants']) && count($this->oOptions['NameTagsParticipants'])>0 ){
                foreach( $this->oOptions['NameTagsParticipants'] as $participants ){
                    $companyId[] = $participants['company_id'];
                }
                $companyId = array_unique($companyId);
            }
            return count($companyId);
        }
        return 0;
    }

    public function getAttendanceCompanies(){
        $html = null;
        if( intval($this->oOptions['ProgramsNameTag']['program_id']) >0 && count($this->oOptions['NameTagsParticipants'])>0 ){
            $companyId = array();
            if(isset($this->oOptions['NameTagsParticipants']) && count($this->oOptions['NameTagsParticipants'])>0 ){
                foreach( $this->oOptions['NameTagsParticipants'] as $participants ){
                    $companyId[] = $participants['company_id'];
                }
                $companyId = array_unique($companyId);
            }

            $this->Company = &ClassRegistry::init('Company');
            $this->Company->contain();
			$aCompanies = $this->Company->find('all',array(
                'fields' => array('Company.id','Company.name','Company.seo_name'),
                'conditions' => array('Company.id IN ('.implode(",",$companyId).')'),
                'group' => array('Company.id'),
                'order' => array('Company.name')
            ));

            if( count($aCompanies) >0 && is_array($aCompanies) ){
                foreach($aCompanies as $company_key => $company ){
                    $link = $this->Html->url(array(
                	 	"controller" => "companies",
                	 	"admin"=>'true',
                	 	"action"=>"info" ,
                	 	intval($company['Company']['id']).'/'.trim($company['Company']['seo_name'])
                	));
                    $html .= '<a href="'.$link.'">'.$this->Text->truncate($company['Company']['name'],44,array(
                    'ending' => '...',
                    'exact' => false
                    )).'</a><br/>';
                }
            }
        }
        return $html;
    }

    public function getTotalAttendanceParticipants(){
        return count($this->oOptions['NameTagsParticipants']);
    }
	
	public function loadNameTagsByProgramId( $programId = null ){
		$oNameTags = null;
		if( isset($programId) && intval($programId) > 0){
    		$this->ProgramsNameTag = &ClassRegistry::init('ProgramsNameTag');
			$this->ProgramsNameTag->contain();
			$oNameTags = $this->ProgramsNameTag->findAllByProgramId($programId);
			if( isset($oNameTags) && count($oNameTags)>0 ){
				foreach( $oNameTags as $key => $oNameTag ){
					if( $oNameTag['ProgramsNameTag']['status'] != Configure::read('status_live') ){
						unset($oNameTags[$key]);
					}		
				}		
			}
		}
		return $oNameTags;
	}

    public function setNameTagData($options = array()) {
		if( $options && is_array($options) ){
            $this->oOptions = $options;
    	}
    }
    
    public function getNameTagDirectory($programId=0){
    	$oDirectoryProperty = new DirectoryProperty();
        $oDirectoryProperty->documentDirectory = Configure::read('docs_directory');
        $oDocumentDirectory = new DocumentDirectory( $oDirectoryProperty, $programId );
        return $oDocumentDirectory->getNameTagsDirectory();	
    }
    
    public function nameTagExist($programId=0,$sourceFile = null){
        if(file_exists($sourceFile)){
        	return true;
        }else{
        	return false;
        }
    }
    
    public function getProgramTitle(){
    	return wordwrap(trim($this->oOptions['Program']['title']),40,'<br/>');	
    }
    
    public function getNameTagId(){
 		return intval($this->oOptions['ProgramsNameTag']['id']);   	
    }
    
	public function getSeoUri(){
        return ( isset($this->oOptions['Program']['title']) ) ? intval($this->oOptions['Program']['id']).DIRECTORY_SEPARATOR.trim($this->oOptions['Program']['seo_name']):null;
    }

    public function createdBy($users=array()){
        $html = isset($users[$this->oOptions['ProgramsNameTag']['who_created']]) ?'Created By:&nbsp;<strong>'.$users[$this->oOptions['ProgramsNameTag']['who_created']].'</strong>':null;
        if( intval($this->oOptions['ProgramsNameTag']['who_modified']) > 0 && !is_null($this->oOptions['ProgramsNameTag']['who_modified']) ){
            $html .='<br/>Modified By:&nbsp;<strong>'.$users[intval($this->oOptions['ProgramsNameTag']['who_modified'])].'</strong>';
        }
        return $html;
    }
    
    public function getFileName(){
    	
    }
    
    public function getSchedule(){
    	
    }
    
    public function getProgramUrl(){
    	
    }
    
    public function getParticipants(){
    	
    }
}