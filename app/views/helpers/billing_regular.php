<?php

class BillingRegularHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html');
    public $components = array('Session');
    public $oOptions = null;
    const billing_type_id = 4;
    
	public function loadBillingRegularByProgramId( $programId = null,$company_id=0 ){
		$oBillingRegulars = null;
		if( isset($programId) && intval($programId) > 0){
    		$this->BillingRegular = &ClassRegistry::init('BillingRegular');
			$this->BillingRegular->contain(array('Company.name','Company.seo_name'));
            if( intval($company_id) < 1 ){
                $oBillingRegulars = $this->BillingRegular->findAllByProgramId($programId);
            }else{
                $oBillingRegulars = $this->BillingRegular->find('all',array(
                    'conditions' => array('BillingRegular.company_id IN ('.intval($company_id).')')
                ));
            }

			if( isset($oBillingRegulars) && count($oBillingRegulars)>0 ){
                foreach( $oBillingRegulars as $key => $oBillingRegular ){
    				if( $oBillingRegular['BillingRegular']['status'] != Configure::read('status_live') && $oBillingRegular['BillingRegular']['billing_type_id'] == constant('self::billing_type_id') ){
                        unset($oBillingRegulars[$key]);
					}
				}
			}
		}
        return $oBillingRegulars;
	}

    public function setBillingRegularData($options = array()) {
		if( $options && is_array($options) ){
            $this->oOptions = $options;
    	}
    }

    public function getBillingRegularDirectory($programId=0){
    	$oDirectoryProperty = new DirectoryProperty();
        $oDirectoryProperty->documentDirectory = Configure::read('docs_directory');
        $oDocumentDirectory = new DocumentDirectory( $oDirectoryProperty, $programId );
        return $oDocumentDirectory->getBillingRegularDirectory();
    }
    
    public function billingRegularExist($programId=0,$sourceFile = null){
        if(file_exists($sourceFile)){
        	return true;
        }else{
        	return false;
        }
    }

    public function getBillingRatePlans(){
        $html  = '<table class="resengine" style="width: 600px;">';
        $html .= '<tr class="striped"><th>Regular Rate</th></tr>';
        $html .= '<tr>';
        $html .= '<td>PHP '.number_format($this->oOptions['BillingRegular']['regular_rate_charges'],2, '.',',').'</td></tr></table>';
        return $html;
    }
    
    public function getProgramTitle(){
    	return wordwrap(trim($this->oOptions['Program']['title']),40,'<br/>');	
    }
    
    public function getBillingRegularId(){
 		return intval($this->oOptions['BillingRegular']['id']);
    }
    
	public function getSeoUri(){
        return ( isset($this->oOptions['Program']['title']) ) ? intval($this->oOptions['Program']['id']).DIRECTORY_SEPARATOR.trim($this->oOptions['Program']['seo_name']):null;
    }

    public function createdBy($users=array(),$billingRegular){
        $html = (isset($users[intval($billingRegular['BillingRegular']['who_created'])])) ? '<strong>'.$users[intval($billingRegular['BillingRegular']['who_created'])].'</strong>':null;
        return $html;
    }
}