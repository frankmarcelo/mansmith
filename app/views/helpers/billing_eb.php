<?php

class BillingEbHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html');
    public $components = array('Session');
    public $oOptions = null;
    const billing_type_id = 1;
    
	public function loadBillingEbByProgramId( $programId = null,$company_id=0 ){
		$oBillingEbs = null;
		if( isset($programId) && intval($programId) > 0){
    		$this->BillingEb = &ClassRegistry::init('BillingEb');
			$this->BillingEb->contain(array('Company.name','Company.seo_name'));
            if( intval($company_id) < 1 ){
                $oBillingEbs = $this->BillingEb->findAllByProgramId($programId);
            }else{
                $oBillingEbs = $this->BillingEb->find('all',array(
                    'conditions' => array('BillingEb.company_id IN ('.intval($company_id).')')
                ));
            }
			if( isset($oBillingEbs) && count($oBillingEbs)>0 ){
                foreach( $oBillingEbs as $key => $oBillingEb ){
                    
					if( $oBillingEb['BillingEb']['status'] != Configure::read('status_live') && $oBillingEb['BillingEb']['billing_type_id'] == constant('self::billing_type_id') ){
                        unset($oBillingEbs[$key]);
					}		
				}		
			}
		}
        return $oBillingEbs;
	}

     public function getTotalAttendingCompanies(){
        if( intval($this->oOptions['ProgramsAttendanceSheet']['program_id']) >0 && count($this->oOptions['ProgramsAttendanceSheet'])>0 ){
            $companyId = array();
            if(isset($this->oOptions['AttendanceSheetsParticipants']) && count($this->oOptions['AttendanceSheetsParticipants'])>0 ){
                foreach( $this->oOptions['AttendanceSheetsParticipants'] as $participants ){
                    $companyId[] = $participants['company_id'];
                }
                $companyId = array_unique($companyId);
            }
            return count($companyId);
        }
    }

    public function getAttendanceCompanies(){
        $html = null;
        if( intval($this->oOptions['ProgramsAttendanceSheet']['program_id']) >0 && count($this->oOptions['ProgramsAttendanceSheet'])>0 ){
            $companyId = array();
            if(isset($this->oOptions['AttendanceSheetsParticipants']) && count($this->oOptions['AttendanceSheetsParticipants'])>0 ){
                foreach( $this->oOptions['AttendanceSheetsParticipants'] as $participants ){
                    $companyId[] = $participants['company_id'];
                }
                $companyId = array_unique($companyId);
            }

            $this->Company = &ClassRegistry::init('Company');
            $this->Company->contain();
			$aCompanies = $this->Company->find('all',array(
                'fields' => array('Company.id','Company.name','Company.seo_name'),
                'conditions' => array('Company.id IN ('.implode(",",$companyId).')'),
                'group' => array('Company.id'),
                'order' => array('Company.name')
            ));

            if( count($aCompanies) >0 && is_array($aCompanies) ){
                foreach($aCompanies as $company_key => $company ){
                    $link = $this->Html->url(array(
                	 	"controller" => "companies",
                	 	"admin"=>'true',
                	 	"action"=>"info" ,
                	 	intval($company['Company']['id']).'/'.trim($company['Company']['seo_name'])
                	));
                    $html .= '<a href="'.$link.'">'.$this->Text->truncate($company['Company']['name'],44,array(
                    'ending' => '...',
                    'exact' => false
                    )).'</a><br/>';
                }
            }
        }
        return $html;
    }

    public function setBillingEbData($options = array()) {
		if( $options && is_array($options) ){
            $this->oOptions = $options;
    	}
    }

    public function getBillingEbDirectory($programId=0){
    	$oDirectoryProperty = new DirectoryProperty();
        $oDirectoryProperty->documentDirectory = Configure::read('docs_directory');
        $oDocumentDirectory = new DocumentDirectory( $oDirectoryProperty, $programId );
        return $oDocumentDirectory->getBillingEbDirectory();
    }
    
    public function billingEbExist($programId=0,$sourceFile = null){
        if(file_exists($sourceFile)){
        	return true;
        }else{
        	return false;
        }
    }

    public function getBillingRatePlans(){
        $html  = '<table class="resengine" style="width: 600px;">';
        $html .= '<tr class="striped"><th>Early Bird</th><th>Regular Rate</th><th>On Site Rate</th></tr>';
        $html .= '<tr><td>PHP '.number_format($this->oOptions['BillingEb']['early_bird_charges'],2, '.',',').'</td>';
        $html .= '<td>PHP '.number_format($this->oOptions['BillingEb']['regular_rate_charges'],2, '.',',').'</td>';
        $html .= '<td>PHP '.number_format($this->oOptions['BillingEb']['on_site_rate_charges'],2, '.',',').'</td></tr></table>';
        return $html;
    }
    
    public function getProgramTitle(){
    	return wordwrap(trim($this->oOptions['Program']['title']),40,'<br/>');	
    }
    
    public function getBillingEbId(){
 		return intval($this->oOptions['BillingEb']['id']);
    }
    
	public function getSeoUri(){
        return ( isset($this->oOptions['Program']['title']) ) ? intval($this->oOptions['Program']['id']).DIRECTORY_SEPARATOR.trim($this->oOptions['Program']['seo_name']):null;
    }

    public function createdBy($users=array(),$billingEb){

        $html = (isset($users[intval($billingEb['BillingEb']['who_created'])])) ? '<strong>'.$users[intval($billingEb['BillingEb']['who_created'])].'</strong>':null;
        return $html;
    }
}