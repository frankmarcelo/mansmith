<?php

class SalesReportsDetailsHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html');
    public $components = array('Session');
    public $oOptions = null;
    
    public function loadSalesReportsDetailsById( $salesreportsId = null ){
		$oSalesReportsDetails = null;
		if( isset($salesreportsId) && intval($salesreportsId) > 0){
    		$this->SalesReportsDetails = &ClassRegistry::init('SalesReportsDetails');
			$oSalesReportsDetails = $this->SalesReportsDetails->findAllBySalesReportsId($salesreportsId);
		}
        return (isset($oSalesReportsDetails[0]))?$oSalesReportsDetails[0]:null;
	}
    
    public function setSalesReportsDetailsData($options = array()) {
		if( $options && is_array($options) ){
            $this->oOptions = $options;
    	}
    }

    public function getSalesReportsId(){
        return intval($this->oOptions[0]['SalesReportsDetails']['id']);
    }

    public function getSalesReportsDirectory($programId=null){
    	$oDirectoryProperty = new DirectoryProperty();
        $oDirectoryProperty->documentDirectory = Configure::read('docs_directory');
        $oDocumentDirectory = new DocumentDirectory( $oDirectoryProperty, $programId );
        return $oDocumentDirectory->getSalesReportsDirectory();
    }
    
    public function billingEbExist($programId=0,$sourceFile = null){
        if(file_exists($sourceFile)){
        	return true;
        }else{
        	return false;
        }
    }
    
    public function getProgramTitle(){
    	return wordwrap(trim($this->oOptions['Program']['title']),40,'<br/>');	
    }
    
    public function getBillingEbId(){
 		return intval($this->oOptions['BillingEb']['id']);
    }
    
	public function getSeoUri(){
        return ( isset($this->oOptions['Program']['title']) ) ? intval($this->oOptions['Program']['id']).DIRECTORY_SEPARATOR.trim($this->oOptions['Program']['seo_name']):null;
    }
}
