<?php

class BillingOspHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html');
    public $components = array('Session');
    public $oOptions = null;
    const billing_type_id = 3;
    
	public function loadBillingOspByProgramId( $programId = null,$company_id=0   ){

        $oBillingOsps = null;
		if( isset($programId) && intval($programId) > 0){
    		$this->BillingOsp = &ClassRegistry::init('BillingOsp');
			$this->BillingOsp->contain(array('Company.name','Company.seo_name'));

            if( intval($company_id) < 1 ){
                $oBillingOsps = $this->BillingOsp->findAllByProgramId($programId);
            }else{
                $oBillingOsps = $this->BillingOsp->find('all',array(
                    'conditions' => array('BillingOsp.company_id IN ('.intval($company_id).')')
                ));
            }

			if( isset($oBillingOsps) && count($oBillingOsps)>0 ){
                foreach( $oBillingOsps as $key => $oBillingOsp ){
    				if( $oBillingOsp['BillingOsp']['status'] != Configure::read('status_live') && $oBillingOsp['BillingOsp']['billing_type_id'] == constant('self::billing_type_id') ){
                        unset($oBillingOsps[$key]);
					}
				}
			}
		}
        return $oBillingOsps;
	}

    public function setBillingOspData($options = array()) {
		if( $options && is_array($options) ){
            $this->oOptions = $options;
    	}
    }

    public function getBillingOspDirectory($programId=0){
    	$oDirectoryProperty = new DirectoryProperty();
        $oDirectoryProperty->documentDirectory = Configure::read('docs_directory');
        $oDocumentDirectory = new DocumentDirectory( $oDirectoryProperty, $programId );
        return $oDocumentDirectory->getBillingOspDirectory();
    }
    
    public function billingOspExist($programId=0,$sourceFile = null){
        if(file_exists($sourceFile)){
        	return true;
        }else{
        	return false;
        }
    }

    public function getBillingRatePlans(){
        $html  = '<table class="resengine" style="width: 600px;">';
        $html .= '<tr class="striped"><th>On Site Rate</th></tr>';
        $html .= '<tr>';
        $html .= '<td>PHP '.number_format($this->oOptions['BillingOsp']['on_site_rate_charges'],2, '.',',').'</td></tr></table>';
        return $html;
    }
    
    
    public function getProgramTitle(){
    	return wordwrap(trim($this->oOptions['Program']['title']),40,'<br/>');	
    }
    
    public function getBillingOspId(){
 		return intval($this->oOptions['BillingOsp']['id']);
    }
    
	public function getSeoUri(){
        return ( isset($this->oOptions['Program']['title']) ) ? intval($this->oOptions['Program']['id']).DIRECTORY_SEPARATOR.trim($this->oOptions['Program']['seo_name']):null;
    }

    public function createdBy($users=array(),$billingOsp){
        $html = (isset($users[intval($billingOsp['BillingOsp']['who_created'])])) ? '<strong>'.$users[intval($billingOsp['BillingOsp']['who_created'])].'</strong>':null;
        return $html;
    }
}