<?php

class ParticipantDirectoryHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html','Text');
    public $components = array('Session');
    public $oOptions = null;
    
	public function loadProgramsParticipantDirectoryByProgramId( $programId = null ){
		$oProgramParticipantDirectories = null;
		if( isset($programId) && intval($programId) > 0){
    		$this->ProgramsParticipantDirectory = &ClassRegistry::init('ProgramsParticipantDirectory');
			$this->ProgramsParticipantDirectory->contain();
			$oProgramParticipantDirectories = $this->ProgramsParticipantDirectory->findAllByProgramId($programId);
			if( isset($oProgramParticipantDirectories) && count($oProgramParticipantDirectories)>0 ){
				foreach( $oProgramParticipantDirectories as $key => $oProgramParticipantDirectory ){
					if( $oProgramParticipantDirectory['ProgramsParticipantDirectory']['status'] != Configure::read('status_live') ){
						unset($oProgramParticipantDirectories[$key]);
					}		
				}		
			}
		}
		return $oProgramParticipantDirectories;
	}

    public function getTotalAttendingCompanies(){
        if( intval($this->oOptions['ProgramsParticipantDirectory']['program_id']) >0 && count($this->oOptions['DirectoriesOfParticipants'])>0 ){
            $companyId = array();
            if(isset($this->oOptions['DirectoriesOfParticipants']) && count($this->oOptions['DirectoriesOfParticipants'])>0 ){
                foreach( $this->oOptions['DirectoriesOfParticipants'] as $participants ){
                    $companyId[] = $participants['company_id'];
                }
                $companyId = array_unique($companyId);
            }
            return count($companyId);
        }
        return 0;
    }

    public function getAttendanceCompanies(){
        $html = null;
        if( intval($this->oOptions['ProgramsParticipantDirectory']['program_id']) >0 && count($this->oOptions['DirectoriesOfParticipants'])>0 ){
            $companyId = array();
            if(isset($this->oOptions['DirectoriesOfParticipants']) && count($this->oOptions['DirectoriesOfParticipants'])>0 ){
                foreach( $this->oOptions['DirectoriesOfParticipants'] as $participants ){
                    $companyId[] = $participants['company_id'];
                }
                $companyId = array_unique($companyId);
            }

            $this->Company = &ClassRegistry::init('Company');
            $this->Company->contain();
			$aCompanies = $this->Company->find('all',array(
                'fields' => array('Company.id','Company.name','Company.seo_name'),
                'conditions' => array('Company.id IN ('.implode(",",$companyId).')'),
                'group' => array('Company.id'),
                'order' => array('Company.name')
            ));

            if( count($aCompanies) >0 && is_array($aCompanies) ){
                foreach($aCompanies as $company_key => $company ){
                    $link = $this->Html->url(array(
                	 	"controller" => "companies",
                	 	"admin"=>'true',
                	 	"action"=>"info" ,
                	 	intval($company['Company']['id']).'/'.trim($company['Company']['seo_name'])
                	));
                    $html .= '<a href="'.$link.'">'.$this->Text->truncate($company['Company']['name'],44,array(
                    'ending' => '...',
                    'exact' => false
                    )).'</a><br/>';
                }
            }
        }
        return $html;
    }

    public function getTotalAttendanceParticipants(){
        return count($this->oOptions['DirectoriesOfParticipants']);
    }

    public function setDirectoryOfParticipantData($options = array()) {
		if( $options && is_array($options) ){
            $this->oOptions = $options;
    	}
    }
    
    public function getProgramDirectoryOfParticipantDirectory($programId=0){
    	$oDirectoryProperty = new DirectoryProperty();
        $oDirectoryProperty->documentDirectory = Configure::read('docs_directory');
        $oDocumentDirectory = new DocumentDirectory( $oDirectoryProperty, $programId );
        return $oDocumentDirectory->getParticipantsDirectory();	
    }
    
    public function directoryOfParticipantsExist($programId=0,$sourceFile = null){
        if(file_exists($sourceFile)){
        	return true;
        }else{
        	return false;
        }
    }
    
    public function getProgramTitle(){
    	return wordwrap(trim($this->oOptions['Program']['title']),40,'<br/>');	
    }
    
    public function getDireectoryOfParticipantId(){
 		return intval($this->oOptions['ProgramsParticipantDirectory']['id']);   	
    }
    
	public function getSeoUri(){
        return ( isset($this->oOptions['Program']['title']) ) ? intval($this->oOptions['Program']['id']).DIRECTORY_SEPARATOR.trim($this->oOptions['Program']['seo_name']):null;
    }

    public function createdBy($users=array()){
        $html = isset($users[$this->oOptions['ProgramsParticipantDirectory']['who_created']]) ?'Created By:&nbsp;<strong>'.$users[$this->oOptions['ProgramsParticipantDirectory']['who_created']].'</strong>':null;
        if( intval($this->oOptions['ProgramsParticipantDirectory']['who_modified']) > 0 && !is_null($this->oOptions['ProgramsParticipantDirectory']['who_modified']) ){
            $html .='<br/>Modified By:&nbsp;<strong>'.$users[intval($this->oOptions['ProgramsParticipantDirectory']['who_modified'])].'</strong>';
        }
        return $html;
    }
}