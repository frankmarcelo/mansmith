<?php

class SalesReportsHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html');
    public $components = array('Session');
    public $oOptions = null;
    
    public function loadSalesReportsById( $salesreportsId = null ){
		$oSalesReports = null;
		if( isset($salesreportsId) && intval($salesreportsId) > 0){
    		$this->SalesReports = &ClassRegistry::init('SalesReports');
			$this->SalesReports->contain();
			$oSalesReports = $this->SalesReports->findAllById($salesreportsId);
		}
        return $oSalesReports;
	}

    public function setSalesReportsData($options = array()) {
		if( $options && is_array($options) ){
            $this->oOptions = $options;
    	}
    }

    public function getSalesReportsDirectory(){
        $oDirectoryProperty = new DirectoryProperty();
        $oDirectoryProperty->documentDirectory = Configure::read('docs_directory');
        $oDocumentDirectory = new DocumentDirectory( $oDirectoryProperty, $programId=null);
        return $oDocumentDirectory->getSalesReportsDirectory(date("Y_m",strtotime($this->oOptions['SalesReports']['report_date'])));
    }
    
    public function salesReportsExist($sourceFile = null){
        if(file_exists($sourceFile)){
        	return true;
        }else{
        	return false;
        }
    }
    
    public function getSalesReportsId(){
 		return intval($this->oOptions['SalesReports']['id']);
    }
}