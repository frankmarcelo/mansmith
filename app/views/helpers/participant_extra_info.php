<?php

final class ParticipantExtraInfoHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html','Certificates');
    public $uses = array('Program','CertificatesParticipants');
    public $participant = array();
    
	public function setExtraInfo($options = array()) {
	   $this->participant = $options;
    }
    
    public function getParticipantsGrouping(){
    	$sHtmlUrl = null;
    	if( isset($this->participant['ParticipantsGrouping']['name']) ){
    		$seo_name  = '?q=&grouping='.$this->participant['ParticipantsGrouping']['seo_name'];
    		$sHtmlUrl .= '<a href="'.$this->Html->url(array('controller'=>'participants','action'=>'search',$seo_name));
			$sHtmlUrl .= '">'.ucwords(strtolower($this->participant['ParticipantsGrouping']['name'])).'</a>';
    	}
    	return (isset($this->participant['ParticipantsGrouping']['name'])) ? $sHtmlUrl:null; 
    }
    
    public function getParticipantsTitle(){
    	return (isset($this->participant['ParticipantsTitle']['name'])) ? trim($this->participant['ParticipantsTitle']['name']):null;
    }
    
    public function getParticipantsCompany(){
    	$sHtmlUrl = null;
		if( isset($this->participant['Company']) && sizeof($this->participant['Company']) > 0){
			$companyId = $this->participant['Company']['id'];
			$seo_name  = $this->participant['Company']['seo_name'];
			$company_name = $this->participant['Company']['name'];	
			$sHtmlUrl .= '<a href="'.$this->Html->url(array('controller'=>'companies','action'=>'info',$companyId.DIRECTORY_SEPARATOR.$seo_name));
			$sHtmlUrl .= '">'.ucwords(strtolower($company_name)).'</a>';	
		}else{
			$sHtmlUrl .= '&nbsp;';
		}
		return $sHtmlUrl;
	}
	
	public function getLatestAttendedProgram(){
    	$sHtmlUrl = null;
		if( isset($this->participant['ProgramsParticipant']) && sizeof($this->participant['ProgramsParticipant']) > 0){
			$programId =$this->participant['ProgramsParticipant'][0]['program_id'];
			$this->Program = &ClassRegistry::init('Program');
			$this->Program->recursive = -1;
			$oProgram = $this->Program->findById($programId);
			if( is_array($oProgram) && $oProgram['Program']['id'] > 0){
				$seo_name = $oProgram['Program']['seo_name'];
				$program_name = $oProgram['Program']['title'];	
				$sHtmlUrl .= '<a href="'.$this->Html->url(array('controller'=>'programs','action'=>'info',$programId.DIRECTORY_SEPARATOR.$seo_name));
				$sHtmlUrl .= '">'.ucwords(strtolower($program_name)).'</a>';	
			}
		}else{
			$sHtmlUrl .= '&nbsp;';
		}
		return $sHtmlUrl;
	}
	
	public function getAttendedProgram($limit=0){
    	$html = null;
    	if( isset($this->participant['ProgramsParticipant']) && sizeof($this->participant['ProgramsParticipant']) > 0){
			foreach( $this->participant['ProgramsParticipant'] as $ProgramInfo ){
				$programId = intval($this->participant['ProgramsParticipant'][0]['program_id']);
				$this->Program = &ClassRegistry::init('Program');
			
				if( $limit > 0 ){
					$programs = $this->Program->find('all',array(
							'conditions'=> array('Program.id IN ('.$ProgramInfo['program_id'].')' ),
							'group' => array('Program.id'),
							'order' => array('Program.title'),
							'limit' => $limit	
					));
				}else{
					$programs = $this->Program->find('all',array(
							'conditions'=> array('Program.id IN ('.$ProgramInfo['program_id'].')' ),
							'group' => array('Program.id'),
							'order' => array('Program.title'),
					));
				}
				if( is_array($programs) && count($programs) > 0){
					foreach( $programs as $programInfo ){
						$seo_name = $programInfo['Program']['seo_name'];
						$program_name = $programInfo['Program']['title'];	
						$html .= '<a href="'.$this->Html->url(array('controller'=>'programs','action'=>'info',$programInfo['Program']['id'].DIRECTORY_SEPARATOR.$seo_name));
						$html .= '">'.ucwords(strtolower($program_name)).'</a><br />';		
					}
				}
			}
			return $html;	
		}else{
			$html .= 'None';
		}
		return $html;
	}
	
	public function getParticipantCertificate(){
		$html = null;
		if( isset($this->participant['Participant']['id']) && intval($this->participant['Participant']['id']) > 0){
			$this->CertificatesParticipants = &ClassRegistry::init('CertificatesParticipants');
			$certificates = $this->CertificatesParticipants->find('all',array(
				'fields' => array(
					'ProgramsCertificate.id',
					'ProgramsCertificate.program_id',
					'ProgramsCertificate.source_file',
					'ProgramsCertificate.status',
					'CertificatesParticipants.participant_id'
				),
				'conditions'=> array('CertificatesParticipants.participant_id IN ('.intval($this->participant['Participant']['id']).')' ),
				'group' => array('ProgramsCertificate.program_id')
			));
			
			if( is_array($certificates) && count($certificates)>0 ){
				foreach( $certificates as $certificate_key => $certificate ){
					$certificate_directory = $this->Certificates->getCertificatesDirectory(intval($certificate['ProgramsCertificate']['program_id'])).trim($certificate['ProgramsCertificate']['source_file']);
					
					if(file_exists($certificate_directory)){
						$html .= '<a target="_blank" href="'.$this->Html->url(array(
        					'controller'=>'certificates',
        					'action'=>'download',
        					$certificate['ProgramsCertificate']['id']
        					)).'">'.trim($certificate['ProgramsCertificate']['source_file']).'</a><br/>';
        			}	
				}
			}
			
			if( is_null($html) ){
				$html .= 'None';	
			}
		}else{
			$html .= 'None';
		}
		return $html;
	}
	
	public function getPosition(){
		$sHtmlUrl = null;
		if( isset($this->participant['ParticipantsPosition']) && sizeof($this->participant['ParticipantsPosition']) > 0){
			$sPosition = strtolower($this->participant['ParticipantsPosition']['name']);
			$sPosition = ($sPosition=='none')? 'Unassigned': $sPosition;
			$sHtmlUrl .= ucwords($sPosition);	
		}else{
			$sHtmlUrl .= 'Unassigned';
		}
		return $sHtmlUrl;
	}

    public function createdBy($users=array()){
        $html = '&nbsp;Created By:&nbsp;<strong>'.$users[intval($this->participant['Participant']['who_created'])].'</strong>';
        if( isset($this->participant['Participant']['who_modified']) && intval($this->participant['Participant']['who_modified']) > 0 && !is_null($this->participant['Participant']['who_modified']) ){
            $html .='&nbsp;Modified By:&nbsp;<strong>'.$users[intval($this->participant['Participant']['who_modified'])].'</strong>';
        }
        return $html;
    }
}