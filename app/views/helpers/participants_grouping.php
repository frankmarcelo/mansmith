<?php

final class ParticipantsGroupingHelper extends AppHelper {
	
	public function getParticipantsGroupingById( $groupingId = null ){
    	$this->ParticipantsGrouping->recursive = -1;
    	$this->ParticipantsGrouping = &ClassRegistry::init('ParticipantsGrouping');
		$oParticipantsGrouping = $this->ParticipantsGrouping->findById($groupingId);
		if( !empty($oParticipantsGrouping) && $oParticipantsGrouping ){
			return $oParticipantsGrouping['ParticipantsGrouping']['name'];		
		}
		return null;
	}    
}