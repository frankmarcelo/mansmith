<?php

echo $this->Html->css(array('fcbkselection'));
echo $this->Html->script(array(
    'jquery/jquery.bgiframe.min',
    'jquery/jquery.ajaxQueue',
    'jquery/jquery.fcbkselection',
    'jquery/jquery.simplemodal',
    'jquery/jquery.tinymce',
    'tiny_mce/tiny_mce',
    'jquery/jquery.maskMoney',
    'jquery/development-bundle/ui/jquery.ui.core',
    'jquery/development-bundle/ui/jquery.ui.widget',
    'jquery/development-bundle/ui/jquery.ui.position',
    'jquery/development-bundle/ui/jquery.ui.autocomplete',
    'jquery/development-bundle/ui/jquery.ui.draggable',
    'jquery/development-bundle/ui/jquery.ui.resizable',
    'jquery/development-bundle/ui/jquery.ui.dialog',
    'jquery/development-bundle/ui/jquery.ui.datepicker'

    )
);

$this->Company->setCompanyData( $company['Company'] );
$this->CompanyExtraInfo->setExtraInfo( $company ); 
?>
<style type="text/css">
    .info {
        width: 80px;
        height: 50px;
    }
</style>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&region=PH"></script>
<script type="text/javascript">
/* <![CDATA[ */


var tips = $("#validateTips");

function setValue(a){
   var selectedId = $(a).find('input[type="hidden"]').attr('id');
   $('#'+selectedId).attr('value',$('#'+selectedId).attr('value')+'_selected');
}

function updateTips(t) {
   tips.text(t).effect("highlight",{},1500);
}

function checkLength(o,n,min,max,format) {
   if ( o.val().length > max || o.val().length < min ) {
	o.addClass('ui-state-error');
        if( format=='company'){
            $('#loader').fadeOut("fast").hide();
            $('#status_msgs').append("Length of " + n + " must be between "+min+" and "+max+".<br />");
            $('#create_new_company').dialog('close');
            o.focus();
        }else{
	    updateTips("Length of " + n + " must be between "+min+" and "+max+".");
        }
	return false;
   } else {
	return true;
   }
}

$(document).ready(function(){

   $("#name").autocomplete({
		source: "<?php echo Configure::read('js_directory');?>search_company.php",
		minLength: 3
   });
   		
   $("#primary_building, #secondary_bldg_name").autocomplete({
		source: "<?php echo Configure::read('js_directory');?>search_primary_bldg.php",
		minLength: 3
   });

   $("#primary_street, #secondary_street").autocomplete({
		source: "<?php echo Configure::read('js_directory');?>search_primary_address.php",
		minLength: 3
   });

   $(window).resize(function() {
      	$("#updatecompany").dialog("option", "position", "center");
   });


   $(window).scroll(function() {
       	$("#updatecompany").dialog("option", "position", "center");
   });

   $('textarea.tinymce').tinymce({
        script_url : '<?php echo Configure::read('js');?>tiny_mce/tiny_mce.js',
        theme : "advanced",
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,anchor,image,cleanup",                        
        theme_advanced_buttons3 : "",                        
        theme_advanced_buttons4 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true
   });

   $('#update_company').live("click",function(){
   		$("#updatecompany").dialog({
 	       bgiframe: true,
	       height: 140,
	       modal: true
	    });
    
        $('#status_msgs').empty();

		var companyValid = true;
		var company_name = $('#name');
	
		var primary_bldg = $('#primary_building');
		var primary_street = $('#primary_street');
		var primary_city = $('#primary_city');
		var primary_phone = $('#primary_phone');
		var primary_email = $('#primary_email');
		var primary_mobile = $('#primary_mobile');
		allFields = $([]).add(company_name).add(primary_bldg).add(primary_street).add(primary_city).add(primary_phone).add(primary_email).add(primary_mobile);
        allFields.removeClass('ui-state-error');

		companyValid = companyValid && checkLength(company_name,"Company name is too short",3,1000,'company');
		companyValid = companyValid && checkLength(primary_bldg,"Primary building name is too short",3,1000,'company');
		companyValid = companyValid && checkLength(primary_street,"Primary street is too short",3,1000,'company');
		companyValid = companyValid && checkLength(primary_city,"Primary city is too short",3,1000,'company');
		companyValid = companyValid && checkLength(primary_phone,"Primary phone is invalid",3,1000,'company');
		companyValid = companyValid && checkLength(primary_email,"Primary email is invalid",3,1000,'company');
		companyValid = companyValid && checkLength(primary_mobile,"Primary mobile is invalid",3,1000,'company');

		if( companyValid ){
		   $("#update_company").empty().fadeIn("slow",function(){
               $('#loader').show();
               $("#update_company").attr('value',"Please Wait....");
               $(this).attr('disabled',"disabled");
               $.ajax({
               	   cache: false  ,
                   type:  "POST" ,
                   url:   "<?php echo Configure::read('js_directory');?>ajax_update_company.php",
                   data:  $("#CompanyAdminUpdateForm :input").serializeArray(),
                   dataType: "json" ,
                   success: function(data){
                   	  var response = eval(data);
                      $(this).dialog('close');
                      $('#loader').append('<br />You are now being redirected to info page.');
                      window.location = (response.data.url);
                   }
               });
           });
       }
	   $("#update_company").removeAttr('disabled');
	   $("#update_company").attr('value',"Update Company");
       $("#update_company").attr('enabled',"enabled");
       $(this).dialog('close');    
   });

     invokeMap();//initialise map
});

function invokeMap() {

	var myLatlng = new google.maps.LatLng(<?php echo Configure::read('latitude').','.Configure::read('longitude');?> );
    var myOptions = {
      zoom: 14,
      center: myLatlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      navigationControl: true,
      mapTypeControl: true,
      disableDefaultUI: true,
      navigationControl: true,
      mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
        position: google.maps.ControlPosition.TOP_LEFT
      },
      streetViewControl: false
    }
    var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        draggable:true
    });
    map.setCenter(myLatlng);

    google.maps.event.addListener(marker, 'dragend', function() {
    	var location = marker.getPosition();
        document.getElementById('latitude').value = location.lat();
        document.getElementById('longitude').value = location.lng();
        $("#longitude").after('<div id="map_msg_status"></div>');

        $.post("<?php echo Configure::read('js_directory');?>save_company_location.php?timer="+Math.random(), { companyId: '<?php echo $this->Company->get('id');?>', lat:location.lat() , lon:location.lng() });
        
		$('#map_msg_status').fadeIn("slow",function(){
           $(this).empty().html("New saved location...");
        });

        this.timer = setTimeout(function (){
           $("#map_msg_status").fadeOut("fast",function(){
              $(this).remove();
           });
        },1000);
    });

    var geocoder = new google.maps.Geocoder();
    $("#primary_city").autocomplete({
    	source: function(request, response) {

 	       if (geocoder == null){
 	       		geocoder = new google.maps.Geocoder();
 	       }
 	       
           geocoder.geocode( {'address': request.term}, function(results, status) {
            		if (status == google.maps.GeocoderStatus.OK) {
 					var searchLoc = results[0].geometry.location;
 	           		var lat = results[0].geometry.location.lat();
 	              	var lng = results[0].geometry.location.lng();
 	              	var latlng = new google.maps.LatLng(lat, lng);
 	              	var bounds = results[0].geometry.bounds;

               		geocoder.geocode({'latLng': latlng}, function(results1, status1) {
 	                	if (status1 == google.maps.GeocoderStatus.OK) {
 	                		if (results1[1]) {
 	                    		response($.map(results1, function(loc) {
 		                    		return {
 		                        		label  : loc.formatted_address,
 		                        		value  : loc.formatted_address,
 		                        		bounds : loc.geometry.bounds,
 		                        		lat    : lat,
 		                        		lng    : lng
 		                      		}
 	                    		}));
 	                    	}
 	                	}
             		});//end of geocoder.geocode
         		}
           });
        },
        select: function(event,ui){
   			var pos = ui.item.position;
   			var lct = ui.item.locType;
   			var bounds = ui.item.bounds;
 			if (bounds){
 				map.fitBounds(bounds);
 				var newlatLng = new google.maps.LatLng(ui.item.lat,ui.item.lng);
 				map.setCenter(newlatLng);
 		       	document.getElementById('latitude').value = ui.item.lat;
 		        document.getElementById('longitude').value = ui.item.lng;
   			}
        }
    });
}
/* ]]> */
</script>
<style type="text/css">
  #map_canvas { height: 600px;width: 400px; }
</style>
<?php 
$this->Html->addCrumb('Companies', '/admin/companies');
$this->Html->addCrumb('<strong>Update Company Information for '.$this->Company->getCompanyName().'</strong>');
?>
<div class="users form">
    <h2><?php __('Update Company Information for <a href="'.$this->Html->url(array("admin"=> true,'controller'=>'companies','action'=>'info',$this->Company->getSeoUri())).'" target="_blank">'.$this->Company->getCompanyName()); ?></a></h2>
    <form id="CompanyAdminUpdateForm" method="post" accept-charset="utf-8" onSubmit="return false;">
        <input type="hidden" name="_method" value="POST" />
        <input type="hidden" name="data[Company][who_modified]" value="<?php echo $this->Session->read('Auth.User.id');?>" />
        <input type="hidden" name="data[Company][id]" value="<?php echo $this->Company->get('id');?>" />
    	<table style="margin-bottom: 7px;" width="698" cellpadding="4" cellspacing="1">
        	<tbody style="background-color: rgb(240, 240, 225);">
            	<tr><th class="th-new th-left" colspan="3">Fill in the required fields <span style="color: rgb(220, 20, 60);"><b>*</b></span></th></tr>
            	<tr class="control-td-row" bgcolor="#E6E6CC">
                	<td>
                    	<table width="100%" cellpadding="0" cellspacing="0">
                    		<tr><th class="th-new th-left" colspan="3">Company Profile</th></tr>
                        	<tr><td>
                        		<table width="100%" cellpadding="0" cellspacing="0">
                    				<tr class="striped"><td width="25%" align="right"><label for="name">Company Name:</label></td><td width="75%" align="left"><input type="text" id="name" name="data[Company][name]" value="<?php echo $this->Company->getCompanyName();?>" maxlength="60" style="width:450px;" class="text ui-widget-content ui-corner-all"  />&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span></td></tr>
                        			<tr class="striped"><td width="25%" align="right"><label for="affiliation">Company Affiliation:</label></td><td width="75%" align="left">
                        			<select id="affiliation" name="data[Company][companies_affiliation_id]" style="width:250px;" class="text ui-widget-content ui-corner-all">
                        			<?php 
                        			if( is_array($company_affiliations) && sizeof($company_affiliations)>0 ){
                        				foreach( $company_affiliations as $affiliation_key => $affiliation_name ){
                        					if( (strstr(strtolower($affiliation_name),'none') && !isset($company['CompaniesAffiliation']['id'])) || $affiliation_key == $company['CompaniesAffiliation']['id'] ){
                        						echo '<option value="'.$affiliation_key.'" selected="selected">'.$affiliation_name.'</option>';
                        					}else{
												echo '<option value="'.$affiliation_key.'">'.$affiliation_name.'</option>';
                        					}
                        				}
                        			}
                        			?>
                        			</select>&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span>
                        			</td></tr>
                        			<tr class="striped"><td width="25%" align="right"><label for="industry">Company Industry Type:</label></td><td width="75%" align="left">
                        			<select id="industry" name="data[Company][companies_industry_id]" style="width:250px;" class="text ui-widget-content ui-corner-all">
                        			<?php 
                        			if( is_array($company_industry) && sizeof($company_industry)>0 ){
                        				foreach( $company_industry as $industry_key => $industry_name ){
                        					if( (strstr(strtolower($industry_name),'none') && !isset($company['CompaniesIndustry']['id'])) || $industry_key == $company['CompaniesIndustry']['id']  ){
                        						echo '<option value="'.$industry_key.'" selected="selected">'.ucwords(strtolower($industry_name)).'</option>';
                        					}else{
												echo '<option value="'.$industry_key.'">'.ucwords(strtolower($industry_name)).'</option>';
                        					}
                        				}
                        			}
                        			?>
                        			</select>&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span>
                        			</td></tr>
                        			<tr class="striped"><td colspan="2"><label for="notes">Notes:</label>&nbsp;<textarea id="notes" name="data[Company][notes]" rows="15" cols="40" style="width: 100%" class="tinymce"><?php echo $this->Company->getNotes();?></textarea></td></tr>
                    			</table>
                        	</td>
                        	</tr>
                        	<tr><th class="th-new th-left" colspan="3">Primary Information</th></tr>
                        	<tr><td>
                        		<table width="100%" cellpadding="0" cellspacing="0">
                    				<tr><td>
                    					<table width="100%" cellpadding="0" cellspacing="0">
                    						<tr valign="top"><td>
                    							<table width="100%" valignt="top" height="100%" style="border:0px;" cellpadding="0" cellspacing="0">
                    								<tr class="striped"><td>
                    									<label for="primary_building">Building Name:</label>&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span><br /><input type="text" id="primary_building" name="data[Company][primary_bldg_name]" maxlength="60" style="width:400px;" class="text ui-widget-content ui-corner-all" value="<?php echo $this->Company->get('primary_bldg_name');?>" /><br />
                    									<label for="primary_street">Address:</label>&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span><br /><input type="text" id="primary_street" name="data[Company][primary_street]" maxlength="60" style="width:400px;" class="text ui-widget-content ui-corner-all" value="<?php echo $this->Company->get('primary_street');?>" /><br />
                    									<label for="primary_street">Suburb:</label>&nbsp;<br /><input type="text" id="primary_suburb" name="data[Company][primary_suburb]" maxlength="60" style="width:400px;" class="text ui-widget-content ui-corner-all" value="<?php echo $this->Company->get('primary_suburb');?>" /><br />
                    									<label for="primary_city">City:</label>&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span><br /><input type="text" id="primary_city" name="data[Company][primary_city]" maxlength="60" style="width:400px;" class="text ui-widget-content ui-corner-all" value="<?php echo $this->Company->get('primary_city');?>" /><br />
                    									<label for="primary_zip_code">Zip Code:</label>&nbsp;<br /><input type="text" id="primary_zip_code" name="data[Company][primary_zip_code]" maxlength="60" style="width:400px;" class="text ui-widget-content ui-corner-all" value="<?php echo $this->Company->get('primary_zip_code');?>" /><br />
                    									<label for="primary_phone">Phone:</label>&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span><br /><input type="text" id="primary_phone" name="data[Company][primary_phone]" maxlength="60" style="width:400px;" class="text ui-widget-content ui-corner-all" value="<?php echo $this->Company->get('primary_phone');?>" /><br />
                    									<label for="primary_fax">Fax:</label>&nbsp;<br /><input type="text" id="primary_fax" name="data[Company][primary_fax]" maxlength="60" style="width:400px;" class="text ui-widget-content ui-corner-all" value="<?php echo $this->Company->get('primary_fax');?>" /><br />
                    									<label for="primary_email">Email:</label>&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span><br /><input type="text" id="primary_email" name="data[Company][primary_email]" maxlength="60" style="width:400px;" class="text ui-widget-content ui-corner-all" value="<?php echo $this->Company->getEmail();?>" /><br />
                    									<label for="primary_website">Website:</label>&nbsp;<br /><input type="text" id="primary_website" name="data[Company][primary_website]" maxlength="60" style="width:400px;" class="text ui-widget-content ui-corner-all" value="<?php echo $this->Company->get('primary_website');?>" /><br />
                    									<label for="primary_mobile">Mobile:</label>&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span><br /><input type="text" id="primary_mobile" name="data[Company][primary_mobile]" maxlength="60" style="width:400px;" class="text ui-widget-content ui-corner-all" value="<?php echo $this->Company->getMobile();?>" /><br />
                    								</td></tr>
                    							</table>
                    						</td>
                    						<td valign="top"><div id="map_canvas"></div>
                  								<input type="hidden" id="latitude" name="data[Company][latitude]" value="<?php echo $this->Company->getLatitude();?>" />
                  								<input type="hidden" id="longitude" name="data[Company][longitude]" value="<?php echo $this->Company->getLongitude();?>" />
                							</td>
                    						</tr>
                    					</table>
                        			</td></tr>
                        		</table>
                        		</td>	
                        	</tr>
                        	<tr><th class="th-new th-left" colspan="3">Secondary Information</th></tr>
                        	<tr><td>
                        		<table width="100%" cellpadding="0" cellspacing="0">
                        		 	<tr class="striped"><td width="25%" align="right"><label for="secondary_building">Building Name:</label></td><td width="75%" align="left"><input type="text" id="secondary_bldg_name" name="data[Company][secondary_bldg_name]" maxlength="60" style="width:400px;" class="text ui-widget-content ui-corner-all" value="<?php echo $this->Company->get('secondary_bldg_name');?>" /></td></tr>
                    				<tr class="striped"><td width="25%" align="right"><label for="secondary_street">Address:</label></td><td width="75%" align="left"><input type="text" id="secondary_street" name="data[Company][secondary_street]" maxlength="60" style="width:400px;" class="text ui-widget-content ui-corner-all" value="<?php echo $this->Company->get('secondary_street');?>" /></td></tr>
                    				<tr class="striped"><td width="25%" align="right"><label for="secondary_street">Suburb:</label></td><td width="75%" align="left"><input type="text" id="secondary_suburb" name="data[Company][secondary_suburb]" maxlength="60" style="width:400px;" class="text ui-widget-content ui-corner-all" value="<?php echo $this->Company->get('secondary_suburb');?>" /></td></tr>
                    				<tr class="striped"><td width="25%" align="right"><label for="secondary_city">City:</label></td><td width="75%" align="left"><input type="text" id="secondary_city" name="data[Company][secondary_city]" maxlength="60" style="width:400px;" class="text ui-widget-content ui-corner-all" value="<?php echo $this->Company->get('secondary_city');?>" /></td></tr>
                    				<tr class="striped"><td width="25%" align="right"><label for="secondary_zip_code">Zip Code:</label></td><td width="75%" align="left"><input type="text" id="secondary_zip_code" name="data[Company][secondary_zip_code]" maxlength="60" style="width:400px;" class="text ui-widget-content ui-corner-all" value="<?php echo $this->Company->get('secondary_zip_code');?>" /></td></tr>
                    				<tr class="striped"><td width="25%" align="right"><label for="secondary_phone">Phone:</label></td><td width="75%" align="left"><input type="text" id="secondary_phone" name="data[Company][secondary_phone]" maxlength="60" style="width:400px;" class="text ui-widget-content ui-corner-all" value="<?php echo $this->Company->get('secondary_phone');?>"  /></td></tr>
                        		</table>
                    		</td></tr>
                        </table>
                	</td>
            	</tr>
            	<tr class="control-td-row" bgcolor="#E6E6CC">
					<td colspan="3" align="right">
        				<input name="data[Company][reset]" id="reset_company" value="Cancel" type="reset" class="ui-button ui-state-default ui-corner-all">
						<input name="data[Company][update]" id="update_company" value="Update Company" type="submit" class="ui-button ui-state-default ui-corner-all">
					</td>
				</tr>
	    	</tbody>
		</table>
	</form>
</div>
<div style="display:none;" id="updatecompany" title="Update <?php echo $this->Company->getCompanyName();?>"><div id="loader">Please wait while processing the request<br /><img src="<?php echo $this->Html->url('/img/ajaxloading.gif');?>" alt="loading..." /></div><div id="status_msgs" /></div></div>