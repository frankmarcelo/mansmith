<style type="text/css">
table tr td {
    border-bottom: 1px solid #DFDFDF !important;
    vertical-align: middle !important;
}

.tblClass{
    width: 100% !important;
}

.listing {
    width: 100% !important;
}
</style>
<div class="users index">
	<table cellspacing="0" cellpadding="0" class="tblClass">
        <?php
        if(!empty($participants) && sizeof($participants) > 0){
        ?>
        <tr><td align="center"><div class="paging">
            <?php
            $paginator->options(array('url' => $this->passedArgs),array('model'=> 'Participant'));
            $sPaginator = $paginator->first('<< First',array('model'=> 'Participant')).' '.
            $paginator->prev('< Previous',array('model'=> 'Participant')).' '.
            $paginator->numbers(array('model'=> 'Participant')).' '.
            $paginator->next('Next >',array('model'=> 'Participant')).' '.
            $paginator->last('Last >>',array('model'=> 'Participant'));
            echo $sPaginator;
            echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true),array('model'=> 'Participant')));?>
            </div>
        </td></tr>
        <?php
        }
        ?>
       
        <tr><td>
        <?php

        $i = 0;
        foreach( $participants as $participant_key => $participant_info ){
            $this->Participant->setParticipantData( $participant_info['Participant'],$participant_info['ParticipantsTitle']['name']);
            $this->ParticipantExtraInfo->setExtraInfo( $participant_info);
            $sCss = ( ($i % 2) ==0 )? 'listing_accomm':'listing_tr';
        ?>
        <div class="listing" id="<?php echo $sCss;?>">
            <div class="box1" id="all_participants_<?php echo $this->Participant->get('id');?>">
                <div><h3><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'participants','action'=>'info',$this->Participant->getSeoUri()));?>"><?php echo $this->Participant->getFullName();?></a></h3></div>
            </div><!--end box1-->
            <div class="box2"><?php echo $this->ParticipantExtraInfo->getPosition();?></div>
            <div class="box3"><?php echo $this->ParticipantExtraInfo->getParticipantsGrouping();?></div>
            <div class="box8"><?php echo $this->Participant->getPhone();?><br/><?php echo $this->Participant->getMobile();?><br/><?php echo $this->Participant->getFax();?></div>
            <div class="box4"><?php if($this->Participant->getEmail()){?><span><strong>Email:</strong>&nbsp;<a href="mailto:<?php echo $this->Participant->getEmail();?>"><?php echo $this->Participant->getEmail();?></a></span><?php if($this->Participant->getFullAddress()){?><span><strong>Address:</strong>&nbsp;<?php echo $this->Participant->getFullAddress();?></span><br /><?php } ?><br /><?php } ?></div><!--end box4-->
            <div class="box4">
                <a class="edit" target="_blank" href="<?php echo $this->Html->url(array('controller'=>'participants','action'=>'info',$this->Participant->getSeoUri()));?>">View Full Details</a>
                <?php
                echo $this->Participant->getDisplayEdit($target_blank=true);
                echo $this->ParticipantExtraInfo->createdBy($users);
                ?>
            </div><!--end box4-->
            <div class="clearboth"></div>
        </div><!--end listing-->
        <?php
            $i++;
        }
        ?>
        </td></tr>
    </table>
</div>