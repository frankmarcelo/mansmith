<style type="text/css">
table tr td {
    border-bottom: 1px solid #DFDFDF !important;
    vertical-align: middle !important;
}

.tblClass{
    width: 910px !important;
}


.listing {
    width: 910px !important;
}

.listing .box1 {
    float: left !important;
    padding: 0 0 0 6px !important;
    width:200px !important;
}
.listing .box2 {
    float: left !important;
    text-align: left !important;
    width: 150px !important;
}

.listing .box3 {
    float: left !important;
    text-align: left !important;
    width: 145px !important;
}

.listing .box5 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 130px !important;
}

.listing .box6 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 128px !important;
}

.listing .box7 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 107px !important;
}
</style>
<div class="users index">
    <table cellspacing="0" cellpadding="0" class="tblClass">
    	<tr><td align="center"><div class="paging">
            <?php
            $paginator->options(array('url' => $this->passedArgs));
            $sPaginator = $paginator->first('<< First').' '.
            $paginator->prev('< Previous').' '.
            $paginator->numbers().' '.
            $paginator->next('Next >').' '.
            $paginator->last('Last >>');
            echo $sPaginator;
            echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));?>
            </div>
        </td></tr>
        <tr><td colspan="5">
        <?php

        $i = 0;
        if( isset($billing_eb) && is_array($billing_eb) ){
            foreach( $billing_eb as $billing_eb_key => $billing_eb_info ){
                $billingEbFileInfo = $this->BillingEb->loadBillingEbByProgramId($billing_eb_info['Program']['id'],$company_id);
                $oProgram = $this->Program->loadProgramDataById($billing_eb_info['Program']['id'],$recursive=false);
                $this->Program->setProgramData($billing_eb_info['Program']);
                $this->ProgramExtraInfo->setExtraInfo($oProgram);
                $this->BillingEb->setBillingEbData($billing_eb_info);
                $sCss = ( ($i % 2) ==0 )? 'listing_accomm':'listing_tr';
        ?>
                <div class="listing" id="<?php echo $sCss;?>">
                    <div class="box1" id="all_programs_<?php echo $this->BillingEb->getBillingEbId();?>">
                        <div>
                            <h3><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'programs','action'=>'info',$this->BillingEb->getSeoUri()));?>"><?php echo $this->BillingEb->getProgramTitle();?></a></h3>
                            <div><schedule><?php echo $this->ProgramExtraInfo->getSchedule($shortcut=true);?></schedule><address><?php echo $billing_eb_info['BillingEb']['venue'];?></address></div>
                        </div>
                    </div><!--end box1-->
                    <div class="box3"><div>
                    <?php
                    if( $this->Session->read('Auth.User.role_id') == Configure::read('administrator')){
                    ?><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'billing_eb','action'=>'list',$this->ProgramExtraInfo->getProgramDivision()));?>">
                    <?php echo $this->ProgramExtraInfo->getProgramDivision();?></a><?php
                    }else{ echo $this->ProgramExtraInfo->getProgramDivision();}?> | <span class="tiny"><?php echo $this->ProgramExtraInfo->getProgramType();?></span></div><!--end box3--></div>
                    <div class="box5">
                       <table>
                            <tr>
                                <th>File</th>
                                <th>Recipient</th>
                                <th>Reference Code</th>
                                <th>Billing Date</th>
                                <th>Created By</th>
                            </tr>
                            <?php
                            if( count($billingEbFileInfo) > 0 ){

                            $billingEbDirectory = $this->BillingEb->getBillingEbDirectory(intval($billing_eb_info['Program']['id']));
                            foreach( $billingEbFileInfo as $key => $billingEbFile ){
                                if( intval($billing_eb_info['Program']['id']) >0 && isset($billingEbFile['BillingEb']['source_file']) ){
                                    $file = trim($billingEbDirectory.$billingEbFile['BillingEb']['source_file']);
                                    if( $this->BillingEb->billingEbExist($billing_eb_info['Program']['id'],$file)){
                            ?>
                           <tr>
                                <td><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'billing_eb','action'=>'download',intval($billingEbFile['BillingEb']['id'])));?>"><?php echo trim($billingEbFile['BillingEb']['source_file']);?></a></td>
                                <td><?php echo trim($billingEbFile['BillingEb']['recipient']);?></td>
                                <td><?php echo trim($billingEbFile['BillingEb']['billing_reference_code']);?></td>
                                <td><?php echo trim($billingEbFile['BillingEb']['billing_date']);?></td>
                                <td><?php echo $this->BillingEb->createdBy($users,$billingEbFile);?></td>
                            </tr>
                           <?php		}else{
                                            echo '&nbsp;';
                                        }
                                    }
                                }
                            }
                           ?>
                        </table>
                    </div>
                    <div class="box5"><div>

                                    </div></div>
                                    <div class="box6"><div align="left"><?php //echo $this->ParticipantDirectory->getAttendanceCompanies();?></div></div>
                                    <div class="box4">
                                       
                                	</div><!--end box4-->
                                    <div class="clearboth"></div>
                </div><!--end listing-->
            <?php
                    $i++;
                }//end of foreach
            }//end of programs attendance sheet
            ?>
        </td></tr>
    </table>
</div>