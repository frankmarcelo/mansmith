<?php echo $this->element('program/program_default_js'); ?>
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){
	$('#companies').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true});
	$(window).resize(function(){});
	$(window).scroll(function(){});
	$('.delete').live('click',function(){});
});
/* ]]> */
</script>
<?php 
echo $this->Html->css(array(
    'jquery.autocomplete',
    'fcbkselection'
    )
);
?>
<style type="text/css">
.listing .box1_companies {
    float: left !important;
    padding: 0 0 0 6px !important;
    width: 231px !important;
}
.listing .box2_companies {
    float: left !important;
    text-align: left !important;
    width: 131px !important;
}
.listing .box3 {
    float: left !important;
    text-align: left !important;
    width: 101px !important;
}
.listing .box3_companies {
    float: left !important;
    text-align: left !important;
    width: 167px !important;
}
.listing .box3a_companies {
    float: left !important;
    text-align: left !important;
    width: 227px !important;
}
</style>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&region=PH"></script>
<script type="text/javascript">
/* <![CDATA[ */
function invokeMap( element, x, y) {
	var myLatlng = new google.maps.LatLng(x,y);
    var myOptions = {
        zoom: 14,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        navigationControl: true,
        mapTypeControl: true,
        disableDefaultUI: true,
        navigationControl: true,
        mapTypeControlOptions: {
           style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
           position: google.maps.ControlPosition.TOP_LEFT
        },
        streetViewControl: false
    }
 
    var map = new google.maps.Map( document.getElementById(element) , myOptions);
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map
    });
    map.setCenter(myLatlng);
}

if(!String.prototype.startsWith){
    String.prototype.startsWith = function (str) {
        return !this.indexOf(str);
    }
}

$(document).ready(function(){
	
	$('.delete').live('click',function(){
		var title = '<strong><a target="_blank" href="'+$(this).attr('seouri')+'">'+$.trim($(this).attr('title'))+'</a></strong>';
		var dialog_message ='<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>';
		dialog_message +=title+' will be permanently deleted and cannot be recovered. Continue?</p><br />';
		dialog_message +='<p align="center"><button style="cursor:pointer;" class="ui-state-default ui-corner-all" type="button" id="cancel">Cancel</button>&nbsp;';
		dialog_message +='<button style="cursor:pointer;" seo="'+$(this).attr('seo')+'" seouri="'+$(this).attr('seouri')+'" class="ui-state-default ui-corner-all" style="font-color:#F0F0F0;" type="button" id="confirm_delete">Confirm delete</button>&nbsp;</p>';
		$("#confirm_msg").empty().html(dialog_message);
		$('#dialog').dialog({modal:true,hide:'explode'});
	});

    $("#confirm_delete").live("click",function(){
		$("#cancel").attr('disabled',true);
		$(this).empty().html("Deleting company. Please wait...").attr('disabled',true);
		var seo_uri = $.trim($(this).attr('seo'));
        $.ajax({
            cache: false  ,
            type:  "GET" ,
            url:   "<?php echo Configure::read('js_directory');?>delete_company.php?id="+seo_uri+'&who_modified=<?php echo $this->Session->read('Auth.User.id');?>',
            dataType: "json" ,
            success: function(data){
                $("#dialog").dialog('close');
                window.location.reload();
            }
        });
    });
	
	$("#cancel").live("click",function(){
		$("#dialog").dialog('close');
	});
	
	$("#q").focus();
	
	$(window).resize(function() {
	    $("#dialog").dialog("option", "position", "center");
	});
	
	$(window).scroll(function() {
	    $("#dialog").dialog("option", "position", "center");
	});
	
    $('.listing a').live('click',function(){
        var string_match = new String( $(this).attr('id') );

		var string_found = string_match.search(/map/i);
        var string_match_data = string_match.split('_');

        if( string_found > 0 ){
           if( !$(this).attr('show_div') ){
              $(this).attr('show_div','show_div');
              var geocodes = new String( $(this).attr('alt') ); 
              var xy = geocodes.split('_');

              if( string_match.startsWith('all') ){
                  $('#all_companies_map_canvas_'+parseInt(string_match_data[4],10)).fadeIn("fast", function(){
                      $(this).show();
                  });
                  invokeMap( 'all_companies_map_canvas_'+ parseInt(string_match_data[4]) , xy[0], xy[1] );        

              }
		   }else{
              $(this).removeAttr('show_div');

              if( string_match.startsWith('all') ){
			  	  $('#all_companies_map_canvas_'+parseInt(string_match_data[4],10)).fadeOut("slow", function(){
                  	$(this).hide();
                  });
              }
		   }
        }

        var employees_found = string_match.search(/employees/i);
        if( employees_found > 0 ){
            if( !$(this).attr('show_div') ){
            	$(this).attr('show_div','show_div');
  			  	if( string_match.startsWith('all') ){ 
  			  	   $('#all_company_active_employees_'+parseInt(string_match_data[4],10)).fadeIn("fast", function(){
                      $(this).show();
                   });
                }
 		   }else{
               $(this).removeAttr('show_div');
			   if( string_match.startsWith('all') ){
 			  	  $('#all_company_active_employees_'+parseInt(string_match_data[4],10)).fadeOut("slow", function(){
                   	$(this).hide();
                  });
               }
 		   }
        }
        
        var programs_found = string_match.search(/programs/i);
        if( programs_found > 0 ){
            if( !$(this).attr('show_div') ){
            	$(this).attr('show_div','show_div');
  			  	if( string_match.startsWith('all') ){ 
  			  	   $('#all_programs_attending_companies_'+parseInt(string_match_data[4],10)).fadeIn("fast", function(){
                      $(this).show();
                   });
                }
 		   }else{
               $(this).removeAttr('show_div');
			   if( string_match.startsWith('all') ){
 			  	  $('#all_programs_attending_companies_'+parseInt(string_match_data[4],10)).fadeOut("slow", function(){
                   	$(this).hide();
                  });
               }
 		   }
        }    	
    });
});
/* ]]> */
</script>
<?php 
$this->Html->addCrumb('<strong>All Companies</strong>');
?>
<div id="dialog" title="Delete Company" style="display:none;"><div id="confirm_msg">&nbsp;</div></div>
<div class="users index">
	<?php 
	if( $this->Session->read('Auth.User.role_id') == 1){	
		echo '<h2>All Companies</h2>';
	}else{
		echo '<h2>All '.$roles[$this->Session->read('Auth.User.role_id')].' Companies</h2>';
	}
	?>
	<?php echo $this->element('company/search_simple_company'); ?>
    <div>
       <div id="companies">
            <ul>
            	<li><a href="#list_companies"><span>Companies</span></a></li>	
            </ul>
            <div id="list_companies">

            	 	<table cellspacing="0" cellpadding="0" style="background-color: #FFFFFF;border: 1px solid #DDDDDD;clear: both;width: 100%;">
    					<?php 
    					if(!empty($companies) && sizeof($companies) > 0){
    					?>
    					<tr><td align="center" colspan="6">
    						<div class="paging">
		                 	<?php
		                 	$paginator->options(array('url' => $this->passedArgs));
				            $sPaginator = $paginator->first('<< First').' '.
				            $paginator->prev('< Previous').' '.
				            $paginator->numbers().' '.
				            $paginator->next('Next >').' '.
				            $paginator->last('Last >>').'<br>';
				            echo $sPaginator;
				            echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));
		                 	?>
	                 		</div>
	                 	</td></tr>
	                 	<?php 
    					}
	                 	?>
	                 	<tr>
	                 		<th style="border-right: 1px solid #DFDFDF;width:220px;"><?php echo $paginator->sort('Sort By Company Name','name');?></th>
	                 		<th style="border-right: 1px solid #DFDFDF;width:110px;"><?php echo $paginator->sort('Affiliation','companies_affiliation_id');?></th>
	                 		<th style="border-right: 1px solid #DFDFDF;"><?php echo $paginator->sort('Industry','companies_industry_id');?></th>
	                 		<th style="border-right: 1px solid #DFDFDF;">Latest Employee</th>
	                 		<th style="border-right: 1px solid #DFDFDF;">Last Program Attended</th>
	                 		<th style="border-right: 1px solid #DFDFDF;width:150px;">Total</th>
	                 	</tr>
	                	<tr><td colspan="6"> 
              	 		<?php 
    					
    					$i = 0;
	                   	foreach( $companies as $company_key => $company_info ){
	                   		
	                    	$this->Company->setCompanyData( $company_info['Company'] );
	                      	$this->CompanyExtraInfo->setExtraInfo( $company_info ); 
	                      	$sCss = ( ($i % 2) ==0 )? 'listing_accomm':'listing_tr';
                    	?>
                    	<div class="listing" id="<?php echo $sCss;?>">
							<div class="box1_companies" id="all_companies_<?php echo $this->Company->get('id');?>">
	                   			<div>
					     			<h3><a href="<?php echo $this->Html->url(array('controller'=>'companies','action'=>'info',$this->Company->getSeoUri()));?>"><?php echo $this->Company->getCompanyName();?></a></h3>
			              			<div>
			              			<?php if($this->Company->getEmail()){?><span><strong>Email:</strong>&nbsp;<a href="mailto:<?php echo $this->Company->getEmail();?>"><?php echo $this->Company->getEmail();?></a></span><br /><?php } ?>
			              			<?php if($this->Company->getPrimaryPhone()){?><span><strong>Phone:</strong>&nbsp;<?php echo $this->Company->getPrimaryPhone();?></span><br /><?php } ?>
			              			<?php if($this->Company->getMobile()){?><span><strong>Mobile:</strong>&nbsp;<?php echo $this->Company->getMobile();?></span><br /><?php } ?>
			              			<?php if($this->Company->getFax()){?><span><strong>Fax:</strong>&nbsp;<?php echo $this->Company->getFax();?></span><br /><?php } ?>
			              			<?php if($this->Company->getWebsite()){?><span><strong>Website:</strong>&nbsp;<a href="<?php echo $this->Company->getWebsite();?>" target="_blank"><?php echo $this->Company->getWebsite();?></a></span><br /><?php } ?>
			              			<?php if($this->Company->getNotes()){?><strong>Notes:</strong>&nbsp;<p><em><?php echo $this->Company->getNotes();?></em></p><?php } ?>
			              			</div>
			                     	<div></div>
	                   			</div>
		        			</div><!--end box1-->
		        			<div class="box2_companies"><?php echo $this->CompanyExtraInfo->getCompanyAffiliationType();?></div>
		        			<div class="box3"><?php echo $this->CompanyExtraInfo->getCompanyIndustryType();?></div>
		        			<div class="box3_companies"><?php echo $this->CompanyExtraInfo->getLatestParticipant();?></div>
		        			<div class="box3a_companies"><?php echo $this->CompanyExtraInfo->getLatestProgramAttended();?></div>
		        			<div class="box3b_companies"><?php echo $this->Company->getTotalEmployees().'<br />'.$this->Company->getTotalAttendedProgram();?></div>
		        			<div class="box4b"><span><strong>Primary Address:</strong>&nbsp;<address><?php echo $this->Company->getPrimaryaddress();?></address></span></div>
		        			<?php if( $this->Company->getSecondaryAddress() ){ 
		        			echo '<div class="box4b"><span><strong>Secondary Address:</strong>&nbsp;<address>'.$this->Company->getSecondaryAddress().'</address></span></div>';
		        			}?>
		        			<div class="box4">
		        				<?php
		        				if( $this->Company->getTotalEmployee() >0 ){
								?>
								<a class="edit" style="cursor:pointer;" id="all_company_view_employees_<?php echo $this->Company->get('id');?>">View Employees</a>
		        				<?php
		        				}
								?>
								<?php
		        				if( $this->Company->getTotalPrograms() >0 ){
								?>
								<a class="edit" style="cursor:pointer;" id="all_company_view_programs_<?php echo $this->Company->get('id');?>">View Attended Programs</a>
		        				<?php
		        				}
								?>
	                        	<a class="edit" style="cursor:pointer;" id="all_companies_view_map_<?php echo $this->Company->get('id');?>" alt="<?php echo $this->Company->getLatitude().'_'.$this->Company->getLongitude();?>">View Map</a>&nbsp;
	                        	<a class="edit_full_details" href="<?php echo $this->Html->url(array('controller'=>'companies','action'=>'info',$this->Company->getSeoUri()));?>">View Full Details</a>
	                        	<?php
                                echo $this->Company->getDisplayDelete();
								echo $this->Company->getDisplayEdit();
								echo $this->Company->getAddEmployee();
								echo $this->CompanyExtraInfo->createdBy($users);
                                ?>
	                        </div><!--end box4-->
	                        <div class="clearboth"></div>
	                		<div id="all_companies_map_canvas_<?php echo $this->Company->get('id');?>" style="display:none;" class="map_canvas"></div>
	                		<?php
	                		if( $this->Company->getTotalEmployee() >0 ){
	                		?>
	                		<div id="all_company_active_employees_<?php echo $this->Company->get('id');?>" style="display:none;">
                            	<?php echo $this->CompanyExtraInfo->getCompanyEmployees();?>
                            </div>
                            <?php
                            }
							?>
							<?php
							if( $this->Company->getTotalPrograms() >0 ){
							?>
	                		<div id="all_programs_attending_companies_<?php echo $this->Company->get('id');?>" style="display:none;">
                            	<?php echo $this->Company->loadCompanyProgramsById($company_info['ProgramsParticipant']);?>
                            </div>
                            <?php
                            }
							?>
		      			</div><!--end listing-->
                    	<?php 
                      		$i++;
                   		}  	
                 		?>
                 		</td></tr>
                  		<?php 
	                  	if(!empty($companies)){
	                  	?>
						<tr><td align="center" colspan="6" class="striped"><div class="paging">
	    	            <?php
	                 	$paginator->options(array('url' => $this->passedArgs));
			            $sPaginator = $paginator->first('<< First').' '.
			            $paginator->prev('< Previous').' '.
			            $paginator->numbers().' '.
			            $paginator->next('Next >').' '.
			            $paginator->last('Last >>');
			            echo $sPaginator.'<br>';
			            echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));
	                 	?>
	                 	</div></td></tr>
	                 	<?php 
	                  	}
	                 	?>
                 	</table>

            </div>
        </div> 
	</div>
</div>