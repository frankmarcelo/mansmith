<style type="text/css">
table tr td {
    border-bottom: 1px solid #DFDFDF !important;
    vertical-align: middle !important;
}

.tblClass{
    width: 910px !important;
}


.listing {
    width: 910px !important;
}

.listing .box1 {
    float: left !important;
    padding: 0 0 0 6px !important;
    width:200px !important;
}
.listing .box2 {
    float: left !important;
    text-align: left !important;
    width: 150px !important;
}

.listing .box3 {
    float: left !important;
    text-align: left !important;
    width: 145px !important;
}

.listing .box5 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 130px !important;
}

.listing .box6 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 128px !important;
}

.listing .box7 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 107px !important;
}
</style>
<div class="users index">
    <table cellspacing="0" cellpadding="0" class="tblClass">
    	<tr><td align="center"><div class="paging">
            <?php
            $paginator->options(array('url' => $this->passedArgs));
            $sPaginator = $paginator->first('<< First').' '.
            $paginator->prev('< Previous').' '.
            $paginator->numbers().' '.
            $paginator->next('Next >').' '.
            $paginator->last('Last >>');
            echo $sPaginator;
            echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));?>
            </div>
        </td></tr>
        <tr><td colspan="5">
        <?php

            $i = 0;
            if( isset($invoice) && is_array($invoice) ){

                foreach( $invoice as $invoice_key => $invoice_info ){
                        $invoiceFileInfo = $this->Invoice->loadInvoiceByProgramId($invoice_info['Program']['id'],$company_id);
                        $oProgram = $this->Program->loadProgramDataById($invoice_info['Program']['id'],$recursive=false);
                        $this->Program->setProgramData($invoice_info['Program']);
                        $this->ProgramExtraInfo->setExtraInfo($oProgram);
                        $this->Invoice->setInvoiceData($invoice_info);
                        $sCss = ( ($i % 2) ==0 )? 'listing_accomm':'listing_tr';

            ?>
                    <div class="listing" id="<?php echo $sCss;?>">
                        <div class="box1" id="all_programs_<?php echo $this->Invoice->getInvoiceId();?>">
                            <div>
                                <h3><a href="<?php echo $this->Html->url(array('controller'=>'programs','action'=>'info',$this->Invoice->getSeoUri()));?>"><?php echo $this->Invoice->getProgramTitle();?></a></h3>
                                <div><schedule><?php echo $this->ProgramExtraInfo->getSchedule($shortcut=true);?></schedule><address><?php echo $invoice_info['Invoice']['venue'];?></address></div>
                            </div>
                        </div><!--end box1-->
                        <div class="box2"><div></div></div>
                        <div class="box3"><div>
                        <?php
                        if( $this->Session->read('Auth.User.role_id') == Configure::read('administrator')){
                        ?><a href="<?php echo $this->Html->url(array('controller'=>'invoice','action'=>'list',$this->ProgramExtraInfo->getProgramDivision()));?>">
                        <?php echo $this->ProgramExtraInfo->getProgramDivision();?></a><?php
                        }else{ echo $this->ProgramExtraInfo->getProgramDivision();}?> | <span class="tiny"><?php echo $this->ProgramExtraInfo->getProgramType();?></span></div><!--end box3--></div>
                        <div class="box5">
                           <table>
                                <tr>
                                    <th>File</th>
                                    <th>Invoice No.</th>
                                    <th>Amount</th>
                                    <th>Company</th>
                                    <th>Created By</th>
                                </tr>
                                </tr>
                                <?php
                                if( count($invoiceFileInfo) > 0 ){
                                    foreach( $invoiceFileInfo as $key => $invoiceFile ){
                                        $invoiceDirectory = $this->Invoice->getInvoiceDirectory(intval($invoice_info['Program']['id'])).$this->Invoice->getInvoiceType($invoiceFile['InvoiceDetails']['billing_type_id']);
                                        if( intval($invoice_info['Invoice']['id']) >0 && isset($invoiceFile['InvoiceDetails']['source_file']) ){
                                            $file = trim($invoiceDirectory.$invoiceFile['InvoiceDetails']['source_file']);
                                            if( $this->Invoice->invoiceExist($file)){
                                ?>
                                <tr>
                                    <td><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'invoice','action'=>'download',intval($invoiceFile['InvoiceDetails']['id'])));?>"><?php echo trim($invoiceFile['InvoiceDetails']['source_file']);?></a></td>
                                    <td><?php echo trim($invoiceFile['InvoiceDetails']['invoice_number']);?></td>
                                    <td>PHP <?php echo number_format(($invoiceFile['InvoiceDetails']['total_amount_due']),2, '.',',');?></td>
                                    <td><a href="<?php echo $this->Html->url(array('controller'=>'companies','action'=>'info',$invoiceFile['Company']['id'].'/'.$invoiceFile['Company']['seo_name']));?>"><?php echo trim($invoiceFile['Company']['name']);?></a></td>
                                    <td><?php echo $this->Invoice->createdBy($users,$invoiceFile);?></td>
                                </tr>
                                <?php       }else{
                                                echo '&nbsp;';
                                            }
                                        }
                                    }
                                }
                                ?>
                            </table>
                        </div>
                        <div class="box6"><div align="left"><?php //echo $this->ParticipantDirectory->getAttendanceCompanies();?></div></div>
                        <div class="box4">&nbsp;</div><!--end box4-->
                        <div class="clearboth"></div>
                    </div><!--end listing-->
            <?php
                    $i++;
                }//end of foreach
            }//end of programs attendance sheet
            ?>
        </td></tr>
    </table>
</div>