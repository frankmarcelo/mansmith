<?php

echo $this->Html->css(array(
    'jquery.autocomplete',
    'fcbkselection'
    )
);

echo $this->Html->script(array(
    'jquery/jquery.bgiframe.min',
    'jquery/jquery.ajaxQueue',
    'jquery/jquery.autocomplete.min',
    'jquery/jquery.tinymce',
    'tiny_mce/tiny_mce',
    'jquery/development-bundle/ui/jquery.ui.core',
    'jquery/development-bundle/ui/jquery.ui.draggable',
    'jquery/development-bundle/ui/jquery.ui.resizable',
    'jquery/development-bundle/ui/jquery.ui.dialog',
    )
);

$this->Company->setCompanyData( $company['Company'] );
$this->CompanyExtraInfo->setExtraInfo( $company ); 
?>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&region=PH"></script>
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){
   $('#program_data').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true });
   $('#program_extra').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true });
   invokeMap();//initialise map
});


function invokeMap() {

    var myLatlng = new google.maps.LatLng(<?php echo $this->Company->getLatitude().','.$this->Company->getLongitude();?> );
    var myOptions = {
        zoom: 14,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        navigationControl: true,
        mapTypeControl: true,
        disableDefaultUI: true,
        navigationControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
            position: google.maps.ControlPosition.TOP_LEFT
        },
        streetViewControl: false
    };
    
    var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    var marker = new google.maps.Marker({
        position: myLatlng,
	map: map,
	draggable:false
    });
    map.setCenter(myLatlng);
}
/* ]]> */
</script>
<style type="text/css">
#map_canvas { height: 220px;width: 450px }

a{
    color: #333333;
}

</style>
<?php

/** breadcrumbs stuff**/
$this->Html->addCrumb('Companies', '/admin/companies');
$this->Html->addCrumb('Search Companies', '/admin/companies/search?q='.urlencode($this->Company->getCompanyName()));
$this->Html->addCrumb('<strong>'.$this->Company->getCompanyName().'</strong>');

?>
<div class="users form">
<h2><?php __('Company Information for '.ucwords($this->Company->getCompanyName()));echo $this->Company->getDisplayEdit();?></h2>
<div class="clearfix hasRightCol" id="contentCol">
   <div id="headerArea">
     <div class="profileHeaderMain" id="program_data">
        <ul>
            <li><a href="#program_info"><span>Overview</span></a></li>
        </ul>
         <div id="program_info">
            <table class="uiInfoTable">
                <tr>
                <td width="50%">
                    <table class="uiInfoTable mvm profileInfoTable">
                      <tbody>
                        <?php
                        if( $this->Company->get('total_employee') > 0 ){
                        ?>
                      	<tr class="striped">
                      	  <th class="label">Total Employees</th>
                          <td class="data"><?php echo $this->Company->get('total_employee');?></td>
                        </tr>
                        <?php
                        }
						?>
                        <tr class="striped">
                          <th class="label">Company</th>
                          <td class="data"><?php echo $this->Company->getCompanyName();?></td>
                        </tr>
                        <tr class="striped">
                          <th class="label">Affiliation</th>
                          <td class="data"><?php echo $this->CompanyExtraInfo->getCompanyAffiliationType();?></td>
                        </tr>
                        <tr class="striped">
                          <th class="label">Industry</th>
                          <td class="data"><?php echo $this->CompanyExtraInfo->getCompanyIndustryType();?></td>
                        </tr>
                        <tr class="striped">
                          <th class="label">Primary Address</th>
                          <td class="data"><?php echo $this->Company->getPrimaryaddress();?></td>
                        </tr>
                        <?php if($this->Company->getPrimaryPhone()){?>
                        <tr class="striped">
                          <th class="label">Phone</th>
                          <td class="data">
                             <div id="company"><?php echo $this->Company->getPrimaryPhone();?></div>
                          </td>
                        </tr>
                        <?php
                        }
						?>
                        <?php if( $this->Company->getSecondaryAddress() ){ ?>
                        <tr class="striped">
                          <th class="label">Secondary Address</th>
                          <td class="data"><?php echo $this->Company->getPrimaryaddress();?></td>
                        </tr>
                        <?php
                        }
						?>
						<?php if($this->Company->getEmail()){?>
                        <tr class="striped">
                          <th class="label">Email Address</th>
                          <td class="data"><a href="mailto:<?php echo $this->Company->getEmail();?>"><?php echo $this->Company->getEmail();?></a></td>
                        </tr>
                        <?php } ?>
			            <?php if($this->Company->getMobile()){?>
			            <tr class="striped">
                          <th class="label">Mobile</th>
                          <td class="data"><?php echo $this->Company->getMobile();?></td>
                        </tr>
                        <?php } ?>
			            <?php if($this->Company->getFax()){?>
			            <tr class="striped">
                          <th class="label">Fax</th>
                          <td class="data"><?php echo $this->Company->getFax();?></td>
                        </tr>
                        <?php } ?>
			            <?php if($this->Company->getWebsite()){?>
			            <tr class="striped">
                          <th class="label">Website</th>
                          <td class="data"><a href="<?php echo $this->Company->getWebsite();?>" target="_blank"><?php echo $this->Company->getWebsite();?></a></td>
                        </tr>
                        <?php } ?>
			            <?php if($this->Company->getNotes()){?>
			            <tr class="striped">
                          <th class="label">Notes</th>
                          <td class="data"><em><?php echo $this->Company->getNotes();?></em></td>
                        </tr>
                        <?php } ?>
			          </tbody>
                    </table>
                </td>
                <td width="50%"><div id="map_canvas"></div><input type="hidden" id="latitude" name="data[Company][latitude]" value="<?php echo $this->Company->getLatitude();?>" /><input type="hidden" id="longitude" name="data[Company][longitude]" value="<?php echo $this->Company->getLongitude();?>" /></td>
                </tr>
            </table>
        </div>
     </div>
            <div class="profileHeaderMain" id="program_extra">
                <ul>
                      <li><a href="#employees"><span>Employees</span></a></li>
                      <?php
                      if( $this->Company->get('total_attended') > 0 ){
                      ?>
                      <li><a href="#attended_programs"><span>Attended Programs</span></a></li>
                      <?php
                      }
                      if( $this->Company->get('total_eb') > 0 ){
                      ?>
                      <li><a href="#eb"><span>Billing Eb</span></a></li>
                      <?php
                      }
                      if( $this->Company->get('total_non_eb') > 0 ){
                      ?>
                      <li><a href="#non_eb"><span>Billing Non Eb</span></a></li>
                      <?php
                      }
                      if( $this->Company->get('total_osp') > 0 ){
                      ?>
                      <li><a href="#osp"><span>Billing Osp</span></a></li>
                      <?php
                      }
                      if( $this->Company->get('total_regular') > 0 ){
                      ?>
                      <li><a href="#regular"><span>Billing Regular</span></a></li>
                      <?php
                      }
                      if( $this->Company->get('total_invoices') > 0 ){
                      ?>
                      <li><a href="#invoices"><span>Invoices</span></a></li>
                      <?php
                      }
                      ?>
                </ul>
                <div id="employees"><?php
                echo $this->Company->getAddEmployee().'<br><br>';
                if( $this->Company->get('total_employee') > 0 ){
                    if( $this->Company->get('total_employee') < 3 ){
                        $height = 220 * $this->Company->get('total_employee');
                    }else{
                        if( $this->Company->get('total_employee') > 10 ){
                            $height = 1600;
                        }else{
                            $height = (220 * $this->Company->get('total_employee'))/1.55;
                        }
                    }
                    $height = '1050';
                    echo '<div><iframe src="'.$this->Html->url(array('controller'=>'companies','admin'=>true,'action'=>'employees', $this->Company->get('id').DIRECTORY_SEPARATOR.'1')).'" width="1050" height="'.$height.'" border="0"></iframe></div>';
                }
                ?>
                </div><!--end employees-->
                <?php
                if( $this->Company->get('total_attended') > 0 ){
                    
                    if( $this->Company->get('total_attended') < 3 ){
                        $height = 220 * $this->Company->get('total_attended');
                    }else{
                         if( $this->Company->get('total_attended') > 10 ){
                            $height = 1600;
                        }else{
                            $height = (220 * $this->Company->get('total_attended'))/1.55;
                        }
                    }
                    echo '<div id="attended_programs"><div><iframe src="'.$this->Html->url(array('controller'=>'companies','admin'=>true,'action'=>'attended_programs', $this->Company->get('id').DIRECTORY_SEPARATOR.'1')).'" width="950" height="'.$height.'" border="0"></iframe></div></div>';
                }
                if( $this->Company->get('total_eb') > 0 ){
                	$height = 0;
                    if( $this->Company->get('total_eb') < 3 ){
                        $height = 220 * $this->Company->get('total_eb');
                    }else{
                        $height = (220 * $this->Company->get('total_eb'))/1.55;
                    }
                    echo '<div id="eb" style="height:'.$height.'px;"><div><iframe src="'.$this->Html->url(array('controller'=>'companies','admin'=>true,'action'=>'billing_eb', $this->Company->get('id').DIRECTORY_SEPARATOR.'1')).'" width="950" height="'.$height.'" border="0"></iframe></div></div>';
                }
                if( $this->Company->get('total_non_eb') > 0 ){
                    if( $this->Company->get('total_non_eb') < 3 ){
                        $height = 220 * $this->Company->get('total_non_eb');
                    }else{
                        $height = (220 * $this->Company->get('total_non_eb'))/1.55;
                    }
                    
                    echo '<div id="non_eb"><div><iframe src="'.$this->Html->url(array('controller'=>'companies','admin'=>true,'action'=>'billing_non_eb', $this->Company->get('id').DIRECTORY_SEPARATOR.'1')).'" width="950" height="'.$height.'" border="0"></iframe></div></div>';
                }
                if( $this->Company->get('total_osp') > 0 ){
                    if( $this->Company->get('total_non_eb') < 3 ){
                        $height = 220 * $this->Company->get('total_osp');
                    }else{
                        $height = (220 * $this->Company->get('total_osp'))/1.55;
                    }

                    echo '<div id="osp"><div><iframe src="'.$this->Html->url(array('controller'=>'companies','admin'=>true,'action'=>'billing_osp', $this->Company->get('id').DIRECTORY_SEPARATOR.'1')).'" width="950" height="'.$height.'" border="0"></iframe></div></div>';
                }
                if( $this->Company->get('total_regular') > 0 ){
                    if( $this->Company->get('total_regular') < 3 ){
                        $height = 220 * $this->Company->get('total_regular');
                    }else{
                        $height = (220 * $this->Company->get('total_regular'))/1.55;
                    }

                    echo '<div id="regular"><div><iframe src="'.$this->Html->url(array('controller'=>'companies','admin'=>true,'action'=>'billing_regular', $this->Company->get('id').DIRECTORY_SEPARATOR.'1')).'" width="950" height="'.$height.'" border="0"></iframe></div></div>';
                }
                if( $this->Company->get('total_invoices') > 0 ){
                    if( $this->Company->get('total_invoices') < 3 ){
                        $height = 220 * $this->Company->get('total_invoices');
                    }else{
                        $height = (220 * $this->Company->get('total_invoices'))/1.55;
                    }
					echo '<div id="invoices"><div><iframe src="'.$this->Html->url(array('controller'=>'companies','admin'=>true,'action'=>'invoices', $this->Company->get('id').DIRECTORY_SEPARATOR.'1')).'" width="950" height="'.$height.'" border="0"></iframe></div></div>';
                }
                ?>
            </div>
        </div>
    </div>
</div>