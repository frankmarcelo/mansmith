<style type="text/css">
table tr td {
    border-bottom: 1px solid #DFDFDF !important;
    vertical-align: middle !important;
}

.tblClass{
    width: 910px !important;
}

.listing {
    width: 910px !important;
}

.listing .box1 {
    float: left !important;
    padding: 0 0 0 6px !important;
    width:250px !important;
}
.listing .box2 {
    float: left !important;
    text-align: left !important;
    width: 150px !important;
}

.listing .box3 {
    float: left !important;
    text-align: left !important;
    width: 145px !important;
}

.listing .box5 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 130px !important;
}

.listing .box6 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 128px !important;
}

.listing .box7 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 107px !important;
}
</style>
<div class="users index">
	<table cellspacing="0" cellpadding="0" class="tblClass">
		<tr><td align="center"><div class="paging">
            <?php
            $paginator->options(array('url' => $this->passedArgs));
            $sPaginator = $paginator->first('<< First').' '.
            $paginator->prev('< Previous').' '.
            $paginator->numbers().' '.
            $paginator->next('Next >').' '.
            $paginator->last('Last >>');
            echo $sPaginator;
            echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));?>
            </div>
        </td></tr>
        <tr><td>
            <?php
            $i = 0;
            foreach( $programs as $program_key => $program_info ){
                $this->Program->setProgramData( $program_info['Program'] );
                $this->ProgramExtraInfo->setExtraInfo( $program_info );
                $sCss = ( ($i % 2) ==0 )? 'listing_accomm':'listing_tr';
            ?>
              <div class="listing" id="<?php echo $sCss;?>">
                    <div class="box1" id="all_programs_<?php echo $this->Program->get('id');?>">
                        <div>
                            <h3><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'programs','action'=>'info',$this->Program->getSeoUri()));?>"><?php echo $this->Program->getTitle();?></a></h3>
                            <div><address><?php echo $this->Program->getFullAddress();?></address></div>
                        </div>
                    </div><!--end box1-->
                    <div class="box2"><div><?php echo $this->ProgramExtraInfo->getSchedule($shortcut=true);?></div></div>
                    <div class="box3"><div><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'programs','action'=>'list',$this->ProgramExtraInfo->getProgramDivision()));?>"><?php echo $this->ProgramExtraInfo->getProgramDivision();?></a> | <span class="tiny"><?php echo $this->ProgramExtraInfo->getProgramType();?></span></div><!--end box3--></div>
                    <div class="box5"><div><?php echo $this->ProgramExtraInfo->getProgramSpeakers($speakers);?></div></div>
                    <div class="box6"><div align="left"><?php echo $this->Program->getTotalProgramClientParticipants().'<br />'.$this->Program->getTotalProgramParticipants();?></div></div>
                    <div class="box7"><div align="left"><?php echo $this->Program->getDiscountApplies();?></div></div>
                    <div class="box8"><div align="left"><em><?php echo $this->Program->getNotes();?></em></div></div>
                    <div class="box4">
                        <a target="_blank" class="edit_full_details" href="<?php echo $this->Html->url(array('controller'=>'programs','action'=>'info',$this->Program->getSeoUri()));?>" target="_blank">View Full Details</a>
                        <?php echo $this->ProgramExtraInfo->createdBy($users);?>
                    </div><!--end box4-->
                    
              </div><!--end listing-->
            <?php
                $i++;
            }
            ?>
        </td></tr>
     </table>
</div>