<?php
$times = array();
$time = strtotime("05:00:00");
$times["05:00:00"] = date("g:i a",$time);

for($i = 1;$i < 76;$i++) {
	$time = strtotime("+ 15 minutes",$time);
	$key = date("H:i:s",$time);
	$times[$key] = date("g:i a",$time);
}

echo $this->Html->css(array('fcbkselection'));
echo $this->Html->script(array(
    'jquery/jquery.bgiframe.min',
    'jquery/jquery.ajaxQueue',
    'jquery/jquery.fcbkselection',
    'jquery/jquery.simplemodal',
//    'jquery/jquery.tinymce',
//    'tiny_mce/tiny_mce',
//    'jquery/jquery.maskMoney',
    'jquery/development-bundle/ui/jquery.ui.core',
    'jquery/development-bundle/ui/jquery.ui.widget',
    'jquery/development-bundle/ui/jquery.ui.position',
    'jquery/development-bundle/ui/jquery.ui.autocomplete',
    'jquery/development-bundle/ui/jquery.ui.draggable',
    'jquery/development-bundle/ui/jquery.ui.resizable',
    'jquery/development-bundle/ui/jquery.ui.dialog',
    'jquery/development-bundle/ui/jquery.ui.datepicker'

    )
);

$this->Program->setProgramData( $program['Program'] );
$this->ProgramExtraInfo->setExtraInfo( $program ); 
?>
<style type="text/css">
.info {
    width: 80px;
    height: 50px;
}
</style>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&region=PH"></script>
<script type="text/javascript">
/* <![CDATA[ */

var times = [<?php echo "'".implode("','",$times)."'";?>];
var tips = $("#validateTips");

function setValue(a){
   var selectedId = $(a).find('input[type="hidden"]').attr('id');
   $('#'+selectedId).attr('value',$('#'+selectedId).attr('value')+'_selected');
}

function updateTips(t) {
   tips.text(t).effect("highlight",{},1500);
}

function checkLength(o,n,min,max,format) {
   if ( o.val().length > max || o.val().length < min ) {
		o.addClass('ui-state-error');
        if( format=='program'){
            $('#loader').fadeOut("fast").hide();
            $('#status_msgs').append("Length of " + n + " must be between "+min+" and "+max+".<br />");
            $('#create_new_program').dialog('close');
            o.focus();
        }else{
	    	updateTips("Length of " + n + " must be between "+min+" and "+max+".");
        }
		return false;
   } else {
		return true;
   }
}

function setCutOffDate(){
   $('input[container*="calendar"]').each(function(){
       $(this).removeClass('hasDatepicker').datepicker({
          changeMonth: true,
          changeYear: true,
          showButtonPanel: true,
          firstDay: 1,
          dateFormat: 'm/d/yy',
          closeText: 'Close'
       });
   });
}

function strstr (haystack, needle, bool) {
    var pos = 0;

    haystack += '';
    pos = haystack.indexOf(needle);
    if (pos == -1) {
        return false;
    } else {
        if (bool) {
            return haystack.substr(0, pos);
        } else {
            return haystack.slice(pos);
        }
    }
}

$(document).ready(function(){
    
   $("#title").autocomplete({
		source: "<?php echo Configure::read('js_directory');?>search_title.php",
		minLength: 3
   });

   $("#venue_name").autocomplete({
		source: "<?php echo Configure::read('js_directory');?>search_venue.php",
		minLength: 3
   });

   $('li[id*="div_speaker_"]').live('click',function(){
       
        var li_id_attribute = $(this).attr('id');
        var input_id_attribute = li_id_attribute.replace('div_','');
        var li_class_attribute = $(this).attr('class');

       if( strstr($(this).find("input").val(), '_selected')){
           var new_input_value = $('#'+input_id_attribute).val();
           var new_checkbox_value = (new_input_value.replace('_selected',''));
           $('#'+input_id_attribute).val(new_checkbox_value);
           $(this).removeAttr('class');

       }else{
            var new_input_value = $('#'+input_id_attribute).val();
           $('#'+input_id_attribute).val(new_input_value+'_selected');
       }
   });

   $.fcbkListSelection("#fcbklist","500","50","4");//for speakers
   $('#all_day').click(function(){
       if( $(this).attr('checked') ){
           $('#startTime,#endTime').fadeOut("slow",function(){
               $(this).hide();
           });

       }else{
           $('#startTime,#endTime').fadeIn("slow",function(){
               $(this).show();
           });
       }
   });

   $('#startDate').datepicker({
       showButtonPanel: true,
       changeMonth: true,
  	   changeYear: true,
       firstDay: 1,
       dateFormat: 'm/d/yy',
       closeText: 'Close'
       //minDate: new Date(<?php echo date("Y");?>, parseInt(<?php echo date("n");?>,10) - 1, <?php echo date("d");?>)
   });

   $('#endDate').datepicker({
       showButtonPanel: true,
       changeMonth: true,
	   changeYear: true,
       firstDay: 1,
       dateFormat: 'm/d/yy',
       closeText: 'Close'
       //minDate: new Date(<?php echo date("Y");?>, parseInt(<?php echo date("n");?>,10) - 1, <?php echo date("d");?>)
   });

   $('input[id="cutOffDate_1"]').datepicker({
      changeMonth: true,
      changeYear: true,
      showButtonPanel: true,
      firstDay: 1,
      dateFormat: 'm/d/yy',
      closeText: 'Close'
   });

   $(window).resize(function() {
       $("#dialog").dialog("option", "position", "center");
       $("#updateprogram").dialog("option", "position", "center");
   });

   $(window).scroll(function() {
       $("#dialog").dialog("option", "position", "center");
       $("#updateprogram").dialog("option", "position", "center");
   });

   $("#startTime,#endTime").autocomplete({
	   autoFocus: true,
	   source: times
   });

   $('input[id*="rate_plan"]').live('focus',function(){
       //$(this).maskMoney();
   });
   /*
   $('textarea.tinymce').tinymce({
      script_url : '<?php echo Configure::read('js');?>tiny_mce/tiny_mce.js',
      theme : "advanced",
      theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
      theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,anchor,image,cleanup",
      theme_advanced_buttons3 : "",
      theme_advanced_buttons4 : "",
      theme_advanced_toolbar_location : "top",
      theme_advanced_toolbar_align : "left",
      theme_advanced_statusbar_location : "bottom",
      theme_advanced_resizing : true
   });


   $("#dialog").dialog({
	  bgiframe: true,
	  autoOpen: false,
	  hide:'explode',
	  height: 350,
	  modal: true,
	  buttons: {
  	     'Create an account': function() {
            $('.ui-dialog-buttonset span[class="ui-button-text"]:first').html('Please wait...');

            var bValid = true;
            var first_name = $('#speaker-first_name');
            var last_name = $('#speaker-last_name');
            var speaker_email = $('#speaker-email');
            allFields = $([]).add(first_name).add(last_name).add(speaker_email),
            allFields.removeClass('ui-state-error');

            var tmp_first_name = first_name.val();
            var tmp_last_name  = last_name.val();
            var tmp_contact_email = speaker_email.val();
            bValid = bValid && checkLength(first_name,"First Name",2,16,'speaker');
            bValid = bValid && checkLength(last_name,"Last Name",2,16,'speaker');
            bValid = bValid && checkLength(speaker_email,"Email",6,80,'speaker');
			if (bValid) {
                $.ajax({
                    cache: false  ,
                    type:  "POST" ,
                    url:   "<?php echo Configure::read('js_directory');?>ajax_create_speaker.php",
                    data:  $("#SpeakerAdminCreateForm :input").serializeArray(),
                    dataType: "json" ,
                    success: function(data){
                        var response = eval(data);
                        if( response.data.created == true){
                            var speaker_last_id = parseInt($("#fcbklist li").size(),10);
                            var new_speaker ='<li id="div_speaker_'+speaker_last_id+'" class="liselected" addedid="tester"><div class="fcbklist_item itemselected" style="height: 50px; width: 139px;"><strong>'+response.data.data.Speaker.first_name+'&nbsp;'+response.data.data.Speaker.last_name+'</strong>'+
                           '<span class="fcbkitem_text"><br />Email:&nbsp;<a href="mailto:'+response.data.data.Speaker.email+'">'+response.data.data.Speaker.email+'</a><br />'
                            +response.data.data.ProgramsDivision.title+'<input type="hidden" name="fcbklist_value[]" value="'+response.data.data.Speaker.id+'" checked="checked" /></div></li>';
                            $(new_speaker).appendTo('ul#fcbklist');
                        }else{
                            $('#validateTips').append('Error please try again.');
                        }
                    }
                });

                $('#speaker-first_name').val();
                $('#speaker-last_name').val();
                $('#speaker-email').val();
                $('#speaker-mobile').val();
                $('.ui-dialog-buttonset span[class="ui-button-text"]:first').html('Create an account');
                $(this).dialog('close');
                return false;
            }else{
                $('.ui-dialog-buttonset span[class="ui-button-text"]:first').html('Create an account');
            }
	     },
	     Cancel: function() {
	 	    $(this).dialog('close');
	     }
	  },
	  close: function() {
	     allFields.val('').removeClass('ui-state-error');
	  }
   });

   $('#add_speakers').live('click',function(){
      $('#dialog').dialog('open');
   });

   $("#add_rate_plan").click(function(){
	    var $totalRows = parseInt($('#rateplan_container tr[container="tableRow"]').size(),10);
	    var $new_record = $('#rateplan_container tr:nth-child(3)').clone();
	    $('#rateplan_container tr:last').after($new_record);
        var newId = $totalRows + 1;
        $('#rateplan_container tr:last input[type="checkbox"]').attr('id','activeplan_'+newId);
        $('#rateplan_container tr:last input[type="checkbox"]').removeAttr('readonly');
        $('#rateplan_container tr:last input[container="calendar"]').attr('id','cutOffDate_'+newId);
        $('#rateplan_container tr:last input[name="data[ProgramsRatePlan][eb_cost][]"]').attr('id','rate_plan_eb_'+newId);
        $('#rateplan_container tr:last input[name="data[ProgramsRatePlan][reg_cost][]"]').attr('id','rate_plan_regular_'+newId);
        $('#rateplan_container tr:last input[name="data[ProgramsRatePlan][osp_cost][]"]').attr('id','rate_plan_osp_'+newId);
        setCutOffDate();
   });
   */
   $('#update_program').live("click",function(){
       $("#updateprogram").dialog({
           bgiframe: true,
           height: 140,
           modal: true
       });
    
       $('#status_msgs').empty();

       var programbValid = true;
       var title = $('#title');
       var startDate = $('#startDate');
       var endDate = $('#endDate');

       var speakersItem = parseInt($('div[class="fcbklist_item itemselected"]').size(),10);
       var venue_name = $('#venue_name');
       var address = $('#address');
       var city = $('#city');

       var contact_name = $('#contact_name');
       var contact_email = $('#contact_email');
       var contact_mobile = $('#contact_phone');

       allFields = $([]).add(title).add(startDate).add(endDate).add(venue_name).add(address).add(city).add(contact_name).add(contact_email).add(contact_mobile),
       allFields.removeClass('ui-state-error');

       if( programbValid ){
           programbValid = ( speakersItem > 0 ) ? true: false;
           if( programbValid == false || typeof(speakersItem)=='undefined' ){
              $('#loader').fadeOut("fast").hide();
              $('#status_msgs').empty().append('Please select at least one speaker.<br />');
           }
       }

       programbValid = programbValid && checkLength(title,"Title",3,1000,'program');
       programbValid = programbValid && checkLength(startDate,"Start Date",3,16,'program');
       programbValid = programbValid && checkLength(endDate,"End Date",6,80,'program');
       programbValid = programbValid && checkLength(venue_name,"Venue is too short",1,1000,'program');
       programbValid = programbValid && checkLength(address,"Address is too short",1,1000,'program');
       programbValid = programbValid && checkLength(city,"City is too short",1,1000,'program');
       programbValid = programbValid && checkLength(contact_name,"Invalid contact name",1,1000,'program');
       programbValid = programbValid && checkLength(contact_email,"Invalid contact email",1,1000,'program');
       programbValid = programbValid && checkLength(contact_mobile,"Invalid contact phone",1,1000,'program');

       if( programbValid ){
          $("#update_program").empty().fadeIn("slow",function(){
             $('#loader').show();
             $(this).attr('value',"Wait....");
             $(this).attr('disabled',"disabled");
             $.ajax({
                cache: false  ,
                type:  "POST" ,
                url:   "<?php echo Configure::read('js_directory');?>ajax_update_program.php",
                data:  $("#ProgramAdminUpdateForm :input").serializeArray(),
                dataType: "json" ,
                success: function(data){
                    var response = eval(data);
                    $(this).dialog('close');
                    $('#loader').append('<br />You are now being redirected to info page.');
                    window.location = (response.data.url);
                }
             });
             $(this).attr('value',"");
             $(this).attr('disabled',"disabled");
          });
        }
        $(this).dialog('close');
     });
     invokeMap();//initialise map
});

function invokeMap() {
	var myLatlng = new google.maps.LatLng(<?php echo $this->Program->getLatitude().','.$this->Program->getLongitude();?> );
    var myOptions = {
      zoom: 14,
      center: myLatlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      navigationControl: true,
      mapTypeControl: true,
      disableDefaultUI: true,
      navigationControl: true,
      mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
        position: google.maps.ControlPosition.TOP_LEFT
      },
      streetViewControl: false
    }
    var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        draggable:true
    });
    map.setCenter(myLatlng);

    google.maps.event.addListener(marker, 'dragend', function() {
        var location = marker.getPosition();
        document.getElementById('latitude').value = location.lat();
        document.getElementById('longitude').value = location.lng();
        
        $.post("<?php echo Configure::read('js_directory');?>save_program_location.php?timer="+Math.random(), { programId: '<?php echo $this->Program->get('id');?>', lat:location.lat() , lon:location.lng() });
        
        $("#longitude").after('<div id="map_msg_status"></div>');

        $('#map_msg_status').fadeIn("slow",function(){
           $(this).empty().html("New saved location...");
        });

        this.timer = setTimeout(function (){
           $("#map_msg_status").fadeOut("fast",function(){
              $(this).remove();
           });
        },1000);
    });

    var geocoder = new google.maps.Geocoder();
    $("#city").autocomplete({
    	source: function(request, response) {

 	       if (geocoder == null){
 	       		geocoder = new google.maps.Geocoder();
 	       }
 	       
           geocoder.geocode( {'address': request.term}, function(results, status) {
            		if (status == google.maps.GeocoderStatus.OK) {
 					var searchLoc = results[0].geometry.location;
 	           		var lat = results[0].geometry.location.lat();
 	              	var lng = results[0].geometry.location.lng();
 	              	var latlng = new google.maps.LatLng(lat, lng);
 	              	var bounds = results[0].geometry.bounds;

               		geocoder.geocode({'latLng': latlng}, function(results1, status1) {
 	                	if (status1 == google.maps.GeocoderStatus.OK) {
 	                		if (results1[1]) {
 	                    		response($.map(results1, function(loc) {
 		                    		return {
 		                        		label  : loc.formatted_address,
 		                        		value  : loc.formatted_address,
 		                        		bounds : loc.geometry.bounds,
 		                        		lat    : lat,
 		                        		lng    : lng
 		                      		}
 	                    		}));
 	                    	}
 	                	}
             		});//end of geocoder.geocode
         		}
           });
        },
        select: function(event,ui){
   			var pos = ui.item.position;
   			var lct = ui.item.locType;
   			var bounds = ui.item.bounds;
 			if (bounds){
 				map.fitBounds(bounds);
 				var newlatLng = new google.maps.LatLng(ui.item.lat,ui.item.lng);
 				map.setCenter(newlatLng);
 		       	document.getElementById('latitude').value = ui.item.lat;
 		        document.getElementById('longitude').value = ui.item.lng;
   			}
        }
    });
}

/* ]]> */
</script>
<style type="text/css">
  #map_canvas { height: 300px;width: 500px; }
</style>
<?php 
$this->Html->addCrumb('Programs', '/admin/programs');
$this->Html->addCrumb('<strong>Update Program Information for '.$this->Program->getTitle().'</strong>');
?>
<!--
<div id="dialog" title="Create new speaker" style="display:none;">
    <p id="validateTips">All form fields are required.</p>
    <form id="SpeakerAdminCreateForm" name="SpeakerAdminCreateForm" method="post" onSubmit="return false;">
        <input type="hidden" name="data[Speaker][who_created]" value="<?php echo $this->Session->read('Auth.User.id');?>" />
        <fieldset>
            <label for="speaker-first_name">* First Name:</label><input type="text" name="data[Speaker][first_name]" id="speaker-first_name" class="text ui-widget-content ui-corner-all" />
            <label for="speaker-last_name">* Last Name:</label><input type="text" name="data[Speaker][last_name]" id="speaker-last_name" class="text ui-widget-content ui-corner-all" />
            <label for="speaker-email">* Email:</label><input type="text" name="data[Speaker][email]" id="speaker-email" class="text ui-widget-content ui-corner-all" />
            <label for="speaker-mobile">* Mobile:</label><input type="text" name="data[Speaker][mobile]" id="speaker-mobile" class="text ui-widget-content ui-corner-all" />
        </fieldset>
    </form>
</div>-->
<div style="display:none;" id="updateprogram" title="Update program <?php echo $this->Program->getTitle();?>"><div id="loader">Please wait while processing the request<br /><img src="<?php echo $this->Html->url('/img/ajaxloading.gif');?>" alt="loading..." /></div><div id="status_msgs" /></div></div>
<div class="users form">
    <h2><?php __('Update Program Information for <a href="'.$this->Html->url(array("admin"=>true,'controller'=>'programs','action'=>'info',$this->Program->getSeoUri())).'" target="_blank">'.$this->Program->getTitle()); ?></a></h2>
    <form id="ProgramAdminUpdateForm" method="post" accept-charset="utf-8" onSubmit="return false;">
    <input type="hidden" name="_method" value="POST" />
    <input type="hidden" name="data[Program][who_modified]" value="<?php echo $this->Session->read('Auth.User.id');?>" />
    <input type="hidden" name="data[Program][id]" value="<?php echo $this->Program->get('id');?>" />
    <table style="margin-bottom: 7px;" width="698" cellpadding="4" cellspacing="1">
        <tbody style="background-color: rgb(240, 240, 225);">
            <tr><th class="th-new th-left" colspan="3">Fill in the required fields <span style="color: rgb(220, 20, 60);"><b>*</b></span></th></tr>
            <tr><th class="th-new th-left" colspan="3">Step 1. Program Details</th></tr>
            <tr class="control-td-row" bgcolor="#E6E6CC">
                <td>
                   <?php
                   	$startDate = date("n/d/Y");
				   	$endDate = date("n/d/Y");
					
					if( isset($program['ProgramsSchedule']) && count($program['ProgramsSchedule']) > 0 ){
						if( count($program['ProgramsSchedule']) > 1 ){
							$aScheduleDates = array();
							foreach( $program['ProgramsSchedule'] as $programSchedule ){
								$aScheduleDate[] = strtotime($programSchedule['start_date']);
							}

							sort($aScheduleDate);
							$startDate = date("n/d/Y",$aScheduleDate[0]);
							$endDate = date("n/d/Y",array_pop($aScheduleDate));
						}else{
							$startDate = date("n/d/Y",strtotime($program['ProgramsSchedule'][0]['start_date']));
							$endDate = date("n/d/Y",strtotime($program['ProgramsSchedule'][0]['end_date']));
						} 
					}
                   
                   	if( $this->Session->read('Auth.User.role_id')==1 ){
				    ?>
                    <select id="programs_division_id" name="data[Program][programs_division_id]">
                    <?php
                   	foreach( $divisions as $division_key => $division ){
						if( $this->Program->get('programs_division_id') > 0 ){
							if( $division_key == $this->Program->get('programs_division_id') ){
								echo '<option value="'.$division_key.'" selected="selected">'.$division.'</option>';
							}else{
								echo '<option value="'.$division_key.'">'.$division.'</option>';
							}	
						}else{
							echo '<option value="'.$division_key.'">'.$division.'</option>';
						}
                   	}
                    ?>
                    </select><br />
                    <?php
                    }else{
                        echo '<input type="hidden" id="programs_division_id" name="data[Program][programs_division_id]" value="'.$this->Session->read('Auth.User.role_id').'" />';
                    }
                    ?>
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td>
                                <table width="100px">
                                    <tr><td><label for="title">Title:</label>&nbsp;<input type="text" id="title" name="data[Program][title]" maxlength="60" style="width:450px;" class="text ui-widget-content ui-corner-all" value="<?php echo $this->Program->getTitle();?>" />&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span></td></tr>
                                    <tr class="striped">
                                       <td>
                                          <table border="1">
                                            <tr class="striped"><th width="40%">When</th><th width="60%">Speaker</th></tr>
                                            <tr class="striped">
                                                <td width="40%">
                                                    <?php
                                                    if( !isset($program['ProgramsSchedule'][0]['start_time']) ){
                                                       $program['ProgramsSchedule'][0]['start_time'] = '8:00 am';
                                                    }

                                                    if( !isset($program['ProgramsSchedule'][0]['end_time']) ){
                                                       $program['ProgramsSchedule'][0]['end_time'] = '5:00 pm';
                                                    }
                                                    
                                                    if( !isset($program['ProgramsSchedule'][0]['all_day_event']) ){
                                                       $program['ProgramsSchedule'][0]['all_day_event'] = '0';
                                                    }

                                                    ?> 
                                                    <div id="start_date">
                                                        <label for="startDate">Start Date:</label><br />
                                                        <span style="color: rgb(220, 20, 60);"><b>*</b></span><input style="width:100px;" name="data[ProgramsSchedule][start_date]" id="startDate" value="<?php echo $startDate;?>" size="15" maxlength="12" type="text" alt="Start Date" class="text ui-widget-content ui-corner-all" readonly="readonly">&nbsp;
                                                        <input style="width:90px;" name="data[ProgramsSchedule][start_time]" id="startTime" size="15" maxlength="12" type="text" value="<?php echo $program['ProgramsSchedule'][0]['start_time'];?>" alt="Start Time" class="text ui-widget-content ui-corner-all"  />
                                                    </div>
                                                    <div id="end_date">
                                                        <label for="endDate">End Date:</label><br />
                                                        <span style="color: rgb(220, 20, 60);"><b>*</b></span><input style="width:100px;" name="data[ProgramsSchedule][end_date]" id="endDate" value="<?php echo $endDate;?>" size="15" maxlength="12" type="text" alt="End Date" class="text ui-widget-content ui-corner-all" readonly="readonly" />&nbsp;
                                                        <input style="width:90px;" name="data[ProgramsSchedule][end_time]" id="endTime" size="15" maxlength="12" type="text" value="<?php echo $program['ProgramsSchedule'][0]['end_time'];?>" alt="End Time" class="text ui-widget-content ui-corner-all" />
                                                    </div>
                                                    <div id="set_to_all_day"><label for="all_day">All Day Event:</label>
                                                    <input type="checkbox" id="all_day" name="data[ProgramsSchedule][all_day_event]" value="1" <?php echo ($program['ProgramsSchedule'][0]['all_day_event']==1) ? 'checked="checked"': '';?> /></div>
                                                </td>
                                                <td width="60%" rowspan="3">
                                                    <div id="speakers">
                                                        <ul id="fcbklist">
                                                            <?php
                                                            $programSpeakers = array();	
                                                            if( isset($program['ProgramsSpeaker']) && count($program['ProgramsSpeaker']) > 0 ){
                                                                foreach( $program['ProgramsSpeaker'] as $aSpeakerInfo ){
                                                                    $programSpeakers[] = $aSpeakerInfo['speaker_id'];
                                                                }	
                                                            }	
                                                            
                                                            if( $speakers && sizeof($speakers)>0 ){
                                                                $i = 0;
                                                                foreach( $speakers as $speaker_key => $speaker_name ){
                                                            ?>
                                                            <li id="div_speaker_<?php echo $speaker_name['Speaker']['id'];?>"<?php echo (in_array($speaker_name['Speaker']['id'],$programSpeakers)) ? ' class="liselected"':'';?>>
                                                                <strong><?php echo $speaker_name['Speaker']['first_name'].' '.$speaker_name['Speaker']['last_name'];?></strong>
                                                                <span class="fcbkitem_text">
                                                                <?php
                                                                if( strlen($speaker_name['Speaker']['email'])> 0 ){
                                                                ?>
                                                                <br />Email:&nbsp;<a href="mailto:<?php echo $speaker_name['Speaker']['email'];?>"><?php echo $speaker_name['Speaker']['email'];?></a>
                                                                <?php
                                                                }
                                                                ?>
                                                                <br /><?php echo ($speaker_name['ProgramsDivision']['title']);?></span>
                                                                <input type="hidden" id="speaker_<?php echo $speaker_name['Speaker']['id'];?>" name="data[ProgramsSpeaker][id][]" value="<?php echo $speaker_name['Speaker']['id'];?><?php echo (in_array($speaker_name['Speaker']['id'],$programSpeakers)) ? '_selected':'';?>" <?php echo (in_array($speaker_name['Speaker']['id'],$programSpeakers)) ? 'checked="checked"':'';?>/>
                                                            </li>
                                                            <?php
                                                                    $i++;
                                                                }
                                                            }else{
                                                            ?>
                                                            <li>
                                                                <strong>No Speaker Created</strong><br />
                                                                <span class="fcbkitem_text">Please create a speaker for this program.</span>
                                                                <input type="hidden" name="data[ProgramsSpeaker][id][]" value="0" />
                                                            </li>
                                                            <?php
                                                            }
                                                            ?>

                                                        </ul>
                                                    </div>
                                                    <!--<div id="div_add_speakers"><a style="cursor:pointer;" id="add_speakers">Add Speaker</a></div>-->

                                                </td>
                                             </tr>
                                             <tr><th>Category</th></tr>
                                             <tr class="striped"><td>
                                             <select id="category" name="data[Program][programs_type_id]">
                                             <?php
                                             foreach( $types as $type_key => $type_name ){
												 if( $type_key == $this->Program->get('programs_type_id') ){
													echo '<option value="'.$type_key.'" selected="selected">'.$type_name.'</option>';	
												 }else{
                                                 	echo '<option value="'.$type_key.'">'.$type_name.'</option>';
												 }
                                             }
                                             ?>
                                             </select>&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span></td></tr>
                                          </table>
                                       </td>
                                    </tr>
                                    <tr><th>Description</th></tr>
                                    <tr class="striped"><td><textarea id="program_content" name="data[Program][content]" rows="15" cols="40" style="width: 100%" class="tinymce"><?php echo $this->Program->getNotes();?></textarea></td></tr>
                                </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr><th class="th-new th-left" colspan="3">Step 2. Location</th></tr>
            <tr class="striped">
                <td colspan="3">
                <table>
                <tr><td width="350px;">
                <label for="venue_name">Venue Name</label>&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span><input type="text" id="venue_name" name="data[Program][venue_name]" class="text ui-widget-content ui-corner-all" value="<?php echo $this->Program->getVenue();?>" /><br />
                <label for="address">Address</label>&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span><input type="text" id="address" name="data[Program][address]" class="text ui-widget-content ui-corner-all" value="<?php echo $this->Program->get('address');?>" /><br />
                <label for="suburb">Suburb</label><span style="color:rgb(220,20,60);"><b></b></span><input type="text" id="suburb" name="data[Program][suburb]" class="text ui-widget-content ui-corner-all" value="<?php echo $this->Program->get('suburb');?>" /><br />
                <label for="suburb">City</label>&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span><input type="text" id="city" name="data[Program][city]" class="text ui-widget-content ui-corner-all" value="<?php echo $this->Program->get('city');?>" /><br />
                <label for="zip_code">Zip Code</label><input type="text" id="zip_code" name="data[Program][zip_code]" class="text ui-widget-content ui-corner-all" value="<?php echo $this->Program->get('zip_code');?>" />
                </td><td width="450px;"><div id="map_canvas"></div>
                  <input type="hidden" id="latitude" name="data[Program][latitude]" value="<?php echo $this->Program->getLatitude();?>" /><input type="hidden" id="longitude" name="data[Program][longitude]" value="<?php echo $this->Program->getLongitude();?>" />
                </td>
                </tr>
                </td>
			</tr>
            <tr><th class="th-new th-left" colspan="3">Step 3. Contact Info</th></tr>
            <tr class="striped">
                <td colspan="3">
                    <table>
                        <tr class="striped"><td><label for="contact_name">Name</label>&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span><input type="text" id="contact_name" name="data[Program][contact_name]" class="text ui-widget-content ui-corner-all" style="width:450px;" value="<?php echo $this->Program->get('contact_name');?>" ></td></tr>
                        <tr class="striped"><td><label for="contact_email">Email</label>&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span><input type="text" id="contact_email" name="data[Program][contact_email]" class="text ui-widget-content ui-corner-all" style="width:450px;" value="<?php echo $this->Program->get('contact_email');?>" /></td></tr>
                        <tr class="striped"><td><label for="contact_phone">Phone</label>&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span><input type="text" id="contact_phone" name="data[Program][contact_phone]" class="text ui-widget-content ui-corner-all" style="width:450px;" value="<?php echo $this->Program->get('contact_phone');?>"  /></td></tr>
                    </table>
                </td>
			</tr>
            <tr><th class="th-new th-left" colspan="3">Step 4. Rate Plans</th></tr>
            <tr class="striped">
                <td colspan="3">
                    <table border="1" id="rateplan_container">
                    <tr>
                    <th width="22%">Discount Applies</th><td width="78%" align="left">
                    <input type="radio" name="data[Program][discount_applies]" value="1" id="yes" <?php echo ($this->Program->get('discount_applies')==1)? 'checked="checked"':'';?>/> YES<br />
					<input type="radio" name="data[Program][discount_applies]" value="0" id="no" <?php echo ($this->Program->get('discount_applies')==0)? 'checked="checked"':'';?>/> NO
					</td>
					</tr>
                    </table>
                    <br/>

                    <table border="1" id="rateplan_container">
                    <tr>
                    <th rowspan="2" width="5%">Activate</th>
                    <th rowspan="2" valign="top" align="center" style="text-align: center; width: 15%;">Cut Off Date</th>
                    <th colspan="3" style="text-align:center;" align="center" width="65%">Rate Plan</th>
                    <th rowspan="2" width="15%" style="text-align:center;">No. of<br />Adult</th>
                    <tr><?php
                    foreach( $rateplans as $rateplan_key => $rateplan_name ){
                        echo '<th style="text-align:center;">'.$rateplan_name.'</th>';
                    }
                    ?>
                    </tr>
                    <?php
                    if( count($program['ProgramsRatePlan']) > 0 ){
                        foreach( $program['ProgramsRatePlan'] as $rate_plan ){
					?>
                    <tr container="tableRow">
                       <td style="text-align:center;"><span style="color: rgb(220, 20, 60);"><b>*</b></span><input type="checkbox" name="data[ProgramsRatePlan][use_plan][]" id="activeplan_<?php echo $rate_plan['rate_plan_id'];?>" checked="checked" readonly="readonly" /></td>
                       <td style="text-align:center;"><span style="color: rgb(220, 20, 60);"><b>*</b></span><input type="text" name="data[ProgramsRatePlan][cut_off_date][]" id="cutOffDate_<?php echo $rate_plan['rate_plan_id'];?>" readonly="readonly" value="<?php echo date("n/d/Y",strtotime($rate_plan['cut_off_date']));?>" size="15" maxlength="12" container="calendar" style="width:80px;" alt="Cut Off Date" class="text ui-widget-content ui-corner-all" /></td>
                       <td style="text-align:center;"><span style="color: rgb(220, 20, 60);"><b>*</b></span>PHP&nbsp;<input type="text" name="data[ProgramsRatePlan][eb_cost][]" id="rate_plan_eb_<?php echo $rate_plan['rate_plan_id'];?>" style="width:100px;" class="text ui-widget-content ui-corner-all" value="<?php echo str_replace(",","",$rate_plan['eb_cost']);?>" /></td>
                       <td style="text-align:center;"><span style="color: rgb(220, 20, 60);"><b>*</b></span>PHP&nbsp;<input type="text" name="data[ProgramsRatePlan][reg_cost][]" id="rate_plan_regular_<?php echo $rate_plan['rate_plan_id'];?>" style="width:100px;" class="text ui-widget-content ui-corner-all" value="<?php echo str_replace(",","",$rate_plan['reg_cost']);?>" /></td>
                       <td style="text-align:center;"><span style="color: rgb(220, 20, 60);"><b>*</b></span>PHP&nbsp;<input type="text" name="data[ProgramsRatePlan][osp_cost][]" id="rate_plan_osp_<?php echo $rate_plan['rate_plan_id'];?>" style="width:100px;" class="text ui-widget-content ui-corner-all" value="<?php echo str_replace(",","",$rate_plan['osp_cost']);?>" /></td>
                       <td width="15%"><select name="data[ProgramsRatePlan][num_of_adults][]" id="num_of_adults_<?php echo $rate_plan['rate_plan_id'];?>">
                           <?php
                           for( $i = 1; $i < 11; $i++ ){
                              $selected = ($rate_plan['num_of_adults']==$i) ?' selected="selected"':'';
                              echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
                           }
                           ?>
                       </select></td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                    </table>
                    <!--<a style="cursor:pointer;" id="add_rate_plan">Add More Rates</a>-->
                </td>
   	   		 </tr>
            <tr style="display:none;"><th class="th-new th-left" colspan="3">Step 6. Broadcast to Social Media</th></tr>
            <tr style="display:none;" class="control-td-row">
            	<td colspan="3">
            		<label for="doBitly" class="radioDistPub">
            			<input value="1" name="data[Program][post_to_facebook]" id="doBitly" type="checkbox" <?php echo ($this->Program->get('post_to_facebook')==1)? 'checked="checked"':'';?>>&nbsp;Post a wall to <b><span style="color: rgb(247, 203, 120);">Facebook</span></b>.
            		</label>
            		<label for="doTwitter" class="radioDistPub">
            			<input value="1" name="data[Program][post_to_twitter]" id="doTwitter" type="checkbox" <?php echo ($this->Program->get('post_to_twitter')==1)? 'checked="checked"':'';?>>&nbsp;Post a tweet to <b><span style="color: rgb(53, 204, 255);">twitter</span></b>.</label>
            	</td>
            </tr>	
            <tr class="control-td-row" bgcolor="#E6E6CC">
				<td colspan="3" align="right">
        			<!--<input name="data[Program][reset]" id="reset_program" value="Cancel" type="reset" class="ui-button ui-state-default ui-corner-all">-->
					<input name="data[Program][update]" id="update_program" value="Update Program" type="submit" class="ui-button ui-state-default ui-corner-all">
				</td>
			</tr>
		</tbody>
	</table>
</form>
</div>
