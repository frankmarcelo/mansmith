<?php


echo $this->Html->css(array('fcbkselection'));
echo $this->Html->script(array(
    'jquery/jquery.bgiframe.min',
    'jquery/jquery.ajaxQueue',
    'jquery/jquery.fcbkselection',
    'jquery/jquery.simplemodal',
    'jquery/development-bundle/ui/jquery.ui.core',
    'jquery/development-bundle/ui/jquery.ui.widget',
    'jquery/development-bundle/ui/jquery.ui.position',
    'jquery/development-bundle/ui/jquery.ui.autocomplete',
    'jquery/development-bundle/ui/jquery.ui.draggable',
    'jquery/development-bundle/ui/jquery.ui.resizable',
    'jquery/development-bundle/ui/jquery.ui.dialog',
    'jquery/development-bundle/ui/jquery.ui.datepicker'

    )
);

$this->Program->setProgramData( $program['Program'] );
$this->ProgramExtraInfo->setExtraInfo( $program ); 
?>
<style type="text/css">
    .info {
        width: 80px;
        height: 50px;
    }
</style>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&region=PH"></script>
<script type="text/javascript">
/* <![CDATA[ */


$(document).ready(function(){
   	$('#program_data').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true });
   	$('#program_extra').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true });
    $('#add_program_participants').live('click',function(){
        var dialog_message = '<div><iframe width="880px" height="500px" src="<?php echo $this->Html->url(array('controller'=>'programs','admin'=>true,'action'=>'add_programs_participants', $this->Program->get('id')))?>" width="auto" height="auto" border="0"></iframe></div>';
        $("#confirm_msg").empty().html(dialog_message);
        $('#dialog').dialog({modal:true,hide:'explode',width:'900px','position':'center',beforeClose: function(event, ui){
            location.reload();
        }});
    });

    $("#cancel").live("click",function(){
        $("#dialog").dialog('close');
    });

    $(window).resize(function() {
        $("#dialog").dialog("option", "position", "center");

    });

    $(window).scroll(function() {
        $("#dialog").dialog("option", "position", "center");
    });

    invokeMap();//initialise map
});

function invokeMap() {

    var myLatlng = new google.maps.LatLng(<?php echo $this->Program->getLatitude();?>,<?php echo $this->Program->getLongitude();?>);
    var myOptions = {
      zoom: 14,
      center: myLatlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      navigationControl: true,
      mapTypeControl: true,
      disableDefaultUI: true,
      navigationControl: true,
      mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
        position: google.maps.ControlPosition.TOP_LEFT
      },
      streetViewControl: false
    }
    var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        draggable:true
    });
    map.setCenter(myLatlng);

    google.maps.event.addListener(marker, 'dragend', function() {
        var location = marker.getPosition();
        document.getElementById('latitude').value = location.lat();
        document.getElementById('longitude').value = location.lng();
        
        $.post("<?php echo Configure::read('js_directory');?>save_program_location.php?timer="+Math.random(), { programId: '<?php echo $this->Program->get('id');?>', lat:location.lat() , lon:location.lng() });
        
        $("#longitude").after('<div id="map_msg_status"></div>');

        $('#map_msg_status').fadeIn("slow",function(){
           $(this).empty().html("New saved location...");
        });

        this.timer = setTimeout(function (){
           $("#map_msg_status").fadeOut("fast",function(){
              $(this).remove();
           });
        },1000);
    });
}

/* ]]> */
</script>
<style type="text/css">
#map_canvas { height: 300px;width: 450px }
	
a{
    color: #333333;
}
</style>
<?php
/** breadcrumbs stuff**/
$this->Html->addCrumb('Programs', '/admin/programs');
$this->Html->addCrumb('Search Programs', '/admin/programs/search?q='.urlencode($this->Program->getTitle()));
$this->Html->addCrumb('<strong>'.$this->Program->getTitle().'</strong>');
?>
<div class="users form">
<h2><?php __('Program Information for '.ucwords($this->Program->getTitle())); echo $this->Program->getDisplayEdit();?></h2>
<div class="clearfix hasRightCol" id="contentCol">
   <div id="headerArea">
     <div class="profileHeaderMain" id="program_data">
        <ul><li><a href="#program_info"><span>Overview</span></a></li></ul>
        <div id="program_info">
            <table class="uiInfoTable">
                <tr vertical-align="top">
                <td width="50%">
                    <table class="uiInfoTable mvm profileInfoTable">
                      <tbody>
                        <tr class="striped">
                          <th class="label">Program</th>
                          <td class="data"><div id="program_title"><?php echo $this->Program->getTitle();?></div></td>
                        </tr>
                        <tr class="striped">
                          <th class="label" width="80px;">Type</th>
                          <td class="data"><div><?php echo $this->ProgramExtraInfo->getProgramType();?> (<?php echo $this->ProgramExtraInfo->getProgramDivision();?>)</div></td>
                        </tr>
                         <!-- 
                         <tr class="striped">
                          <th class="label" width="80px;">Social Networking</th>
                          <td class="data">
                             <iframe src="http://www.facebook.com/plugins/like.php?href=http://www.mansmith.net/<?php echo $this->Program->getSeoUri();?>&amp;layout=button_count&amp;show_faces=true&amp;&amp;action=like&amp;font=verdana&amp;colorscheme=light&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe>
                             <?php 
                             if( $this->Program->get('post_to_facebook') ){
                             ?>
                             	<div id="share">
									<fb:share-button class="meta">
									<meta name="title" content="<?php echo ucwords($this->Program->getTitle());?>"/>
									<meta name="description" content="Read the Static FBML Bible and Rejoice!"/>
									<link rel="image_src" href="http://www.hyperarts.com/facebook/static-fbml-bible/_img/share-popup_80x80.gif"/>
									<link rel="target_url" href="http://www.facebook.com/StaticFBMLBible"/>
									</fb:share-button>
								</div>
								<?php 
                             }
                             	
                             if( $this->Program->get('post_to_twitter') ){
                             	?>	
                             	<script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>
								<div><a href="http://twitter.com/share" class="twitter-share-button" data-via="twitterapi" data-text="<?php echo ucwords($this->Program->getTitle());?>" data-count="horizontal">Tweet</a></div>
                             	<?php 	
                             }
							?>
						</td>
                        </tr>-->
                      </tbody>
                      <tbody>
                        <tr class="striped">
                          <th class="label">Speakers</th>
                          <td class="data">
                            <?php
                            if( isset($program['ProgramsSpeaker']) && count($program['ProgramsSpeaker'])>0 && isset($speakers) && count($speakers) > 0 ){
                                foreach( $speakers as $speaker_key => $speaker ){
                                    foreach( $program['ProgramsSpeaker'] as $program_speaker_key => $program_speaker ){
                                        if( $speaker['Speaker']['id'] == $program_speaker['speaker_id'] ){
                                            echo '<div>'.$speaker['Speaker']['first_name'].'&nbsp;'.$speaker['Speaker']['last_name'];
                                            if(strlen($speaker['Speaker']['email'])>0 ){
                                                echo '&nbsp;<a href="mailto:'.$speaker['Speaker']['email'].';">'.$speaker['Speaker']['email'].'</a>';
                                            }
                                            echo '</div>
                                            <!--<a class="delete" style="cursor:pointer;" id="delete_speaker_'.$program_speaker['speaker_id'].'">Delete</a>-->';
                                   	}
                                    }
                             	}
                            }
                            ?>
                            <!--<a class="add_info" style="cursor:pointer;" id="add_speaker">Add Speaker</a>-->
                          </td>
                        </tr>
                      </tbody>
                      <tbody>
                        <tr class="striped">
                          <th class="label">Schedule</th>
                          <td class="data">
                             <?php
                             if( isset($program['ProgramsSchedule']) && count($program['ProgramsSchedule']) > 0 ){
                             ?>
                             <div>
                                 <?php
                                 foreach( $program['ProgramsSchedule'] as $program_schedule_key => $program_schedule ){
                                 ?> 
                                    <div><strong><?php echo date("l, jS F Y",strtotime($program_schedule['start_date']) );?></strong>&nbsp;-&nbsp;
                                      <?php
                                      foreach( $programsScheduleType as $programType ){
                                          if( $program_schedule['programs_schedule_type_id'] == $programType['ProgramsScheduleType']['id'] ){
                                              echo $programType['ProgramsScheduleType']['name'];
                                          }
                                      }
                                      ?>
                                      <br />
                                      (<span class="dtstart">
                                      <span title="<?php echo date("c",strtotime($program_schedule['start_date']." ".$program_schedule['start_time']));?>" class="value-title"> </span><?php echo $program_schedule['start_time'];?></span> - <span class="dtend">
                                      <span title="<?php echo date("c",strtotime($program_schedule['end_date']." ".$program_schedule['end_time']));?>" class="value-title"> </span><?php echo $program_schedule['end_time'];?></span>) 
                                      <!--<a style="cursor:pointer;" class="edit_info" id="edit_schedule_<?php echo $program_schedule['id'];?>">Edit</a>
                                      <a style="cursor:pointer;" class="delete" id="delete_schedule_<?php echo $program_schedule['id'];?>">Delete</a>-->
                                    </div>  
                                 <?php
                                 }
                                 ?>  
                             </div>
                             <br />
                             <?php
                             }
                             ?>
                             <!--<a class="add_info" style="cursor:pointer;" id="add_speaker">Add Schedule</a>-->
                          </td>
                        </tr>
                        <tr class="spacer"><td colspan="2"><hr></td></tr>
                      </tbody>
                      <tbody>
                        <tr class="striped">
                          <th class="label">Location</th>
                          <td class="data"><div class="location vcard"><?php echo $this->Program->getFullAddress();?></div></td>
                        </tr>
                        <tr class="spacer"><td colspan="2"><hr></td></tr>
                      </tbody>
                      <tbody>
                        <tr class="striped">
                            <th class="label">Description</th>
                            <td class="data"><div class="description summary"><?php echo $this->Program->getNotes();?></div></td>
                        </tr>
                      </tbody>
                    </table>
                </td>
                <td width="50%" valign="top">
                	<div id="map_canvas"></div><input type="hidden" id="latitude" name="data[Program][latitude]" value="<?php echo $this->Program->getLatitude();?>" /><input type="hidden" id="longitude" name="data[Program][longitude]" value="<?php echo $this->Program->getLongitude();?>" />
                	<br /><br />
                	<table class="uiInfoTable mvm profileInfoTable">
                      <tbody>
                        <tr class="striped">
                          <th class="label" width="40%">Primary Contact Person</th>
                          <td class="data" width="60%"><?php echo $this->Program->get('contact_name');?></td>
                        </tr>
                        <tr class="striped">
                          <th class="label" width="40%">Primary Contact Email</th>
                          <td class="data" width="60%"><?php echo $this->Program->get('contact_email');?></td>
                        </tr>
                        <tr class="striped">
                          <th class="label" width="40%">Primary Contact Phone</th>
                          <td class="data" width="60%"><?php echo $this->Program->get('contact_phone');?></td>
                        </tr>
                      </tbody>
                   	</table>
                </td>
                </tr>
                <tr>
                    <td colspan="2"><table class="uiInfoTable mvm profileInfoTable">

                        <tbody>
                        <tr class="striped">
                            <th class="label" width="40%">Discount Applies</th>
                            <td class="data" width="60%"><?php echo ($this->Program->get('discount_applies')==1)? "YES":"NO";?></td>
                        </tr>
                        <tr class="striped">
                            <th class="label" width="40%">Number of Adults</th>
                            <td class="data" width="60%"><?php echo ($program['ProgramsRatePlan'][0]['num_of_adults']);?></td>
                        </tr>
                        <tr class="striped">
                            <th class="label" width="40%">Early Bird Date</th>
                            <td class="data" width="60%"><?php echo date("j",strtotime($program['ProgramsRatePlan'][0]['cut_off_date'])).'<sup>'.date("S",strtotime($program['ProgramsRatePlan'][0]['cut_off_date'])).'</sup>&nbsp;'.date("M Y",strtotime($program['ProgramsRatePlan'][0]['cut_off_date']));?></td>
                        </tr>
                        <tr class="striped">
                            <th class="label" width="40%">Early Bird Rate</th>
                            <td class="data" width="60%">PHP <?php echo number_format($program['ProgramsRatePlan'][0]['eb_cost'],2,'.',',');?></td>
                        </tr>
                        <tr class="striped">
                            <th class="label" width="40%">Regular Rate</th>
                            <td class="data" width="60%">PHP <?php echo number_format($program['ProgramsRatePlan'][0]['reg_cost'],2,'.',',');?></td>
                        </tr>
                        <tr class="striped">
                            <th class="label" width="40%">On Site Rate</th>
                            <td class="data" width="60%">PHP <?php echo number_format($program['ProgramsRatePlan'][0]['osp_cost'],2,'.',',');?></td>
                        </tr>
                        </tbody>
                    </table></td>
                </tr>
            </table>
        </div>
     </div>
     <div class="profileHeaderMain" id="program_extra">
     	<ul>
            <li><a href="#program_participants"><span>Program Participants</span></a></li>
     		<li><a href="#program_schedule"><span>Program Schedule</span></a></li>
            <li><a href="#program_documents"><span>Program Documents</span></a></li>
            <li><a href="#program_billing"><span>Billing &amp; Invoice</span></a></li>
        </ul>
        <div id="program_participants">
            <?php
                echo $this->Program->getAddProgramParticipants().'<br><br>';
                $tot_participants = $this->Program->get('total_participants') +1 ;
                if( $tot_participants < 3 ){
                    $height = 220 * $tot_participants;
                }else{
                    if( $tot_participants > 10 ){
                        $height = 1600;
                    }else{
                        $height = (220 * $tot_participants)/1.55;
                    }
                }
                $height = '2000px';
                echo '<div><iframe src="'.$this->Html->url(array('controller'=>'programs','admin'=>true,'action'=>'program_participants', $this->Program->get('id').DIRECTORY_SEPARATOR.'1')).'" width="1050" height="'.$height.'" border="0"></iframe></div>';
            ?>
        </div>
        <div id="program_schedule"><iframe src="<?php echo $this->Html->url(array('controller'=>'programs','admin'=>true,'action'=>'programs_schedule', $this->Program->get('id').DIRECTORY_SEPARATOR.'1'));?>" width="1050" height="700px" border="0"></iframe></div>
        <div id="program_documents"><iframe src="<?php echo $this->Html->url(array('controller'=>'programs','admin'=>true,'action'=>'programs_document', $this->Program->get('id').DIRECTORY_SEPARATOR.'1'));?>" width="1050" height="850px" border="0"></iframe></div>
        <div id="program_billing" height="1600px"><iframe src="<?php echo $this->Html->url(array('controller'=>'programs','admin'=>true,'action'=>'programs_billing', $this->Program->get('id').DIRECTORY_SEPARATOR.'1'));?>" width="1050" height="1660px" border="0"></iframe></div>
     </div>
   </div>
</div>
</div>
<div id="dialog" title="Add Program Participant"><div id="confirm_msg">&nbsp;</div></div>
