<link rel="stylesheet" type="text/css" href="/css/admin.css?1357879311">
<script type="text/javascript" src="/js/jquery/jquery.min.js?1347027524"></script>
<script type="text/javascript" src="/js/jquery/jquery-ui.min.js?1347027524"></script>
<script type="text/javascript" src="/js/jquery/superfish.js"></script>
<script type="text/javascript" src="/js/jquery/supersubs.js"></script>
<script type="text/javascript" src="/js/jquery/jquery.tipsy.js?1347027523"></script>
<script type="text/javascript" src="/js/jquery/jquery.elastic-1.6.1.js?1347027522"></script>
<style type="text/css">
    table tr td {
        border-bottom: 1px solid #DFDFDF !important;
        vertical-align: middle !important;
    }

    .tblClass{
        width: 1000px !important;
    }

    .listing {
        width: 1000px !important;
    }

    .listing .box10 {
        float: right !important;
        padding: 0 116px !important;
        text-align: left !important;
        width: 6px !important;
    }
</style>
<?php
echo $this->Html->script(array(
        'jquery/jquery.bgiframe.min',
        'jquery/jquery.ajaxQueue',
        'jquery/development-bundle/ui/jquery.ui.core',
        'jquery/development-bundle/ui/jquery.ui.widget',
        'jquery/development-bundle/ui/jquery.ui.position',
        'jquery/development-bundle/ui/jquery.ui.autocomplete',
        'jquery/development-bundle/ui/jquery.ui.draggable',
        'jquery/development-bundle/ui/jquery.ui.resizable',
        'jquery/development-bundle/ui/jquery.ui.dialog',
        'jquery/development-bundle/ui/jquery.ui.datepicker'
    )
);
?>
<script type="text/javascript">
    $(document).ready(function(){

        function searchEmployees(){
            $.ajax({
                cache: false  ,
                type:  "get" ,
                url:   "<?php echo Configure::read('js_directory');?>search_company_employees.php",
                data:  "term="+$('#company_id').val()+'&program_id=<?php echo $program_id;?>',
                dataType: "json" ,
                success: function(data){
                    var html = '';
                    if( data.length >0 ){
                        html += '<input type="submit" name="data[ProgramsParticipant][add]" value="Add Participants" /><br>';
                    }
                    html += '<table cellspacing="0" cellpadding="0" style="width:800px !important">';
                    html += '<tr><th style="width:100px !important;">Contact Person</th>';
                    html += '<th style="width:350px !important;">Employee Name</th>';
                    html += '<th style="width:350px !important;">Position</th></tr>';
                    if( data.length > 0 ){
                        for( i = 0; i < data.length; i++ ){
                            var css = (i % 2 ==0) ?'striped':'';
                            var is_checked = ( data[i].is_contact == true ) ?' checked="checked" ':' ';
                            html += '<tr class="'+css+'"><td><input type="radio" '+is_checked+'value="'+data[i].id+'" name="data[ProgramsCompanyContact][participant_id]"></td>';
                            html += '<td>';
                            if( data[i].selected==false){
                                html +='<input type="checkbox" name="data[ProgramsParticipant][participant_id][]" value="'+data[i].id+'" />&nbsp;&nbsp;'+data[i].full_name+'</td>';
                            }else{
                                html +='&nbsp;&nbsp;'+data[i].full_name+'  <strong>(Already Added)</strong></td>';
                            }

                            html += '<td>'+data[i].position+'</td></tr>';
                        }
                    }else{
                        html += '<tr><td colspan="3">No employees found.</td></tr>';
                    }
                    html += '</table>';
                    $('#load_data').empty().fadeIn("slow",function(){
                       $(this).html(html);
                    });
                }
            });
        }

        $( "#q" ).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "<?php echo Configure::read('js_directory');?>search_company_id.php",
                    dataType: "json",
                    data: {q: request.term},
                    success: function( data ) {
                        response( $.map(data,function(item) {
                            return {
                                label: item.name,
                                value: item.name,
                                data: item
                            }
                        }));
                    }
                });
            },
            minLength: 3,
            select: function( event, ui,item ) {
                $('#company_id').val(ui.item.data.id);
            },
            open: function(event,ui) {
                $('.ui-autocomplete').each(function(i){
                    if( parseInt(i,10)== 0 ){
                        var css_style = $(this).attr('style');
                        var css_property = css_style.replace('left: -1000px','left: 0px');
                        $(this).attr('style',css_property+';z-index:100001;');
                    }
                });
            }
        });

        $("#ProgramsAddParticipantForm input").keypress(function(e) {
            if (e.keyCode == 13) {
                searchEmployees();
            }
        });

        $("#search_company").live("click",function(){
            searchEmployees();
        });
    })

</script>
<div class="users index">
    <?php  $this->Layout->sessionFlash();?>
    <form id="ProgramsAddParticipantForm" method="POST" onSubmit="return false;" accept-charset="utf-8">
        <div id="standard_search">
            <table cellspacing="0" cellpadding="0" style="background-color:#FFFFFF;border:1px solid #DDDDDD;clear:both;width:100%;">
                <tr><td><label for="q">Search Company:</label><input type="text" name="q" value="<?php echo $qValue;?>" style="width:450px;" id="q" class="text ui-widget-content ui-corner-all" />&nbsp;<a id="search_company" style="position: relative;text-decoration: none !important;text-align: center;padding: 0;overflow: visible;margin-right: 0.1em;cursor: pointer;display: inline-block;font-size:13px;padding: 0.4em 1em;border-radius: 4px 4px 4px 4px;" class="ui-button ui-state-default ui-corner-all">Search</a>&nbsp;&nbsp;e.g 3k Foods<br />
            </table>
        </div>
    </form>
    <form id="ProgramsParticipantUpdateForm" method="post" accept-charset="utf-8" action="<?php echo $this->Html->url(array("admin"=>true, "controller" => "programs" ,"action"=>"add_program_participants"));?>">
        <input type="hidden" name="data[ProgramsParticipant][who_created]" value="<?php echo $this->Session->read('Auth.User.id');?>" />
        <input type="hidden" name="data[ProgramsParticipant][program_id]" value="<?php echo $program_id;?>" />
        <input type="hidden" name="data[ProgramsParticipant][company_id]" id="company_id" value="0" />
        <input type="hidden" name="data[ProgramsParticipant][programs_participant_status_id]" value="0" />
        <div id="load_data"></div>
    </form>
</div>