<?php
$times = array();
$time = strtotime("05:00:00");
$times["05:00:00"] = date("g:i a",$time);

for($i = 1;$i < 76;$i++) {
    $time = strtotime("+ 15 minutes",$time);
    $key = date("H:i:s",$time);
    $times[$key] = date("g:i a",$time);
}
?>
<link rel="stylesheet" type="text/css" href="/css/admin.css?1357879311">
<script type="text/javascript" src="/js/jquery/jquery.min.js?1347027524"></script>
<script type="text/javascript" src="/js/jquery/jquery-ui.min.js?1347027524"></script>
<script type="text/javascript" src="/js/jquery/superfish.js"></script>
<script type="text/javascript" src="/js/jquery/supersubs.js"></script>
<script type="text/javascript" src="/js/jquery/jquery.tipsy.js?1347027523"></script>
<script type="text/javascript" src="/js/jquery/jquery.elastic-1.6.1.js?1347027522"></script>
<?php
echo $this->Html->script(array(
        'jquery/jquery.bgiframe.min',
        'jquery/jquery.ajaxQueue',
        'jquery/jquery.fcbkselection',
        'jquery/jquery.simplemodal',
        'jquery/jquery.tinymce',
        'tiny_mce/tiny_mce',
        'jquery/jquery.maskMoney',
        'jquery/development-bundle/ui/jquery.ui.core',
        'jquery/development-bundle/ui/jquery.ui.widget',
        'jquery/development-bundle/ui/jquery.ui.position',
        'jquery/development-bundle/ui/jquery.ui.autocomplete',
        'jquery/development-bundle/ui/jquery.ui.draggable',
        'jquery/development-bundle/ui/jquery.ui.resizable',
        'jquery/development-bundle/ui/jquery.ui.dialog',
        'jquery/development-bundle/ui/jquery.ui.datepicker'

    )
);
?>
<style type="text/css">
table tr td {
    border-bottom: 1px solid #DFDFDF !important;
    vertical-align: middle !important;
}

.tblClass{
    width: 1000px !important;
}

.listing {
    width: 1000px !important;
}

.listing .box10 {
    float: right !important;
    padding: 0 116px !important;
    text-align: left !important;
    width: 6px !important;
}
</style>
<script type="text/javascript">
var times = [<?php echo "'".implode("','",$times)."'";?>];
$(document).ready(function(){
    $('select[id*="program_schedule_type_id_"]').live('change',function(){
        $.ajax({
            cache: false  ,
            type:  "POST" ,
            url:   "<?php echo Configure::read('js_directory');?>ajax_update_program_schedule.php",
            data:  "status="+$(this).val()+"&id="+$(this).attr('id')+"&who_modified=<?php echo $this->Session->read('Auth.User.id');?>"
        });
    });

    $('textarea[id*="program_schedule_notes_id_"]').keyup(function(){
        $.ajax({
            cache: false  ,
            type:  "POST" ,
            url:   "<?php echo Configure::read('js_directory');?>ajax_update_program_schedule_notes.php",
            data:  "status="+$(this).val()+"&id="+$(this).attr('id')+"&who_modified=<?php echo $this->Session->read('Auth.User.id');?>"
        });
    });

    $('textarea[id*="program_schedule_address_id_"]').keyup(function(){
        $.ajax({
            cache: false  ,
            type:  "POST" ,
            url:   "<?php echo Configure::read('js_directory');?>ajax_update_program_schedule_address.php",
            data:  "status="+$(this).val()+"&id="+$(this).attr('id')+"&who_modified=<?php echo $this->Session->read('Auth.User.id');?>"
        });
    });

    $('#startDate').datepicker({
        showButtonPanel: true,
        changeMonth: true,
        changeYear: true,
        firstDay: 1,
        dateFormat: 'm/d/yy',
        closeText: 'Close'
    });

    $("#startTime,#endTime").autocomplete({
        autoFocus: true,
        source: times
    });
});

</script>
<div class="users index">
    <form id="ProgramsScheduletUpdateForm" method="post" accept-charset="utf-8" action="<?php echo $this->Html->url(array("admin"=>true, "controller" => "programs" ,"action"=>"programs_schedule_delete"));?>">
    <input type="hidden" name="data[Program][who_created]" value="<?php echo $this->Session->read('Auth.User.id');?>" />
    <input type="hidden" name="data[Program][id]" value="<?php echo $program_id;?>" />
    <?php  $this->Layout->sessionFlash();?>
    <table cellspacing="0" cellpadding="0" class="tblClass">
        <tr>
            <th width="200px">Date</th>
            <th width="150px">Schedule</th>
            <th width="100px">Schedule Type</th>
            <th>Notes</th>
            <th>Address</th>
        </tr>
        <?php
        if( isset($programs_schedules)) {
            foreach( $programs_schedules as $program_schedule) {
                $sched_date =  date("D j",strtotime(trim($program_schedule['ProgramsSchedule']['start_date']))).'<sup>'.
                    date("S",strtotime(trim($program_schedule['ProgramsSchedule']['start_date']))).'</sup>&nbsp;'.
                    date("F Y",strtotime(trim($program_schedule['ProgramsSchedule']['start_date'])));
        ?>
        <tr>
            <td><input type="checkbox" id="label_<?php echo $program_schedule['ProgramsSchedule']['id'];?>" name="data[ProgramsSchedule][id][]"  value="<?php echo $program_schedule['ProgramsSchedule']['id'];?>" />&nbsp;&nbsp;<label for="label_<?php echo $program_schedule['ProgramsSchedule']['id'];?>"><?php echo $sched_date;?></label></td>
            <td><?php echo $program_schedule['ProgramsSchedule']['start_time'].' - '.$program_schedule['ProgramsSchedule']['end_time'];?></td>
            <td>
                <select id="program_schedule_type_id_<?php echo $program_schedule['ProgramsSchedule']['id'];?>">
                <?php
                if(count($program_schedule_types)>0){
                    foreach( $program_schedule_types as $program_schedule_type ){
                        $selected = ($program_schedule['ProgramsSchedule']['programs_schedule_type_id'] == $program_schedule_type['ProgramsScheduleType']['id'] ) ?' selected="selected" ':' ';
                        echo '<option '.$selected.' value="'.$program_schedule_type['ProgramsScheduleType']['id'].'">'.$program_schedule_type['ProgramsScheduleType']['name'].'</option>';
                    }
                }
                ?>
            </select>
            </td>
            <td><textarea id="program_schedule_notes_id_<?php echo $program_schedule['ProgramsSchedule']['id'];?>"><?php echo $program_schedule['ProgramsSchedule']['notes'];?></textarea></td>
            <td><textarea id="program_schedule_address_id_<?php echo $program_schedule['ProgramsSchedule']['id'];?>"><?php echo $program_schedule['ProgramsSchedule']['address'];?></textarea></td>
        </tr>

        <?php
            }
        }
        ?>
        <tr><td colspan="5"><div class="paging">
            <input name="data[Program][Delete]" value="Delete" type="submit"> <input name="reset" value="Uncheck All" type="reset">
        </div></td></tr>
    </table>
    </form>
    <form id="ProgramsScheduleAddForm" method="post" accept-charset="utf-8" action="<?php echo $this->Html->url(array("admin"=>true, "controller" => "programs" ,"action"=>"programs_schedule_add"));?>">
        <input type="hidden" name="data[Program][who_created]" value="<?php echo $this->Session->read('Auth.User.id');?>" />
        <input type="hidden" name="data[Program][id]" value="<?php echo $program_id;?>" />
        <table cellspacing="0" cellpadding="0" class="tblClass">
            <tr>
                <th width="200px">Date</th>
                <th width="150px">Schedule</th>
                <th width="100px">Schedule Type</th>
                <th>Notes</th>
                <th>Address</th>
            </tr>
            <tr><td> <div id="start_date">
                <label for="startDate">Start Date:</label><br />
                <span style="color: rgb(220, 20, 60);"><b>*</b></span><input style="width:100px;" name="data[ProgramsSchedule][start_date]" id="startDate" value="<?php echo date("n/d/Y");?>" size="15" maxlength="12" type="text" alt="Start Date" class="text ui-widget-content ui-corner-all" readonly="readonly">&nbsp;

            </div></td>
            <td>Start - <input style="width:90px;" name="data[ProgramsSchedule][start_time]" id="startTime" size="15" maxlength="12" type="text" value="5:00 am" alt="Start Time" class="text ui-widget-content ui-corner-all"  /> Until <input style="width:90px;" name="data[ProgramsSchedule][end_time]" id="endTime" size="15" maxlength="12" type="text" value="6:00 am" alt="End Time" class="text ui-widget-content ui-corner-all" /></td>
            <td><select name="data[ProgramsSchedule][programs_schedule_type_id]">
                <?php
                if(count($program_schedule_types)>0){
                    foreach( $program_schedule_types as $program_schedule_type ){
                        echo '<option value="'.$program_schedule_type['ProgramsScheduleType']['id'].'">'.$program_schedule_type['ProgramsScheduleType']['name'].'</option>';
                    }
                }
                ?>
            </select></td>
            <td><textarea name="data[ProgramsSchedule][notes]"></textarea></td>
            <td><textarea name="data[ProgramsSchedule][address]"></textarea></td>


            <tr><td colspan="5"><div class="paging">
                <input name="data[Program][Add]" value="Add" type="submit" class="add_btn_info" />
            </div></td></tr>
        </table>
    </form>

</div>