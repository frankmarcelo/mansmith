<?php


echo $this->Html->css(array('fcbkselection'));
echo $this->Html->script(array(
        'jquery/jquery.bgiframe.min',
        'jquery/jquery.ajaxQueue',
        'jquery/jquery.fcbkselection',
        'jquery/jquery.simplemodal',
        'jquery/jquery.tinymce',
        'tiny_mce/tiny_mce',
        'jquery/jquery.maskMoney',
        'jquery/development-bundle/ui/jquery.ui.core',
        'jquery/development-bundle/ui/jquery.ui.widget',
        'jquery/development-bundle/ui/jquery.ui.position',
        'jquery/development-bundle/ui/jquery.ui.autocomplete',
        'jquery/development-bundle/ui/jquery.ui.draggable',
        'jquery/development-bundle/ui/jquery.ui.resizable',
        'jquery/development-bundle/ui/jquery.ui.dialog',
        'jquery/development-bundle/ui/jquery.ui.datepicker'

    )
);
?>
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){
    $('#documents').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true });
});
/* ]]> */
</script>
<style type="text/css">
    table tr td {
        border-bottom: 1px solid #DFDFDF;
        padding: 10px;
        vertical-align: top !important;
    }
</style>
<div class="users form striped">
    <div class="clearfix hasRightCol" id="contentCol">
        <div class="profileHeaderMain" id="documents">
            <?php  $this->Layout->sessionFlash();?>
            <ul>
                <li><a href="#attendance_sheets"><span>Attendance Sheets</span></a></li>
                <li><a href="#certificates"><span>Certificates</span></a></li>
                <li><a href="#directory_of_participants"><span>Directory Of Participants</span></a></li>
                <li><a href="#name_tags"><span>Name Tags</span></a></li>
            </ul>
            <div id="attendance_sheets" class="striped">
                <table width="700px">
                    <tr>
                        <td width="50%" vertical-align="top">
                            <form name="AttendanceSheetsParticipants" id="AttendanceSheetsParticipants" method="post" accept-charset="utf-8" action="<?php echo $this->Html->url(array("admin"=>true, "controller" => "programs" ,"action"=>"add_attendance_sheets"));?>">
                                <input type="hidden" name="data[Program][who_created]" value="<?php echo $this->Session->read('Auth.User.id');?>" />
                                <input type="hidden" name="data[Program][id]" value="<?php echo $program_id;?>" />
                                <table valign="top">
                                    <tr><th colspan="3">Participants</th></tr>
                                    <?php
                                    if( count($participants) >0 ){
                                        foreach( $participants as $participant ){
                                    ?>
                                    <tr><td><input type="checkbox" name="data[AttendanceSheetsParticipants][participant_id][]" value="<?php echo $participant['Participant']['id'].'_'.$participant['Company']['id'];?>" id="attendance_sheets_<?php echo $participant['Participant']['id'];?>" /></td><td><label for="attendance_sheets_<?php echo $participant['Participant']['id'];?>"><?php echo $participant['Participant']['full_name'];?></label></td><td><?php echo $participant['Company']['name'];?></td></tr>
                                    <?php
                                        }
                                    }
                                    ?>
                                </table>
                                <input type="submit" name="Add" value="Add" class="btn_add_info" /><input type="reset" value="Reset" />
                            </form>
                        </td>
                        <td width="50%" vertical-align="top">
                            <table><tr><th>Attendance Sheets</th><th>Created By/Date</th></tr>
                            <?php
                            if( isset($attendancesheets) && is_array($attendancesheets) ){
                                //$attendance_sheet_directory = $this->AttendanceSheets->getAttendanceSheetsDirectory(intval($program_id));
                                foreach( $attendancesheets as $attendancesheet ){
                                    //$source = $attendance_sheet_directory.trim($attendancesheet['ProgramsAttendanceSheet']['source_file']);
                                   // if(file_exists($source)){
                                        ?>
                                        <tr><td width="50%"><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'attendance_sheets','action'=>'download',$attendancesheet['ProgramsAttendanceSheet']['id']));?>"><?php echo trim($attendancesheet['ProgramsAttendanceSheet']['source_file']);?></a></td><td width="50%"><?php echo $users[$attendancesheet['ProgramsAttendanceSheet']['who_created']];?><br><?php echo date("j",strtotime(trim($attendancesheet['ProgramsAttendanceSheet']['date_created']))).'<sup>'.date("S",strtotime(trim($attendancesheet['ProgramsAttendanceSheet']['date_created']))).'</sup>&nbsp;'.date("M Y",strtotime(trim($attendancesheet['ProgramsAttendanceSheet']['date_created']))).' '.date("h:i A",strtotime(trim($attendancesheet['ProgramsAttendanceSheet']['date_created'])));?></td></tr>
                                        <?php
                                   //}
                                }
                            }
                            ?>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="certificates" class="striped">
                <table width="700px">
                    <tr>
                        <td width="50%" vertical-align="top">
                            <form name="CertificatesParticipants" id="CertificatesParticipants" method="post" accept-charset="utf-8" action="<?php echo $this->Html->url(array("admin"=>true, "controller" => "programs" ,"action"=>"add_certificates"));?>">
                                <input type="hidden" name="data[Program][who_created]" value="<?php echo $this->Session->read('Auth.User.id');?>" />
                                <input type="hidden" name="data[Program][id]" value="<?php echo $program_id;?>" />
                                <table>
                                    <tr><th colspan="3">Participants</th></tr>
                                    <?php
                                    if( count($participants) >0 ){
                                        foreach( $participants as $participant ){
                                            ?>
                                            <tr><td><input type="checkbox" name="data[CertificatesParticipants][participant_id][]" value="<?php echo $participant['Participant']['id'].'_'.$participant['Company']['id'];?>" id="certificates_<?php echo $participant['Participant']['id'];?>" /></td><td><label for="certificates_<?php echo $participant['Participant']['id'];?>"><?php echo $participant['Participant']['full_name'];?></label></td><td><?php echo $participant['Company']['name'];?></td></tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </table>
                                <input type="submit" name="Add" value="Add" class="btn_add_info" /><input type="reset" value="Reset" />
                            </form>
                        </td>
                        <td width="50%" vertical-align="top">
                            <table><tr><th>Certificates</th><th>Created By/Date</th></tr>
                                <?php
                                if( isset($certificates) && is_array($certificates) ){
                                    //$certificates_directory = $this->Certificates->getCertificatesDirectory(intval($program_id));
                                    foreach( $certificates as $certificate ){
                                        //$source = $certificates_directory.trim($certificate['ProgramsAttendanceSheet']['source_file']);
                                        //if(file_exists($source)){
                                            ?>
                                            <tr><td width="50%"><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'certificates','action'=>'download',$certificate['ProgramsCertificate']['id']));?>"><?php echo trim($certificate['ProgramsCertificate']['source_file']);?></a></td><td width="50%"><?php echo $users[$certificate['ProgramsCertificate']['who_created']];?><br><?php echo date("j",strtotime(trim($certificate['ProgramsCertificate']['date_created']))).'<sup>'.date("S",strtotime(trim($certificate['ProgramsCertificate']['date_created']))).'</sup>&nbsp;'.date("M Y",strtotime(trim($certificate['ProgramsCertificate']['date_created']))).' '.date("h:i A",strtotime(trim($certificate['ProgramsCertificate']['date_created'])));?></td></tr>
                                            <?php
                                        //}
                                    }
                                }
                                ?>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="directory_of_participants" class="striped">
                <table width="700px">
                    <tr>
                        <td width="50%" vertical-align="top">
                            <form name="DirectoriesOfParticipants" id="DirectoriesOfParticipants" method="post" accept-charset="utf-8" action="<?php echo $this->Html->url(array("admin"=>true, "controller" => "programs" ,"action"=>"add_dop"));?>">
                                <input type="hidden" name="data[Program][who_created]" value="<?php echo $this->Session->read('Auth.User.id');?>" />
                                <input type="hidden" name="data[Program][id]" value="<?php echo $program_id;?>" />
                                <table>
                                    <tr><th colspan="3">Participants</th></tr>
                                    <?php
                                    if( count($participants) >0 ){
                                        foreach( $participants as $participant ){
                                            ?>
                                            <tr><td><input type="checkbox" name="data[DirectoriesOfParticipants][participant_id][]" value="<?php echo $participant['Participant']['id'].'_'.$participant['Company']['id'];?>" id="dop_<?php echo $participant['Participant']['id'];?>" /></td><td><label for="dop_<?php echo $participant['Participant']['id'];?>"><?php echo $participant['Participant']['full_name'];?></label></td><td><?php echo $participant['Company']['name'];?></td></tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </table>
                                <input type="submit" name="Add" value="Add" class="btn_add_info" /><input type="reset" value="Reset" />
                            </form>
                        </td>
                        <td width="50%" vertical-align="top">
                            <table><tr><th>Directory Of Participants</th><th>Created By/Date</th></tr>
                                <?php
                                if( isset($directoryparticipants) && is_array($directoryparticipants) ){
                                    //$dop_directory = $this->ParticipantDirectory->getProgramDirectoryOfParticipantDirectory(intval($program_id));
                                    foreach( $directoryparticipants as $directoryparticipant ){
                                        //$source = $dop_directory.trim($directoryparticipant['ProgramsParticipantDirectory']['source_file']);
                                        //if(file_exists($source)){
                                            ?>
                                            <tr><td width="50%"><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'directory_of_participants','action'=>'download',$directoryparticipant['ProgramsParticipantDirectory']['id']));?>"><?php echo trim($directoryparticipant['ProgramsParticipantDirectory']['source_file']);?></a></td><td width="50%"><?php echo $users[$directoryparticipant['ProgramsParticipantDirectory']['who_created']];?><br><?php echo date("j",strtotime(trim($directoryparticipant['ProgramsParticipantDirectory']['date_created']))).'<sup>'.date("S",strtotime(trim($directoryparticipant['ProgramsParticipantDirectory']['date_created']))).'</sup>&nbsp;'.date("M Y",strtotime(trim($directoryparticipant['ProgramsParticipantDirectory']['date_created']))).' '.date("h:i A",strtotime(trim($directoryparticipant['ProgramsParticipantDirectory']['date_created'])));?></td></tr>
                                            <?php
                                       // }
                                    }
                                }
                                ?>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="name_tags" class="striped">
                <table width="700px">
                    <tr vertical-align="top">
                        <td width="50%" vertical-align="top">
                            <form name="NameTagsParticipants" id="NameTagsParticipants" method="post" accept-charset="utf-8" action="<?php echo $this->Html->url(array("admin"=>true, "controller" => "programs" ,"action"=>"add_name_tags"));?>">
                                <input type="hidden" name="data[Program][who_created]" value="<?php echo $this->Session->read('Auth.User.id');?>" />
                                <input type="hidden" name="data[Program][id]" value="<?php echo $program_id;?>" />
                                <table>
                                    <tr><th colspan="3">Participants</th></tr>
                                    <?php
                                    if( count($participants) >0 ){
                                        foreach( $participants as $participant ){
                                    ?>
                                        <tr><td><input type="checkbox" name="data[NameTagsParticipants][participant_id][]" value="<?php echo $participant['Participant']['id'].'_'.$participant['Company']['id'];?>" id="name_tags_<?php echo $participant['Participant']['id'];?>" /></td><td><label for="name_tags_<?php echo $participant['Participant']['id'];?>"><?php echo $participant['Participant']['full_name'];?></label></td><td><?php echo $participant['Company']['name'];?></td></tr>
                                    <?php
                                        }
                                    }
                                    ?>
                                </table>
                                <input type="submit" name="Add" value="Add" class="btn_add_info" /><input type="reset" value="Reset" />
                            </form>
                        </td>
                        <td width="50%" vertical-align="top">
                            <table>
                                <tr><th>NameTags</th><th>Created By/Date</th></tr>
                                <?php
                                if( is_array($nametags) && isset($nametags)){
                                    //$name_tag_directory = $this->NameTags->getNameTagDirectory(intval($program_id));
                                    foreach( $nametags as $nametag ){
                                       // $source = $name_tag_directory.trim($nametag['ProgramsNameTag']['source_file']);
                                       // if(file_exists($source)){
                                ?>
                                        <tr><td width="50%"><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'name_tags','action'=>'download',$nametag['ProgramsNameTag']['id']));?>"><?php echo trim($nametag['ProgramsNameTag']['source_file']);?></a></td><td width="50%"><?php echo $users[$nametag['ProgramsNameTag']['who_created']];?><br><?php echo date("j",strtotime(trim($nametag['ProgramsNameTag']['date_created']))).'<sup>'.date("S",strtotime(trim($nametag['ProgramsNameTag']['date_created']))).'</sup>&nbsp;'.date("M Y",strtotime(trim($nametag['ProgramsNameTag']['date_created']))).' '.date("h:i A",strtotime(trim($nametag['ProgramsNameTag']['date_created'])));;?></td></tr>
                                <?php
                                       // }
                                    }
                                }
                                ?>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>