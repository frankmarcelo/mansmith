<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){
	<?php 
	if(isset($selected_program) && $selected_program>0 ){
	?>
	$('#programs').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true,selected:<?php echo $selected_program;?>});
	<?php	
	}else{	
	?>
	$('#programs').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true});
	<?php 
	}
	?>
});
/* ]]> */
</script>
<?php
echo $this->Html->css(array(
    'jquery.autocomplete',
    'fcbkselection'
    )
);

echo $this->Html->script(array(
    'jquery/jquery.highlight'
	)
);
?>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&region=PH"></script>
<script type="text/javascript">
/* <![CDATA[ */
function invokeMap( element, x, y) {
	var myLatlng = new google.maps.LatLng(x,y);
    var myOptions = {
        zoom: 14,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        navigationControl: true,
        mapTypeControl: true,
        disableDefaultUI: true,
        navigationControl: true,
        mapTypeControlOptions: {
           style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
           position: google.maps.ControlPosition.TOP_LEFT
        },
        streetViewControl: false
    }
 
    var map = new google.maps.Map( document.getElementById(element) , myOptions);
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map
    });
    map.setCenter(myLatlng);
}

if(!String.prototype.startsWith){
    String.prototype.startsWith = function (str) {
        return !this.indexOf(str);
    }
}

$(document).ready(function(){
	<?php 
	$qValue = null;
	if (isset($this->params['url']['q']) && strlen($this->params['url']['q'])> 0) {
   		$qValue = $this->params['url']['q'];
	}

	if (isset($this->params['url']['division']) && strlen($this->params['url']['division'])> 0) {
   		$qValue = $this->params['url']['division'];
	}
	
	if( !is_null($qValue) ){
		$aKeywordsSearch = explode(" ",$qValue);
		$i = 0;
		foreach( $aKeywordsSearch as $keyword ){
			if( strlen(trim($keyword)) >= 3 ){
				if( $i < 1 ){
					echo "$('.listing').removeHighlight().highlight('".addslashes($keyword)."');";
				}else{
					echo "$('.listing').highlight('".addslashes($keyword)."');";
				}
				$i++;
			}
		}
	}
	
	if( isset($program_type) ){
	?>
	$('.listing').highlight(<?php echo "'".addslashes($program_type)."'";?>);
	<?php 
	}
	?>
	
	$('.delete').live('click',function(){
		var title = '<strong><a target="_blank" href="'+$(this).attr('seouri')+'">'+$.trim($(this).attr('title'))+'</a></strong>';
		var dialog_message ='<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>';
		dialog_message +=title+' will be permanently deleted and cannot be recovered. Continue?</p><br />';
		dialog_message +='<p align="center"><button style="cursor:pointer;" class="ui-state-default ui-corner-all" type="button" id="cancel">Cancel</button>&nbsp;';
		dialog_message +='<button style="cursor:pointer;" seo="'+$(this).attr('seo')+'" seouri="'+$(this).attr('seouri')+'" class="ui-state-default ui-corner-all" style="font-color:#F0F0F0;" type="button" id="confirm_delete">Confirm delete</button>&nbsp;</p>';
		$("#confirm_msg").empty().html(dialog_message);
		$('#dialog').dialog({modal:true,hide:'explode'});
	});
	
	$("#confirm_delete").live("click",function(){
		$("#cancel").attr('disabled',true);
		$(this).empty().html("Deleting program. Please wait...").attr('disabled',true);
		var seo_uri = $.trim($(this).attr('seo'));
        this.timer = setTimeout(function (){
        	$.ajax({
            	cache: false  ,
                type:  "GET" ,
                url:   "<?php echo Configure::read('js_directory');?>delete_program.php?id="+seo_uri+'&who_modified=<?php echo $this->Session->read('Auth.User.id');?>',
                dataType: "json" ,
                success: function(data){
                	$("#dialog").dialog('close');
                    window.location.reload();
                }
            });
        },1000);
    });
	
	$("#cancel").live("click",function(){
		$("#dialog").dialog('close');
	});
	
	$("#q").focus();
	
	$(window).resize(function() {
	    $("#dialog").dialog("option", "position", "center");
	});
	
	$(window).scroll(function() {
	    $("#dialog").dialog("option", "position", "center");
	});
	
    $('.listing a').live('click',function(){
        var string_match = new String( $(this).attr('id') );

		var string_found = string_match.search(/map/i);
        var string_match_data = string_match.split('_');

        if( string_found > 0 ){
           if( !$(this).attr('show_div') ){
              $(this).attr('show_div','show_div');
              var geocodes = new String( $(this).attr('alt') ); 
              var xy = geocodes.split('_');

              if( string_match.startsWith('all') ){
                  $('#all_programs_map_canvas_'+parseInt(string_match_data[4],10)).fadeIn("fast", function(){
                      $(this).show();
                  });
                  invokeMap( 'all_programs_map_canvas_'+ parseInt(string_match_data[4]) , xy[0], xy[1] );        

              }else{
				  $('#'+string_match_data[0]+'_map_canvas_'+parseInt(string_match_data[3],10)).fadeIn("fast", function(){
                      $(this).show();
                  });
                  invokeMap(string_match_data[0]+'_map_canvas_'+parseInt(string_match_data[3],10), xy[0], xy[1] );	
			  }
		   }else{
              $(this).removeAttr('show_div');

              if( string_match.startsWith('all') ){
			  	  $('#all_programs_map_canvas_'+parseInt(string_match_data[4],10)).fadeOut("slow", function(){
                  	$(this).hide();
                  });
              }else{
            	  $('#'+string_match_data[0]+'_map_canvas_'+parseInt(string_match_data[3],10)).fadeOut("slow", function(){
                    	$(this).hide();
                  });
              }
		   }
        }
      
        var rateplan_found = string_match.search(/rate/i);
        if( rateplan_found > 0 ){
           if( !$(this).attr('show_div') ){
              $(this).attr('show_div','show_div');
			  if( string_match.startsWith('all') ){ 
              	 $('#all_programs_rate_plans_'+parseInt(string_match_data[5],10)).fadeIn("fast", function(){
                    $(this).show();
                 });
              }else{
            	 $('#'+string_match_data[0]+'_rate_plans_'+parseInt(string_match_data[4],10)).fadeIn("fast", function(){
                      $(this).show();
                 });
              }
           }else{
              $(this).removeAttr('show_div');
              if( string_match.startsWith('all') ){
				 $('#all_programs_rate_plans_'+parseInt(string_match_data[5],10)).fadeOut("slow", function(){
                    $(this).hide();
                 });
              }else{
            	 $('#'+string_match_data[0]+'_rate_plans_'+parseInt(string_match_data[4],10)).fadeOut("slow", function(){
                      $(this).hide();
                 }); 	
              }
           }
        } 

        var attendees_found = string_match.search(/companies/i);
        if( attendees_found> 0 ){
           if( !$(this).attr('show_div') ){
              $(this).attr('show_div','show_div');
			  if( string_match.startsWith('all') ){ 
              	 $('#all_programs_attending_companies_'+parseInt(string_match_data[3],10)).fadeIn("fast", function(){
                    $(this).show();
                 });
              }else{
            	 $('#'+string_match_data[0]+'_programs_attending_companies_'+parseInt(string_match_data[3],10)).fadeIn("fast", function(){
                      $(this).show();
                 });
              }
           }else{
              $(this).removeAttr('show_div');
              if( string_match.startsWith('all') ){
				 $('#all_programs_attending_companies_'+parseInt(string_match_data[3],10)).fadeOut("slow", function(){
                    $(this).hide();
                 });
              }else{
            	 $('#'+string_match_data[0]+'_programs_attending_companies_'+parseInt(string_match_data[3],10)).fadeOut("slow", function(){
                      $(this).hide();
                 }); 	
              }
           }
        }
    });
});
/* ]]> */
</script>
<style type="text/css">
.listing .box1 {
    float:left !important;
    padding:0 0 0 6px !important;
    width:192px !important;
}
.listing .box2 {
    float: left !important;
    text-align: left !important;
    width: 162px !important;
}

.listing .box3 {
    float: left !important;
    text-align: left !important;
    width: 168px !important;
}

.listing .box5 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 151px !important;
}

.listing .box6 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 128px !important;
}

.listing .box7 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 111px !important;
}
</style>
<div id="dialog" title="Delete Program" style="display:none;"><div id="confirm_msg">&nbsp;</div></div>
<?php
if( isset($this->params['url']['q']) ){
    $this->Html->addCrumb('Programs', '/admin/programs');
    $this->Html->addCrumb('Search Programs', '/admin/programs/search?q='.$this->params['url']['q']);
    $this->Html->addCrumb('<strong>'.trim($this->params['url']['q']).'</strong>');
}else{
    $this->Html->addCrumb('Programs', '/admin/programs');
    $this->Html->addCrumb('<strong>Search Programs</strong>');
}
?>
<div class="users index">
    <h2><?php __('Search Programs');?></h2>
    <?php echo $this->element('program/search_advance_program'); ?>
    <div>
       <div id="programs">
            <ul>
             <?php 
             if(empty($programs)){
             ?>
             <li><a href="#search_view"><span>Empty result</span></a></li>
             <?php 
             }else{
             	$qValue .= (isset($program_type))? '&nbsp;&nbsp;Program Type:&nbsp;'.$program_type: null;
            	$qValue .= (empty($qValue) && isset($schedule_date)) ? '&nbsp;&nbsp;Schedule Date:&nbsp;'.$schedule_date: null;
				if( is_null($qValue) || strlen(trim($qValue)) <1 || empty($qValue) ){
			 ?>
			 <li><a href="#search_view"><span>Search Programs</span></a></li>
			 <?php
				}else{		
             ?>
             <li><a href="#search_view"><span>Search results for <?php echo '<strong>'.$qValue.'</strong>';?></span></a></li>
             <?php
             	}
             } 
             ?>
            </ul>
            <div id="search_view">

              		<table cellspacing="0" cellpadding="0" style="background-color:#FFFFFF;border:1px solid #DDDDDD;clear:both;width:100%;">

              			<?php
                    	if(!empty($programs)){

              			$sUrl = '/'; 
              			if(!empty($programs) && sizeof($programs)> 0 ){
              			?>
    					<tr><td align="center" colspan="7">
    						<div class="paging">
		                 	<?php
		                 	if( isset($this->params['url']['q']) ){
		                 		$sUrl = '../search?q='.$qValue;
		                 		if( isset($this->params['url']['type']) ){
		                 			$sUrl .= '&type='.$this->params['url']['type'];
		                 		}
		                 		if( isset($this->params['url']['search_date']) ){
		                 			$sUrl .= '&search_date='.$this->params['url']['search_date'];
		                 		}
		                 	}

		                 	$paginator->options(array('url' => $sUrl )); 
		                 	$sPaginator = $paginator->first('<< First').' '.
							$paginator->prev('< Previous') .' '. 
							$paginator->numbers().' '.
							$paginator->next('Next >'). ' '.
							$paginator->last('Last >>');
		                 	$sPaginator = str_replace("MansmithProgram.","",$sPaginator); 
		                 	echo str_replace("/page:","&page=",$sPaginator)."<br />";
		                 	echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true))); 
		                 	?>
	                 		</div>
	                 	</td></tr>
	                 	<?php 
              			}
	                 	?>
	                 	<tr>
                            <th style="border-right: 1px solid #DFDFDF;width:150px;"><?php echo $paginator->sort('Sort By Title','title');?></th>
                            <th style="border-right: 1px solid #DFDFDF;width:115px;">Schedule</th>
                            <th style="border-right: 1px solid #DFDFDF;width:110px;">Sort <?php echo $paginator->sort('Division','programs_division_id');?>/<?php echo $paginator->sort('Type','programs_type_id');?></th>
                            <th style="border-right: 1px solid #DFDFDF;width:100px;">Speaker</th>
                            <th style="border-right: 1px solid #DFDFDF;width:90px;">Total</th>
                            <th style="border-right: 1px solid #DFDFDF;width:80px;">Discount<br />Applies</th>
                            <th style="border-right: 1px solid #DFDFDF;width:90px;">Notes</th>
	                 	</tr>
	                	<tr><td colspan="7">
              			<?php 
                  		$i = 0;
                   		foreach( $programs as $program_key => $program_info ){
                    		$this->Program->setProgramData( $program_info['Program'] );
                      		$this->ProgramExtraInfo->setExtraInfo( $program_info ); 
                      		$sCss = ( ($i % 2) ==0 )? 'listing_accomm':'listing_tr';
             			 ?>
                          <div class="listing" id="<?php echo $sCss;?>">
                                <div class="box1" id="all_programs_<?php echo $this->Program->get('id');?>">
                                	<div>
                                    	<h3><a href="<?php echo $this->Html->url(array('controller'=>'programs','action'=>'info',$this->Program->getSeoUri()));?>"><?php echo $this->Program->getTitle();?></a></h3>
                                        <div><address><?php echo $this->Program->getFullAddress();?></address></div>
                                    </div>
                                </div><!--end box1-->
                                <div class="box2"><div><?php echo $this->ProgramExtraInfo->getSchedule($shortcut=true);?></div></div>
                                <div class="box3"><div><a href="<?php echo $this->Html->url(array('controller'=>'programs','action'=>'list',$this->ProgramExtraInfo->getProgramDivision()));?>"><?php echo $this->ProgramExtraInfo->getProgramDivision();?></a> | <span class="tiny"><?php echo $this->ProgramExtraInfo->getProgramType();?></span></div><!--end box3--></div>
                                <div class="box5"><div><?php echo $this->ProgramExtraInfo->getProgramSpeakers($speakers);?></div></div>
                                <div class="box6"><div align="left"><?php echo $this->Program->getTotalProgramClientParticipants().'<br />'.$this->Program->getTotalProgramParticipants().'<br />'.$this->Program->getTotalEarnings().'<br />'.$this->Program->getTotalBalanceOwing();?></div></div>
                                <div class="box7"><div align="left"><?php echo $this->Program->getDiscountApplies();?></div></div>
                                <div class="box8"><div align="left"><em><?php echo $this->Program->getNotes();?></em></div></div>
                                <?php
                                if( $this->Program->get('total_participants') > 0|| $this->Program->get('total_companies') >0){
                                ?>
                                <div class="box4b"><?php echo $this->ProgramExtraInfo->getLatestCompany();?>&nbsp;<?php echo $this->ProgramExtraInfo->getLatestParticipant();?></div>
                                <?php
                                }
								?>
								<div class="box4">
                                	<a class="edit" style="cursor:pointer;" id="all_programs_view_rate_plans_<?php echo $this->Program->get('id');?>">View RatePlans</a>&nbsp;
                                	<a class="edit" style="cursor:pointer;" id="all_programs_view_map_<?php echo $this->Program->get('id');?>" alt="<?php echo $this->Program->getLatitude().'_'.$this->Program->getLongitude();?>">View Map</a>&nbsp;
                                	<a class="edit_full_details" href="<?php echo $this->Html->url(array('controller'=>'programs','action'=>'info',$this->Program->getSeoUri()));?>">View Full Details</a>

	                                <?php
	                                echo $this->Program->getDisplayDelete('all_programs');
									echo $this->Program->getDisplayEdit();
									echo $this->Program->getAddParticipants();

                                	if( $this->Program->get('total_companies') > 0){
                                	?>
	                                <a class="edit" style="cursor:pointer;" id="all_programs_companies_<?php echo $this->Program->get('id');?>">View Attending Companies</a>
	                                <?php
	                                }
									?>
                                    <?php echo $this->ProgramExtraInfo->createdBy($users);?>
                        		</div><!--end box4-->
                                <div class="clearboth"></div>
                                <div id="all_programs_rate_plans_<?php echo $this->Program->get('id');?>" style="display:none;">
                                	<?php echo $this->ProgramExtraInfo->getProgramRatePlans($rateplans);?>
                                </div>
                                <div id="all_programs_attending_companies_<?php echo $this->Program->get('id');?>" style="display:none;">
                                	<?php echo $this->ProgramExtraInfo->getAttendingCompanies();?>
                                </div>
                                <div id="all_programs_map_canvas_<?php echo $this->Program->get('id');?>" style="display:none;" class="map_canvas"></div>
                          </div><!--end listing-->
						<?php 
                     		$i++;
                  		}
                        ?>
                  	</td></tr>
                  	<?php
                    }

               	  	if(!empty($programs) && sizeof($programs)> 0){
               	  	?>
    				<tr><td align="center" colspan="7" class="striped">
    				<div class="paging">
    	            <?php
	                if( isset($this->params['url']['q']) ){
		            	$sUrl = '../search?q='.$qValue;
		                if( isset($this->params['url']['type']) ){
		                	$sUrl .= '&type='.$this->params['url']['type'];
		                }
		                if( isset($this->params['url']['search_date']) ){
		                	$sUrl .= '&search_date='.$this->params['url']['search_date'];
		                }
		            }

		            $paginator->options(array('url' => $sUrl )); 
		            $sPaginator = $paginator->first('<< First').' '.$paginator->prev('< Previous') .' '. $paginator->numbers().' '.$paginator->next('Next >'). ' '.$paginator->last('Last >>');
		            $sPaginator = str_replace("MansmithProgram.","",$sPaginator); 
		            echo str_replace("/page:","&page=",$sPaginator)."<br />";
		            echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true))); 
                 	?>
                 	</div>
                 	</td></tr>
                 	<?php 
               	  	}
                 	?>
                 </table>        

				</div>
       </div> 
</div>
