<?php
echo $this->Html->css(array('fcbkselection'));
echo $this->Html->script(array(
        'jquery/jquery.bgiframe.min',
        'jquery/jquery.ajaxQueue',
        'jquery/jquery.fcbkselection',
        'jquery/jquery.simplemodal',
        'jquery/jquery.tinymce',
        'tiny_mce/tiny_mce',
        'jquery/jquery.maskMoney',
        'jquery/development-bundle/ui/jquery.ui.core',
        'jquery/development-bundle/ui/jquery.ui.widget',
        'jquery/development-bundle/ui/jquery.ui.position',
        'jquery/development-bundle/ui/jquery.ui.autocomplete',
        'jquery/development-bundle/ui/jquery.ui.draggable',
        'jquery/development-bundle/ui/jquery.ui.resizable',
        'jquery/development-bundle/ui/jquery.ui.dialog',
        'jquery/development-bundle/ui/jquery.ui.datepicker'

    )
);
?>
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){
    $('#documents').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true });
    $('#companies').live('change',function(){
        var companies_id = $(this).val().split("|");
        $('#program_billing_summary').attr('src',companies_id[0]);
        $('#iframe_billing_eb').attr('src',companies_id[1]);
        $('#iframe_billing_non_eb').attr('src',companies_id[2]);
        $('#iframe_billing_regular').attr('src',companies_id[3]);
        $('#iframe_billing_on_site').attr('src',companies_id[4]);
        $('#iframe_billing_invoice').attr('src',companies_id[5]);
    });
});
/* ]]> */
</script>
<style type="text/css">
    table tr td {
        border-bottom: 1px solid #DFDFDF;
        padding: 10px;
        vertical-align: top !important;
    }
</style>
<div class="users form striped">
    <div class="clearfix hasRightCol" id="contentCol">
        <div class="profileHeaderMain" id="documents">
            <?php  $this->Layout->sessionFlash();?>
            <table>
                <tr><td>
                    <?php
                    if( $companies ){
                    ?>
                        <select id="companies" name="data[ProgramsParticipants][company_id]">
                        <option value="<?php echo $this->Html->url(array('controller'=>'programs','admin'=>true,'action'=>'programs_billing_summary', $program_id));?>|<?php echo $this->Html->url(array('controller'=>'programs','admin'=>true,'action'=>'programs_billing_eb', $program_id));?>|<?php echo $this->Html->url(array('controller'=>'programs','admin'=>true,'action'=>'programs_billing_non_eb', $program_id));?>|<?php echo $this->Html->url(array('controller'=>'programs','admin'=>true,'action'=>'programs_billing_regular', $program_id));?>|<?php echo $this->Html->url(array('controller'=>'programs','admin'=>true,'action'=>'programs_billing_on_site', $program_id));?>">Select a company</option>
                        <?php
                        foreach( $companies as $company ){
                        ?><option value="<?php echo $this->Html->url(array('controller'=>'programs','admin'=>true,'action'=>'programs_billing_summary', $program_id.'/'));?>/<?php echo $company['Company']['id'];?>|<?php echo $this->Html->url(array('controller'=>'programs','admin'=>true,'action'=>'programs_billing_eb', $program_id.'/'));?>/<?php echo $company['Company']['id'];?>|<?php echo $this->Html->url(array('controller'=>'programs','admin'=>true,'action'=>'programs_billing_non_eb', $program_id.'/'));?>/<?php echo $company['Company']['id'];?>|<?php echo $this->Html->url(array('controller'=>'programs','admin'=>true,'action'=>'programs_billing_regular', $program_id.'/'));?>/<?php echo $company['Company']['id'];?>|<?php echo $this->Html->url(array('controller'=>'programs','admin'=>true,'action'=>'programs_billing_on_site', $program_id.'/'));?>/<?php echo $company['Company']['id'];?>|<?php echo $this->Html->url(array('controller'=>'programs','admin'=>true,'action'=>'programs_invoice', $program_id.'/'));?>/<?php echo $company['Company']['id'];?>"><?php echo $company['Company']['name'];?></option>
                        <?php
                        }
                        ?>
                        </select>
                    <?php
                    }
                    ?>
                    </td></tr>
                <tr><td width="50%">
                    <iframe id="program_billing_summary" src="<?php echo $this->Html->url(array('controller'=>'programs','admin'=>true,'action'=>'programs_billing_summary', $program_id.'/'));?>" width="100%" height="472px" border="0"></iframe>

                </td></tr>
                <tr>
                    <td>
                        <ul>
                            <li><a href="#billing_eb"><span>Billing Eb</span></a></li>
                            <li><a href="#billing_non_eb"><span>Billing Non Eb</span></a></li>
                            <li><a href="#regular"><span>Regular</span></a></li>
                            <li><a href="#on_site"><span>On Site</span></a></li>
                            <li><a href="#invoice"><span>Invoice</span></a></li>
                        </ul>
                        <div id="billing_eb" class="striped">
                            <iframe id="iframe_billing_eb" src="<?php echo $this->Html->url(array('controller'=>'programs','admin'=>true,'action'=>'programs_billing_eb', $program_id.'/'));?>" width="100%" height="970px" border="0"></iframe>
                        </div>
                        <div id="billing_non_eb" class="striped">
                            <iframe id="iframe_billing_non_eb" src="<?php echo $this->Html->url(array('controller'=>'programs','admin'=>true,'action'=>'programs_billing_non_eb', $program_id.'/'));?>" width="100%" height="970px" border="0"></iframe>
                        </div>
                        <div id="regular" class="striped">
                            <iframe id="iframe_billing_regular" src="<?php echo $this->Html->url(array('controller'=>'programs','admin'=>true,'action'=>'programs_billing_regular', $program_id.'/'));?>" width="100%" height="970px" border="0"></iframe>
                        </div>
                        <div id="on_site" class="striped">
                            <iframe id="iframe_billing_on_site" src="<?php echo $this->Html->url(array('controller'=>'programs','admin'=>true,'action'=>'programs_billing_on_site', $program_id.'/'));?>" width="100%" height="970px" border="0"></iframe>
                        </div>
                        <div id="invoice" class="striped">
                            <iframe id="iframe_billing_invoice" src="<?php echo $this->Html->url(array('controller'=>'programs','admin'=>true,'action'=>'programs_invoice', $program_id.'/'));?>" width="100%" height="943px" border="0"></iframe>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>