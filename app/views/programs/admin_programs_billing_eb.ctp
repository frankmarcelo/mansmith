<style type="text/css">
    table tr td {
        border-bottom: 1px solid #DFDFDF !important;
        padding: 10px !important;
        vertical-align: top !important;
    }
</style>
<?php  $this->Layout->sessionFlash();?>
<form name="BillingEb" id="BillingEb" method="post" accept-charset="utf-8" action="<?php echo $this->Html->url(array("admin"=>true, "controller" => "programs" ,"action"=>"add_programs_billing_eb"));?>">
    <input type="hidden" name="data[Program][who_created]" value="<?php echo $this->Session->read('Auth.User.id');?>" />
    <input type="hidden" name="data[Program][id]" value="<?php echo $program_id;?>" />
<table width="700px">
    <tr>
        <td width="50%" valign="top">
                <table valign="top">
                    <tr><th colspan="3">Billing Computation</th></tr>
                    <tr><td colspan="2">Program Discount</td><td><?php  echo ($oProgram['Program']['discount_applies']==1)? 'On':'Off';?></td></tr>
                    <tr><td colspan="2">Group Rates</td><td><?php  echo $group_discount;?></td></tr>
                    <tr><td colspan="2">Early Bird CutOff</td><td><?php echo date("F j, Y",strtotime($oProgram['ProgramsRatePlan'][0]['cut_off_date']));?></td></tr>
                    <tr><td colspan="3">
                        <table>
                            <tr><th>Early Bird Rate</th></tr>
                            <tr><td><?php echo $ebDisplay;?></td></tr>
                        </table>
                    </td></tr>
                    <tr><td colspan="3">
                        <table>
                            <tr><th>Regular Rate</th></tr>
                            <tr><td><?php echo $regularDisplay;?></td></tr>
                        </table>
                    </td></tr>
                    <tr><td colspan="3">
                        <table>
                            <tr><th>On-Site Rate</th></tr>
                            <tr><td><?php echo $ospDisplay;?></td></tr>
                        </table>
                    </td></tr>
                </table>


        </td>
        <td width="50%" valign="top">
            <table vertical-align="top" valign="top">
                <tr valign="top" vertical-align="top">
                    <th valign="top" width="50%">Billing (with early bird)</th>
                    <th valign="top">Created By/Date</th>
                </tr>
                <?php
                if( isset($billing_eb) ){
                    foreach( $billing_eb as $billing_info ){
                ?>
    <tr><td width="50%">
        <a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'billing_eb','action'=>'download',$billing_info['BillingEb']['id']));?>"><?php echo trim($billing_info['BillingEb']['source_file']);?></a></td><td width="50%"><?php echo $users[$billing_info['BillingEb']['who_created']];?><br>
        <?php echo date("j",strtotime(trim($billing_info['BillingEb']['date_created']))).'<sup>'.
            date("S",strtotime(trim($billing_info['BillingEb']['date_created']))).'</sup>&nbsp;'.
            date("M Y",strtotime(trim($billing_info['BillingEb']['date_created']))).' '.
            date("h:i A",strtotime(trim($billing_info['BillingEb']['date_created'])));?></td></tr>
                <?php
                    }
                }
                ?>
                <tr><td>Billing Notes</td><td><textarea rows="10" cols="40" name="data[BillingEb][notes]"></textarea></td></tr>
                <tr><td colspan="2"> <?php
                    if( isset($company_id) && $totalParticipants > 0 ){
                        ?>
                        <input type="hidden" name="data[BillingEb][company_id]" value="<?php echo $company_id;?>" />
                        <input type="submit" name="Add" value="Add" class="btn_add_info" />
                        <?php
                    }
                    ?></td></tr>
            </table>

        </td>
    </tr>

</table>
</form>