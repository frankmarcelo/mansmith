<?php echo $this->element('program/program_default_js'); ?>
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){
   $('#programs').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true});
});
/* ]]> */
</script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&region=PH"></script>
<script type="text/javascript">
/* <![CDATA[ */
function invokeMap( element, x, y) {
	var myLatlng = new google.maps.LatLng(x,y);
    var myOptions = {
        zoom: 14,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        navigationControl: true,
        mapTypeControl: true,
        disableDefaultUI: true,
        navigationControl: true,
        mapTypeControlOptions: {
           style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
           position: google.maps.ControlPosition.TOP_LEFT
        },
        streetViewControl: false
    }
 
    var map = new google.maps.Map( document.getElementById(element) , myOptions);
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map
    });
    map.setCenter(myLatlng);
}

if(!String.prototype.startsWith){
    String.prototype.startsWith = function (str) {
        return !this.indexOf(str);
    }
}

$(document).ready(function(){
	
	$('.delete').live('click',function(){
		var title = '<strong><a target="_blank" href="'+$(this).attr('seouri')+'">'+$.trim($(this).attr('title'))+'</a></strong>';
		var dialog_message ='<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>';
		dialog_message +=title+' will be permanently deleted and cannot be recovered. Continue?</p><br />';
		dialog_message +='<p align="center"><button style="cursor:pointer;" class="ui-state-default ui-corner-all" type="button" id="cancel">Cancel</button>&nbsp;';
		dialog_message +='<button style="cursor:pointer;" seo="'+$(this).attr('seo')+'" seouri="'+$(this).attr('seouri')+'" class="ui-state-default ui-corner-all" style="font-color:#F0F0F0;" type="button" id="confirm_delete">Confirm delete</button>&nbsp;</p>';
		$("#confirm_msg").empty().html(dialog_message);
		$('#dialog').dialog({modal:true,hide:'explode'});
	});
	
	$("#confirm_delete").live("click",function(){
		$("#cancel").attr('disabled',true);
		$(this).empty().html("Deleting program. Please wait...").attr('disabled',true);
		var seo_uri = $.trim($(this).attr('seo'));
        this.timer = setTimeout(function (){
        	$.ajax({
            	cache: false  ,
                type:  "GET" ,
                url:   "<?php echo Configure::read('js_directory');?>delete_program.php?id="+seo_uri+'&who_modified=<?php echo $this->Session->read('Auth.User.id');?>',
                dataType: "json" ,
                success: function(data){
                	$("#dialog").dialog('close');
                    window.location.reload();
                }
            });
        },1000);
    });
	
	
	$("#cancel").live("click",function(){
		$("#dialog").dialog('close');
	});
	
	$("#q").focus();
	
	$(window).resize(function() {
	    $("#dialog").dialog("option", "position", "center");
	});
	
	$(window).scroll(function() {
	    $("#dialog").dialog("option", "position", "center");
	});
	
    $('.listing a').live('click',function(){
        var string_match = new String( $(this).attr('id') );

		var string_found = string_match.search(/map/i);
        var string_match_data = string_match.split('_');

        if( string_found > 0 ){
           if( !$(this).attr('show_div') ){
              $(this).attr('show_div','show_div');
              var geocodes = new String( $(this).attr('alt') ); 
              var xy = geocodes.split('_');

              if( string_match.startsWith('all') ){
                  $('#all_programs_map_canvas_'+parseInt(string_match_data[4],10)).fadeIn("fast", function(){
                      $(this).show();
                  });
                  invokeMap( 'all_programs_map_canvas_'+ parseInt(string_match_data[4]) , xy[0], xy[1] );        

              }else{
				  $('#'+string_match_data[0]+'_map_canvas_'+parseInt(string_match_data[3],10)).fadeIn("fast", function(){
                      $(this).show();
                  });
                  invokeMap(string_match_data[0]+'_map_canvas_'+parseInt(string_match_data[3],10), xy[0], xy[1] );	
			  }
		   }else{
              $(this).removeAttr('show_div');

              if( string_match.startsWith('all') ){
			  	  $('#all_programs_map_canvas_'+parseInt(string_match_data[4],10)).fadeOut("slow", function(){
                  	$(this).hide();
                  });
              }else{
            	  $('#'+string_match_data[0]+'_map_canvas_'+parseInt(string_match_data[3],10)).fadeOut("slow", function(){
                    	$(this).hide();
                  });
              }
		   }
        }
      
        var rateplan_found = string_match.search(/rate/i);
        if( rateplan_found > 0 ){
           if( !$(this).attr('show_div') ){
              $(this).attr('show_div','show_div');
			  if( string_match.startsWith('all') ){ 
              	 $('#all_programs_rate_plans_'+parseInt(string_match_data[5],10)).fadeIn("fast", function(){
                    $(this).show();
                 });
              }else{
            	 $('#'+string_match_data[0]+'_rate_plans_'+parseInt(string_match_data[4],10)).fadeIn("fast", function(){
                      $(this).show();
                 });
              }
           }else{
              $(this).removeAttr('show_div');
              if( string_match.startsWith('all') ){
				 $('#all_programs_rate_plans_'+parseInt(string_match_data[5],10)).fadeOut("slow", function(){
                    $(this).hide();
                 });
              }else{
            	 $('#'+string_match_data[0]+'_rate_plans_'+parseInt(string_match_data[4],10)).fadeOut("slow", function(){
                      $(this).hide();
                 }); 	
              }
           }
        }

        var attendees_found = string_match.search(/companies/i);
        if( attendees_found> 0 ){
           if( !$(this).attr('show_div') ){
              $(this).attr('show_div','show_div');
			  if( string_match.startsWith('all') ){ 
              	 $('#all_programs_attending_companies_'+parseInt(string_match_data[3],10)).fadeIn("fast", function(){
                    $(this).show();
                 });
              }else{
            	 $('#'+string_match_data[0]+'_programs_attending_companies_'+parseInt(string_match_data[3],10)).fadeIn("fast", function(){
                      $(this).show();
                 });
              }
           }else{
              $(this).removeAttr('show_div');
              if( string_match.startsWith('all') ){
				 $('#all_programs_attending_companies_'+parseInt(string_match_data[3],10)).fadeOut("slow", function(){
                    $(this).hide();
                 });
              }else{
            	 $('#'+string_match_data[0]+'_programs_attending_companies_'+parseInt(string_match_data[3],10)).fadeOut("slow", function(){
                      $(this).hide();
                 }); 	
              }
           }
        } 
    });
});
/* ]]> */
</script>
<style type="text/css">
.listing .box1 {
    float:left !important;
    padding:0 0 0 6px !important;
    width:192px !important;
}
.listing .box2 {
    float: left !important;
    text-align: left !important;
    width: 162px !important;
}

.listing .box3 {
    float: left !important;
    text-align: left !important;
    width: 168px !important;
}

.listing .box5 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 151px !important;
}

.listing .box6 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 128px !important;
}

.listing .box7 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 111px !important;
}
</style>
<?php 
echo $this->Html->css(array(
    'jquery.autocomplete',
    'fcbkselection'
    )
);
?>
<?php 
$this->Html->addCrumb('Programs', '/admin/programs');
$this->Html->addCrumb('<strong>'.ucfirst($division).' Programs</strong>');
?>
<div id="dialog" title="Delete Program" style="display:none;"><div id="confirm_msg">&nbsp;</div></div>
<div class="users index">
	<h2><?php __(''.ucwords($division).' Programs');?></h2>
	<?php echo $this->element('program/search_simple_program'); ?>
    <div>
    	<div id="programs">
        	<ul>
            	<?php 
            	echo '<li><a href="#programs_'.strtolower($division).'"><span>'.ucwords($division).'</span></a></li>';
            	?>  
            	<li><a href="#calendar_view"><span>Calendar View</span></a></li>
            </ul>
            <div id="programs_<?php echo strtolower($division);?>">

            		<table cellspacing="0" cellpadding="0" style="background-color:#FFFFFF;border:1px solid #DDDDDD;clear:both;width:100%;">
               	  		<?php 
               	  		if(!empty($programs)){
               	  		?>
    					<tr><td align="center" colspan="7">
    						<div class="paging">
		                 	<?php
		                 	$this->Paginator->options(array('url' => strtolower($division),array('model'=> $division.'Programs')));
                        	$sPaginator = 
                        	$this->Paginator->first('<< First') .' '.
                        	$this->Paginator->prev('< Previous').' '. 
                        	$this->Paginator->numbers()         .' '.
                        	$this->Paginator->next('Next >')    .' '.
                        	$this->Paginator->last('Last >>');
                        	echo $sPaginator."<br />";
                        	echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));
		                 	?>
	                 		</div>
	                 	</td></tr>
	                 	<?php 
               	  		}
	                 	?>
	                 	<tr>
                            <th style="border-right: 1px solid #DFDFDF;width:150px;"><?php echo $paginator->sort('Sort By Title','title');?></th>
                            <th style="border-right: 1px solid #DFDFDF;width:115px;">Schedule</th>
                            <th style="border-right: 1px solid #DFDFDF;width:110px;">Sort <?php echo $paginator->sort('Division','programs_division_id');?>/<?php echo $paginator->sort('Type','programs_type_id');?></th>
                            <th style="border-right: 1px solid #DFDFDF;width:100px;">Speaker</th>
                            <th style="border-right: 1px solid #DFDFDF;width:90px;">Total</th>
                            <th style="border-right: 1px solid #DFDFDF;width:80px;">Discount<br />Applies</th>
                            <th style="border-right: 1px solid #DFDFDF;width:90px;">Notes</th>
	                 	</tr>
	                	<tr><td colspan="7">
	                	<?php 
                  		$i = 0;
                   		foreach( $programs as $program_key => $program_info ){
                    		$this->Program->setProgramData( $program_info['Program'] );
                      		$this->ProgramExtraInfo->setExtraInfo( $program_info ); 
                      		$sCss = ( ($i % 2) ==0 )? 'listing_accomm':'listing_tr';
             			?>
                        <div class="listing" id="<?php echo $sCss;?>">
                                <div class="box1" id="all_programs_<?php echo $this->Program->get('id');?>">
                                	<div>
                                    	<h3><a href="<?php echo $this->Html->url(array('controller'=>'programs','action'=>'info',$this->Program->getSeoUri()));?>"><?php echo $this->Program->getTitle();?></a></h3>
                                        <div><address><?php echo $this->Program->getFullAddress();?></address></div>
                                    </div>
                                </div><!--end box1-->
                                <div class="box2"><div><?php echo $this->ProgramExtraInfo->getSchedule($shortcut=true);?></div></div>
                                <div class="box3"><div><a href="<?php echo $this->Html->url(array('controller'=>'programs','action'=>'list',$this->ProgramExtraInfo->getProgramDivision()));?>"><?php echo $this->ProgramExtraInfo->getProgramDivision();?></a> | <span class="tiny"><?php echo $this->ProgramExtraInfo->getProgramType();?></span></div><!--end box3--></div>
                                <div class="box5"><div><?php echo $this->ProgramExtraInfo->getProgramSpeakers($speakers);?></div></div>
                                <div class="box6"><div align="left"><?php echo $this->Program->getTotalProgramClientParticipants().'<br />'.$this->Program->getTotalProgramParticipants().'<br />'.$this->Program->getTotalEarnings().'<br />'.$this->Program->getTotalBalanceOwing();?></div></div>
                                <div class="box7"><div align="left"><?php echo $this->Program->getDiscountApplies();?></div></div>
                                <div class="box8"><div align="left"><em><?php echo $this->Program->getNotes();?></em></div></div>
                                <?php 
                                if( $this->Program->get('total_participants') > 0|| $this->Program->get('total_companies') >0){
                                ?>
                                <div class="box4b"><?php echo $this->ProgramExtraInfo->getLatestCompany();?>&nbsp;<?php echo $this->ProgramExtraInfo->getLatestParticipant();?></div>
                                <?php
                                }
								?>
								<div class="box4">
                                	<a class="edit" style="cursor:pointer;" id="all_programs_view_rate_plans_<?php echo $this->Program->get('id');?>">View RatePlans</a>&nbsp;
                                	<a class="edit" style="cursor:pointer;" id="all_programs_view_map_<?php echo $this->Program->get('id');?>" alt="<?php echo $this->Program->getLatitude().'_'.$this->Program->getLongitude();?>">View Map</a>&nbsp;
                                	<a class="edit_full_details" href="<?php echo $this->Html->url(array('controller'=>'programs','action'=>'info',$this->Program->getSeoUri()));?>">View Full Details</a>
                                		
	                                <?php
	                                echo $this->Program->getDisplayDelete('all_programs');
									echo $this->Program->getDisplayEdit();
									echo $this->Program->getAddParticipants();
                                
                                	if( $this->Program->get('total_companies') > 0){
                                	?>
	                                <a class="edit" style="cursor:pointer;" id="all_programs_companies_<?php echo $this->Program->get('id');?>">View Attending Companies</a>
	                                <?php
	                                }
									?>
                                    <?php echo $this->ProgramExtraInfo->createdBy($users);?>
                        		</div><!--end box4-->
                                <div class="clearboth"></div>
                                <div id="all_programs_rate_plans_<?php echo $this->Program->get('id');?>" style="display:none;">
                                	<?php echo $this->ProgramExtraInfo->getProgramRatePlans($rateplans);?>
                                </div>
                                <div id="all_programs_attending_companies_<?php echo $this->Program->get('id');?>" style="display:none;">
                                	<?php echo $this->ProgramExtraInfo->getAttendingCompanies();?>
                                </div>
                                <div id="all_programs_map_canvas_<?php echo $this->Program->get('id');?>" style="display:none;" class="map_canvas"></div>
                        </div><!--end listing-->
						<?php 
                     		$i++;
                  		}
                  		?>
            		    </td></tr>
                  	<?php 
               	  	if(!empty($programs)){
               	  	?>
    				<tr><td align="center" colspan="7" class="striped"><div class="paging">
    	            <?php
	                $this->Paginator->options(array('url' => strtolower($division),array('model'=> $division.'Programs')));
                    $sPaginator = 
                    $this->Paginator->first('<< First') .' '.
                    $this->Paginator->prev('< Previous').' '. 
                    $this->Paginator->numbers()         .' '.
                    $this->Paginator->next('Next >')    .' '.
                    $this->Paginator->last('Last >>');
                    echo $sPaginator."<br />";
                    echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));
	                ?>
                 	</div></td></tr>
                 	<?php 
               	  	}
                 	?>
                 </table>

            </div>
           <div id="calendar_view">
            	<table cellspacing="0" cellpadding="0" style="background-color: #FFFFFF;border: 1px solid #DDDDDD;clear: both;width: 100%;">
    				<tr><td align="center"><iframe src="https://www.google.com/calendar/embed?src=mansmith.net%40gmail.com&ctz=Asia/Manila&showTitle=1;&width=1000" style="border: 0" width="1000" height="500" frameborder="0" scrolling="no"></iframe>
                        <!--<iframe src="https://www.google.com/calendar/embed?title=Mansmith%20Calendar%20of%20Events&amp;showTitle=0&amp;showTz=0&amp;height=300&amp;wkst=2&amp;bgcolor=%23cccccc&amp;src=mansmith.net%40gmail.com&amp;color=%23A32929&amp;ctz=Asia%2FManila" style=" border:solid 1px #777 " width="1000" height="500" frameborder="0" scrolling="no"></iframe>--></td></tr>
                </table>
            </div>
        </div> 
	</div>
</div>
