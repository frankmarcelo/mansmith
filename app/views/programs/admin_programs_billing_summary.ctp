<style type="text/css">
    table tr td {
        border-bottom: 1px solid #DFDFDF !important;
        padding: 10px !important;
        vertical-align: top !important;
    }
</style>
<?php  $this->Layout->sessionFlash();?>
<form name="ProgramBillingInfo" id="ProgramBillingInfo" method="post" accept-charset="utf-8" action="<?php echo $this->Html->url(array("admin"=>true, "controller" => "programs" ,"action"=>"add_programs_billing_summary"));?>">
    <input type="hidden" name="data[Program][who_created]" value="<?php echo $this->Session->read('Auth.User.id');?>" />
    <input type="hidden" name="data[Program][id]" value="<?php echo $program_id;?>" />
    <?php if (isset($program_billing_info) && $program_billing_info){
    ?>
    <input type="hidden" name="data[ProgramBillingInfo][id]" value="<?php echo intval($program_billing_info['ProgramBillingInfo']['id']);?>" />
    <?php
    }
    ?>
<table>
    <tr><th colspan="2">Billing Summary</th></tr>
    <tr><td width="50%"><table>
        <tr><td width="30%">Sending Officer</td>
            <td>
                <select id="participant_id" name="data[ProgramBillingInfo][participant_id]">
                <option> - non assigned -</option>
                <?php
                if( isset($company_id) && !is_null($company_id) && intval($company_id) > 0 && count($participants)>0 ){
                    foreach( $participants as $participant ){
                        $selected = (isset($program_billing_info) && $program_billing_info && $participant['Participant']['id'] == $program_billing_info['ProgramBillingInfo']['participant_id'])?'selected="selected"':'';
                        echo '<option '.$selected.' value="'.$participant['Participant']['id'].'_'.$participant['Participant']['company_id'].'">'.$participant['Participant']['last_name'].', '.$participant['Participant']['first_name'].' '.$participant['Participant']['middle_name'].'<br>['.$this->ParticipantsPosition->getParticipantsPositionById($participant['Participant']['participants_position_id']).']['.$participant['Participant']['email'].'] </option>';
                    }
                }
                ?>
                </select>
            </td>
        </tr>
        <tr><td width="30%">Billing Status</td><td>
            <select id="billing_status" name="data[ProgramBillingInfo][billing_status_id]">
                <?php
                foreach( $billing_status as $status_key => $status ){
                    $selected = (isset($program_billing_info) && $program_billing_info && $status_key == $program_billing_info['ProgramBillingInfo']['billing_status_id'])?'selected="selected" ':'';
                    ?>
                    <option <?php echo $selected;?>value="<?php echo $status_key;?>"><?php echo $status;?></option>
                    <?php
                }
                ?>
            </select>
        </td></tr>
        <tr><td width="30%">Billing Date</td><td>
            <?php
            $billed_date_year =null;
            $billed_date_month=null;
            $billed_date_day=null;
            $billed_date_hh=null;
            $billed_date_mm=null;
            $billed_date_ss=null;
            if(isset($program_billing_info) && $program_billing_info){
                list($billed_date_year,$billed_date_month,$billed_date_day) = explode("-",$program_billing_info['ProgramBillingInfo']['billed_date']);
                list($billed_date_day,$extra_info) = explode(" ",$billed_date_day);
                list($billed_date_hh,$billed_date_mm,$billed_date_ss) = explode(":",date("h:i:a",strtotime($program_billing_info['ProgramBillingInfo']['billed_date'])));
            }

            $billed_date_year = ($billed_date_year==null) ? date("Y"): $billed_date_year;
            $billed_date_month = ($billed_date_month==null) ? date("m"):$billed_date_month;
            $billed_date_day = ($billed_date_day==null) ? date("d"): $billed_date_day;
            $billed_date_hh = ($billed_date_hh==null) ? date("h"): $billed_date_hh;
            $billed_date_mm = ($billed_date_mm==null) ? date("i"): $billed_date_mm;
            $billed_date_ss = ($billed_date_ss==null) ? date("a"): $billed_date_ss;

            $payment_date_year =null;
            $payment_date_month=null;
            $payment_date_day=null;
            $payment_date_hh=null;
            $payment_date_mm=null;
            $payment_date_ss=null;
            if(isset($program_billing_info) && $program_billing_info){
                list($payment_date_year,$payment_date_month,$payment_date_day) = explode("-",$program_billing_info['ProgramBillingInfo']['payment_date']);
                list($payment_date_day,$extra_info) = explode(" ",$payment_date_day);
                list($payment_date_hh,$payment_date_mm,$payment_date_ss) = explode(":",date("h:i:a",strtotime($program_billing_info['ProgramBillingInfo']['payment_date'])));
            }

            $payment_date_year = ($payment_date_year==null) ? date("Y"): $payment_date_year;
            $payment_date_month = ($payment_date_month==null) ? date("m"):$payment_date_month;
            $payment_date_day = ($payment_date_day==null) ? date("d"): $payment_date_day;
            $payment_date_hh = ($payment_date_hh==null) ? date("h"): $payment_date_hh;
            $payment_date_mm = ($payment_date_mm==null) ? date("i"): $payment_date_mm;
            $payment_date_ss = ($payment_date_ss==null) ? date("a"): $payment_date_ss;
            ?>
            <select id="billed_date_month" name="data[ProgramBillingInfo][billed_date_month]">
            <?php
            for( $i = 1; $i < 13; $i++){
                $month_name = date('M', mktime(0, 0, 0, $i, 1, date("Y")));
                $month_id = date('m', mktime(0, 0, 0, $i, 1, date("Y")));

                $selected_month = ($billed_date_month==$month_id) ?'selected="selected" ':' ';
            ?><option <?php echo $selected_month;?>value="<?php echo $month_id;?>"><?php echo $month_name;?></option><?php
            }
            ?>
            </select>
            <select id="billed_date_day" name="data[ProgramBillingInfo][billed_date_day]">
                <?php
                for( $i = 1; $i < 32; $i++){

                    $selected_day = ($billed_date_day==$i) ?'selected="selected" ':' ';
                    ?><option <?php echo $selected_day;?>value="<?php echo $i;?>"><?php echo $i;?></option><?php
                }
                ?>
            </select>
            <select id="billed_date_year" name="data[ProgramBillingInfo][billed_date_year]">
                <?php
                $from = 2001;
                $until = $from+20;//20 years plus start 2001
                for( $i = $from ; $i < $until; $i++){
                    $selected_year = ($billed_date_year==$i) ?'selected="selected" ':' ';
                    ?><option <?php echo $selected_year;?>value="<?php echo $i;?>"><?php echo $i;?></option>
                <?php
                }
                ?>
            </select>
            <select id="billed_date_hh" name="data[ProgramBillingInfo][billed_date_hh]">
                <?php
                $from = 1;
                $until = 13;
                for( $i = $from ; $i < $until; $i++){
                    $selected_hh = ($billed_date_hh==$i) ?'selected="selected" ':' ';
                    $i = (strlen($i)<2)? '0'.$i:$i;
                    ?><option <?php echo $selected_hh;?>value="<?php echo $i;?>"><?php echo $i;?></option>
                    <?php
                }
                ?>
            </select>
            <select id="billed_date_mm" name="data[ProgramBillingInfo][billed_date_mm]">
                <?php
                $from = 1;
                $until = 60;//20 years plus start 2001
                for( $i = $from ; $i < $until; $i++){
                    $selected_mm = ($billed_date_mm==$i) ?'selected="selected" ':' ';
                    $i = (strlen($i)<2)? '0'.$i:$i;
                    ?><option <?php echo $selected_mm;?>value="<?php echo $i;?>"><?php echo $i;?></option>
                    <?php
                }
                ?>
            </select>
            <select id="billed_date_ss" name="data[ProgramBillingInfo][billed_date_ss]">
                <option<?php echo ($billed_date_ss=="am")?' selected="selected"':'';?> value="am">AM</option>
                <option<?php echo ($billed_date_ss=="pm")?' selected="selected"':'';?> value="pm">PM</option>
            </select>
        </td></tr>
        <tr><td width="30%">Created By</td><td>
        <?php
        if( !empty($program_billing_info) && $program_billing_info ){
            echo $users[$program_billing_info['ProgramBillingInfo']['who_created']];
        }else{
            echo $this->Session->read('Auth.User.name');
        }
        ?>
        </td></tr>
    </table></td>
        <td width="50%">
            <table>
                <tr><td width="30%">Payment Mode</td><td width="70%">
                    <select id="billing_type_id" name="data[ProgramBillingInfo][billing_type_id]">
                        <?php
                        foreach( $billing_type as $type_key => $billing ){
                            if( $type_key != Configure::read('status_deleted')){
                                $selected = (isset($program_billing_info) && $program_billing_info && $type_key == $program_billing_info['ProgramBillingInfo']['billing_type_id'])?'selected="selected" ':'';
                                ?>
                                <option <?php echo $selected;?>value="<?php echo $type_key;?>"><?php echo $billing;?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </td></tr>
                <tr><td width="30%">Payment Date</td><td width="70%">
                    <select id="payment_date_month" name="data[ProgramBillingInfo][payment_date_month]">
                        <?php
                        for( $i = 1; $i < 13; $i++){
                            $month_name = date('M', mktime(0, 0, 0, $i, 1, date("Y")));
                            $month_id = date('m', mktime(0, 0, 0, $i, 1, date("Y")));

                            $selected_month = ($payment_date_month==$month_id) ?'selected="selected" ':' ';
                            ?><option <?php echo $selected_month;?>value="<?php echo $month_id;?>"><?php echo $month_name;?></option><?php
                        }
                        ?>
                    </select>
                    <select id="payment_date_day" name="data[ProgramBillingInfo][payment_date_day]">
                        <?php
                        for( $i = 1; $i < 32; $i++){

                            $selected_day = ($payment_date_day==$i) ?'selected="selected" ':' ';
                            ?><option <?php echo $selected_day;?>value="<?php echo $i;?>"><?php echo $i;?></option><?php
                        }
                        ?>
                    </select>
                    <select id="payment_date_year" name="data[ProgramBillingInfo][payment_date_year]">
                        <?php
                        $from = 2001;
                        $until = $from+20;//20 years plus start 2001
                        for( $i = $from ; $i < $until; $i++){
                            $selected_year = ($payment_date_year==$i) ?'selected="selected" ':' ';
                            ?><option <?php echo $selected_year;?>value="<?php echo $i;?>"><?php echo $i;?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <select id="payment_date_hh" name="data[ProgramBillingInfo][payment_date_hh]">
                        <?php
                        $from = 1;
                        $until = 13;
                        for( $i = $from ; $i < $until; $i++){
                            $selected_hh = ($payment_date_hh==$i) ?'selected="selected" ':' ';
                            $i = (strlen($i)<2)? '0'.$i:$i;
                            ?><option <?php echo $selected_hh;?>value="<?php echo $i;?>"><?php echo $i;?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <select id="payment_date_mm" name="data[ProgramBillingInfo][payment_date_mm]">
                        <?php
                        $from = 1;
                        $until = 60;//20 years plus start 2001
                        for( $i = $from ; $i < $until; $i++){
                            $selected_mm = ($payment_date_mm==$i) ?'selected="selected" ':' ';
                            $i = (strlen($i)<2)? '0'.$i:$i;
                            ?><option <?php echo $selected_mm;?>value="<?php echo $i;?>"><?php echo $i;?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <select id="payment_date_ss" name="data[ProgramBillingInfo][payment_date_ss]">
                        <option<?php echo ($payment_date_ss=="am")?' selected="selected"':'';?> value="am">AM</option>
                        <option<?php echo ($payment_date_ss=="pm")?' selected="selected"':'';?> value="pm">PM</option>
                    </select>
                </td></tr>
                <tr><td width="30%">Payment Status</td>
                    <td width="70%">
                        <select id="payment_status" name="data[ProgramBillingInfo][status]">
                            <?php
                            foreach( $payment_status as $payment_key => $status ){
                                $selected = (isset($program_billing_info) && $program_billing_info &&  $payment_key == $program_billing_info['ProgramBillingInfo']['status'])?'selected="selected" ':'';
                                ?>
                                <option <?php echo $selected;?>value="<?php echo $payment_key;?>"><?php echo $status;?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </td></tr>
                    <tr><td width="30%">Billing/Payment Notes</td><td width="70%"><textarea cols="40" name="data[ProgramBillingInfo][notes]">
                    <?php echo (isset($program_billing_info) && $program_billing_info)? $program_billing_info['ProgramBillingInfo']['notes']:'';?></textarea><td></tr>
                    <tr><td width="30%">Modified By</td><td>
                    <?php
                    if( isset($program_billing_info) && $program_billing_info ){
                        echo (isset($users[$program_billing_info['ProgramBillingInfo']['who_modified']])) ? $users[$program_billing_info['ProgramBillingInfo']['who_modified']]:null;
                    }else{
                        echo $this->Session->read('Auth.User.name');
                    }
                    ?>
                </td></tr>
            </table>
        </td></tr>
    <?php
    if( isset($company_id)){
?>
    <tr><td colspan="2"><input type="submit" value="Save" class="add_btn_info" /></td></tr>
        <?php
    }
?>
</table>
</form>