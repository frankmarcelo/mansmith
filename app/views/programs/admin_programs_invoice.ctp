<style type="text/css">
    table tr td {
        border-bottom: 1px solid #DFDFDF !important;
        padding: 10px !important;
        vertical-align: top !important;
    }
</style>
<?php  $this->Layout->sessionFlash();?>
<form name="Invoice" id="Invoice" method="post" accept-charset="utf-8" action="<?php echo $this->Html->url(array("admin"=>true, "controller" => "programs" ,"action"=>"add_programs_invoice"));?>">
    <input type="hidden" name="data[Program][who_created]" value="<?php echo $this->Session->read('Auth.User.id');?>" />
    <input type="hidden" name="data[Program][id]" value="<?php echo $program_id;?>" />
<table width="700px">
    <tr><th>Invoice</th></tr>
    <tr>
        <td valign="top">
            <table vertical-align="top" valign="top">
                <tr valign="top" vertical-align="top">
                    <th valign="top" width="40%">Invoice Documents</th>
                    <th valign="top" width="30%">Author</th>
                    <th valign="top" width="20%">Date Created</th>
                </tr>
                <?php
                if(isset($invoice)){
                    foreach( $invoice as $invoiceInfo){

                        if( isset($invoiceInfo['InvoiceDetails']) ){
                            foreach( $invoiceInfo['InvoiceDetails'] as $invoiceDetail ){
                        ?>
                        <tr><td><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'invoice','action'=>'download',$invoiceDetail['id']));?>"><?php echo trim($invoiceDetail['source_file']);?></a></td>
                        <td><?php echo $users[$invoiceDetail['who_created']];?></td>
                        <td><?php echo date("j",strtotime(trim($invoiceDetail['date_created']))).'<sup>'.
                                date("S",strtotime(trim($invoiceDetail['date_created']))).'</sup>&nbsp;'.
                                date("M Y",strtotime(trim($invoiceDetail['date_created']))).' '.
                                date("h:i A",strtotime(trim($invoiceDetail['date_created'])));?>
                        </td></tr>
                        <?php
                            }
                        }
                    }
                }
                ?>
                <tr><td colspan="1"> <?php
                    if( $participants > 0 ){
                        ?>
                        <input type="hidden" name="data[Invoice][company_id]" value="<?php echo $company_id;?>" />
                        <input type="submit" name="Add" value="Add" class="btn_add_info" />
                        <?php
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if( $participants > 0 ){
                        $invoice_date_year =null;
                        $invoice_date_month=null;
                        $invoice_date_day=null;

                        $invoice_date_year = ($invoice_date_year==null) ? date("Y"): $invoice_date_year;
                        $invoice_date_month = ($invoice_date_month==null) ? date("m"):$invoice_date_month;
                        $invoice_date_day = ($invoice_date_day==null) ? date("d"): $invoice_date_day;
                    ?>Payment Due Date:
                    <select id="billed_date_month" name="data[Invoice][month]">
                        <?php
                        for( $i = 1; $i < 13; $i++){
                            $month_name = date('M', mktime(0, 0, 0, $i, 1, date("Y")));
                            $month_id = date('m', mktime(0, 0, 0, $i, 1, date("Y")));

                            $selected_month = ($invoice_date_month==$month_id) ?'selected="selected" ':' ';
                            ?><option <?php echo $selected_month;?>value="<?php echo $month_id;?>"><?php echo $month_name;?></option><?php
                        }
                        ?>
                    </select>
                    <select id="billed_date_day" name="data[Invoice][day]">
                        <?php
                        for( $i = 1; $i < 32; $i++){

                            $selected_day = ($invoice_date_day==$i) ?'selected="selected" ':' ';
                            ?><option <?php echo $selected_day;?>value="<?php echo $i;?>"><?php echo $i;?></option><?php
                        }
                        ?>
                    </select>
                    <select id="billed_date_year" name="data[Invoice][year]">
                        <?php
                        $from = 2001;
                        $until = $from+20;//20 years plus start 2001
                        for( $i = $from ; $i < $until; $i++){
                            $selected_year = ($invoice_date_year==$i) ?'selected="selected" ':' ';
                            ?><option <?php echo $selected_year;?>value="<?php echo $i;?>"><?php echo $i;?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <?php
                    }
                    ?>
                </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</form>