<link rel="stylesheet" type="text/css" href="/css/admin.css?1357879311">
<script type="text/javascript" src="/js/jquery/jquery.min.js?1347027524"></script>
<script type="text/javascript" src="/js/jquery/jquery-ui.min.js?1347027524"></script>
<script type="text/javascript" src="/js/jquery/superfish.js"></script>
<script type="text/javascript" src="/js/jquery/supersubs.js"></script>
<script type="text/javascript" src="/js/jquery/jquery.tipsy.js?1347027523"></script>
<script type="text/javascript" src="/js/jquery/jquery.elastic-1.6.1.js?1347027522"></script>
<style type="text/css">
table tr td {
    border-bottom: 1px solid #DFDFDF !important;
    vertical-align: middle !important;
}

.tblClass{
    width: 1000px !important;
}

.listing {
    width: 1000px !important;
}

.listing .box10 {
    float: right !important;
    padding: 0 116px !important;
    text-align: left !important;
    width: 6px !important;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
    $('select[id*="program_participant_id"]').live('change',function(){
        $.ajax({
            cache: false  ,
            type:  "POST" ,
            url:   "<?php echo Configure::read('js_directory');?>ajax_update_program_participants.php",
            data:  "status="+$(this).val()+"&id="+$(this).attr('id')+"&who_modified=<?php echo $this->Session->read('Auth.User.id');?>"
        });
    });
})

</script>
<div class="users index">
    <form id="ProgramsParticipantUpdateForm" method="post" accept-charset="utf-8" action="<?php echo $this->Html->url(array("admin"=>true, "controller" => "programs" ,"action"=>"programs_participants"));?>">
        <input type="hidden" name="data[Program][who_created]" value="<?php echo $this->Session->read('Auth.User.id');?>" />
        <input type="hidden" name="data[Program][id]" value="<?php echo $program_id;?>" />
        <table cellspacing="0" cellpadding="0" class="tblClass">
        <?php
        if(!empty($participants) && sizeof($participants) > 0){
        ?>
        <tr><td align="center">
            <div class="paging">
                <input type="submit" name="data[Program][Delete]" value="Delete" />
                <input type="reset" name="reset" value="Uncheck All" />
            <?php
            $paginator->options(array('url' => $this->passedArgs),array('model'=> 'ProgramsParticipant'));
            $sPaginator = $paginator->first('<< First',array('model'=> 'ProgramsParticipant')).' '.
            $paginator->prev('< Previous',array('model'=> 'ProgramsParticipant')).' '.
            $paginator->numbers(array('model'=> 'ProgramsParticipant')).' '.
            $paginator->next('Next >',array('model'=> 'ProgramsParticipant')).' '.
            $paginator->last('Last >>',array('model'=> 'ProgramsParticipant'));
            echo $sPaginator;
            echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true),array('model'=> 'ProgramsParticipant')));?>
              <br><?php  $this->Layout->sessionFlash();?>
            </div>
        </td></tr>
        <?php
        }
        ?>
        <tr><td>
        <?php
        $i = 0;
        foreach( $participants as $participant_key => $participant_info ){
            if( $participant_info['ProgramsParticipant']['status']==Configure::read('status_live')){
            $this->Participant->setParticipantData( $participant_info['Participant'],$participant_info['ParticipantsTitle']['name']);
            $this->ParticipantExtraInfo->setExtraInfo( $participant_info);
            $sCss = ( ($i % 2) ==0 )? 'listing_accomm':'listing_tr';
        ?>
        <div class="listing" id="<?php echo $sCss;?>">
            <div class="box1" id="all_participants_<?php echo $this->Participant->get('id');?>">
                <div><input type="checkbox" id="data[ProgramsParticipant][id][]" name="data[ProgramsParticipant][id][]" value="<?php echo intval($participant_info['ProgramsParticipant']['id']);?>" /><h3><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'participants','action'=>'info',$this->Participant->getSeoUri()));?>"><?php echo $this->Participant->getFullName();?></a></h3></div>
            </div><!--end box1-->
            <div class="box2"><?php echo $this->ParticipantExtraInfo->getPosition();?></div>
            <div class="box3"><?php echo $this->ParticipantExtraInfo->getParticipantsGrouping();?></div>
            <div class="box5"><?php echo $this->ParticipantExtraInfo->getParticipantsCompany();?></div>
            <div class="box10"><select name="data[ProgramsParticipant][status][]" id="program_participant_id_<?php echo intval($participant_info['ProgramsParticipant']['id']);?>">
                <?php
                foreach( $program_participant_status as $status ){
                    $selected= ( $status['ProgramsParticipantStatus']['id'] == $participant_info['ProgramsParticipant']['programs_participant_status_id'] ) ?'selected="selected" ':' ';
                ?>
                <option <?php echo $selected;?>value="<?php echo $status['ProgramsParticipantStatus']['id'];?>"><?php echo $status['ProgramsParticipantStatus']['name'];?></option>
                <?php
                }
                ?></select>
                </div>
            <div class="box8"><?php echo $this->Participant->getPhone();?><br/><?php echo $this->Participant->getMobile();?><br/><?php echo $this->Participant->getFax();?></div>
            <div class="box4"><?php if($this->Participant->getEmail()){?><span><strong>Email:</strong>&nbsp;<a href="mailto:<?php echo $this->Participant->getEmail();?>"><?php echo $this->Participant->getEmail();?></a></span><?php if($this->Participant->getFullAddress()){?><span><strong>Address:</strong>&nbsp;<?php echo $this->Participant->getFullAddress();?></span><br /><?php } ?><br /><?php } ?></div><!--end box4-->
            <div class="box4">
                <a class="edit" target="_blank" href="<?php echo $this->Html->url(array('controller'=>'participants','action'=>'info',$this->Participant->getSeoUri()));?>">View Full Details</a>
                <?php
                echo $this->Participant->getDisplayEdit($target_blank=true);
                echo $this->ParticipantExtraInfo->createdBy($users);
                ?>
            </div><!--end box4-->
            <div class="clearboth"></div>
        </div><!--end listing-->
        <?php
            $i++;
            }
        }
        ?>
        </td></tr>
    </table>
    </form>
</div>