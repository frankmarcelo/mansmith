<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){
	$('#nametags').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true});
	$(window).resize(function(){});
	$(window).scroll(function(){});
	$('.delete').live('click',function(){});
});
/* ]]> */
</script>
<?php
echo $this->Html->css(array(
    'jquery.autocomplete',
    'fcbkselection'
    )
);

echo $this->Html->script(array(
    'jquery/jquery.highlight'
	)
);
?>
<script type="text/javascript">
/* <![CDATA[ */
if(!String.prototype.startsWith){
    String.prototype.startsWith = function (str) {
        return !this.indexOf(str);
    }
}

$(document).ready(function(){
	<?php 
	$qValue = null;
	$sUrl   = '/';  
	if (isset($this->params['url']['q']) && strlen($this->params['url']['q'])> 0) {
   		$qValue = $this->params['url']['q'];
	}

	if (isset($this->params['url']['division']) && strlen($this->params['url']['division'])> 0) {
   		$qValue = $this->params['url']['division'];
	}
	
	if( !is_null($qValue) ){
		$aKeywordsSearch = explode(" ",$qValue);
		$i = 0;
		foreach( $aKeywordsSearch as $keyword ){
			if( strlen(trim($keyword)) >= 3 ){
				if( $i < 1 ){
					echo "$('.listing').removeHighlight().highlight('".addslashes($keyword)."');";
				}else{
					echo "$('.listing').highlight('".addslashes($keyword)."');";
				}
				$i++;
			}
		}
	}
	
	if( isset($program_type) ){
	?>
	$('.listing').highlight(<?php echo "'".addslashes($program_type)."'";?>);
	<?php 
	}
	?>

    $("#cancel").live("click",function(){
		$("#dialog").dialog('close');
	});
    
	$("#q").focus();
});
/* ]]> */
</script>
<style type="text/css">
.listing .box1 {
    float: left !important;
    padding: 0 0 0 6px !important;
    width: 216px !important;
}

.listing .box2 {
    float: left !important;
    text-align: left !important;
    width: 185px !important;
}

.listing .box3 {
    float: left !important;
    text-align: left !important;
    width: 175px !important;
}

.listing .box5 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 207px !important;
}

.listing .box6 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 226px !important;
}

.listing .box7 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 107px !important;
}
.listing .box4 {
    clear: both !important;
    margin: 0 !important;
    height: -7px !important;
    padding: 10px 0 0 10px !important;
    width: 950px !important;
}
</style>
<?php
if( isset($this->params['url']['q']) ){
    $this->Html->addCrumb('Name Tags', '/admin/name_tags');
    $this->Html->addCrumb('Search Name Tags', '/admin/name_tags/search?q='.$this->params['url']['q']);
    $this->Html->addCrumb('<strong>'.trim($this->params['url']['q']).'</strong>');
}else{
    $this->Html->addCrumb('Name Tags', '/admin/name_tags');
    $this->Html->addCrumb('<strong>Search Name Tags</strong>');
}
?>
<div class="nametags index">
	<h2><?php __('Search Name Tags');?></h2>
    <?php echo $this->element('name_tags/search_advance_nametags'); ?>
	<div>
       <div id="nametags">
            <ul>
             	<?php 
             	if(empty($nametags)){
             	?>
            	<li><a href="#all_nametags"><span>Empty result</span></a></li>
             	<?php 
             	}else{
             		$qValue .= (isset($program_type))? '&nbsp;&nbsp;Program Type:&nbsp;'.$program_type: null;
            		$qValue .= (empty($qValue) && isset($schedule_date)) ? '&nbsp;&nbsp;Schedule Date:&nbsp;'.$schedule_date: null;
					if( is_null($qValue) || strlen(trim($qValue)) <1 || empty($qValue) ){
			 	?>
			 	<li><a href="#all_nametags"><span>Search Name Tags</span></a></li>
			 	<?php
					}else{		
             	?>
             	<li><a href="#all_nametags"><span>Search results for <?php echo '<strong>'.$qValue.'</strong>';?></span></a></li>
             	<?php
             		}
             	}	 
             	?>
            </ul>
            <?php
    		if( isset($nametags) && count($nametags) ){
    		?>
            <div id="all_nametags">
            	<ul>
                 	 <table cellspacing="0" cellpadding="0" style="background-color:#FFFFFF;border:1px solid #DDDDDD;clear:both;width:100%;">
    					<?php
    					if(!empty($nametags)){
    					?>
    					<tr><td align="center" colspan="5"><div class="paging">
		                <?php
		                $this->Paginator->options(array('url' => '/'));
                        $sPaginator = $this->Paginator->first('<< First').' '.
						$this->Paginator->prev('< Previous') .' '.
						$this->Paginator->numbers().' '.
						$this->Paginator->next('Next >'). ' '.
						$this->Paginator->last('Last >>');
                        echo $sPaginator."<br />";
                        echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));
		                ?>
	                 	</div></td></tr>
	                 	<?php
    					}
	                 	?>
	                 	<tr>
                            <th style="border-right: 1px solid rgb(223, 223, 223); width:150px;">Program</th>
                            <th style="border-right: 1px solid rgb(223, 223, 223); width:115px;">Schedule</th>
                            <th style="border-right: 1px solid rgb(223, 223, 223); width:110px;">Division/Type</th>
                            <th style="border-right: 1px solid rgb(223, 223, 223); width:135px;">File</th>
                            <th style="border-right: 1px solid rgb(223, 223, 223); width:180px;">Name Tags<br/> Company</th>
                        </tr>
                        <tr><td colspan="5">
                        <?php
                        $i = 0;
                        if( isset($nametags) && is_array($nametags) ){
                            foreach( $nametags as $nametag_key => $nametag_info ){
                                $nametagFileInfo = $this->NameTags->loadNameTagsByProgramId($nametag_info['Program']['id']);
                                $oProgram = $this->Program->loadProgramDataById($nametag_info['Program']['id'],$recursive=false);
                                $this->Program->setProgramData($nametag_info['Program']);
                                $this->ProgramExtraInfo->setExtraInfo($oProgram);
                                $this->NameTags->setNameTagData($nametag_info);
                                $sCss = ( ($i % 2) ==0 )? 'listing_accomm':'listing_tr';
                        ?>		<div class="listing" id="<?php echo $sCss;?>">
                                    <div class="box1" id="all_programs_<?php echo $this->NameTags->getNameTagId();?>">
                                            <div>
                                                <h3><a href="<?php echo $this->Html->url(array('controller'=>'programs','action'=>'info',$this->NameTags->getSeoUri()));?>"><?php echo $this->NameTags->getProgramTitle();?></a></h3>
                                                <div><address><?php echo $this->Program->getFullAddress();?></address><br/>Total Participants: <?php echo $this->NameTags->getTotalAttendanceParticipants();?>&nbsp;Total Companies:<?php echo $this->NameTags->getTotalAttendingCompanies();?></div>
                                            </div>
                                    </div><!--end box1-->
                                    <div class="box2"><div><?php echo $this->ProgramExtraInfo->getSchedule($shortcut=true);?></div></div>
                                    <div class="box3"><div>
                                    <?php
                                    if( $this->Session->read('Auth.User.role_id') == Configure::read('administrator')){
                                    ?><a href="<?php echo $this->Html->url(array('controller'=>'name_tags','action'=>'list',$this->ProgramExtraInfo->getProgramDivision()));?>">
                                    <?php echo $this->ProgramExtraInfo->getProgramDivision();?></a><?php
                                    }else{ echo $this->ProgramExtraInfo->getProgramDivision();}?> | <span class="tiny"><?php echo $this->ProgramExtraInfo->getProgramType();?></span></div><!--end box3--></div>
                                    <div class="box5">
                                    <?php
                                    if( count($nametagFileInfo) > 0 ){
                                        $nameTagDirectory = $this->NameTags->getNameTagDirectory(intval($nametag_info['Program']['id']));
                                        foreach( $nametagFileInfo as $key => $nameTagFile ){
                                            if( intval($nametag_info['Program']['id']) >0 && isset($nameTagFile['ProgramsNameTag']['source_file']) ){
                                                $file = trim($nameTagDirectory.$nameTagFile['ProgramsNameTag']['source_file']);
                                                if( $this->NameTags->nameTagExist($nametag_info['Program']['id'],$file)){
                                    ?>
                                    <a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'name_tags','action'=>'download',$nameTagFile['ProgramsNameTag']['id']));?>"><?php echo trim($nameTagFile['ProgramsNameTag']['source_file']);?></a>
                                    <?php		}else{
                                                    echo '&nbsp;';
                                                }
                                            }
                                        }
                                    }
                                    ?><br/><?php echo $this->NameTags->createdBy($users);?>
                                    </div>
                                    <div class="box6"><div align="left"><?php echo $this->NameTags->getAttendanceCompanies();?></div></div>
                                    <div class="box4">&nbsp;</div><!--end box4-->
                                    <div class="clearboth"></div>
                                </div><!--end listing-->
                        <?php
                                $i++;
                            }//end of foreach
                        }//end of nametags
                        ?>
                  		</td></tr>
                        <?php
                        if(!empty($nametags)){
                        ?>
                        <tr><td align="center" colspan="5" class="striped"><div class="paging">
                        <?php
                        $this->Paginator->options(array('url' => '/'));
                        $sPaginator = $this->Paginator->first('<< First').' '.
                        $this->Paginator->prev('< Previous') .' '.
                        $this->Paginator->numbers().' '.
                        $this->Paginator->next('Next >'). ' '.
                        $this->Paginator->last('Last >>');
                        echo $sPaginator."<br />";
                        echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));
                        ?>
                        </div></td></tr>
                        <?php
                        }
                        ?>
                  	</table>
               	</ul>
            </div><!-- end of name tags -->
            <?php
    		}//end of isset and count
    		?> 
        </div>
	</div>