<?php
echo $this->Html->css(array('jquery.autocomplete'));
echo $this->Html->script(array(
    'jquery/jquery.bgiframe.min',
    'jquery/jquery.ajaxQueue',
    'jquery/development-bundle/ui/jquery.ui.core',
    'jquery/development-bundle/ui/jquery.ui.widget',
    'jquery/development-bundle/ui/jquery.ui.position',
    'jquery/development-bundle/ui/jquery.ui.autocomplete',
    'jquery/development-bundle/ui/jquery.ui.draggable',
    'jquery/development-bundle/ui/jquery.ui.resizable',
    'jquery/development-bundle/ui/jquery.ui.dialog',
    'jquery/development-bundle/ui/jquery.ui.datepicker'
    )
);
?>
