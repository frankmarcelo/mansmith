<style type="text/css">
#blog_search {
    margin-bottom: 29px;
}

fieldset { 
	border:1px solid grey;
	width: 70%;  
}

legend {
	padding-left: 30px;
 	font-size:90%;
  	text-align:right;
}
</style>
<?php 
echo $this->Html->css(array('jquery.autocomplete'));
echo $this->Html->script(array(
	    'jquery/jquery.bgiframe.min',
	    'jquery/jquery.ajaxQueue',
	    'jquery/development-bundle/ui/jquery.ui.core',
		'jquery/development-bundle/ui/jquery.ui.widget',
		'jquery/development-bundle/ui/jquery.ui.position',
		'jquery/development-bundle/ui/jquery.ui.autocomplete',
	    'jquery/development-bundle/ui/jquery.ui.draggable',
	    'jquery/development-bundle/ui/jquery.ui.resizable',
	    'jquery/development-bundle/ui/jquery.ui.dialog',
	    'jquery/development-bundle/ui/jquery.ui.datepicker'
	)
);
?>
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){
	$("#q").autocomplete({
		source: "<?php echo Configure::read('js_directory');?>search_participants.php",
		minLength: 3,
		autoFocus: true
	});
	
	$("#ParticipantsAdminSearchForm input").keypress(function(e) {
		if (e.keyCode == 13) {
			$(this).closest("form").submit();
		}
	});
		    
	$("#search_participants").live("click",function(){
		$("#ParticipantsAdminSearchForm").submit();	
    });
});
/* ]]> */
</script>
<div class="search">
	<form id="ParticipantsAdminSearchForm" method="get" action="<?php echo $this->Html->url(array("admin"=>true,"controller" => "participants","action" => "search"));?>" accept-charset="utf-8">
		<?php
	    $qValue = null;
	    if (isset($this->params['url']['q'])) {
	    	$qValue = $this->params['url']['q'];
	    }
	    ?>
	    <div id="standard_search">	
		    <table cellspacing="0" cellpadding="0" style="background-color:#FFFFFF;border:1px solid #DDDDDD;clear:both;width:100%;">
		    	<tr><td align="right" width="30%"><label for="q">Search keyword:</label></td><td width="70%" align="left"><input type="text" name="q" value="<?php echo $qValue;?>" style="width:450px;" id="q" class="text ui-widget-content ui-corner-all" />&nbsp;<br />e.g Sanjay OR Manager OR Pasig OR marejola@solae.com OR 8189911 OR 09189265186.</td></tr>
		    	<tr><td align="right" width="30%"><label for="company_affiliation">Participant Grouping: </label></td><td width="70%" align="left">
		    		<select id="grouping" name="grouping">
		    		 	<option value="All">All</option>	
		    		 	<?php
		    		 	foreach( $participantsgrouping as $grouping_key => $grouping ){
	                 		if( isset($this->params['url']['grouping']) && $grouping['ParticipantsGrouping']['seo_name']== $this->params['url']['grouping'] ){
	                 			echo '<option value="'.$grouping['ParticipantsGrouping']['seo_name'].'" selected="selected">'.$grouping['ParticipantsGrouping']['name'].'</option>';
	                 		}else{ 	
	                 			echo '<option value="'.$grouping['ParticipantsGrouping']['seo_name'].'">'.$grouping['ParticipantsGrouping']['name'].'</option>';
	                 		}
	                	}
	                 	?>
	                </select>
		    	</td></tr>
		    	<tr><td colspan="2" align="center"><a id="search_participants" style="position: relative;text-decoration: none !important;text-align: center;padding: 0;overflow: visible;margin-right: 0.1em;cursor: pointer;display: inline-block;font-size:13px;padding: 0.4em 1em;border-radius: 4px 4px 4px 4px;" class="ui-button ui-state-default ui-corner-all">Search</a></td></tr>
		    </table>
		</div>
	</form>
</div>
<br />