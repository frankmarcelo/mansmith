<style type="text/css">
#blog_search {
    margin-bottom: 29px;
}

fieldset { 
	border:1px solid grey;
	width: 70%;  
}

legend {
	padding-left: 30px;
 	font-size:90%;
  	text-align:right;
}
</style>
<?php 
echo $this->Html->css(array('jquery.autocomplete'));
echo $this->Html->script(array(
	    'jquery/jquery.bgiframe.min',
	    'jquery/jquery.ajaxQueue',
	    'jquery/development-bundle/ui/jquery.ui.core',
  	    'jquery/development-bundle/ui/jquery.ui.widget',
	    'jquery/development-bundle/ui/jquery.ui.position',
	    'jquery/development-bundle/ui/jquery.ui.autocomplete',
	    'jquery/development-bundle/ui/jquery.ui.draggable',
	    'jquery/development-bundle/ui/jquery.ui.resizable',
	    'jquery/development-bundle/ui/jquery.ui.dialog',
	    'jquery/development-bundle/ui/jquery.ui.datepicker'
	)
);
?>
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){

    $("#q").autocomplete({
        source: "<?php echo Configure::read('js_directory');?>search_salesreports.php",
        minLength: 3,
        autoFocus: true
    });


	$('#schedule_date').datepicker({
        showButtonPanel: true,
        changeMonth: true,
	    changeYear: true,
        firstDay: 1,
        dateFormat: 'm/yy',
        closeText: 'Close'
    });

	$("#ProgramsSalesReports input").keypress(function(e) {
		if (e.keyCode == 13) {
		    $(this).closest("form").submit();
		}
	});
		    
	$("#search_salesreports").live("click",function(){
		$("#ProgramsSalesReports").submit();
    });
});
/* ]]> */
</script>
<div class="search">
	<form id="ProgramsSalesReports" method="get" action="<?php echo $this->Html->url(array("admin"=>true,"controller" => "sales_reports","action" => "search"));?>" accept-charset="utf-8">
	<?php
    $qValue = null;
    if (isset($this->params['url']['q'])) {
    	$qValue = $this->params['url']['q'];
    }
    
    $qScheduleDate =null;
    if (isset($this->params['url']['q']) && isset($this->params['url']['schedule_date'])) {
    	$qScheduleDate = $this->params['url']['schedule_date'];
    }
    ?>
	    <div id="standard_search">	
		    <table cellspacing="0" cellpadding="0" style="background-color:#FFFFFF;border:1px solid #DDDDDD;clear:both;width:100%;">
		    	<tr><td align="right" width="30%"><label for="q">Search keyword:</label></td><td width="70%" align="left"><input type="text" name="q" value="<?php echo $qValue;?>" style="width:450px;" id="q" class="text ui-widget-content ui-corner-all" />&nbsp;<br />e.g 20110317--2011-03--1300330167-sr.xls OR KVKNEBOSUZ</td></tr>

		    	<tr><td align="right" width="30%"><label for="schedule_date">Schedule Program Date format(mm/YY)</label></td><td align="left" width="70%"><input style="width:100px;" name="schedule_date" id="schedule_date" size="15" value="<?php echo $qScheduleDate;?>" maxlength="12" type="text" alt="End Date" class="text ui-widget-content ui-corner-all" readonly="readonly" /></td></tr>
		    	<tr><td colspan="2" align="center"><a id="search_salesreports" style="position: relative;text-decoration: none !important;text-align: center;padding: 0;overflow: visible;margin-right: 0.1em;cursor: pointer;display: inline-block;font-size:13px;padding: 0.4em 1em;border-radius: 4px 4px 4px 4px;" class="ui-button ui-state-default ui-corner-all">Search</a></td></tr>
		    </table>
		</div>
	</form>
</div>
<br />