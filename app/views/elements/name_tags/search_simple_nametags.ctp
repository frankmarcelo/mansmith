<style type="text/css">
#blog_search {
    margin-bottom: 29px;
}

fieldset { 
	border:1px solid grey;
	width: 70%;  
}

legend {
	padding-left: 30px;
 	font-size: 90%;
  	text-align:right;
}
</style>
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){
	<?php 
	if( $this->Session->read('Auth.User.role_id') == Configure::read('administrator')){
	?>
	$("#q").autocomplete({
		source: "<?php echo Configure::read('js_directory');?>search_nametags.php",
		minLength: 3,
		autoFocus: true
	});
	<?php 
	}else{
	?>
	$("#q").autocomplete({
		source: "<?php echo Configure::read('js_directory');?>search_nametags.php?division_id=<?php echo $this->Session->read('Auth.User.role_id');?>",
		minLength: 3,
		autoFocus: true
	});
	<?php 	
	}	
	?>
	$("#NametagAdminSearchForm input").keypress(function(e) {
		if (e.keyCode == 13) {
		    $(this).closest("form").submit();
		}
	});
		    
	$("#search_nametag").live("click",function(){
		$("#NametagAdminSearchForm").submit();	
    });
});
/* ]]> */
</script>
<div class="search">
	<form id="NametagAdminSearchForm" method="get" action="<?php echo $this->Html->url(array("admin"=>true,"controller" => "name_tags","action" => "search"));?>" accept-charset="utf-8">
	<?php
    $qValue = null;
    if (isset($this->params['url']['q'])) {
    	$qValue = trim($this->params['url']['q']);
    }
    ?>
	    <div id="standard_search">	
		    <table cellspacing="0" cellpadding="0" style="background-color:#FFFFFF;border:1px solid #DDDDDD;clear:both;width:100%;">
		    	<tr><td><label for="q">Search keyword:</label><input type="text" name="q" value="<?php echo $qValue;?>" style="width:450px;" id="q" class="text ui-widget-content ui-corner-all" />&nbsp;<a id="search_nametag" style="position: relative;text-decoration: none !important;text-align: center;padding: 0;overflow: visible;margin-right: 0.1em;cursor: pointer;display: inline-block;font-size:13px;padding: 0.4em 1em;border-radius: 4px 4px 4px 4px;" class="ui-button ui-state-default ui-corner-all">Search</a>&nbsp;e.g 10th Marketing Strategy OR Pasig OR 1-Day seminar OR
Glorious Industrial and Dev't. Corp.(Company Name)<br />
		    	<a href="<?php echo $this->Html->url(array("admin"=>true,"controller" => "name_tags","action" => "search"));?>">Advanced Search</a>
		    	</td></tr>
		    </table>
		</div>
	</form>
</div>
<br />
