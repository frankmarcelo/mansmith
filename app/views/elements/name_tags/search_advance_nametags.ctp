<style type="text/css">
#blog_search {
    margin-bottom: 29px;
}

fieldset { 
	border:1px solid grey;
	width: 70%;  
}

legend {
	padding-left: 30px;
 	font-size:90%;
  	text-align:right;
}
</style>
<?php 
echo $this->Html->css(array('jquery.autocomplete'));
echo $this->Html->script(array(
	    'jquery/jquery.bgiframe.min',
	    'jquery/jquery.ajaxQueue',
	    'jquery/development-bundle/ui/jquery.ui.core',
  	    'jquery/development-bundle/ui/jquery.ui.widget',
	    'jquery/development-bundle/ui/jquery.ui.position',
	    'jquery/development-bundle/ui/jquery.ui.autocomplete',
	    'jquery/development-bundle/ui/jquery.ui.draggable',
	    'jquery/development-bundle/ui/jquery.ui.resizable',
	    'jquery/development-bundle/ui/jquery.ui.dialog',
	    'jquery/development-bundle/ui/jquery.ui.datepicker'
	)
);
?>
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){
	<?php 
	if( $this->Session->read('Auth.User.role_id') ==  Configure::read('administrator') ){
	?>
		$("#q").autocomplete({
			source: "<?php echo Configure::read('js_directory');?>search_nametags.php",
			minLength: 3,
			autoFocus: true
		});
	<?php 
	}else{
	?>	
		$("#q").autocomplete({
			source: "<?php echo Configure::read('js_directory');?>search_nametags.php?division_id=<?php echo $this->Session->read('Auth.User.role_id');?>",
			minLength: 3,
			autoFocus: true
		});
	<?php 	
	}	
	?>

	$('#schedule_date').datepicker({
        showButtonPanel: true,
        changeMonth: true,
	    changeYear: true,
        firstDay: 1,
        dateFormat: 'm/yy',
        closeText: 'Close'
    });

	$("#NametagAdminSearchForm input").keypress(function(e) {
		if (e.keyCode == 13) {
		    $(this).closest("form").submit();
		}
	});
		    
	$("#search_nametag").live("click",function(){
		$("#NametagAdminSearchForm").submit();	
    });
});
/* ]]> */
</script>
<div class="search">
	<form id="NametagAdminSearchForm" method="get" action="<?php echo $this->Html->url(array("admin"=>true,"controller" => "name_tags","action" => "search"));?>" accept-charset="utf-8">
	<?php
    $qValue = null;
    if (isset($this->params['url']['q'])) {
    	$qValue = $this->params['url']['q'];
    }
    
    $qScheduleDate =null;
    if (isset($this->params['url']['q']) && isset($this->params['url']['schedule_date'])) {
    	$qScheduleDate = $this->params['url']['schedule_date'];
    }
    ?>
	    <div id="standard_search">	
		    <table cellspacing="0" cellpadding="0" style="background-color:#FFFFFF;border:1px solid #DDDDDD;clear:both;width:100%;">
		    	<tr><td align="right" width="30%"><label for="q">Search keyword:</label></td><td width="70%" align="left"><input type="text" name="q" value="<?php echo $qValue;?>" style="width:450px;" id="q" class="text ui-widget-content ui-corner-all" />&nbsp;<br />e.g 10th Marketing Strategy OR Pasig OR 1-Day seminar OR
Glorious Industrial and Dev't. Corp.(Company Name)</td></tr>
		    	<?php
		    	if( $this->Session->read('Auth.User.role_id') == Configure::read('administrator') ){
					if(isset($divisions) && count($divisions)>0 ){
						$qDivision =null;
    					if (isset($this->params['url']['division']) && isset($this->params['url']['division'])) {
    						$qDivision = $this->params['url']['division'];
    					}	
		    	?>
		    	<tr><td align="right" width="30%"><label for="division">Division : </label></td><td width="70%" align="left">
		    		<select id="division" name="division">
		    		 	<option value="All">All</option>	
		    		 	<?php
	                 	foreach( $divisions as $division_key => $division ){
	                 		if( isset($this->params['url']['division']) && strtolower(trim($this->params['url']['division'])) == strtolower($division) ){
	                 			echo '<option value="'.$division.'" selected="selected">'.$division.'</option>';
	                 		}else{ 	
	                 			echo '<option value="'.$division.'">'.$division.'</option>';
	                 		}
	                	}
	                 	?>
	                </select>
		    	</td></tr>	
		    	<?php
		    		}
		    	}
		    	?>
		    	<tr><td align="right" width="30%"><label for="address">Program type: </label></td><td width="70%" align="left">
		    		<select id="type" name="type">
		    		 	<option value="All">All</option>	
		    		 	<?php
		    		 	if(is_array($types) && count($types)>0){
							foreach( $types as $type_key => $type_name ){
	                 			if( isset($this->params['url']['type']) && $type_name['ProgramsType']['seo_name']== $this->params['url']['type'] ){
	                 				echo '<option value="'.$type_name['ProgramsType']['seo_name'].'" selected="selected">'.$type_name['ProgramsType']['name'].'</option>';
	                 			}else{ 	
	                 				echo '<option value="'.$type_name['ProgramsType']['seo_name'].'">'.$type_name['ProgramsType']['name'].'</option>';
	                 			}
	                		}
						}
	                 	?>
	                </select>
		    	</td></tr>
		    	<tr><td align="right" width="30%"><label for="schedule_date">Schedule Program Date format(mm/YY)</label></td><td align="left" width="70%"><input style="width:100px;" name="schedule_date" id="schedule_date" size="15" value="<?php echo $qScheduleDate;?>" maxlength="12" type="text" alt="End Date" class="text ui-widget-content ui-corner-all" readonly="readonly" /></td></tr>
		    	<tr><td colspan="2" align="center"><a id="search_nametag" style="position: relative;text-decoration: none !important;text-align: center;padding: 0;overflow: visible;margin-right: 0.1em;cursor: pointer;display: inline-block;font-size:13px;padding: 0.4em 1em;border-radius: 4px 4px 4px 4px;" class="ui-button ui-state-default ui-corner-all">Search</a></td></tr>
		    </table>
		</div>
	</form>
</div>
<br />