<div id="header">
    <div class="container_16">
        <div class="grid_8 header-left">
        <?php
        	$sAdmin = 'Administrator';
        	if( $this->Session->read('Auth.User.role_id')==2){
        		$sAdmin = 'Mansmith';
        	}elseif( $this->Session->read('Auth.User.role_id')==3){
        		$sAdmin = 'Coach';	
        	}elseif( $this->Session->read('Auth.User.role_id')==4){
        		$sAdmin = 'Day8';
        	}
            echo $this->Html->link(__($sAdmin.' Dashboard', true), '/admin');
            echo ' <span></span> ';
        ?>
        </div>

        <div class="grid_8 header-right">
        <?php
            echo sprintf(__("You are currently logged in as: %s", true), $this->Session->read('Auth.User.name'));
            echo ' <span>|</span> ';
            echo $this->Html->link(__('Profile', true), array('admin'=>true,'controller' => 'users', 'action' => 'edit', $this->Session->read('Auth.User.id')));
            echo ' <span>|</span> ';
            echo $this->Html->link(__("Log out", true), array('plugin' => 0, 'controller' => 'users', 'action' => 'logout'));
        ?>
        </div>

        <div class="clear">&nbsp;</div>
    </div>
</div>
