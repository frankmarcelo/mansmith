<div id="nav">
    <ul class="sf-menu">
        <li><?php echo $this->Html->link(__('Programs', true), array('admin'=>true, 'controller' => 'programs', 'action' => 'index')); ?>
          <ul>
          <li><?php echo $this->Html->link(__('Add Programs',true),array('admin'=>true,'controller'=>'programs', 'action' => 'create'));?></li>
          <?php
          if( $this->Session->read('Auth.User.role_id')==1 ){
          ?>
          <li><?php echo $this->Html->link(__('All Programs',true),array('admin'=>true,'controller'=>'programs', 'action' => 'index'));?></li>
          <li><?php echo $this->Html->link(__('Search Programs',true),array('admin'=>true,'controller'=>'programs', 'action' => 'search'));?></li>
          <?php
             foreach( $roles as $navigation_key => $navigation_roles ){
                if( is_array($navigation_roles) ){
                   if( $navigation_roles['Role']['alias'] !== 'admin'){
                        echo '<li>'.$this->Html->link(__($navigation_roles['Role']['title'],true),array('admin'=>true,'controller'=>'programs', 'action' => 'list',$navigation_roles['Role']['title'])). '</li>';
                   }
                }elseif( !is_array($navigation_roles) ){
                   if( strtolower($navigation_roles) !=='admin' ){
                        echo '<li>'.$this->Html->link(__($navigation_roles,true),array('admin'=>true,'controller'=>'programs', 'action' => 'list',$navigation_roles)). '</li>';
                   }
                }
             }
          }else{
             foreach( $roles as $navigation_key => $navigation_roles ){
                if( $navigation_key == $this->Session->read('Auth.User.role_id') ){
                   echo '<li>'.$this->Html->link(__('Search '.$navigation_roles,true),array('admin'=>true,'controller'=>'programs', 'action' => 'search')). '</li>';
                   echo '<li>'.$this->Html->link(__($navigation_roles,true),array('admin'=>true,'controller'=>'programs', 'action' => 'index')). '</li>';
                }
             }
          }
          ?>
          </ul>
        <li>
        <li><?php echo $this->Html->link(__('Companies', true), array('admin'=>true, 'controller' => 'companies', 'action' => 'index')); ?>
          <ul>
          <li><?php echo $this->Html->link(__('Add Companies',true),array('admin'=>true,'controller'=>'companies', 'action' => 'create'));?></li>
          <li><?php echo $this->Html->link(__('Search Companies',true),array('admin'=>true,'controller'=>'companies', 'action' => 'search'));?></li>
          <?php
            if( $this->Session->read('Auth.User.role_id') == 1 ){
               echo '<li>'.$this->Html->link(__('View Companies',true),array('admin'=>true,'controller'=>'companies', 'action' => 'index')). '</li>';
            }else{
               foreach( $roles as $navigation_key => $navigation_roles ){
                  if( $navigation_key == $this->Session->read('Auth.User.role_id') ){
                     echo '<li>'.$this->Html->link(__('View Companies',true),array('admin'=>true,'controller'=>'companies', 'action' => 'index')). '</li>';
                  }
               }
            }
          ?>
          </ul>
        </li>
        <li>
        <?php echo $this->Html->link(__('Participants', true), array('admin'=>true, 'controller' => 'participants', 'action' => 'index')); ?>
          <ul>
          <li><?php echo $this->Html->link(__('Add Participants',true),array('admin'=>true,'controller'=>'participants', 'action' => 'create'));?></li>
          <li><?php echo $this->Html->link(__('Search Participants',true),array('admin'=>true,'controller'=>'participants', 'action' => 'search'));?></li>
          <?php 
            if( $this->Session->read('Auth.User.role_id') == 1 ){
               echo '<li>'.$this->Html->link(__('View Participants',true),array('admin'=>true,'controller'=>'participants', 'action' => 'index')). '</li>';
            }else{
               foreach( $roles as $navigation_key => $navigation_roles ){
                 if( $navigation_key == $this->Session->read('Auth.User.role_id') ){
                    echo '<li>'.$this->Html->link(__('View Participants',true),array('admin'=>true,'controller'=>'participants', 'action' => 'index')). '</li>';
                 }
               }
            }
            ?>
          </ul>
        </li>
        <li>
          <?php 
			echo $this->Html->link(__('Documents', true), array('admin'=>true, 'controller' => 'attendance_sheets', 'action' => 'index')); ?>
          <ul>
            <li><?php echo $this->Html->link(__('Attendance Sheets',true),array('admin'=>true,'controller'=>'attendance_sheets','action' => 'index'));?></li>
            <li><?php echo $this->Html->link(__('Certificates',true),array('admin'=>true,'controller'=>'certificates','action' => 'index'));?></li>
            <li><?php echo $this->Html->link(__('Directory of Participants',true),array('admin'=>true,'controller'=>'directory_of_participants','action' => 'index'));?></li>
            <li><?php echo $this->Html->link(__('Name Tags',true),array('admin'=>true,'controller'=>'name_tags', 'action' => 'index'));?></li>
          </ul>
        </li>
        <li>
          <?php echo $this->Html->link(__('Financials', true), array('admin'=>true, 'controller' => 'billing_eb','action'=>'index')); ?>
          <ul>
          <li><?php echo $this->Html->link(__('Billing Eb',true),array('admin'=>true,'controller'=>'billing_eb','action'=>'index'));?></li>
          <li><?php echo $this->Html->link(__('Billing Non-eb',true),array('admin'=>true,'controller'=>'billing_non_eb','action'=>'index'));?></li>
          <li><?php echo $this->Html->link(__('Billing OSP',true),array('admin'=>true,'controller'=>'billing_osp','action'=>'index'));?></li>
          <li><?php echo $this->Html->link(__('Billing Regular',true),array('admin'=>true,'controller'=>'billing_regular','action'=>'index'));?></li>
          <li><?php echo $this->Html->link(__('Invoice',true),array('admin'=>true,'controller'=>'invoice','action'=>'index'));?></li>
          </ul>
        </li>
        <li>
        	<?php echo $this->Html->link(__('Sales Reports', true), array('admin' => true, 'controller' => 'sales_reports', 'action' => 'index')); ?>
        	<ul>
         	<li><?php echo $this->Html->link(__('Sales Reports',true),array('admin'=>true,'controller'=>'sales_reports','action'=>'index'));?></li>
          	<li><?php echo $this->Html->link(__('Generate Sales Report',true),array('admin'=>true,'controller'=>'sales_reports','action'=>'generate_sales_reports'));?></li>
          	</ul>
        </li>
        <!--<li><?php echo $this->Html->link(__('Newsletter', true),array('admin'=>true,'controller'=>'newsletter','action'=>'index'))?>
          <ul>
            <li><?php echo $this->Html->link(__('Newsletter', true),array('admin'=>true,'controller'=>'newsletter','action'=>'index')); ?>
            <li><?php echo $this->Html->link(__('Newsletter Templates', true),array('admin'=>true,'controller'=>'newsletter','action'=>'index')); ?>
            <li><?php echo $this->Html->link(__('Signups', true),array('admin'=>true,'controller'=>'newsletter','action'=>'index')); ?></li>
          </ul>
        </li>-->
        <?php
        if( $this->Session->read('Auth.User.role_id')==1 ){
        ?>
        <li><?php echo $this->Html->link(__('System', true), array('admin' => true, 'controller' => 'systems', 'action' => 'index')); ?>
           <ul>
             <li><?php echo $this->Html->link(__('Manage Lookups', true), array('plugin' => null, 'controller' => 'systems', 'action' => 'index')); ?></li>
             <li><?php echo $this->Html->link(__('Data Dumps', true), array('plugin' => null, 'controller' => 'systems', 'action' => 'data_dumps')); ?></li>
             <li><?php echo $this->Html->link(__('Back Ups', true), array('plugin' => null, 'controller' => 'systems', 'action' => 'backups')); ?></li>
           </ul>
        </li>
        <li><?php echo $this->Html->link(__('Speakers', true), array('plugin' => null, 'controller' => 'speakers', 'action' => 'index')); ?>
           <ul>
             <li><?php echo $this->Html->link(__('Speakers', true), array('plugin' => null, 'controller' => 'speakers', 'action' => 'index')); ?></li>
           </ul>
        </li>
        <li><?php echo $this->Html->link(__('Users', true), array('plugin' => null, 'controller' => 'users', 'action' => 'index')); ?>
           <ul>
             <li><?php echo $this->Html->link(__('Users', true), array('plugin' => null, 'controller' => 'users', 'action' => 'index')); ?></li>
             <li><?php echo $this->Html->link(__('Roles', true), array('plugin' => null, 'controller' => 'roles', 'action' => 'index')); ?></li>
           </ul>
        </li>
        <?php
        }
        ?>
    </ul>
</div>
