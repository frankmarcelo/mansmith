<?php

echo $this->Html->css(array('fcbkselection'));
echo $this->Html->script(array(
    'jquery/jquery.bgiframe.min',
    'jquery/jquery.ajaxQueue',
    'jquery/jquery.fcbkselection',
    'jquery/jquery.simplemodal',
    'jquery/jquery.tinymce',
    'tiny_mce/tiny_mce',
    'jquery/jquery.maskMoney',
    'jquery/development-bundle/ui/jquery.ui.core',
    'jquery/development-bundle/ui/jquery.ui.widget',
    'jquery/development-bundle/ui/jquery.ui.position',
    'jquery/development-bundle/ui/jquery.ui.autocomplete',
    'jquery/development-bundle/ui/jquery.ui.draggable',
    'jquery/development-bundle/ui/jquery.ui.resizable',
    'jquery/development-bundle/ui/jquery.ui.dialog',
    'jquery/development-bundle/ui/jquery.ui.datepicker'

    )
);

$this->Participant->setParticipantData( $participant['Participant'] );
$this->ParticipantExtraInfo->setExtraInfo( $participant ); 
?>
<style type="text/css">
    .info {
        width: 80px;
        height: 50px;
    }
</style>
<script type="text/javascript">
/* <![CDATA[ */
var tips = $("#validateTips");

function setValue(a){
   var selectedId = $(a).find('input[type="hidden"]').attr('id');
   $('#'+selectedId).attr('value',$('#'+selectedId).attr('value')+'_selected');
}

function updateTips(t) {
   tips.text(t).effect("highlight",{},1500);
}

function checkLength(o,n,min,max,format) {
   if ( o.val().length > max || o.val().length < min ) {
		o.addClass('ui-state-error');
        if( format=='participant'){
            $('#loader').fadeOut("fast").hide();
            $('#status_msgs').append("Length of " + n + " must be between "+min+" and "+max+".<br />");
            $('#update_participant').dialog('close');
            o.focus();
        }else{
	    	updateTips("Length of " + n + " must be between "+min+" and "+max+".");
        }
		return false;
   } else {
	return true;
   }
}

$(document).ready(function(){
    
   $( "#name" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "<?php echo Configure::read('js_directory');?>search_company_starts_with.php",
                dataType: "json",
                data: {
                    term: request.term,
                    limit: 20
                },
                success: function( data ) {
                    response( $.map(data,function(item) {
                        return {
                            label: item.name,
                            value: item.name,
                            data: item
                        }
                    }));
                }
            });
       },
       minLength: 3,
	   select: function( event, ui,item ) {
           $('#company_id').val(parseInt(ui.item.data.company_id,10));
       }
   });
   
   $("#position").autocomplete({
		source: "<?php echo Configure::read('js_directory');?>search_position_starts_with.php",
		minLength: 1
   });
 
   $("#address").autocomplete({
		source: "<?php echo Configure::read('js_directory');?>search_participant_address.php",
		minLength: 3
   });

   $(window).resize(function() {
      	$("#updateparticipant").dialog("option", "position", "center");
   });

   $(window).scroll(function() {
       	$("#updateparticipant").dialog("option", "position", "center");
   });

   $('textarea.tinymce').tinymce({
        script_url : '<?php echo Configure::read('js');?>tiny_mce/tiny_mce.js',
        theme : "advanced",
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,anchor,image,cleanup",                        
        theme_advanced_buttons3 : "",                        
        theme_advanced_buttons4 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true
   });

   $('#update_participant').live("click",function(){
   		$("#updateparticipant").dialog({
 	       bgiframe: true,
	       height: 140,
	       modal: true
	    });
    
        $('#status_msgs').empty();

		var participantValid = true;
		var company_name = $('#name');
        var company_id = $('#company_id');

		var first_name = $('#first_name');
		var last_name = $('#last_name');
		var position = $('#position');
		var address = $('#address');
		var email = $('#email');
		var phone = $('#phone');

		allFields = $([]).add(company_name).add(company_id).add(first_name).add(last_name).add(position).add(address).add(email).add(phone);
        allFields.removeClass('ui-state-error');

        participantValid = participantValid && checkLength(company_name,"Company name is too short",3,1000,'participant');
        participantValid = participantValid && checkLength(company_id,"Company name is not found please search a valid one.",1,1000,'participant');
		participantValid = participantValid && checkLength(first_name,"First name is too short",1,1000,'participant');
		participantValid = participantValid && checkLength(last_name,"Last name is too short",1,1000,'participant');
		participantValid = participantValid && checkLength(position,"Position name",1,1000,'participant');
		participantValid = participantValid && checkLength(address,"Address",3,1000,'participant');
		participantValid = participantValid && checkLength(email,"Email address",3,1000,'participant');
		participantValid = participantValid && checkLength(phone,"Phone number",3,1000,'participant');

		if( participantValid ){
           $("#update_participant").empty().fadeIn("slow",function(){
               $('#loader').show();
               $("#update_participant").attr('value',"Please Wait....");
               $(this).attr('disabled',"disabled");

               $.ajax({
                  cache: false  ,
                  type:  "POST" ,
                  url:   "<?php echo Configure::read('js_directory');?>ajax_update_participant.php",
                  data:  $("#ParticipantAdminUpdateForm :input").serializeArray(),
                  dataType: "json" ,
                  success: function(data){
                     var response = eval(data);
                     $(this).dialog('close');
                     $('#loader').append('<br />You are now being redirected to info page.');
                     window.location = (response.data.url);
                  }
               });
           });
       }
	   $("#update_participant").removeAttr('disabled');
	   $("#update_participant").attr('value',"Update participant");
       $("#update_participant").attr('enabled',"enabled");
       $(this).dialog('close');    
   });
});
/* ]]> */
</script>
<style type="text/css">
  #map_canvas { height: 600px;width: 400px; }
  .participant_table tr, th, td {
    background: none repeat scroll 0 0 transparent;
    border: 1;
    font-size: 100%;
    margin: 0;
    outline: 0 none;
    padding: 0;
    vertical-align: baseline;
  }
</style>
<?php 
$this->Html->addCrumb('Participants', '/admin/participants');
$this->Html->addCrumb('<strong>Update Participant Information for '.$this->Participant->getFullName().'</strong>');

$company = $participant['Company']['name'];
?>
<div class="users form">
    <h2><?php __('Update Participant Information for <a href="'.$this->Html->url(array("admin"=> true,'controller'=>'participants','action'=>'info',$this->Participant->getSeoUri())).'" target="_blank">'.$this->Participant->getFullName()); ?></a></h2>
    <form id="ParticipantAdminUpdateForm" name="ParticipantAdminUpdateForm" method="post" accept-charset="utf-8" onSubmit="return false;">
        <input type="hidden" name="_method" value="POST" />
        <input type="hidden" name="data[Participant][who_modified]" value="<?php echo $this->Session->read('Auth.User.id');?>" />
        <input type="hidden" name="data[Participant][id]" value="<?php echo $this->Participant->get('id');?>" />
    	<table style="margin-bottom: 7px;" width="698" cellpadding="4" cellspacing="1">
        	<tbody style="background-color: rgb(240, 240, 225);">
            	<tr><th class="th-new th-left" colspan="3">Fill in the required fields <span style="color: rgb(220, 20, 60);"><b>*</b></span></th></tr>
            	<tr class="control-td-row" bgcolor="#E6E6CC">
                	<td>
                    	<table width="100%" cellpadding="0" cellspacing="0">
                    		<tr><th class="th-new th-left" colspan="3">Participant Profile</th></tr>
                        	<tr><td>
                        		<table width="100%" cellpadding="0" cellspacing="0">
                    				<tr class="striped">
                    					<td width="15%" align="right"><label for="company_name">Company Name:</label></td>
                    					<td width="90%" align="left" colspan="5"><input type="text" id="name" name="data[Company][name]" maxlength="60" style="width:350px;" class="text ui-widget-content ui-corner-all" value="<?php echo (isset($company))? $company:'';?>" />&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span><input type="hidden" id="company_id" name="data[Company][id]" value="<?php echo intval($participant['Company']['id']);?>" /></td>
                    				</tr>
                    				<tr class="striped">
                        				<td width="10%" align="right"><label for="participants_grouping_id">Company Grouping:</label></td>
                        				<td width="90%" align="left" colspan="5">
                        					<select id="grouping" name="data[Participant][participants_grouping_id]" style="width:250px;" class="text ui-widget-content ui-corner-all">
                        					<?php 
                        					if( is_array($participant_grouping) && sizeof($participant_grouping)>0 ){
                        						foreach( $participant_grouping as $grouping_key => $grouping_name ){
                        							if( (strstr(strtolower($grouping_name),'none') && !isset($participant['ParticipantsGrouping']['id'])) ||
														$grouping_key == $participant['ParticipantsGrouping']['id']	
													){
                        								echo '<option value="'.$grouping_key.'" selected="selected">'.$grouping_name.'</option>';
                        							}else{
														echo '<option value="'.$grouping_key.'">'.$grouping_name.'</option>';
                        							}
                        						}
											}
                        					?>
                        					</select>&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span>
                        				</td>
                        			</tr>
                        			<tr class="striped">
                        				<td width="10%" align="right"><label for="participants_grouping_id">Participant Info:</label></td>
                        				<td align="left" colspan="5">
                        					<table width="100%" cellpadding="0" border="1" cellspacing="0">
                        						<tr >
	                        						<th rowspan="2">Designation&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span></th>
	                        						<th colspan="4" style="text-align:center;">Name</th>
	                        					</tr>
                        						<tr >
	                        						<th>First&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span></th>
	                        						<th>Middle</th>
	                        						<th>Last&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span></th>
	                        						<th>Nick</th>
	                        					</tr>
                        						<tr>
                        							<td>
                        							<select id="title" name="data[Participant][participants_title_id]" style="width:70px;" class="text ui-widget-content ui-corner-all">
                        							<?php
                        							if( is_array($participant_title) && sizeof($participant_title)>0 ){
                        								foreach( $participant_title as $title_key => $title_name ){
                        									if( (strstr(strtolower($title_name),'mr.') &&  !isset($participant['ParticipantsTitle']['id']) ) ||
																$title_key == $participant['ParticipantsTitle']['id']		
															){
                        										echo '<option value="'.$title_key.'" selected="selected">'.$title_name.'</option>';
                        									}else{
																echo '<option value="'.$title_key.'">'.$title_name.'</option>';
                        									}
                        								}
													}
                        							?>
                        							</select></td>
                        							<td><input type="text" id="first_name" name="data[Participant][first_name]" value="<?php echo $this->Participant->get('first_name');?>" maxlength="60" class="text ui-widget-content ui-corner-all"  /></td>
                        							<td><input type="text" id="middle_name" name="data[Participant][middle_name]" value="<?php echo $this->Participant->get('middle_name');?>" maxlength="60"class="text ui-widget-content ui-corner-all"  /></td>
                        							<td><input type="text" id="last_name" name="data[Participant][last_name]" value="<?php echo $this->Participant->get('last_name');?>" maxlength="60" class="text ui-widget-content ui-corner-all" /></td>
                        							<td><input type="text" id="nick_name" name="data[Participant][nick_name]" value="<?php echo $this->Participant->get('nick_name');?>" maxlength="60" class="text ui-widget-content ui-corner-all" /></td>
                        						</tr>
                        					</table>
                        				</td>
                        			</tr>
                        			<tr class="striped">
                    					<td width="15%" align="right"><label for="company_name">Participant Position:</label></td>
                    					<td width="90%" align="left" colspan="5"><input type="text" id="position" name="data[Participant][participants_position_id]" value="<?php echo $participant['ParticipantsPosition']['name'];?>" maxlength="60" style="width:350px;" class="text ui-widget-content ui-corner-all" />&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span></td>
                    				</tr>
                        			<tr class="striped">
                    					<td width="15%" align="right"><label for="notes">Notes:</label></td>
                    					<td width="90%" align="left" colspan="5"><textarea id="notes" name="data[Participant][notes]" value="<?php echo $this->Participant->get('notes');?>" rows="15" cols="40" style="width: 100%" class="tinymce"></textarea></td>
                    				</tr>
                        					
                        		</table>
                        	</td>
                        	</tr>
                        	<tr><th class="th-new th-left" colspan="3">Contact Information</th></tr>
                        	<tr class="control-td-row" bgcolor="#E6E6CC">
                				<td>
			                    	<table width="100%" cellpadding="0" cellspacing="0">
			                    		<tr class="striped">
                    						<td width="15%" align="right"><label for="address">Address:</label></td>
                    						<td width="90%" align="left" colspan="5"><input type="text" id="address" name="data[Participant][address]" value="<?php echo $this->Participant->get('address');?>" maxlength="60" style="width:350px;" class="text ui-widget-content ui-corner-all"  />&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span></td>
                    					</tr>
                    					<tr class="striped">
                    						<td width="15%" align="right"><label for="zip_code">Zip Code:</label></td>
                    						<td width="90%" align="left" colspan="5"><input type="text" id="zip_code" name="data[Participant][zip_code]" value="<?php echo $this->Participant->get('zip_code');?>" maxlength="60" style="width:350px;" class="text ui-widget-content ui-corner-all"  /></td>
                    					</tr>
			                    		<tr class="striped">
                    						<td width="15%" align="right"><label for="email">Email:</label></td>
                    						<td width="90%" align="left" colspan="5"><input type="text" id="email" name="data[Participant][email]" value="<?php echo $this->Participant->get('email');?>" maxlength="60" style="width:350px;" class="text ui-widget-content ui-corner-all"  />&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span></td>
                    					</tr>
                    					<tr class="striped">
                    						<td width="15%" align="right"><label for="phone">Phone:</label></td>
                    						<td width="90%" align="left" colspan="5"><input type="text" id="phone" name="data[Participant][phone]" value="<?php echo $this->Participant->get('phone');?>" maxlength="60" style="width:350px;" class="text ui-widget-content ui-corner-all"  />&nbsp;<span style="color: rgb(220, 20, 60);"><b>*</b></span></td>
                    					</tr>
                    					<tr class="striped">
                    						<td width="15%" align="right"><label for="mobile">Mobile :</label></td>
                    						<td width="90%" align="left" colspan="5"><input type="text" id="mobile" name="data[Participant][mobile]" value="<?php echo $this->Participant->get('mobile');?>" maxlength="60" style="width:350px;" class="text ui-widget-content ui-corner-all"  /></td>
                    					</tr>
                    					<tr class="striped">
                    						<td width="15%" align="right"><label for="fax">Fax :</label></td>
                    						<td width="90%" align="left" colspan="5"><input type="text" id="fax" name="data[Participant][fax]" value="<?php echo $this->Participant->get('fax');?>" maxlength="60" style="width:350px;" class="text ui-widget-content ui-corner-all"  /></td>
                    					</tr>	
			                    	</table>
                    			</td>
                    		</tr>
                        </table>
                	</td>
            	</tr>
            	<tr class="control-td-row" bgcolor="#E6E6CC">
					<td colspan="3" align="right">
        				<input name="data[Participant][reset]" id="reset_participant" value="Cancel" type="reset" class="ui-button ui-state-default ui-corner-all">
						<input name="data[Participant][update]" id="update_participant" value="Update Participant" type="submit" class="ui-button ui-state-default ui-corner-all">
					</td>
				</tr>
	    	</tbody>
		</table>
	</form>
</div>
<div style="display:none;" id="updateparticipant" title="Update <?php echo $this->Participant->getFullName();?>"><div id="loader">Please wait while processing the request<br /><img src="<?php echo $this->Html->url('/img/ajaxloading.gif');?>" alt="loading..." /></div><div id="status_msgs" /></div></div>