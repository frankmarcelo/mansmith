<style type="text/css">
table tr td {
    border-bottom: 1px solid #DFDFDF !important;
    vertical-align: middle !important;
}

.tblClass{
    width: 910px !important;
}

.listing {
    width: 910px !important;
}

.listing .box1 {
    float: left !important;
    padding: 0 0 0 6px !important;
    width:250px !important;
}
.listing .box2 {
    float: left !important;
    text-align: left !important;
    width: 150px !important;
}

.listing .box3 {
    float: left !important;
    text-align: left !important;
    width: 145px !important;
}

.listing .box5 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 190px !important;
}

.listing .box6 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 128px !important;
}

.listing .box7 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 107px !important;
}
</style>
<div class="users index">
	<table cellspacing="0" cellpadding="0" class="tblClass">
		<tr><td align="center"><div class="paging">
            <?php
            $paginator->options(array('url' => $this->passedArgs));
            $sPaginator = $paginator->first('<< First').' '.
            $paginator->prev('< Previous').' '.
            $paginator->numbers().' '.
            $paginator->next('Next >').' '.
            $paginator->last('Last >>');
            echo $sPaginator;
            echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));?>
            </div>
        </td></tr>
        <tr><td>
            <?php
            $i = 0;
            if( isset($nametags) && is_array($nametags) ){
                foreach( $nametags as $nametag_key => $nametag_info ){

                    $nametagFileInfo = $this->NameTags->loadNameTagsByProgramId($nametag_info['Program']['id']);
                    $oProgram = $this->Program->loadProgramDataById($nametag_info['Program']['id'],$recursive=false);
                    $this->Program->setProgramData($nametag_info['Program']);
                    $this->ProgramExtraInfo->setExtraInfo($oProgram);
                    $this->NameTags->setNameTagData($nametag_info);
                    $sCss = ( ($i % 2) ==0 )? 'listing_accomm':'listing_tr';
            ?>
                    <div class="listing" id="<?php echo $sCss;?>">
                        <div class="box1" id="all_programs_<?php echo $this->NameTags->getNameTagId();?>">
                            <div>
                                <h3><a href="<?php echo $this->Html->url(array('controller'=>'programs','action'=>'info',$this->NameTags->getSeoUri()));?>"><?php echo $this->NameTags->getProgramTitle();?></a></h3>
                                <div><address><?php echo $this->Program->getFullAddress();?></address></div>
                            </div>
                        </div><!--end box1-->
                        <div class="box2"><div><?php echo $this->ProgramExtraInfo->getSchedule($shortcut=true);?></div></div>
                        <div class="box3"><div>
                        <?php
                        if( $this->Session->read('Auth.User.role_id') == Configure::read('administrator')){
                        ?><a href="<?php echo $this->Html->url(array('controller'=>'name_tags','action'=>'list',$this->ProgramExtraInfo->getProgramDivision()));?>">
                        <?php echo $this->ProgramExtraInfo->getProgramDivision();?></a><?php
                        }else{ echo $this->ProgramExtraInfo->getProgramDivision();}?> | <span class="tiny"><?php echo $this->ProgramExtraInfo->getProgramType();?></span></div><!--end box3--></div>
                        <div class="box5">
                        <?php
                        if( count($nametagFileInfo) > 0 ){
                            $nameTagDirectory = $this->NameTags->getNameTagDirectory(intval($nametag_info['Program']['id']));
                            foreach( $nametagFileInfo as $key => $nameTagFile ){
                                if( intval($nametag_info['Program']['id']) >0 && isset($nameTagFile['ProgramsNameTag']['source_file']) ){
                                    
                                    $nametagIds = array();
                                    if(isset($nametag_info['NameTagsParticipants']) && count($nametag_info['NameTagsParticipants']) ){
                                        foreach( $nametag_info['NameTagsParticipants'] as $nametagInfo ){
                                            $nametagIds[] = intval($nametagInfo['participant_id']);
                                        }
                                    }
                                    $file = trim($nameTagDirectory.$nameTagFile['ProgramsNameTag']['source_file']);
                                    if( in_array($participantId,$nametagIds) &&  $this->NameTags->nameTagExist($nametag_info['Program']['id'],$file)){
                        ?>
                        <a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'name_tags','action'=>'download',$nameTagFile['ProgramsNameTag']['id']));?>"><?php echo trim($nameTagFile['ProgramsNameTag']['source_file']);?></a>
                        <?php		}else{
                                        echo '&nbsp;';
                                    }
                                }
                            }
                        }
                        ?><br/><?php echo $this->NameTags->createdBy($users);?>
                        </div>
                        <div class="box6"><div align="left">&nbsp;</div></div>
                        <div class="box4">&nbsp;</div><!--end box4-->
                        <div class="clearboth"></div>
                    </div><!--end listing-->
            <?php
                    $i++;
                }//end of foreach
            }//end of programs attendance sheet
            ?>
        </td></tr>
     </table>
</div>