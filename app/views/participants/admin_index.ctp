<?php echo $this->element('program/program_default_js'); ?>
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){
	$('#participants').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true});
	$(window).resize(function(){});
	$(window).scroll(function(){});
	$('.delete').live('click',function(){});
});
/* ]]> */
</script>

<?php 
echo $this->Html->css(array(
    'jquery.autocomplete',
    'fcbkselection'
    )
);
?>
<script type="text/javascript">
/* <![CDATA[ */
if(!String.prototype.startsWith){
    String.prototype.startsWith = function (str) {
        return !this.indexOf(str);
    }
}

$(document).ready(function(){
    
	$('.delete').live('click',function(){
		var title = '<strong><a target="_blank" href="'+$(this).attr('seouri')+'">'+$.trim($(this).attr('title'))+'</a></strong>';
		var dialog_message ='<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>';
		dialog_message +=title+' will be permanently deleted and cannot be recovered. Continue?</p><br />';
		dialog_message +='<p align="center"><button style="cursor:pointer;" class="ui-state-default ui-corner-all" type="button" id="cancel">Cancel</button>&nbsp;';
		dialog_message +='<button style="cursor:pointer;" seo="'+$(this).attr('seo')+'" seouri="'+$(this).attr('seouri')+'" class="ui-state-default ui-corner-all" style="font-color:#F0F0F0;" type="button" id="confirm_delete">Confirm delete</button>&nbsp;</p>';
		$("#confirm_msg").empty().html(dialog_message);
		$('#dialog').dialog({modal:true,hide:'explode'});
	});

	$('.add_info').live('click',function(){
		$(this).empty().html("Please wait...");
		var participantId = parseInt($(this).attr('id'),10);
		$.post( "<?php echo Configure::read('js_directory');?>save_participant_newsletter.php?"+Math.random() , { participantId: participantId, status: 1 },
        function(data) {
            $('#'+participantId).removeClass().empty().fadeIn("slow",function(){
                $(this).addClass("remove_newsletter").html("Remove from mailing list");
            });
        },
        "json"
        );
		
	});

	$('.remove_newsletter').live('click',function(){
		$(this).empty().html("Please wait...");
		var participantId = parseInt($(this).attr('id'),10);
		$.post( "<?php echo Configure::read('js_directory');?>save_participant_newsletter.php?"+Math.random() , { participantId: participantId, status: 0 },
        function(data) {
            $('#'+participantId).removeClass().empty().fadeIn("slow",function(){
                $(this).addClass("add_info").html("Add to mailing list");
            });
        },
        "json"
        );
	});

    $("#confirm_delete").live("click",function(){
		$("#cancel").attr('disabled',true);
		$(this).empty().html("Deleting participant. Please wait...").attr('disabled',true);
		var seo_uri = $.trim($(this).attr('seo'));
        $.ajax({
            cache: false,
            type:  "GET",
            url:   "<?php echo Configure::read('js_directory');?>delete_participant.php?id="+seo_uri+'&who_modified=<?php echo $this->Session->read('Auth.User.id');?>',
            dataType: "json" ,
            success: function(data){
                $("#dialog").dialog('close');
                window.location.reload();
            }
        });
    });
	
	$("#cancel").live("click",function(){
		$("#dialog").dialog('close');
	});
	
	$("#q").focus();
	
	$(window).resize(function() {
	    $("#dialog").dialog("option", "position", "center");
	});
	
	$(window).scroll(function() {
	    $("#dialog").dialog("option", "position", "center");
	});
	
});
/* ]]> */
</script>
<style type="text/css">
.listing .box1 {
    float: left !important;
    padding: 0 0 0 6px !important;
    width:165px !important;
}
.listing .box2 {
    float: left !important;
    text-align: left !important;
    width: 114px !important;
}

.listing .box3 {
    float: left !important;
    text-align: left !important;
    width: 158px !important;
}

.listing .box5 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 150px !important;
}

.listing .box6 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 156px !important;
}

.listing .box7 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 147px !important;
}

.listing .box7 a {
    color: #0171B9 !important;
    text-decoration: underline !important;
}

.listing .box8 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 105px !important;
}
</style>
<?php 
$this->Html->addCrumb('<strong>All Participants</strong>');
?>
<div id="dialog" title="Delete Participant" style="display:none;"><div id="confirm_msg">&nbsp;</div></div>
<div class="users index">
	<?php 
	if( $this->Session->read('Auth.User.role_id') == 1){	
		echo '<h2>All Participants</h2>';
	}else{
		echo '<h2>All '.$roles[$this->Session->read('Auth.User.role_id')].' Participants</h2>';
	}
	?>
	<?php echo $this->element('participant/search_simple_participant'); ?>
    <div>
       <div id="participants">
            <ul><li><a href="#list_participants"><span>Participants</span></a></li></ul>
            <div id="list_participants">

            	 	<table cellspacing="0" cellpadding="0" style="background-color: #FFFFFF;border: 1px solid #DDDDDD;clear: both;width: 100%;">
    					<?php 
    					if(!empty($participants) && sizeof($participants) > 0){
    					?>
    					<tr><td align="center" colspan="7"><div class="paging">
		                 	<?php
		                 	$paginator->options(array('url' => '/'),array('model'=> 'Participant'));
                            $sPaginator = $paginator->first('<< First',array('model'=> 'Participant')).' '.
							$paginator->prev('< Previous',array('model'=> 'Participant')).' '. 
							$paginator->numbers(array('model'=> 'Participant')).' '.
							$paginator->next('Next >',array('model'=> 'Participant')).' '.
							$paginator->last('Last >>',array('model'=> 'Participant'));
                            echo $sPaginator."<br />";
                            echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true),array('model'=> 'Participant')));?>
	                 		</div>
	                 	</td></tr>
	                 	<?php 
    					}
	                 	?>
	                 	<tr>
	                 		<th style="border-right: 1px solid #DFDFDF;width:170px;"><?php echo $paginator->sort('Sort By Name','first_name');?></th>
	                 		<th style="border-right: 1px solid #DFDFDF;width:100px;"><?php echo $paginator->sort('Position','position');?></th>
	                 		<th style="border-right: 1px solid #DFDFDF;width:150px;"><?php echo $paginator->sort('Grouping','participants_grouping_id');?></th>
	                 		<th style="border-right: 1px solid #DFDFDF;width:150px;"><?php echo $paginator->sort('Company','company_id');?></th>
	                 		<th style="border-right: 1px solid #DFDFDF;width:150px;">Attended Programs</th>
	                 		<th style="border-right: 1px solid #DFDFDF;width:150px;">Certificates</th>
	                 		<th style="border-right: 1px solid #DFDFDF;width:100px;">Phone/Mobile/Fax</th>
	                 	</tr>
	                	<tr><td colspan="7"> 
              	 		<?php 
    					
    					$i = 0;
	                   	foreach( $participants as $participant_key => $participant_info ){
	                   		$this->Participant->setParticipantData( $participant_info['Participant'],$participant_info['ParticipantsTitle']['name']);
	                      	$this->ParticipantExtraInfo->setExtraInfo( $participant_info); 
	                      	$sCss = ( ($i % 2) ==0 )? 'listing_accomm':'listing_tr';
                    	?>
                    	<div class="listing" id="<?php echo $sCss;?>">
							<div class="box1" id="all_participants_<?php echo $this->Participant->get('id');?>">
	                   			<div><h3><a href="<?php echo $this->Html->url(array('controller'=>'participants','action'=>'info',$this->Participant->getSeoUri()));?>"><?php echo $this->Participant->getFullName();?></a></h3></div>
		        			</div><!--end box1-->
		        			<div class="box2"><?php echo $this->ParticipantExtraInfo->getPosition();?></div>
		        			<div class="box3"><?php echo $this->ParticipantExtraInfo->getParticipantsGrouping();?></div>
		        			<div class="box5"><?php echo $this->ParticipantExtraInfo->getParticipantsCompany();?></div>
		        			<div class="box6"><?php echo $this->ParticipantExtraInfo->getAttendedProgram($limit=3);?></div>
		        			<div class="box7"><?php echo $this->ParticipantExtraInfo->getParticipantCertificate();?></div>
		        			<div class="box8"><?php echo $this->Participant->getPhone();?><br/><?php echo $this->Participant->getMobile();?><br/><?php echo $this->Participant->getFax();?></div>
		        			<div class="box4"><?php if($this->Participant->getEmail()){?><span><strong>Email:</strong>&nbsp;<a href="mailto:<?php echo $this->Participant->getEmail();?>"><?php echo $this->Participant->getEmail();?></a></span><?php if($this->Participant->getFullAddress()){?><span><strong>Address:</strong>&nbsp;<?php echo $this->Participant->getFullAddress();?></span><br /><?php } ?><br /><?php } ?></div><!--end box4-->
		        			<div class="box4">
		        				<a class="edit" href="<?php echo $this->Html->url(array('controller'=>'participants','action'=>'info',$this->Participant->getSeoUri()));?>">View Full Details</a>
	                        	<?php
                                echo $this->Participant->getDisplayDelete();
								echo $this->Participant->getDisplayEdit();
								echo $this->Participant->getNewsletterButton();
                                echo $this->ParticipantExtraInfo->createdBy($users);
                                ?>
	                        </div><!--end box4-->
		        			<div class="clearboth"></div>
	                	</div><!--end listing-->
                    	<?php 
                      		$i++;
                   		}  	
                 		?>
                 		</td></tr>
                  		<?php 
	                  	if(!empty($participants)){
	                  	?>
						<tr><td align="center" colspan="7" class="striped"><div class="paging">
	    	            <?php
	    	            $paginator->options(array('url' => '/'),array('model'=> 'Participant'));
                        $sPaginator = $paginator->first('<< First',array('model'=> 'Participant')).' '.
						$paginator->prev('< Previous',array('model'=> 'Participant')) .' '. 
						$paginator->numbers(array('model'=> 'Participant')).' '.
						$paginator->next('Next >',array('model'=> 'Participant')). ' '.
						$paginator->last('Last >>',array('model'=> 'Participant'));
                        echo $sPaginator."<br />";
                        echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true),array('model'=> 'Participant')));
		                ?>
	                 	</div></td></tr>
	                 	<?php 
	                  	}
	                 	?>
                 	</table>

            </div>
        </div> 
	</div>
</div>