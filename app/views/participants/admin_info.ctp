<?php

echo $this->Html->css(array(
    'jquery.autocomplete',
    'fcbkselection'
    )
);

echo $this->Html->script(array(
    'jquery/jquery.bgiframe.min',
    'jquery/jquery.ajaxQueue',
    'jquery/jquery.autocomplete.min',
    'jquery/jquery.tinymce',
    'tiny_mce/tiny_mce',
    'jquery/development-bundle/ui/jquery.ui.core',
    'jquery/development-bundle/ui/jquery.ui.draggable',
    'jquery/development-bundle/ui/jquery.ui.resizable',
    'jquery/development-bundle/ui/jquery.ui.dialog',
    )
);

$this->Participant->setParticipantData( $participant['Participant'],$participant['ParticipantsTitle']['name'] );
$this->ParticipantExtraInfo->setExtraInfo( $participant ); 
?>
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){
   $('#program_data').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true });
   $('#program_extra').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true });
});
/* ]]> */
</script>
<style type="text/css">
#map_canvas { height: 400px;width: 450px }

a{
    color: #333333;
}
</style>
<?php
/** breadcrumbs stuff**/
$oCompany = $this->Company->loadCompanyDataById( $this->Participant->get('company_id') );
$this->Company->setCompanyData( $oCompany['Company'] );


$this->Html->addCrumb('Participants', '/admin/participants');
$this->Html->addCrumb('Search Participants', '/admin/participants/search?q='.urlencode($this->Participant->getFullName()));
$this->Html->addCrumb( $participant['Company']['name'], '/admin/companies/info/'.$this->Company->getSeoUri());
$this->Html->addCrumb('<strong>'.$this->Participant->getFullNameWithHtml().'</strong>');

?>
<div class="users form">
<h2><?php __('Participant Information for '.ucwords($this->Participant->getFullName()));echo $this->Participant->getDisplayEdit()?></h2>
<div class="clearfix hasRightCol" id="contentCol">
   <div id="headerArea">
     <div class="profileHeaderMain" id="program_data">
        <ul><li><a href="#program_info"><span>Overview</span></a></li></ul>
         <div id="program_info">
            <table class="uiInfoTable" valign="top">
                <tr valign="top">
                <td width="50%">
                    <table class="uiInfoTable mvm profileInfoTable">
                      <tbody>
                      	<tr class="striped">
                          <th class="label" width="40%">Full Name</th>
                          <td class="data" width="60%"><strong><?php echo $this->Participant->getFullName();?></strong></td>
                        </tr>
                        <tr class="striped">
                          <th class="label">Nick Name</th>
                          <td class="data"><?php echo $this->Participant->getNickName();?></td>
                        </tr>
                        <?php
                        if($this->Participant->getFullAddress()){
                        ?>
                        <tr class="striped">
                          <th class="label" width="40%">Address</th>
                          <td class="data" width="60%"><?php echo $this->Participant->getFullAddress();?></td>
                        </tr>
                        <?php
                        }
						?>
                        <tr class="striped">
                          <th class="label" width="40%">Email</th>
                          <td class="data" width="60%"><a href="mailto:<?php echo $this->Participant->getEmail();?>"><?php echo $this->Participant->getEmail();?></a></td>
                        </tr>
                        <tr class="striped">
                          <th class="label" width="40%">Phone</th>
                          <td class="data" width="60%"><?php echo $this->Participant->getPhone();?></td>
                        </tr>
                        <tr class="striped">
                          <th class="label" width="40%">Mobile</th>
                          <td class="data" width="60%"><?php echo $this->Participant->getMobile();?></td>
                        </tr>
                        <tr class="striped">
                          <th class="label" width="40%">Fax</th>
                          <td class="data" width="60%"><?php echo $this->Participant->getFax();?></td>
                        </tr>
			          </tbody>
                    </table>
                </td>
                <td width="50%">
                	<table class="uiInfoTable mvm profileInfoTable">
                      <tbody>
                      	<tr class="striped">
                          <th class="label" width="40%">Company Name</th>
                          <td class="data"  width="60%"><a href="<?php echo $this->Html->url(array("admin"=> true,'controller'=>'companies','action'=>'info',$this->Company->getSeoUri()));?>" target="_blank"><strong><?php echo $this->Company->getCompanyName();?></strong></a></td>
                        </tr>
                        <tr class="striped">
                          <th class="label" width="40%">Position</th>
                          <td class="data"  width="60%"><strong><?php echo $this->ParticipantExtraInfo->getPosition();?></strong></td>
                        </tr>
                        <tr class="striped">
                          <th class="label" width="40%">Grouping</th>
                          <td class="data"  width="60%"><strong><?php echo $this->ParticipantExtraInfo->getParticipantsGrouping();?></strong></td>
                        </tr>
                        <tr class="striped">
                          <th class="label">Newsletter</th>
                          <td class="data"></td>
                        </tr>
                        <tr class="striped">
                      	  <th class="label">Total Attended Programs</th>
                          <td class="data"><?php echo $this->Participant->get('total_programs_attended');?></td>
                        </tr>
                        
                      </tbody>
                    </table>
                </td>
                </tr>
            </table>
        </div>
     </div>
     <?php
     if( $this->Participant->get('total_programs_attended') > 0 ||
         $this->Participant->get('total_attendance_sheets') > 0 ||     
         $this->Participant->get('total_certificates') > 0 ||
         $this->Participant->get('total_directory_of_participants') > 0 ||
         $this->Participant->get('total_name_tags') > 0     
     ){
     ?>
     <div class="profileHeaderMain" id="program_extra">
         <ul>
          <?php
          if( $this->Participant->get('total_programs_attended') > 0 ){
          ?>
          <li><a href="#attended_programs"><span>Attended Programs</span></a></li>
          <?php
          }
          if( $this->Participant->get('total_attendance_sheets') > 0 ){
          ?>
          <li><a href="#attendance_sheets"><span>Attendance Sheets</span></a></li>
          <?php
          }
          if( $this->Participant->get('total_certificates') > 0 ){
          ?>
          <li><a href="#certificates"><span>Certificates</span></a></li>
          <?php
          }
          if( $this->Participant->get('total_directory_of_participants') > 0 ){
          ?>
          <li><a href="#directory_of_participants"><span>Directory of Participants</span></a></li>
          <?php
          }
          if( $this->Participant->get('total_name_tags') > 0 ){
          ?>
          <li><a href="#name_tags"><span>Name Tags</span></a></li>
          <?php
          }
          ?>
        </ul>
        <?php
        if( $this->Participant->get('total_programs_attended') > 0 ){

            if( $this->Participant->get('total_programs_attended') < 3 ){
                $height = 220 * $this->Participant->get('total_programs_attended');
            }else{
                 if( $this->Participant->get('total_programs_attended') > 10 ){
                    $height = 1600;
                }else{
                    $height = (220 * $this->Participant->get('total_programs_attended'))/1.55;
                }
            }
            echo '<div id="attended_programs"><div><iframe src="'.$this->Html->url(array('controller'=>'participants','admin'=>true,'action'=>'attended_programs', $this->Participant->get('id').DIRECTORY_SEPARATOR.'1')).'" width="950" height="'.$height.'" border="0"></iframe></div></div>';
        }
        
        if( $this->Participant->get('total_attendance_sheets') > 0 ){

            if( $this->Participant->get('total_attendance_sheets') < 3 ){
                $height = 220 * $this->Participant->get('total_attendance_sheets');
            }else{
                 if( $this->Participant->get('total_attendance_sheets') > 10 ){
                    $height = 1600;
                }else{
                    $height = (220 * $this->Participant->get('total_attendance_sheets'))/1.55;
                }
            }
            echo '<div id="attendance_sheets"><div><iframe src="'.$this->Html->url(array('controller'=>'participants','admin'=>true,'action'=>'attendance_sheets', $this->Participant->get('id').DIRECTORY_SEPARATOR.'1')).'" width="950" height="'.$height.'" border="0"></iframe></div></div>';
        }
        
        if( $this->Participant->get('total_certificates') > 0 ){

            if( $this->Participant->get('total_certificates') < 3 ){
                $height = 220 * $this->Participant->get('total_certificates');
            }else{
                 if( $this->Participant->get('total_certificates') > 10 ){
                    $height = 1600;
                }else{
                    $height = (220 * $this->Participant->get('total_certificates'))/1.55;
                }
            }
            echo '<div id="certificates"><div><iframe src="'.$this->Html->url(array('controller'=>'participants','admin'=>true,'action'=>'certificates', $this->Participant->get('id').DIRECTORY_SEPARATOR.'1')).'" width="950" height="'.$height.'" border="0"></iframe></div></div>';
        }
        
        if( $this->Participant->get('total_directory_of_participants') > 0 ){

            if( $this->Participant->get('total_directory_of_participants') < 3 ){
                $height = 220 * $this->Participant->get('total_directory_of_participants');
            }else{
                 if( $this->Participant->get('total_directory_of_participants') > 10 ){
                    $height = 1600;
                }else{
                    $height = (220 * $this->Participant->get('total_directory_of_participants'))/1.55;
                }
            }
            echo '<div id="directory_of_participants"><div><iframe src="'.$this->Html->url(array('controller'=>'participants','admin'=>true,'action'=>'directory_of_participants', $this->Participant->get('id').DIRECTORY_SEPARATOR.'1')).'" width="950" height="'.$height.'" border="0"></iframe></div></div>';
        }
        
        if( $this->Participant->get('total_name_tags') > 0 ){

            if( $this->Participant->get('total_name_tags') < 3 ){
                $height = 220 * $this->Participant->get('total_name_tags');
            }else{
                 if( $this->Participant->get('total_name_tags') > 10 ){
                    $height = 1600;
                }else{
                    $height = (220 * $this->Participant->get('total_name_tags'))/1.55;
                }
            }
            echo '<div id="name_tags"><div><iframe src="'.$this->Html->url(array('controller'=>'participants','admin'=>true,'action'=>'name_tags', $this->Participant->get('id').DIRECTORY_SEPARATOR.'1')).'" width="950" height="'.$height.'" border="0"></iframe></div></div>';
        }
        ?>
     </div>
     <?php
     }
     ?>
   </div>
</div>
</div>