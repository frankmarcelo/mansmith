<?php echo $this->element('program/program_default_js'); ?>
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){
	$('#billing_osp').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true});
	$(window).resize(function(){});
	$(window).scroll(function(){});
	$('.delete').live('click',function(){});
});
/* ]]> */
</script>
<style type="text/css">
    .listing .box1 {
        float: left !important;
        padding: 0 0 0 6px !important;
        width:200px !important;
    }
    .listing .box2 {
        float: left !important;
        text-align: left !important;
        width: 165px !important;
    }

    .listing .box3 {
        float: left !important;
        text-align: left !important;
        width: 128px !important;
    }

    .listing .box5 {
        float: left !important;
        padding-left: 10px !important;
        text-align: left !important;
        width: 675px !important;
    }

    .listing .box6 {
        float: left !important;
        padding: 6px 0 0 !important;
        text-align: left !important;
        width: 226px !important;
    }

    .listing .box7 {
        float: left !important;
        padding: 6px 0 0 !important;
        text-align: left !important;
        width: 107px !important;
    }
    .listing .box4 {
        clear: both !important;
        margin: 0 !important;
        height: -7px !important;
        padding: 10px 0 0 10px !important;
        width: 950px !important;
    }
</style>
<?php 
echo $this->Html->css(array(
    'jquery.autocomplete',
    'fcbkselection'
    )
);
?>
<script type="text/javascript">
/* <![CDATA[ */
if(!String.prototype.startsWith){
    String.prototype.startsWith = function (str) {
        return !this.indexOf(str);
    }
}

$(document).ready(function(){
	$("#q").focus();

    $('.listing a').live('click',function(){
        var string_match = new String( $(this).attr('id') );

		var string_found = string_match.search(/map/i);
        var string_match_data = string_match.split('_');
		if( string_found > 0 ){
           if( !$(this).attr('show_div') ){
              $(this).attr('show_div','show_div');
              var geocodes = new String( $(this).attr('alt') );
              var xy = geocodes.split('_');

              if( string_match.startsWith('all') ){
                  $('#all_programs_map_canvas_'+parseInt(string_match_data[4],10)).fadeIn("fast", function(){
                      $(this).show();
                  });
                  invokeMap( 'all_programs_map_canvas_'+ parseInt(string_match_data[4]) , xy[0], xy[1] );

              }else{
				  $('#'+string_match_data[0]+'_programs_map_canvas_'+parseInt(string_match_data[4],10)).fadeIn("fast", function(){
                      $(this).show();
                  });
                  invokeMap(string_match_data[0]+'_programs_map_canvas_'+parseInt(string_match_data[4],10), xy[0], xy[1] );
			  }
		   }else{
              $(this).removeAttr('show_div');

              if( string_match.startsWith('all') ){
			  	  $('#all_programs_map_canvas_'+parseInt(string_match_data[4],10)).fadeOut("slow", function(){
                  	$(this).hide();
                  });
              }else{
            	  $('#'+string_match_data[0]+'_programs_map_canvas_'+parseInt(string_match_data[4],10)).fadeOut("slow", function(){
                    	$(this).hide();
                  });
              }
		   }
        }

        var rateplan_found = string_match.search(/rate/i);
        if( rateplan_found > 0 ){
           if( !$(this).attr('show_div') ){
              $(this).attr('show_div','show_div');
			  if( string_match.startsWith('all') ){
              	 $('#all_programs_rate_plans_'+parseInt(string_match_data[5],10)).fadeIn("fast", function(){
                    $(this).show();
                 });
              }else{
                 $('#'+string_match_data[0]+'_programs_rate_plans_'+parseInt(string_match_data[5],10)).fadeIn("fast", function(){
                      $(this).show();
                 });
              }
           }else{
              $(this).removeAttr('show_div');
              if( string_match.startsWith('all') ){
				 $('#all_programs_rate_plans_'+parseInt(string_match_data[5],10)).fadeOut("slow", function(){
                    $(this).hide();
                 });
              }else{
            	 $('#'+string_match_data[0]+'_programs_rate_plans_'+parseInt(string_match_data[5],10)).fadeOut("slow", function(){
                      $(this).hide();
                 });
              }
           }
        }


        var attendees_found = string_match.search(/companies/i);
        if( attendees_found> 0 ){
           if( !$(this).attr('show_div') ){
              $(this).attr('show_div','show_div');
			  if( string_match.startsWith('all') ){
              	 $('#all_programs_attending_companies_'+parseInt(string_match_data[3],10)).fadeIn("fast", function(){
                    $(this).show();
                 });
              }else{
            	 $('#'+string_match_data[0]+'_programs_attending_companies_'+parseInt(string_match_data[3],10)).fadeIn("fast", function(){
                      $(this).show();
                 });
              }
           }else{
              $(this).removeAttr('show_div');
              if( string_match.startsWith('all') ){
				 $('#all_programs_attending_companies_'+parseInt(string_match_data[3],10)).fadeOut("slow", function(){
                    $(this).hide();
                 });
              }else{
            	 $('#'+string_match_data[0]+'_programs_attending_companies_'+parseInt(string_match_data[3],10)).fadeOut("slow", function(){
                      $(this).hide();
                 });
              }
           }
        }
    });
});
/* ]]> */
</script>
<?php
$this->Html->addCrumb('Billing Osp', '/admin/billing_osp');
$this->Html->addCrumb('<strong>'.ucfirst($division).' Billing Osp</strong>');
?>
<div class="billing_osp index">
	<h2><?php __(''.ucwords($division).' Billing Osp');?></h2>
	<?php echo $this->element('billing_osp/search_simple_billing_osp'); ?>
	<div>
       	<div id="billing_osp">
       		<ul>
             <?php 
             if( $this->Session->read('Auth.User.role_id')== Configure::read('administrator') ){
             	echo '<li><a href="#all_billing_non_eb"><span>Billing Osp</span></a></li>';
             }else{
                foreach( $divisions as $division_key => $division ){
                   if( $division_key == $this->Session->read('Auth.User.role_id') ){
                      echo '<li><a href="#all_billing_non_eb"><span>Search '.$division.' Billing Osp</span></a></li>';
                   }
                }                
             }?>  
    		</ul>
    		<?php
            if( isset($billing_osp) && count($billing_osp)>0 ){
            ?>
            <div id="all_billing_osp">

                	<table cellspacing="0" cellpadding="0" style="background-color:#FFFFFF;border:1px solid #DDDDDD;clear:both;width:100%;">
    				<?php
    				if(!empty($billing_osp)){
    				?>
                    <tr><td align="center" colspan="5"><div class="paging">
                    <?php
                    $this->Paginator->options(array('url' => '/'));
                    $sPaginator = $this->Paginator->first('<< First').' '.
                    $this->Paginator->prev('< Previous') .' '.
                    $this->Paginator->numbers().' '.
                    $this->Paginator->next('Next >'). ' '.
                    $this->Paginator->last('Last >>');
                    echo $sPaginator."<br />";
                    echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));
                    ?>
                    </div></td></tr>
	                <?php
    				}
	                ?>
                    <tr>
                        <th style="border-right: 1px solid rgb(223, 223, 223); width:180px;">Program</th>
                        <th style="border-right: 1px solid rgb(223, 223, 223); width:110px;" colspan="4">Division/Type</th>
                    </tr>
                    <tr><td colspan="5">
                        <?php
                        $i = 0;
                        if( isset($billing_osp) && is_array($billing_osp) ){
                            foreach( $billing_osp as $billing_osp_key => $billing_osp_info ){

                                $billingOspFileInfo = $this->BillingOsp->loadBillingOspByProgramId($billing_osp_info['Program']['id']);
                                $oProgram = $this->Program->loadProgramDataById($billing_osp_info['Program']['id'],$recursive=false);
                                $this->Program->setProgramData($billing_osp_info['Program']);
                                $this->ProgramExtraInfo->setExtraInfo($oProgram);
                                $this->BillingOsp->setBillingOspData($billing_osp_info);
                                $sCss = ( ($i % 2) ==0 )? 'listing_accomm':'listing_tr';
                        ?>
                                <div class="listing" id="<?php echo $sCss;?>">
                                    <div class="box1" id="all_programs_<?php echo $this->BillingOsp->getBillingOspId();?>">
                                        <div>
                                            <h3><a href="<?php echo $this->Html->url(array('controller'=>'programs','action'=>'info',$this->BillingOsp->getSeoUri()));?>"><?php echo $this->BillingOsp->getProgramTitle();?></a></h3>
                                            <div><schedule><?php echo $this->ProgramExtraInfo->getSchedule($shortcut=true);?></schedule><address><?php echo $billing_osp_info['BillingOsp']['venue'];?></address></div>
                                        </div>
                                    </div><!--end box1-->
                                    <div class="box2"><div></div></div>
                                    <div class="box3"><div>
                                    <?php
                                    if( $this->Session->read('Auth.User.role_id') == Configure::read('administrator')){
                                    ?><a href="<?php echo $this->Html->url(array('controller'=>'billing_osp','action'=>'list',$this->ProgramExtraInfo->getProgramDivision()));?>">
                                    <?php echo $this->ProgramExtraInfo->getProgramDivision();?></a><?php
                                    }else{ echo $this->ProgramExtraInfo->getProgramDivision();}?> | <span class="tiny"><?php echo $this->ProgramExtraInfo->getProgramType();?></span></div><!--end box3--></div>
                                    <div class="box5">
                                        <table width="100%">
                                            <tr>
                                                <th width="25%">File</th>
                                                <th width="25%">Company</th>
                                                <th width="20%">Recipient</th>
                                                <!--<th>Reference Code</th>-->
                                                <th width="15%">Billing Date</th>
                                                <th width="5%">No.<br />Pax</th>
                                                <th width="10%">Created</th>
                                            </tr>
                                            <?php
                                            if( count($billingOspFileInfo) > 0 ){
                                                $billingOspDirectory = $this->BillingOsp->getBillingOspDirectory(intval($billing_osp_info['Program']['id']));
                                                foreach( $billingOspFileInfo as $key => $billingOspFile ){
                                                    if( intval($billing_osp_info['Program']['id']) >0 && isset($billingOspFile['BillingOsp']['source_file']) ){
                                                        $file = trim($billingOspDirectory.$billingOspFile['BillingOsp']['source_file']);
                                                        if( $this->BillingOsp->billingOspExist($billing_osp_info['Program']['id'],$file)){
                                            ?>
                                           <tr>
                                                <td><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'billing_osp','action'=>'download',intval($billingOspFile['BillingOsp']['id'])));?>"><?php echo trim($billingOspFile['BillingOsp']['source_file']);?></a></td>
                                                <td><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'companies','admin'=>true,'action'=>'info',intval($billingOspFile['BillingOsp']['company_id']).'/'.trim($billingOspFile['Company']['seo_name'])));?>"><?php echo trim($billingOspFile['Company']['name']);?></a></td>
                                                <td><?php echo trim($billingOspFile['BillingOsp']['recipient']);?></td>
                                                <!--<td><?php echo trim($billingOspFile['BillingOsp']['billing_reference_code']);?></td>-->
                                                <td><?php echo trim($billingOspFile['BillingOsp']['billing_date']);?></td>
                                                <td><?php echo trim($billingOspFile['BillingOsp']['number_of_participant']);?></td>
                                                <td><?php echo $this->BillingOsp->createdBy($users,$billingOspFile);?></td>
                                            </tr>
                                            <?php		}else{
                                                            echo '&nbsp;';
                                                        }
                                                    }
                                                }
                                            }
                                        ?>
                                         </table>
                                    </div>
                                    <div class="box5"><div></div></div>
                                    <div class="box6"><div align="left"><?php //echo $this->ParticipantDirectory->getAttendanceCompanies();?></div></div>
                                    <div class="box4">
                                        <a class="edit" style="cursor:pointer;" id="all_programs_view_rate_plans_<?php echo $this->Program->get('id');?>">View RatePlans</a>&nbsp;
                                	</div><!--end box4-->
                                    <div class="clearboth"></div>
                                    <div id="all_programs_rate_plans_<?php echo $this->Program->get('id');?>" style="display:none;">
                                	<?php echo $this->BillingOsp->getBillingRatePlans();?>
                                    </div>
                                </div><!--end listing-->
                        <?php
                                $i++;
                            }//end of foreach
                        }//end of programs attendance sheet
                        ?>
                    </td></tr>
                  	<?php
                  	if(!empty($billing_osp)){
                  	?>
					<tr><td align="center" colspan="5" class="striped"><div class="paging">
    	            <?php
    	            	$this->Paginator->options(array('url' => '/'));
                        $sPaginator = $this->Paginator->first('<< First').' '.
                        $this->Paginator->prev('< Previous') .' '.
                        $this->Paginator->numbers().' '.
                        $this->Paginator->next('Next >'). ' '.
                        $this->Paginator->last('Last >>');
                        echo $sPaginator."<br />";
                        echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));
                 	?>
                 	</div></td></tr>
                 	<?php
                  	}
                 	?>
                  	</table>

            </div><!-- end of name tags -->
            <?php
    		}//end of isset and count
    		?>
    	</div>
	</div>