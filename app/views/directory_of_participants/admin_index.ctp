<?php echo $this->element('program/program_default_js'); ?>
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){
    $('#directory_of_participants').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true});
    $(window).resize(function(){});
    $(window).scroll(function(){});
    $('.delete').live('click',function(){});
});
/* ]]> */
</script>
<?php
echo $this->Html->css(array(
    'jquery.autocomplete',
    'fcbkselection'
    )
);
?>
<script type="text/javascript">
/* <![CDATA[ */
if(!String.prototype.startsWith){
    String.prototype.startsWith = function (str) {
        return !this.indexOf(str);
    }
}

$(document).ready(function(){
    $("#q").focus();
});
/* ]]> */
</script>
<?php
if( isset($selected_program) && $selected_program> 0 ){
	$this->Html->addCrumb('<strong>'.ucfirst($selected_program_name).' Directory Of Participants</strong>');
}else{
	if( $this->Session->read('Auth.User.role_id') == 1){
		$this->Html->addCrumb('<strong>All Directory Of Participants</strong>');
	}else{
		$this->Html->addCrumb('<strong>All '.$roles[$this->Session->read('Auth.User.role_id')].' Directory Of Participants</strong>');
	}
}
?>
<style type="text/css">
.listing .box1 {
    float: left !important;
    padding: 0 0 0 6px !important;
    width:214px !important;
}
.listing .box2 {
    float: left !important;
    text-align: left !important;
    width: 185px !important;
}

.listing .box3 {
    float: left !important;
    text-align: left !important;
    width: 180px !important;
}

.listing .box5 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 205px !important;
}

.listing .box6 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 226px !important;
}

.listing .box7 {
    float: left !important;
    padding: 6px 0 0 !important;
    text-align: left !important;
    width: 107px !important;
}
.listing .box4 {
    clear: both !important;
    margin: 0 !important;
    height: -7px !important;
    padding: 10px 0 0 10px !important;
    width: 950px !important;
}
</style>
<div class="directory_of_participants index">
	<?php 
    if( $this->Session->read('Auth.User.role_id')== Configure::read('administrator') ){
    	echo '<h2>All Directory Of Participants</h2>';
    }else{
        foreach( $divisions as $division_key => $division ){
        	if( $division_key == $this->Session->read('Auth.User.role_id') ){
            	echo '<h2>Search '.$division.' Directory Of Participants</h2>';
            }
        }                
    }
    ?> 
	<?php echo $this->element('directory_of_participants/search_simple_directories_of_participants'); ?>
    <div>
       	<div id="directory_of_participants">
       		<ul>
             <?php
             if( $this->Session->read('Auth.User.role_id')== Configure::read('administrator') ){
             	echo '<li><a href="#all_directory_of_participants"><span>Directory of Participants</span></a></li>';
             }else{
                foreach( $divisions as $division_key => $division ){
                   if( $division_key == $this->Session->read('Auth.User.role_id') ){
                      echo '<li><a href="#all_directory_of_participants"><span>Search '.$division.' Directory of Participants</span></a></li>';
                   }
                }
             }?>  
    		</ul>
    		<?php
    		if( isset($programsParticipantDirectories) && count($programsParticipantDirectories)>0 ){
    		?>
            <div id="all_directory_of_participants">

                	<table cellspacing="0" cellpadding="0" style="background-color:#FFFFFF;border:1px solid #DDDDDD;clear:both;width:100%;">
    				<?php
    				if(!empty($programsParticipantDirectories)){
    				?>
    					<tr><td align="center" colspan="5"><div class="paging">
		                <?php
		                $this->Paginator->options(array('url' => '/'));
                        $sPaginator = $this->Paginator->first('<< First').' '.
						$this->Paginator->prev('< Previous') .' '.
						$this->Paginator->numbers().' '.
						$this->Paginator->next('Next >'). ' '.
						$this->Paginator->last('Last >>');
                        echo $sPaginator."<br />";
                        echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));
		                ?>
	                 	</div></td></tr>
	                <?php
    				}
	                ?>
                    <tr>
                        <th style="border-right: 1px solid rgb(223, 223, 223); width:150px;">Program</th>
                        <th style="border-right: 1px solid rgb(223, 223, 223); width:115px;">Schedule</th>
                        <th style="border-right: 1px solid rgb(223, 223, 223); width:110px;">Division/Type</th>
                        <th style="border-right: 1px solid rgb(223, 223, 223); width:135px;">File</th>
                        <th style="border-right: 1px solid rgb(223, 223, 223); width:180px;">Directory of Participants<br/> Company</th>
                    </tr>
                    <tr><td colspan="5">
                        <?php

                        $i = 0;
                        if( isset($programsParticipantDirectories) && is_array($programsParticipantDirectories) ){

                            foreach( $programsParticipantDirectories as $directories_of_participants_key => $programsParticipantDirectory ){

                                $programParticipantDirectoryFileInfo = $this->ParticipantDirectory->loadProgramsParticipantDirectoryByProgramId($programsParticipantDirectory['Program']['id']);
                                $oProgram = $this->Program->loadProgramDataById($programsParticipantDirectory['Program']['id'],$recursive=false);
                                $this->Program->setProgramData($programsParticipantDirectory['Program']);
                                $this->ProgramExtraInfo->setExtraInfo($oProgram);
                                $this->ParticipantDirectory->setDirectoryOfParticipantData($programsParticipantDirectory);
                                $sCss = ( ($i % 2) ==0 )? 'listing_accomm':'listing_tr';
                        ?>
                                <div class="listing" id="<?php echo $sCss;?>">
                                    <div class="box1" id="all_programs_<?php echo $this->ParticipantDirectory->getDireectoryOfParticipantId();?>">
                                        <div>
                                            <h3><a href="<?php echo $this->Html->url(array('controller'=>'programs','action'=>'info',$this->ParticipantDirectory->getSeoUri()));?>"><?php echo $this->ParticipantDirectory->getProgramTitle();?></a></h3>
                                            <div><address><?php echo $this->Program->getFullAddress();?></address><br/>Total Participants: <?php echo $this->ParticipantDirectory->getTotalAttendanceParticipants();?>&nbsp;Total Companies:<?php echo $this->ParticipantDirectory->getTotalAttendingCompanies();?></div>
                                        </div>
                                    </div><!--end box1-->
                                    <div class="box2"><div><?php echo $this->ProgramExtraInfo->getSchedule($shortcut=true);?></div></div>
                                    <div class="box3"><div>
                                    <?php
                                    if( $this->Session->read('Auth.User.role_id') == Configure::read('administrator')){
                                    ?><a href="<?php echo $this->Html->url(array('controller'=>'directory_of_participants','action'=>'list',$this->ProgramExtraInfo->getProgramDivision()));?>">
                                    <?php echo $this->ProgramExtraInfo->getProgramDivision();?></a><?php
                                    }else{ echo $this->ProgramExtraInfo->getProgramDivision();}?> | <span class="tiny"><?php echo $this->ProgramExtraInfo->getProgramType();?></span></div><!--end box3--></div>
                                    <div class="box5">
                                     <?php
                                    if( count($programParticipantDirectoryFileInfo) > 0 ){
                                        $programParticipantDirectory = $this->ParticipantDirectory->getProgramDirectoryOfParticipantDirectory(intval($programsParticipantDirectory['Program']['id']));
                                        foreach( $programParticipantDirectoryFileInfo as $key => $programParticipantDirectoryFile ){
                                            if( intval($programsParticipantDirectory['Program']['id']) >0 && isset($programParticipantDirectoryFile['ProgramsParticipantDirectory']['source_file']) ){
                                                $file = trim($programParticipantDirectory.$programParticipantDirectoryFile['ProgramsParticipantDirectory']['source_file']);
                                                if( $this->ParticipantDirectory->directoryOfParticipantsExist($programsParticipantDirectory['Program']['id'],$file)){
                                    ?>
                                    <a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'directory_of_participants','action'=>'download',$programParticipantDirectoryFile['ProgramsParticipantDirectory']['id']));?>"><?php echo trim($programParticipantDirectoryFile['ProgramsParticipantDirectory']['source_file']);?></a>
                                    <?php		}else{
                                                    echo '&nbsp;';
                                                }
                                            }
                                        }
                                    }
                                    ?><br/><?php echo $this->ParticipantDirectory->createdBy($users);?>
                                    </div>
                                    <div class="box6"><div align="left"><?php echo $this->ParticipantDirectory->getAttendanceCompanies();?></div></div>
                                    <div class="box4">&nbsp;</div><!--end box4-->
                                    <div class="clearboth"></div>
                                </div><!--end listing-->
                        <?php
                                $i++;
                            }//end of foreach
                        }//end of programs attendance sheet
                        ?>
                    </td></tr>
                  	<?php
                  	if(!empty($programsParticipantDirectories)){
                  	?>
					<tr><td align="center" colspan="5" class="striped"><div class="paging">
    	            <?php
    	            	$this->Paginator->options(array('url' => '/'));
                        $sPaginator = $this->Paginator->first('<< First').' '.
                        $this->Paginator->prev('< Previous') .' '.
                        $this->Paginator->numbers().' '.
                        $this->Paginator->next('Next >'). ' '.
                        $this->Paginator->last('Last >>');
                        echo $sPaginator."<br />";
                        echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));
                 	?>
                 	</div></td></tr>
                 	<?php
                  	}
                 	?>
                  	</table>

            </div><!-- end of name tags -->
            <?php
    		}//end of isset and count
    		?>
    	</div>
	</div>