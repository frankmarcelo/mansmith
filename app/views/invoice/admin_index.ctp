<?php echo $this->element('program/program_default_js'); ?>
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){
	$('#invoice').tabs({ fxSlide: true, fxFade: true, fxSpeed: 'normal' , fxAutoHeight: true});
	$(window).resize(function(){});
	$(window).scroll(function(){});
	$('.delete').live('click',function(){});
    $("#q").focus();
});
/* ]]> */
</script>
<?php
echo $this->Html->css(array(
    'jquery.autocomplete',
    'fcbkselection'
    )
);
?>
<script type="text/javascript">
/* <![CDATA[ */
if(!String.prototype.startsWith){
    String.prototype.startsWith = function (str) {
        return !this.indexOf(str);
    }
}
/* ]]> */
</script>
<style type="text/css">
    .listing .box1 {
        float: left !important;
        padding: 0 0 0 6px !important;
        width:200px !important;
    }
    .listing .box2 {
        float: left !important;
        text-align: left !important;
        width: 165px !important;
    }

    .listing .box3 {
        float: left !important;
        text-align: left !important;
        width: 128px !important;
    }

    .listing .box5 {
        float: left !important;
        padding-left: 10px !important;
        text-align: left !important;
        width: 675px !important;
    }

    .listing .box6 {
        float: left !important;
        padding: 6px 0 0 !important;
        text-align: left !important;
        width: 226px !important;
    }

    .listing .box7 {
        float: left !important;
        padding: 6px 0 0 !important;
        text-align: left !important;
        width: 107px !important;
    }
    .listing .box4 {
        clear: both !important;
        margin: 0 !important;
        height: -7px !important;
        padding: 10px 0 0 10px !important;
        width: 950px !important;
    }
</style>
<?php
if( isset($selected_program) && $selected_program> 0 ){
	$this->Html->addCrumb('<strong>'.ucfirst($selected_program_name).' Invoice</strong>');
}else{
	if( $this->Session->read('Auth.User.role_id') == 1){
		$this->Html->addCrumb('<strong>All Invoice</strong>');
	}else{
		$this->Html->addCrumb('<strong>All '.$roles[$this->Session->read('Auth.User.role_id')].' Invoice</strong>');
	}
}
?>
<div class="invoice index">
    <?php
    if( $this->Session->read('Auth.User.role_id')== Configure::read('administrator') ){
    	echo '<h2>All Invoice</h2>';
    }else{
        foreach( $divisions as $division_key => $division ){
        	if( $division_key == $this->Session->read('Auth.User.role_id') ){
            	echo '<h2>Search '.$division.' Invoice</h2>';
            }
        }
    }
    ?>
	<?php echo $this->element('invoice/search_simple_invoice'); ?>
    <div>
       	<div id="invoice">
       		<ul>
             <?php
             if( $this->Session->read('Auth.User.role_id')== Configure::read('administrator') ){
                echo '<li><a href="#all_invoice"><span>Invoice</span></a></li>';
             }else{
                foreach( $divisions as $division_key => $division ){
                    if( $division_key == $this->Session->read('Auth.User.role_id') ){
                        echo '<li><a href="#all_invoice"><span>Search '.$division.' Invoice</span></a></li>';
                    }
                }
             }
             ?>
            </ul>
    		<?php
            if( isset($invoice) && count($invoice)>0 ){
            ?>
            <div id="all_invoice">

                	<table cellspacing="0" cellpadding="0" style="background-color:#FFFFFF;border:1px solid #DDDDDD;clear:both;width:100%;">
    				<?php
    				if(!empty($invoice)){
    				?>
                    <tr><td align="center" colspan="5"><div class="paging">
                    <?php
                    $this->Paginator->options(array('url' => '/'));
                    $sPaginator = $this->Paginator->first('<< First').' '.
                    $this->Paginator->prev('< Previous') .' '.
                    $this->Paginator->numbers().' '.
                    $this->Paginator->next('Next >'). ' '.
                    $this->Paginator->last('Last >>');
                    echo $sPaginator."<br />";
                    echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));
                    ?>
                    </div></td></tr>
	                <?php
    				}
	                ?>
                     <tr>
                        <th style="border-right: 1px solid rgb(223, 223, 223); width:180px;">Program</th>
                        <th style="border-right: 1px solid rgb(223, 223, 223); width:110px;" colspan="4">Division/Type</th>
                    </tr>
                    <tr><td colspan="5">
                        <?php

                        $i = 0;
                        if( isset($invoice) && is_array($invoice) ){

                            foreach( $invoice as $invoice_key => $invoice_info ){
                                    $invoiceFileInfo = $this->Invoice->loadInvoiceByProgramId($invoice_info['Program']['id']);
                                    $oProgram = $this->Program->loadProgramDataById($invoice_info['Program']['id'],$recursive=false);
                                    $this->Program->setProgramData($invoice_info['Program']);
                                    $this->ProgramExtraInfo->setExtraInfo($oProgram);
                                    $this->Invoice->setInvoiceData($invoice_info);
                                    $sCss = ( ($i % 2) ==0 )? 'listing_accomm':'listing_tr';
                                    
                        ?>
                                <div class="listing" id="<?php echo $sCss;?>">
                                    <div class="box1" id="all_programs_<?php echo $this->Invoice->getInvoiceId();?>">
                                        <div>
                                            <h3><a href="<?php echo $this->Html->url(array('controller'=>'programs','action'=>'info',$this->Invoice->getSeoUri()));?>"><?php echo $this->Invoice->getProgramTitle();?></a></h3>
                                            <div><schedule><?php echo $this->ProgramExtraInfo->getSchedule($shortcut=true);?></schedule><address><?php echo $invoice_info['Invoice']['venue'];?></address></div>
                                        </div>
                                    </div><!--end box1-->
                                    <div class="box2"><div></div></div>
                                    <div class="box3"><div>
                                    <?php
                                    if( $this->Session->read('Auth.User.role_id') == Configure::read('administrator')){
                                    ?><a href="<?php echo $this->Html->url(array('controller'=>'invoice','action'=>'list',$this->ProgramExtraInfo->getProgramDivision()));?>">
                                    <?php echo $this->ProgramExtraInfo->getProgramDivision();?></a><?php
                                    }else{ echo $this->ProgramExtraInfo->getProgramDivision();}?> | <span class="tiny"><?php echo $this->ProgramExtraInfo->getProgramType();?></span></div><!--end box3--></div>
                                    <div class="box5">
                                       <table>
                                           <tr>
                                               <th width="25%">File</th>
                                               <th width="20%">Invoice No.</th>
                                               <th width="25%">Amount</th>
                                               <th width="20%">Company</th>
                                               <th width="10%">Created</th>
                                           </tr>
                                            </tr>
                                            <?php
                                            if( count($invoiceFileInfo) > 0 ){
                                                foreach( $invoiceFileInfo as $key => $invoiceFile ){
                                                    $invoiceDirectory = $this->Invoice->getInvoiceDirectory(intval($invoice_info['Program']['id'])).$this->Invoice->getInvoiceType($invoiceFile['InvoiceDetails']['billing_type_id']);
                                                    if( intval($invoice_info['Invoice']['id']) >0 && isset($invoiceFile['InvoiceDetails']['source_file']) ){
                                                        $file = trim($invoiceDirectory.$invoiceFile['InvoiceDetails']['source_file']);
                                                        if( $this->Invoice->invoiceExist($file)){
                                            ?>
                                            <tr>
                                                <td><a target="_blank" href="<?php echo $this->Html->url(array('controller'=>'invoice','action'=>'download',intval($invoiceFile['InvoiceDetails']['id'])));?>"><?php echo trim($invoiceFile['InvoiceDetails']['source_file']);?></a></td>
                                                <td><?php echo trim($invoiceFile['InvoiceDetails']['invoice_number']);?></td>
                                                <td>PHP <?php echo number_format(($invoiceFile['InvoiceDetails']['total_amount_due']),2, '.',',');?></td>
                                                <td><a href="<?php echo $this->Html->url(array('controller'=>'companies','action'=>'info',$invoiceFile['Company']['id'].'/'.$invoiceFile['Company']['seo_name']));?>"><?php echo trim($invoiceFile['Company']['name']);?></a></td>
                                                <td><?php echo $this->Invoice->createdBy($users,$invoiceFile);?></td>
                                            </tr>
                                            <?php       }else{
                                                            echo '&nbsp;';
                                                        }
                                                    }
                                                }
                                            }
                                            ?>
                                        </table>
                                    </div>
                                    <div class="box6"><div align="left"><?php //echo $this->ParticipantDirectory->getAttendanceCompanies();?></div></div>
                                    <div class="box4">&nbsp;</div><!--end box4-->
                                    <div class="clearboth"></div>
                                </div><!--end listing-->
                        <?php
                                $i++;
                            }//end of foreach
                        }//end of programs attendance sheet
                        ?>
                    </td></tr>
                  	<?php
                  	if(!empty($invoice)){
                  	?>
					<tr><td align="center" colspan="6" class="striped"><div class="paging">
    	            <?php
    	            	$this->Paginator->options(array('url' => '/'));
                        $sPaginator = $this->Paginator->first('<< First').' '.
                        $this->Paginator->prev('< Previous') .' '.
                        $this->Paginator->numbers().' '.
                        $this->Paginator->next('Next >'). ' '.
                        $this->Paginator->last('Last >>');
                        echo $sPaginator."<br />";
                        echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total', true)));
                 	?>
                 	</div></td></tr>
                 	<?php
                  	}
                 	?>
                  	</table>

            </div><!-- end of name tags -->
            <?php
    		}//end of isset and count
    		?>
    	</div>
	</div>