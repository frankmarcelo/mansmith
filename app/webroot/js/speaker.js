/*
 * SimpleModal Contact Form
 * http://www.ericmmartin.com/projects/simplemodal/
 * http://code.google.com/p/simplemodal/
 *
 * Copyright (c) 2010 Eric Martin - http://ericmmartin.com
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Revision: $Id: contact.js 254 2010-07-23 05:14:44Z emartin24 $
 */

jQuery(function ($) {
	var contact = {
		message: null,
		init: function () {
			$('#add_speakers').click(function (e) {
				e.preventDefault();
                var sHtml = '<div style="display:none"><div class="contact-top"></div><div class="contact-content"><h1 class="contact-title">Create Speaker:</h1><div class="contact-loading" style="display:none"></div><div class="contact-message" style="display:none"></div><form id="SpeakerAdminCreateForm" name="SpeakerAdminCreateForm" method="post" onSubmit="return false;"><label for="contact-name">*First Name:</label><input type="text" id="contact-first_name" class="contact-input" name="data[Speaker][first_name]" tabindex="1001" style="width:200px;"/><br /><label for="contact-name">*Last Name:</label><input type="text" id="contact-last_name" class="contact-input" name="data[Speaker][last_name]" tabindex="1002" style="width:200px;" /><br /><label for="contact-email">*Email:</label><input type="text" id="contact-email" class="contact-input" name="data[Speaker][email]" tabindex="1003" style="width:200px;" /><br /><label for="contact-email">*Mobile:</label><input type="text" id="contact-mobile" class="contact-input" name="data[Speaker][mobile]" tabindex="1004" style="width:200px;" /><br /><label>&nbsp;</label><button type="submit" id="create_speakerbtn" onClick="" class="contact-send contact-button" tabindex="1005" style="width:100px;">Create</button><button type="submit" id="cancel_speakerbtn" class="contact-cancel contact-button simplemodal-close" tabindex="1008" style="width:100px;">Cancel</button></form></div></div>';
				$(sHtml).modal({
					closeHTML: "<a style='cursor:pointer;' title='Close' class='modal-close'>x</a>",
					position: ["15%",],
					overlayId: 'contact-overlay',
					containerId: 'contact-container',
					onOpen: contact.open,
					onClose: contact.close
				});
			});
		},
		open: function (dialog) {
			// add padding to the buttons in firefox/mozilla
			if ($.browser.mozilla) {
				$('#contact-container .contact-button').css({
					'padding-bottom': '2px'
				});
			}
			// input field font size
			if ($.browser.safari) {
				$('#contact-container .contact-input').css({
					'font-size': '.9em'
				});
			}

			// dynamically determine height
			var title = $('#contact-container .contact-title').html();
			$('#contact-container .contact-title').html('Loading...');
			dialog.overlay.fadeIn(200, function () {
				dialog.container.fadeIn(200, function () {
					dialog.data.fadeIn(200, function () {
						$('#contact-container .contact-content').animate(function () {
							$('#contact-container .contact-title').html(title);
							$('#contact-container form').fadeIn(200, function () {
								// fix png's for IE 6
								if ($.browser.msie && $.browser.version < 7) {
									$('#contact-container .contact-button').each(function () {
										if ($(this).css('backgroundImage').match(/^url[("']+(.*\.png)[)"']+$/i)) {
											var src = RegExp.$1;
											$(this).css({
												backgroundImage: 'none',
												filter: 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src="' +  src + '", sizingMethod="crop")'
											});
										}
									});
								}
							});
						});
					});
				});
			});
            
            /*$(".simplemodal-overlay").css({
                'opacity': '0',
                'height': '100%',
                'width': '100%',
                'position' : 'fixed',
                'left': '0px',
                'top': '0px',
                'z-index':'1001'
            });
            $(".simplemodal-container").css({
                'position': 'fixed',
                'z-index' : '1002',
                'height'  : '501px',
                'width' : '350px',
                'left': '405.5px',
                'top': '15%;'});*/

            $('#contact-container .contact-title').empty().fadeIn("slow",function(){
                $(this).html('Create Speaker');
            });

		},
		error: function (xhr) {
			alert(xhr.statusText);
		},
		validateEmail: function (email) {
			var at = email.lastIndexOf("@");

			// Make sure the at (@) sybmol exists and  
			// it is not the first or last character
			if (at < 1 || (at + 1) === email.length)
				return false;

			// Make sure there aren't multiple periods together
			if (/(\.{2,})/.test(email))
				return false;

			// Break up the local and domain portions
			var local = email.substring(0, at);
			var domain = email.substring(at + 1);

			// Check lengths
			if (local.length < 1 || local.length > 64 || domain.length < 4 || domain.length > 255)
				return false;

			// Make sure local and domain don't start with or end with a period
			if (/(^\.|\.$)/.test(local) || /(^\.|\.$)/.test(domain))
				return false;

			// Check for quoted-string addresses
			// Since almost anything is allowed in a quoted-string address,
			// we're just going to let them go through
			if (!/^"(.+)"$/.test(local)) {
				// It's a dot-string address...check for valid characters
				if (!/^[-a-zA-Z0-9!#$%*\/?|^{}`~&'+=_\.]*$/.test(local))
					return false;
			}

			// Make sure domain contains only valid characters and at least one period
			if (!/^[-a-zA-Z0-9\.]*$/.test(domain) || domain.indexOf(".") === -1)
				return false;	

			return true;
		},
		showError: function () {
			$('#contact-container .contact-message')
				.html($('<div class="contact-error"></div>').append(contact.message))
				.fadeIn(200);
		}
	};

	contact.init();

});
