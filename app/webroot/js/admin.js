/**
 * Admin
 *
 * for admin pages
 */
var Admin = {};


/**
 * Navigation
 *
 * @return void
 */
Admin.navigation = function() {
    $('ul.sf-menu').supersubs({
        minWidth: 12,
        maxWidth: 27,
        extraWidth: 1
    }).superfish({
        delay: 200,
        animation: {opacity:'show',height:'show'},
        speed: 'fast',
        autoArrows: true,
        dropShadows: false,
        disableHI: true
    });
}


/**
 * Forms
 *
 * @return void
 */
Admin.form = function() {
    $("input[type=text][rel], select[rel]").not(":hidden").each(function() {
        var sd = $(this).attr('rel');
        $(this).after("<span class=\"description\">"+sd+"</span>");
    });
    
    $("textarea[rel]").not(":hidden").each(function() {
        var sd = $(this).attr('rel');
        if (sd != '') {
            $(this).after("<br /><span class=\"description nospace\">"+sd+"</span>");
        }
    });
}


/**
 * Extra stuff
 *
 * rounded corners, striped table rows, etc
 *
 * @return void
 */
Admin.extra = function() {
    $("table tr:nth-child(odd)").not('.controller-row').addClass("striped");
    $("div.message").addClass("notice");
    $('#loading p').addClass('ui-corner-bl ui-corner-br');
}


/**
 * Document ready
 *
 * @return void
 */
$(document).ready(function() {
    Admin.navigation();
    Admin.form();
    Admin.extra();

    $('.tabs').tabs();
    $('a.tooltip').tipsy({gravity: 's', html: false});
    $('textarea').not('.content').elastic();

    //Adjust panel height
	$.fn.adjustPanel = function(){
		$(this).find("ul, .subpanel").css({ 'height' : 'auto'}); //Reset subpanel and ul height
		var windowHeight = $(window).height(); //Get the height of the browser viewport
		var panelsub = $(this).find(".subpanel").height(); //Get the height of subpanel
		var panelAdjust = windowHeight - 100; //Viewport height - 100px (Sets max height of subpanel)
		var ulAdjust =  panelAdjust - 25; //Calculate ul size after adjusting sub-panel (27px is the height of the base panel)
		if ( panelsub >= panelAdjust ) {	 //If subpanel is taller than max height...
			$(this).find(".subpanel").css({ 'height' : panelAdjust }); //Adjust subpanel to max height
			$(this).find("ul").css({ 'height' : ulAdjust}); //Adjust subpanel ul to new size
		}else if ( panelsub < panelAdjust ) { //If subpanel is smaller than max height...
			$(this).find("ul").css({ 'height' : 'auto'}); //Set subpanel ul to auto (default size)
		}
	};

	//Execute function on load
	$("#chatpanel").adjustPanel(); //Run the adjustPanel function on #chatpanel
	$("#alertpanel").adjustPanel(); //Run the adjustPanel function on #alertpanel

	//Each time the viewport is adjusted/resized, execute the function
	$(window).resize(function () {
		$("#chatpanel").adjustPanel();
		$("#alertpanel").adjustPanel();
	});

	//Click event on Chat Panel + Alert Panel
	$("#chatpanel a:first, #alertpanel a:first").click(function() { //If clicked on the first link of #chatpanel and #alertpanel...
		if($(this).next(".subpanel").is(':visible')){ //If subpanel is already active...
			$(this).next(".subpanel").hide(); //Hide active subpanel
			$("#footpanel li a").removeClass('active'); //Remove active class on the subpanel trigger
		}
		else { //if subpanel is not active...
			$(".subpanel").hide(); //Hide all subpanels
			$(this).next(".subpanel").toggle(); //Toggle the subpanel to make active
			$("#footpanel li a").removeClass('active'); //Remove active class on all subpanel trigger
			$(this).toggleClass('active'); //Toggle the active class on the subpanel trigger
		}
		return false; //Prevent browser jump to link anchor
	});

	//Click event outside of subpanel
	$(".subpanel").hide(); //hide subpanel
	$("#footpanel li a").removeClass('active'); //remove active class on subpanel trigger

	$('.subpanel ul').click(function(e) {
		e.stopPropagation(); //Prevents the subpanel ul from closing on click
	});

	//Delete icons on Alert Panel
	$("#alertpanel li").hover(function() {
		$(this).find("a.delete").css({'visibility': 'visible'}); //Show delete icon on hover
	},function() {
		$(this).find("a.delete").css({'visibility': 'hidden'}); //Hide delete icon on hover out
	});
	
	monkeyPatchAutocomplete();
});

function ucfirst (str) {
    str += '';
    var f = str.charAt(0).toUpperCase();
    return f + str.substr(1);
}

function monkeyPatchAutocomplete() {
    var oldFn = $.ui.autocomplete.prototype._renderItem;
	$.ui.autocomplete.prototype._renderItem = function( ul, item) {
		var re = new RegExp(this.term, "i") ;
		var t = item.label.replace(re,'<strong class="highlight_keyword">' + this.term.toUpperCase() + "</strong>");
		return $( "<li></li>" )
		  .data( "item.autocomplete", item )
		  .append( "<a>" + t + "</a>" )
		  .appendTo( ul );
	};
}