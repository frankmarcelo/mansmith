<?php

require_once '../../../config/database.php';

$dbConfig = new DATABASE_CONFIG();
$mysqli = new mysqli( $dbConfig->default['host'] , 
                      $dbConfig->default['login'] ,
                      $dbConfig->default['password'] ,
                      $dbConfig->default['database'] );
if( $mysqli->ping() && isset($_POST['participantId']) &&  intval($_POST['participantId']) > 0 ){
  
   $participantId = intval($_POST['participantId']);
   $status = ( intval($_POST['status']) > 0 ) ? 1 :0;

   $sql = "UPDATE participants SET receive_newsletter=$status WHERE id=".$mysqli->escape_string($participantId);
   header('Cache-Control: no-cache, must-revalidate');
   header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
   header('Content-type: application/json');
   $aData = array();
   if( $mysqli->query($sql) ){
      echo json_encode(true);
   }else{
      echo json_encode(false);
   }
}else{
   die;
}

$mysqli->close();
?>
