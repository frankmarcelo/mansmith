<?php
if(
    isset($_POST) &&
    isset($_POST['data']) &&
    isset($_POST['data']['CompanyAffiliation'])
){
    require_once './sanitize.php';
    require_once '../../../config/database.php';
    require_once '../../../models/program.php';

    $dbConfig = new DATABASE_CONFIG();
    $mysqli = new mysqli(   $dbConfig->default['host'] ,
        $dbConfig->default['login'] ,
        $dbConfig->default['password'] ,
        $dbConfig->default['database']
    );
    if ($mysqli->ping()) {
        if (strlen($_POST['data']['CompanyAffiliation']['add_company_affiliation']) > 0) {
            $seo_name = Sanitize::convertToSeoUri(trim($_POST['data']['CompanyAffiliation']['add_company_affiliation']));
            $sql = "INSERT INTO companies_affiliation SET
                       `status`             = 1,
                       `name`               ='".Sanitize::clean(trim($_POST['data']['CompanyAffiliation']['add_company_affiliation']))."',
                       `seo_name`           ='".$seo_name."',
                       `date_created`       ='".date("Y-m-d H:i:s")."',
                       `who_created`        ='".intval($_POST['data']['CompanyAffiliation']['who_modified'])."'";
            //insert
            $mysqli->query($sql);
        }
    }
    $mysqli->close();
}