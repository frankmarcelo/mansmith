<?php

require_once '../../../config/database.php';

$dbConfig = new DATABASE_CONFIG();
$mysqli = new mysqli( $dbConfig->default['host'] , 
                      $dbConfig->default['login'] ,
                      $dbConfig->default['password'] ,
                      $dbConfig->default['database'] );
if( $mysqli->ping() ){
  
   $limit = ( isset($_GET['limit']) && intval($_GET['limit']) > 0 ) ? intval($_GET['limit']) : 20;
   $q = trim($_GET['term']);

   $sql = "SELECT SQL_CACHE DISTINCT(primary_bldg_name) FROM companies
   		   WHERE (MATCH(primary_bldg_name) AGAINST ('+".$mysqli->escape_string($q)."*' IN boolean MODE) OR 
   		          TRIM(primary_bldg_name) like '%".$mysqli->escape_string($q)."%') AND LENGTH(primary_bldg_name) > 0 AND primary_bldg_name IS NOT NULL
   		          AND status = 1
   		   GROUP BY TRIM(primary_bldg_name)
   		   ORDER BY MATCH(primary_bldg_name) AGAINST ('+".$mysqli->escape_string($q)."*' IN boolean MODE) DESC, UPPER(TRIM(primary_bldg_name)) ASC 
   		   LIMIT ".$limit;
   header('Cache-Control: no-cache, must-revalidate');
   header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
   header('Content-type: application/json');
   $aData = array();
   if( $aRes = $mysqli->query($sql) ){
      while( $obj = $aRes->fetch_object() ){
      	 if(strlen($obj->primary_bldg_name) > 80 ){
       	     $obj->primary_bldg_name = substr($obj->primary_bldg_name,0,80);		
       	 }	
         $aData[] = ucwords(trim(strtolower($obj->primary_bldg_name))); 
      }
      $aRes->close();
   }

   if( sizeof($aData) < 20 ){ 
      $sql = "SELECT SQL_CACHE DISTINCT(secondary_bldg_name) FROM companies
     		   WHERE (MATCH(secondary_bldg_name) AGAINST ('+".$mysqli->escape_string($q)."*' IN boolean MODE) OR 
   		          TRIM(secondary_bldg_name) like '%".$mysqli->escape_string($q)."%') AND LENGTH(secondary_bldg_name) > 0 AND secondary_bldg_name IS NOT NULL
   		          AND status = 1
   		   GROUP BY TRIM(secondary_bldg_name)
   		   ORDER BY MATCH(secondary_bldg_name) AGAINST ('+".$mysqli->escape_string($q)."*' IN boolean MODE) DESC, UPPER(TRIM(secondary_bldg_name)) ASC 
   		   LIMIT ".$limit;
      if( $aRes = $mysqli->query($sql) ){
         while( $obj = $aRes->fetch_object() ){
      	    if(strlen($obj->secondary_bldg_name) > 80 ){
       	        $obj->secondary_bldg_name = substr($obj->secondary_bldg_name,0,80);		
       	    }	
            $aData[] = ucwords(trim(strtolower($obj->secondary_bldg_name))); 
         }
         $aRes->close();
      }

   }
   if( strlen($q) > 0 ){
      echo json_encode($aData);
   }else{
      echo json_encode(null);
   }
}else{
   die;
}

$mysqli->close();
?>
