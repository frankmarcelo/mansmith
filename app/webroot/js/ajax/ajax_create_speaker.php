<?php
if( isset($_POST) && isset($_POST['data']) && isset($_POST['data']['Speaker']) ){
	require_once './sanitize.php';	
	require_once '../../../config/database.php';
	require_once '../../../models/program.php';
	
	$dbConfig = new DATABASE_CONFIG();
	$mysqli = new mysqli( $dbConfig->default['host'] ,
	                      $dbConfig->default['login'] ,
	                      $dbConfig->default['password'] ,
	                      $dbConfig->default['database'] );
	if( $mysqli->ping() ){
        if( (strlen($_POST['data']['Speaker']['first_name']) > 0 ) &&
            (strlen($_POST['data']['Speaker']['last_name']) > 0  ) &&
            (strlen($_POST['data']['Speaker']['email']) > 0      )
        ){
			$_POST['data']['Speaker']['mobile'] = (isset($_POST['data']['Speaker']['mobile'])) ? trim($_POST['data']['Speaker']['mobile']):null;
            $sql = "INSERT INTO speakers SET
					    programs_division_id='2',
                        first_name          ='".$mysqli->escape_string(trim($_POST['data']['Speaker']['first_name']))."',
                     	last_name           ='".$mysqli->escape_string(trim($_POST['data']['Speaker']['last_name']))."',
                        email               ='".$mysqli->escape_string(trim($_POST['data']['Speaker']['email']))."',
                        mobile 	            ='".$mysqli->escape_string(trim($_POST['data']['Speaker']['mobile']))."',
                        status              = 1,
                        date_created        = '".date("Y-m-d H:i:s")."',
                        date_modified       = '".date("Y-m-d H:i:s")."',
                        who_created         = '".$_POST['data']['Speaker']['who_created']."'";
			//insert
            if( $mysqli->query($sql) ){
                $data = array(
                    'data' => array(
                        'created' => true,
                        'data' => array(
                            'Speaker' => array(
                                'email' => trim($_POST['data']['Speaker']['email']),
                                'id' => $mysqli->insert_id,
                                'first_name' => trim($_POST['data']['Speaker']['first_name']),
                                'last_name' => trim($_POST['data']['Speaker']['last_name']),
                                'mobile'=> trim($_POST['data']['Speaker']['mobile']),
                            ),
                            'ProgramsDivision' => array(
                                'title' => 'Mansmith'
                            )
                        )
                    )
                );
                echo json_encode($data);
            }
		}
	}
	$mysqli->close();
}