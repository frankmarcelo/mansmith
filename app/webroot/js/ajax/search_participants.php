<?php

require_once '../../../config/database.php';

$dbConfig = new DATABASE_CONFIG();
$mysqli = new mysqli( $dbConfig->default['host'] , 
                      $dbConfig->default['login'] ,
                      $dbConfig->default['password'] ,
                      $dbConfig->default['database'] );
if( $mysqli->ping() ){
  
   $limit = ( isset($_GET['limit']) && intval($_GET['limit']) > 0 ) ? intval($_GET['limit']) : 20;
   $q = trim(strtolower($_GET['term']));
   $sql = "	SELECT SQL_CACHE DISTINCT(full_name), 
                       MATCH(full_name,nick_name,email,address,phone,mobile,fax) AGAINST ('".$mysqli->escape_string($q)."')  FROM participants 
   		WHERE  ( 
                         ( 
                           (MATCH(full_name,nick_name,email,address,phone,mobile,fax) AGAINST ('+".$mysqli->escape_string($q)."*')) OR 
                           (TRIM(UPPER(full_name))) LIKE '%".$mysqli->escape_string($q)."%'
                         )
                       ) 
                       AND ( LENGTH(TRIM(full_name)) > 0 ) AND status = 1
   		GROUP BY full_name 
   		ORDER BY MATCH(full_name,nick_name,email,address,phone,mobile,fax) AGAINST ('".$mysqli->escape_string($q)."') DESC, 
                         TRIM(UPPER(full_name)) ASC
   	        LIMIT ".$limit;
   header('Cache-Control: no-cache, must-revalidate');
   header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
   header('Content-type: application/json');
   $aData = array();
   if( $aRes = $mysqli->query($sql) ){
      while( $obj = $aRes->fetch_object() ){
      	 if(strlen($obj->full_name) > 80 ){
       	     $obj->full_name = substr($obj->full_name,0,80);		
       	 }
         $aData[] = ucwords(trim(strtolower($obj->full_name))); 
      }
      $aRes->close();
   }

   if( strlen($q) > 0 ){
       echo json_encode($aData);
   }else{
       echo json_encode(null);
   }
}else{
   die;
}

$mysqli->close();
?>
