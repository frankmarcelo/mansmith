<?php

require_once '../../../config/database.php';

$dbConfig = new DATABASE_CONFIG();
$mysqli = new mysqli( $dbConfig->default['host'] , 
                      $dbConfig->default['login'] ,
                      $dbConfig->default['password'] ,
                      $dbConfig->default['database'] );
if( $mysqli->ping() && isset($_POST['programId']) && intval($_POST['programId']) > 0 && strlen($_POST['lat']) > 0 && strlen($_POST['lon']) > 0 ){
  
   $programId = intval($_POST['programId']);
   $lat = trim($_POST['lat']);
   $lon = trim($_POST['lon']);

   $sql = "UPDATE programs SET latitude='".$mysqli->escape_string($lat)."',longitude='".$mysqli->escape_string($lon)."' WHERE id=".$mysqli->escape_string($programId);
   header('Cache-Control: no-cache, must-revalidate');
   header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
   header('Content-type: application/json');
   $aData = array();
   if( $mysqli->query($sql) ){
      echo json_encode(true);
   }else{
      echo json_encode(false);
   }
}else{
   die;
}

$mysqli->close();
?>
