<?php

require_once '../../../config/database.php';

$dbConfig = new DATABASE_CONFIG();
$mysqli = new mysqli( $dbConfig->default['host'] , 
                      $dbConfig->default['login'] ,
                      $dbConfig->default['password'] ,
                      $dbConfig->default['database'] );
if( $mysqli->ping() ){
    $limit = ( isset($_GET['limit']) && intval($_GET['limit']) > 0 ) ? intval($_GET['limit']) : 20;
    $q = trim($_GET['term']);
    $division_id = (isset($_GET['division_id']))? intval(trim($_GET['division_id'])):0;
    $sql  = " SELECT SQL_CACHE DISTINCT(title) FROM programs 
    		  WHERE (MATCH(title) AGAINST ('+".$mysqli->escape_string($q)."*' IN boolean MODE) OR 
    		        (title like '%".$mysqli->escape_string($q)."%')) AND LENGTH(title) > 0
    		        AND status = 1 
    		";
    $sql .= ( $division_id > 1 ) ? " AND programs_division_id='".$mysqli->escape_string($division_id)."' ":"";
    $sql .= " GROUP BY  TRIM(title) 
    		  ORDER BY (MATCH(title) AGAINST ('+".$mysqli->escape_string($q)."*' IN boolean MODE)) DESC, UPPER(TRIM(title)) ASC 
    		  LIMIT ".$limit;
    
    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    $aData = array();
    if( $aRes = $mysqli->query($sql) ){
       while( $obj = $aRes->fetch_object() ){
       	  if(strlen($obj->title) > 80 ){
       	     $obj->title = substr($obj->title,0,80);		
       	  }	
          $aData[] = ucwords(trim(strtolower($obj->title))); 
       }
       $aRes->close();
    }

    if( strlen($q) > 0 ){
        echo json_encode($aData);
    }else{
        echo json_encode(null);
    }
}else{
   	die;
}

$mysqli->close();
?>
