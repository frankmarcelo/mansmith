<?php

require_once '../../../config/database.php';

$dbConfig = new DATABASE_CONFIG();
$mysqli = new mysqli( $dbConfig->default['host'] , 
                      $dbConfig->default['login'] ,
                      $dbConfig->default['password'] ,
                      $dbConfig->default['database'] );
if( $mysqli->ping() ){
   function is_program_company_participant($program_id,$company_id,$participant_id){
       global $mysqli;

       $sql = "	SELECT SQL_CACHE * FROM programs_company_contacts p
       WHERE `p`.`program_id` = ".intval($program_id)." AND
             `p`.`company_id`= ".intval($company_id)." AND
             `p`.`participant_id` = ".intval($participant_id);
       $aData = array();
       if( $aRes = $mysqli->query($sql) ){
           if( $aRes->num_rows > 0 ){
               return true;
           }
       }
       return false;
   }

 
   function is_program_participant($program_id,$company_id,$participant_id){
       global $mysqli;

       $sql = "	SELECT SQL_CACHE * FROM programs_participants p
       WHERE `p`.`program_id` = ".intval($program_id)." AND
             `p`.`status`=1 AND
             `p`.`company_id`= ".intval($company_id)." AND
             `p`.`participant_id` = ".intval($participant_id);
       $aData = array();
       if( $aRes = $mysqli->query($sql) ){
           if( $aRes->num_rows > 0 ){
               return true;
           }
       }
       return false;
   }

   $q = trim($_GET['term']);
   $sql = "	SELECT SQL_CACHE p.id,full_name,pp.name  FROM participants p JOIN participants_position pp ON (p.participants_position_id=pp.id)
   			WHERE `p`.company_id = ".intval($q)." AND p.`status`=1 AND pp.`status`=1
   			ORDER BY trim(full_name)
   			";
   header('Cache-Control: no-cache, must-revalidate');
   header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
   header('Content-type: application/json');
   $aData = array();
   if( $aRes = $mysqli->query($sql) ){
      while( $obj = $aRes->fetch_object() ){
          $is_contact =  is_program_company_participant($_GET['program_id'],$q,$obj->id);
          if(is_program_participant($_GET['program_id'],$q,$obj->id)){
              $aData[] = array('id'=>$obj->id, 'full_name'=> $obj->full_name,'position'=> $obj->name,'selected'=>true,'is_contact'=>$is_contact);
          }else{
              $aData[] = array('id'=>$obj->id, 'full_name'=> $obj->full_name,'position'=> $obj->name,'selected'=>false,'is_contact'=>$is_contact);
          }
      }
      $aRes->close();
   }

   if( strlen($q) > 0 ){
       echo json_encode($aData);
   }else{
       echo json_encode(null);
   }
}else{
   die;
}

$mysqli->close();
