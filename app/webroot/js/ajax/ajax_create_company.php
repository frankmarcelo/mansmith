<?php
if(  
	isset($_POST) && 
    isset($_POST['data']) && 
    isset($_POST['data']['Company']) && 
    intval($_POST['data']['Company']['who_created']) > 0
){
	require_once './sanitize.php';	
	require_once '../../../config/database.php';
	require_once '../../../models/program.php';
	
	$dbConfig = new DATABASE_CONFIG();
	$mysqli = new mysqli( $dbConfig->default['host'] ,
	                      $dbConfig->default['login'] ,
	                      $dbConfig->default['password'] ,
	                      $dbConfig->default['database'] );
	if( $mysqli->ping() ){
		if( strlen($_POST['data']['Company']['name']) > 0 &&
			strlen($_POST['data']['Company']['primary_bldg_name']) > 0 &&
			strlen($_POST['data']['Company']['primary_street'])    > 0 &&
			strlen($_POST['data']['Company']['primary_city'])      > 0 &&
			strlen($_POST['data']['Company']['primary_phone'])     > 0 &&
			strlen($_POST['data']['Company']['primary_email'])     > 0 &&
			strlen($_POST['data']['Company']['primary_mobile'])    > 0 &&
			intval($_POST['data']['Company']['companies_affiliation_id']) > 0 &&
			intval($_POST['data']['Company']['companies_industry_id']) > 0
		){
			$seo_name = Sanitize::convertToSeoUri(trim($_POST['data']['Company']['name']));
			$sql = "INSERT INTO companies SET 
					   `status` = 1,
					   `programs_division_id`     ='4',
					   `companies_affiliation_id` ='".intval($_POST['data']['Company']['companies_affiliation_id'])."',
					   `companies_industry_id`    ='".intval($_POST['data']['Company']['companies_affiliation_id'])."',
					   `name`                  	  ='".Sanitize::clean(trim($_POST['data']['Company']['name']))."',
					   `seo_name`                 ='".$seo_name."',
					   `email`                    ='".Sanitize::clean(trim($_POST['data']['Company']['primary_email']))."',
					   `notes`                    ='".Sanitize::clean(trim($_POST['data']['Company']['notes']))."',
					   `primary_phone`            ='".trim($_POST['data']['Company']['primary_phone'])."',
					   `secondary_phone`          ='".trim($_POST['data']['Company']['secondary_phone'])."',
					   `mobile`                   ='".Sanitize::clean(trim($_POST['data']['Company']['primary_mobile']))."',
					   `fax`                      ='".Sanitize::clean(trim($_POST['data']['Company']['primary_fax']))."',
					   `website`                  ='".Sanitize::clean(trim($_POST['data']['Company']['primary_website']))."',
					   `primary_bldg_name`        ='".Sanitize::clean(trim($_POST['data']['Company']['primary_bldg_name']))."',
					   `primary_suburb`           ='".Sanitize::clean(trim($_POST['data']['Company']['primary_suburb']))."',
					   `primary_street`        	  ='".Sanitize::clean(trim($_POST['data']['Company']['primary_street']))."',
					   `primary_city`         	  ='".Sanitize::clean(str_replace(", philippines","",strtolower($_POST['data']['Company']['primary_city'])))."',
					   `primary_zip_code`         ='".trim($_POST['data']['Company']['primary_zip_code'])."',
					   `secondary_bldg_name`      ='".Sanitize::clean(trim($_POST['data']['Company']['secondary_bldg_name']))."',
					   `secondary_street`         ='".Sanitize::clean(trim($_POST['data']['Company']['secondary_street']))."',
					   `secondary_suburb`         ='".Sanitize::clean(trim($_POST['data']['Company']['secondary_suburb']))."',
					   `secondary_city`           ='".Sanitize::clean(str_replace(", philippines","",strtolower($_POST['data']['Company']['secondary_city'])))."',
					   `secondary_zip_code`       ='".trim($_POST['data']['Company']['secondary_zip_code'])."',
					   `latitude`                 ='".trim($_POST['data']['Company']['latitude'])."',
					   `longitude`                ='".trim($_POST['data']['Company']['longitude'])."',
					   `date_created`             ='".date("Y-m-d H:i:s")."',
					   `date_modified`            ='".date("Y-m-d H:i:s")."',
					   `who_created`              ='".intval($_POST['data']['Company']['who_created'])."'";
            //insert
            if( $mysqli->query($sql) ){
				$id = $mysqli->insert_id;
				$data = array( 'data' => array('url'=> $dbConfig->default['directory'].'/admin/companies/info/'.$id.'/'.trim($seo_name)));
                echo json_encode($data);
            }
		}
	}
	$mysqli->close();
}