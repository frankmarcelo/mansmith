<?php
if(
	isset($_POST) &&
    isset($_POST['data']) &&
    isset($_POST['data']['Company']) &&
	isset($_POST['data']['Participant']) &&
    intval($_POST['data']['Participant']['who_created']) > 0
){
    require_once './sanitize.php';
	require_once '../../../config/database.php';
	require_once '../../../models/program.php';

	$dbConfig = new DATABASE_CONFIG();
	$mysqli = new mysqli( $dbConfig->default['host'] ,
	                      $dbConfig->default['login'] ,
	                      $dbConfig->default['password'] ,
	                      $dbConfig->default['database'] );
	if( $mysqli->ping() ){
        if( strlen($_POST['data']['Company']['name']) 						  > 0 &&
            strlen($_POST['data']['Company']['id']) 						  > 0 &&
			strlen($_POST['data']['Participant']['first_name']) 			  > 0 &&
			strlen($_POST['data']['Participant']['last_name'])    			  > 0 &&
			strlen($_POST['data']['Participant']['address'])     			  > 0 &&
			strlen($_POST['data']['Participant']['email'])     				  > 0 &&
			strlen($_POST['data']['Participant']['phone'])   				  > 0 &&
			intval($_POST['data']['Participant']['participants_grouping_id']) > 0 &&
			strlen($_POST['data']['Participant']['participants_title_id'])    > 0 &&
			strlen($_POST['data']['Participant']['participants_position_id']) > 0
        ){
            $sql = " SELECT SQL_CACHE * FROM companies WHERE id='".intval($_POST['data']['Company']['id'])."' LIMIT 1 ";
            $result = $mysqli->query($sql);
			if( $result->num_rows > 0 ){
				$obj = $result->fetch_object();
				$company_id = $obj->id;
				if( intval($company_id)> 0 ){
                    $total_employees = intval($obj->total_employee);
                    $participant_position_id = null;
                    $sql = " SELECT SQL_CACHE * FROM participants_position WHERE name='".trim($_POST['data']['Participant']['participants_position_id'])."' LIMIT 1 ";
                    $result = $mysqli->query($sql);
                    if( $result->num_rows > 0 ){
                        $obj = $result->fetch_object();
                        $participant_position_id = $obj->id;
                    }else{//create new participant position id
                        $sql = "INSERT INTO participants_position
                                    SET `name`         = '".trim($_POST['data']['Participant']['participants_position_id'])."',
                                        `seo_name`     = '".Sanitize::convertToSeoUri(trim($_POST['Participant']['participants_position_id']))."',
                                        `status`       = 1,
                                        `date_created` ='".date("Y-m-d H:i:s")."',
                                        `date_modified`='".date("Y-m-d H:i:s")."',
                                        `who_created`  = '".intval($_POST['data']['Participant']['who_created'])."'";
                        if( $mysqli->query($sql) ){
                            $participant_position_id = $mysqli->insert_id;
                        }
                    }

                    $full_name = trim($_POST['data']['Participant']['first_name']).' '.trim($_POST['data']['Participant']['middle_name']).' '.trim($_POST['data']['Participant']['last_name']);
					$seo_name = Sanitize::convertToSeoUri(trim($full_name));

                    $sql = "INSERT INTO participants SET
							   `status` = 1,
                               `company_id`               ='".$company_id."',
							   `programs_division_id`     ='4',
                               `participants_title_id` 	  ='".intval($_POST['data']['Participant']['participants_title_id'])."',
                               `participants_grouping_id` ='".intval($_POST['data']['Participant']['participants_grouping_id'])."',
                               `participants_position_id` ='".intval($participant_position_id)."',
                               `full_name`                ='".$full_name."',
                               `nick_name`                ='".Sanitize::clean(trim($_POST['data']['Participant']['nick_name']))."',
							   `first_name`               ='".Sanitize::clean(trim($_POST['data']['Participant']['first_name']))."',
                               `middle_name`              ='".Sanitize::clean(trim($_POST['data']['Participant']['middle_name']))."',
                               `last_name`                ='".Sanitize::clean(trim($_POST['data']['Participant']['last_name']))."',
                               `seo_name`                 ='".$seo_name."',
                               `phone`                    ='".Sanitize::clean(trim($_POST['data']['Participant']['phone']))."',
                               `email`                    ='".Sanitize::clean(trim($_POST['data']['Participant']['email']))."',
                               `address`                  ='".Sanitize::clean(trim($_POST['data']['Participant']['address']))."',
                               `zip_code`                 ='".Sanitize::clean(trim($_POST['data']['Participant']['zip_code']))."',
                               `fax`                      ='".Sanitize::clean(trim($_POST['data']['Participant']['fax']))."',
                               `mobile`                   ='".Sanitize::clean(trim($_POST['data']['Participant']['mobile']))."',
                               `notes`                    ='".Sanitize::clean(trim($_POST['data']['Participant']['notes']))."',
                               `date_created`             ='".date("Y-m-d H:i:s")."',
							   `date_modified`            ='".date("Y-m-d H:i:s")."',
							   `who_created`              ='".intval($_POST['data']['Participant']['who_created'])."'";
					//insert
                    if( $mysqli->query($sql) ){
						$id = $mysqli->insert_id;
                        $total_employees++;
                        $sqlUpdate = " UPDATE companies SET total_employee='".intval($total_employees)."' WHERE id='".intval($company_id)."'";
                        if( $mysqli->query($sqlUpdate) ){
                            $data = array( 'data' => array('url'=> $dbConfig->default['directory'].'/admin/participants/info/'.$id.'/'.trim($seo_name)));
                            echo json_encode($data);
                        }
					}
				}
			}
		}
	}
	$mysqli->close();
}