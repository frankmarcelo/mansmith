<?php

require_once '../../../config/database.php';

$dbConfig = new DATABASE_CONFIG();
$mysqli = new mysqli( $dbConfig->default['host'] , 
                      $dbConfig->default['login'] ,
                      $dbConfig->default['password'] ,
                      $dbConfig->default['database'] );
if( $mysqli->ping() ){
  
   $limit = ( isset($_GET['limit']) && intval($_GET['limit']) > 0 ) ? intval($_GET['limit']) : 20;
   $q = trim($_GET['term']);
   $sql = "	SELECT SQL_CACHE DISTINCT(name),id FROM companies 
                WHERE LENGTH(TRIM(name))>0 AND name like '".$mysqli->escape_string($q)."%'
   		GROUP BY TRIM(name) 
   		ORDER BY UPPER(TRIM(name)) ASC
                limit 20 
   		";
   header('Cache-Control: no-cache, must-revalidate');
   header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
   header('Content-type: application/json');
   $aData = array();
   if( $aRes = $mysqli->query($sql) ){
      while( $obj = $aRes->fetch_object() ){
      	 if(strlen($obj->name) > 80 ){
       	     $obj->name = substr($obj->name,0,80);		
       	 }	
         $aData[] = array('company_id'=> $obj->id , 'name'=>ucwords(trim(strtolower($obj->name)))); 
      }
      $aRes->close();
   }

   if( strlen($q) > 0 ){
       echo json_encode($aData);
   }else{
       echo json_encode(null);
   }
}else{
   die;
}

$mysqli->close();
