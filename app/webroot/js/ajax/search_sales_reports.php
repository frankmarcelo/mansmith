<?php

require_once '../../../config/database.php';

$dbConfig = new DATABASE_CONFIG();
$mysqli = new mysqli( $dbConfig->default['host'] , 
                      $dbConfig->default['login'] ,
                      $dbConfig->default['password'] ,
                      $dbConfig->default['database'] );
if( $mysqli->ping() && isset($_GET['term']) ){
  
   $limit = ( isset($_GET['limit']) && intval($_GET['limit']) > 0 ) ? intval($_GET['limit']) : 20;
   $q = trim($_GET['term']);
   $sql = "
SELECT 	SQL_CACHE DISTINCT(`SalesReports`.`source_file`)
FROM `sales_reports` AS `SalesReports`
JOIN `sales_reports_details` AS `SalesReportsDetails` ON (`SalesReportsDetails`.sales_reports_id = `SalesReports`.id)
JOIN `programs` AS `Program` ON (`SalesReportsDetails`.`program_id` = `Program`.`id` AND `Program`.`status` = 1)

	WHERE 
	";
   $sql .="

    (
        (
            (
                MATCH(`Program`.`title`) AGAINST ('+".trim($mysqli->escape_string($q))."*' IN boolean MODE)
            )
            OR
            (
                UPPER(`Program`.`title`) LIKE '%".trim($mysqli->escape_string($q))."%'
            )
            OR
            (
                UPPER(`SalesReports`.`source_file`) LIKE '%".trim($mysqli->escape_string($q))."%'
            )
        )
        AND
        (
            LENGTH(`Program`.`title`) > 0
        )
        AND (`Program`.`status` = 1)
    )
    limit 20
   		";

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    $aData = array();
    if( $aRes = $mysqli->query($sql) ){
        while( $obj = $aRes->fetch_object() ){
      	    if(strlen($obj->source_file) > 80 ){
       	        $obj->source_file = substr($obj->source_file,0,80);
       	    }
            $aData[] = ucwords(trim(strtolower($obj->source_file)));
        }
        $aRes->close();
    }

    if( strlen($q) > 0 ){
        echo json_encode($aData);
    }else{
        echo json_encode(null);
    }
}else{
   die;
}

$mysqli->close();
