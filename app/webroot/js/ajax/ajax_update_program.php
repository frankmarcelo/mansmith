<?php

if(
	isset($_POST) &&
    isset($_POST['data']) &&
    isset($_POST['data']['Program']) &&
    isset($_POST['data']['ProgramsSchedule']) &&
    isset($_POST['data']['ProgramsRatePlan']) &&
    intval($_POST['data']['Program']['who_modified']) > 0
){
	require_once './sanitize.php';	
	require_once '../../../config/database.php';
	require_once '../../../models/program.php';

	$dbConfig = new DATABASE_CONFIG();
	$mysqli = new mysqli( $dbConfig->default['host'] ,
	                      $dbConfig->default['login'] ,
	                      $dbConfig->default['password'] ,
	                      $dbConfig->default['database'] );
	if( $mysqli->ping() ){
		
		if( (isset($_POST['data']) ) &&
		    (strlen($_POST['data']['Program']['title']) > 0     )    &&
            (strlen($_POST['data']['Program']['venue_name']) > 0)    &&
            (strlen($_POST['data']['Program']['address']) > 0   )    &&
            (strlen($_POST['data']['Program']['city']) > 0      )    &&
            (strlen($_POST['data']['Program']['contact_name']) > 0 ) && 
            ($_POST['data']['Program']['programs_type_id'] > 0     ) &&
            ($_POST['data']['Program']['programs_division_id'] > 0 ) &&
			intval($_POST['data']['Program']['id']) >0
		){
			$program_id = intval($_POST['data']['Program']['id']);
			$sql = " SELECT * FROM programs WHERE id = '".$program_id."' ";
			if( $res = $mysqli->query($sql) ){
				if( $res->num_rows > 0 ){

                    $_POST['data']['Program']['post_to_facebook'] = (isset($_POST['data']['Program']['post_to_facebook'])) ? 1:0;
                    $_POST['data']['Program']['post_to_twitter'] = (isset($_POST['data']['Program']['post_to_twitter'])) ? 1:0;
                    $_POST['data']['Program']['city'] = str_replace(", philippines","",strtolower($_POST['data']['Program']['city']));	
					$seo_name = Sanitize::convertToSeoUri($_POST['data']['Program']['title']);

                    $sql = "UPDATE programs SET
                               programs_type_id ='".intval($_POST['data']['Program']['programs_type_id'])."',
                               programs_division_id ='".intval($_POST['data']['Program']['programs_division_id'])."',
                               title ='".Sanitize::clean($_POST['data']['Program']['title'])."',
                               seo_name ='".$seo_name."',
                               venue_name ='".Sanitize::clean($_POST['data']['Program']['venue_name'])."',
                               address ='".Sanitize::clean($_POST['data']['Program']['address'])."',
                               suburb ='".Sanitize::clean($_POST['data']['Program']['suburb'])."',
                               city ='".Sanitize::clean($_POST['data']['Program']['city'])."',
                               zip_code ='".intval($_POST['data']['Program']['zip_code'])."',
                               latitude ='".$_POST['data']['Program']['latitude']."',
                               discount_applies ='".intval($_POST['data']['Program']['discount_applies'])."',
                               longitude ='".$_POST['data']['Program']['longitude']."',
                               content ='".trim(str_replace(array("<p>","</p>"),array("",""),$_POST['data']['Program']['content']))."',
                               contact_name ='".Sanitize::clean($_POST['data']['Program']['contact_name'])."',
                               contact_email ='".trim($_POST['data']['Program']['contact_email'])."',
                               contact_phone ='".trim($_POST['data']['Program']['contact_phone'])."',
                               post_to_facebook ='".intval($_POST['data']['Program']['post_to_facebook'])."',
                               post_to_twitter ='".intval($_POST['data']['Program']['post_to_twitter'])."',
                               status ='1',
                               date_modified ='".date("Y-m-d H:i:s")."',
                               who_modified ='".intval($_POST['data']['Program']['who_modified'])."'
                           WHERE id ='".$program_id."'";
            
					if( $mysqli->query($sql) ){

                        if( isset($_POST['data']['ProgramsSchedule']) && count($_POST['data']['ProgramsSchedule'])>0 ){
                            $sql = " DELETE FROM programs_schedules WHERE program_id ='".$program_id."'";
							$mysqli->query($sql);

                            $all_day_event = ( isset($_POST['data']['ProgramsSchedule']['all_day_event']) ) ? 1: 0;

                            $start_date = Sanitize::getDateCriteria($_POST['data']['ProgramsSchedule']['start_date']);
                            $end_date = Sanitize::getDateCriteria($_POST['data']['ProgramsSchedule']['end_date']);

                            $lookUpDate = date("Ymd",strtotime($start_date));
                            $compareDate = date("Ymd",strtotime("+1 day",strtotime($end_date)));
                            while( $lookUpDate < $compareDate ){

                                $sql = "INSERT INTO programs_schedules SET
                                            program_id='".$program_id."',
                                            programs_schedule_type_id=1,
                                            all_day_event='".$all_day_event."',
                                            start_date='".date("Y-m-d",strtotime($lookUpDate))."',
                                            end_date='".date("Y-m-d",strtotime($lookUpDate))."',
                                            start_time ='".$_POST['data']['ProgramsSchedule']['start_time']."',
                                            end_time='".$_POST['data']['ProgramsSchedule']['end_time']."',
                                            status='1',
                                            date_created='".date("Y-m-d H:i:s")."'";
                                $mysqli->query($sql);
                                $lookUpDate = date("Ymd",strtotime("+1 day",strtotime($lookUpDate)));
                            }
                        }

                        if( isset($_POST['data']['ProgramsRatePlan']) && count($_POST['data']['ProgramsRatePlan'])>0 && !empty($_POST['data']['ProgramsRatePlan'])){
                            $sql = " DELETE FROM programs_rate_plans WHERE program_id ='".$program_id."'";
							$mysqli->query($sql);
                            
                            $num_of_loop = count($_POST['data']['ProgramsRatePlan']['eb_cost']);
                            $rate_plans = array();

                            for( $i = 0 ; $i < $num_of_loop ; $i++ ){
                                if( strtolower(trim($_POST['data']['ProgramsRatePlan']['use_plan'][$i])) == 'on' ){
                                    $rate_plans[] = array(
                                        'use_plan' => $_POST['data']['ProgramsRatePlan']['use_plan'][$i],
                                        'cut_off_date' => $_POST['data']['ProgramsRatePlan']['cut_off_date'][$i],
                                        'eb_cost' => $_POST['data']['ProgramsRatePlan']['eb_cost'][$i],
                                        'reg_cost' => $_POST['data']['ProgramsRatePlan']['reg_cost'][$i],
                                        'osp_cost' => $_POST['data']['ProgramsRatePlan']['osp_cost'][$i],
                                        'num_of_adults' => $_POST['data']['ProgramsRatePlan']['num_of_adults'][$i],
                                    );
                                }
                            }

                            if( count($rate_plans)> 0 && is_array($rate_plans) ){
                                foreach( $rate_plans as $rate_plan_key => $rate_plan  ){
                                    $rate_plan_id = intval($rate_plan_key)+1;
                                    $sql = "INSERT INTO  `programs_rate_plans` SET
                                                program_id    ='".intval($program_id)."',
                                                rate_plan_id  ='".intval($rate_plan_id)."',
                                                name          = 'Plan ".intval($rate_plan_id)."',
                                                status        ='1',
                                                cut_off_date  ='".Sanitize::getDateCriteria($rate_plan['cut_off_date'])."',
                                                num_of_adults ='".intval($rate_plan['num_of_adults'])."',
                                                reg_cost      ='".$rate_plan['reg_cost']."',
                                                eb_cost       ='".$rate_plan['eb_cost']."',
                                                osp_cost      ='".$rate_plan['osp_cost']."',
                                                date_created  ='".date("Y-m-d H:i:s")."',
                                                date_modified ='".date("Y-m-d H:i:s")."'
                                                ";
                                    $mysqli->query($sql);

                                }
                            }
                        }

                        if( isset($_POST['data']['ProgramsSpeaker']) && count($_POST['data']['ProgramsSpeaker']) > 0 ){
                            $sql = " DELETE FROM programs_speakers WHERE program_id ='".$program_id."'";
							$mysqli->query($sql);

                            $sql = null;
                            foreach( $_POST['data']['ProgramsSpeaker'] as $programs_speaker ){
                                if( is_array($programs_speaker) && count($programs_speaker)>0 ){
                                    foreach( $programs_speaker as $program_speaker ){
                                        if( strstr($program_speaker, '_selected') ){
                                            list( $speaker_id, $selected ) = explode("_",$program_speaker);
                                            $sql =" INSERT INTO programs_speakers SET
                                                        program_id ='".intval($program_id)."',
                                                        speaker_id ='".intval($speaker_id)."' ";
                                            $mysqli->query($sql);
                                        }
                                    }
                                }
                            }
                        }
                        $data = array( 'data' => array('url'=> $dbConfig->default['directory'].'/admin/programs/info/'.$program_id.'/'.($seo_name).'#program_participants'));
                        echo json_encode($data);
               		}
               	}
			}
		}
	}
	$mysqli->close();
}