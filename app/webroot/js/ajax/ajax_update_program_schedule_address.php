<?php

if( isset($_POST) ){
	require_once './sanitize.php';	
	require_once '../../../config/database.php';

	$dbConfig = new DATABASE_CONFIG();
         $mysqli = new mysqli( $dbConfig->default['host'] ,
	                      $dbConfig->default['login'] ,
	                      $dbConfig->default['password'] ,
	                      $dbConfig->default['database'] );
	if( $mysqli->ping() ){
        if( (isset($_POST['status']) ) && strlen($_POST['id']) > 0 ){
            $program_schedule_id = intval(str_replace("program_schedule_address_id_","",$_POST['id']));
            $sql = " SELECT * FROM programs_schedules WHERE id = '".$program_schedule_id."' ";
            if( $res = $mysqli->query($sql) ){
                if( $res->num_rows > 0 ){
                    $sql = "UPDATE programs_schedules SET
                           `address` ='".$mysqli->escape_string(trim($_POST['status']))."',
                           `date_modified`            ='".date("Y-m-d H:i:s")."'
                           WHERE id = '".$program_schedule_id."'";
                    if( $mysqli->query($sql) ){
                        $data = array( 'data' => "true");
                        echo json_encode($data);
                    }
                }
            }
        }
	}
	$mysqli->close();
}
