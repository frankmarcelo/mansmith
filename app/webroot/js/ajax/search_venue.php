<?php

require_once '../../../config/database.php';

$dbConfig = new DATABASE_CONFIG();
$mysqli = new mysqli( $dbConfig->default['host'] , 
                      $dbConfig->default['login'] ,
                      $dbConfig->default['password'] ,
                      $dbConfig->default['database'] );
if( $mysqli->ping() ){
  
   $limit = ( isset($_GET['limit']) && intval($_GET['limit']) > 0 ) ? intval($_GET['limit']) : 20;
   $q = trim($_GET['term']);
   $sql = "SELECT SQL_CACHE DISTINCT(venue_name) FROM programs 
   		   WHERE (MATCH(venue_name) AGAINST ('+".$mysqli->escape_string($q)."*' IN boolean MODE) OR 
   		          venue_name like '%".$mysqli->escape_string($q)."%') AND LENGTH(venue_name) > 0 
   		          AND status = 1
   		   GROUP BY TRIM(venue_name) 
   		   ORDER BY MATCH(venue_name) AGAINST ('+".$mysqli->escape_string($q)."*' IN boolean MODE) DESC, UPPER(TRIM(venue_name)) ASC 
   		   LIMIT ".$limit;
   header('Cache-Control: no-cache, must-revalidate');
   header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
   header('Content-type: application/json');
   $aData = array();
   if( $aRes = $mysqli->query($sql) ){
      while( $obj = $aRes->fetch_object() ){
      	 if(strlen($obj->venue_name) > 80 ){
       	     $obj->venue_name = substr($obj->venue_name,0,80);		
       	 }	
         $aData[] = ucwords(trim(strtolower($obj->venue_name))); 
      }
      $aRes->close();
   }

   if( strlen($q) > 0 ){
       echo json_encode($aData);
   }else{
       echo json_encode(null);
   }
}else{
   die;
}

$mysqli->close();
?>
