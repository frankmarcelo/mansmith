<?php

$basedir = dirname(dirname(dirname(dirname(dirname(__DIR__)))));
require_once $basedir.'/config/database.php';

ini_set('memory_limit','512M');
$dbConfig = new DATABASE_CONFIG();
$mysqli = new mysqli( $dbConfig->default['host'] , 
                      $dbConfig->default['login'] ,
                      $dbConfig->default['password'] ,
                      $dbConfig->default['database'] );

$sSql = "SELECT * FROM participants";
$aData = array();
if ($result = $mysqli->query($sSql)) {
    while($obj = $result->fetch_object()){
       $aData[] = $obj;
    }
    $result->close();
}

foreach( $aData as $key => $value ){

   $sSql = "SELECT count(pdp.program_id) as total FROM programs_directories_of_participants  pdp 
            JOIN directories_of_participants asp ON (pdp.id=asp.programs_directories_of_participant_id) WHERE asp.participant_id='".$value->id."' GROUP BY program_id";
   if ($result = $mysqli->query($sSql)) {
       $i = 0;
       while($obj2 = $result->fetch_object()){
          $i++; 
       }

       $sql ="UPDATE participants SET total_directory_of_participants='".$i."' WHERE id='".$value->id."'";
       $mysqli->query($sql);
   }
   $result->close();
}
$mysqli->close();