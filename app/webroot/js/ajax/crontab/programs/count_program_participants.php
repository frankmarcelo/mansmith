<?php
/*
 * Update the total number of participants and companies in program
 * This will update columns total_companies, total_participants in programs table
 */
$basedir = dirname(dirname(dirname(dirname(dirname(__DIR__)))));


require_once $basedir.'/config/database.php';
$dbConfig = new DATABASE_CONFIG();
$mysqli = new mysqli( $dbConfig->default['host'] ,
                      $dbConfig->default['login'] ,
                      $dbConfig->default['password'] ,
                      $dbConfig->default['database'] );

function countParticipants( $program_id =0 ){
    global $mysqli;

    $sql = "
         SELECT *
         FROM   programs_participants 
         WHERE  program_id ='".$program_id."' AND status=1
         GROUP  BY company_id

    ";
    $intCompanies = 0;
    if ($result = $mysqli->query($sql)) {
       $intCompanies = $result->num_rows;
       $result->close();
    }

    $sql = "
         SELECT *
         FROM   programs_participants 
         WHERE  program_id ='".$program_id."' AND status= 1
         GROUP BY participant_id
    ";
    $intParticipants = 0;
    if ($result = $mysqli->query($sql)) {
       $intParticipants = $result->num_rows;
       $result->close();
    }
    return array( 'companies'=> $intCompanies, 'participants' => $intParticipants );
}


$sql = "SELECT * FROM programs ";
$aSubTypes = array();
if ($result = $mysqli->query($sql)) {
    while( $obj = $result->fetch_object()){
        $aData = countParticipants( $obj->id );
        $sql = "UPDATE programs SET total_companies='".$aData['companies']."', total_participants ='".$aData['participants']."'
                WHERE  id ='".$obj->id."'

        ";

        if( !$mysqli->query($sql) ){
            echo $sql;
            break;
            die;
        }
    }
    $result->close();
}
$mysqli->close();
