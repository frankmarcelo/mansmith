<?php

/*
 * This crontab will generate the latest companies,programs, participants and upcoming programs
 * this should be run every hour
 */
$basedir = dirname(dirname(dirname(dirname(__DIR__))));
require_once $basedir. '/config/database.php';

$dbConfig = new DATABASE_CONFIG();
$mysqli = new mysqli( $dbConfig->default['host'] , 
                      $dbConfig->default['login'] ,
                      $dbConfig->default['password'] ,
                      $dbConfig->default['database'] );
if( $mysqli->ping() ){
   $sSql = "SELECT * FROM invoice  ORDER BY id";
   if( $aRes = $mysqli->query($sSql) ){
      if( $aRes->num_rows > 0 ){
         $aProgramIds = array();
         while( $obj = $aRes->fetch_object() ){
            $sSql = "
                SELECT  id
                FROM    programs_billing_info
                WHERE   program_id = {$obj->program_id} AND company_id={$obj->company_id} 
            ";
            if( $res = $mysqli->query($sSql) ){
                if( $res->num_rows > 0 ){
                    $fetch_obj = $res->fetch_object();
                    $sql = " UPDATE invoice SET program_billing_info_id=".$fetch_obj->id." WHERE id = ".$obj->id;
                    $mysqli->query($sql);
                }
            }
         }
      }
   }
}else{
   die;
}

$mysqli->close();
