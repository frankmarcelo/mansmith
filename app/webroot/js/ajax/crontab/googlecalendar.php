<?php


$basedir = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
ini_set('include_path', $basedir.'/vendors');
ini_set('max_execution_time',100000); 
require_once 'Zend/Loader.php';

Zend_Loader::loadClass('Zend_Gdata');
Zend_Loader::loadClass('Zend_Gdata_AuthSub');
Zend_Loader::loadClass('Zend_Gdata_ClientLogin');
Zend_Loader::loadClass('Zend_Gdata_HttpClient');
Zend_Loader::loadClass('Zend_Gdata_Calendar');


$service = Zend_Gdata_Calendar::AUTH_SERVICE_NAME;
$sGCalendarLogin = 'mansmith.net@gmail.com';
$sGCalendarPass  = 'm4nsmith';
$oGHttpClient = Zend_Gdata_ClientLogin::getHttpClient($sGCalendarLogin, $sGCalendarPass, $service);
$oGHttpClient->setConfig(array( 'timeout' => 18000 ));
$service = new Zend_Gdata_Calendar($oGHttpClient);


$basedir = dirname(dirname(dirname(dirname(__DIR__))));
require_once $basedir. '/config/database.php';

$dbConfig = new DATABASE_CONFIG();
$mysqli = new mysqli( $dbConfig->default['host'] , 
                      $dbConfig->default['login'] ,
                      $dbConfig->default['password'] ,
                      $dbConfig->default['database'] );

error_reporting(E_ALL);

$sql = "

SELECT MIN(ps.start_date),MAX(ps.start_date),p.*,ps.* ,ps.id as schedule_id
FROM programs p
JOIN  programs_schedules ps ON (ps.program_id=p.id)
WHERE p.status = 1 AND ps.status = 1 AND ps.gCalendar_eventId IS NULL
GROUP BY p.id
ORDER BY ps.start_date
LIMIT 20
";
if ($result = $mysqli->query($sql)) {

    while($obj = $result->fetch_object()){

       $event = $service->newEventEntry();
     
       // Populate the event with the desired information
       // Note that each attribute is crated as an instance of a matching class

       $where  = ucwords(trim($obj->venue_name));
       $where .= (strlen($obj->address)>0) ? ', '.ucwords(trim($obj->address)):'';
       $where .= (strlen($obj->suburb)>0) ? ', '.ucwords(trim($obj->suburb)):'';
       $where .= (strlen($obj->city)>0) ? ', '.ucwords(trim($obj->city)):'';
       $where .= (strlen($obj->country)>0) ? ', '.ucwords(trim($obj->country)):'';
       $where .= (strlen($obj->zip_code)>0 && intval($obj->zip_code) > 0) ? ' '.ucwords(trim($obj->zip_code)):'';
      
       $content  = (strlen($obj->notes)>0) ? trim($obj->notes):'';
       $content .= "For complete course outlines, enquiries and reservations please check our website http://www.mansmith.net";
       $content .= "<br /> or contact ".ucwords($obj->contact_name)." at Office: ".ucwords($obj->contact_phone);
       $content .= "\nMobile: 0918-81-168-88 \nEmail: ".trim($obj->contact_email)."\n";
       $content .= "\nLink: http://db.mansmith.net/admin/programs/info/".$obj->program_id.'/'. $obj->seo_name;

       $event->title = $service->newTitle(ucwords(trim($obj->title)));
       $event->where = array($service->newWhere($where));
       $event->content = $service->newContent($content);
     
       // Set the date using RFC 3339 format.
       $startDate = $obj->start_date;
       //$startDate = '2013-09-20';
       $startTime = date("H:i" , strtotime($startDate.' '.$obj->start_time));
       //$endDate   = '2013-09-21';
       $endDate   = $obj->end_date;
       $obj->end_time = "05:00 pm";   
       $endTime   = date("H:i" , strtotime($endDate.' '.$obj->end_time));
       $tzOffset  = '+08';

       $when = $service->newWhen();
       $when->startTime = "{$startDate}T{$startTime}:00.000{$tzOffset}:00";
       //echo "{$startDate}T{$startTime}:00.000{$tzOffset}:00<br>";
       //$when->endTime = "{$endDate}T{$endTime}:00.000{$tzOffset}:00";
       //echo "{$endDate}T{$endTime}:00.000{$tzOffset}:00";
       $event->when = array($when);

       $newEvent = $service->insertEvent($event);

       $calendar_id = (string) $newEvent->id;
       $event_id = $calendar_id;
       $sql = "UPDATE programs_schedules SET gCalendar_eventId='".$event_id."' WHERE program_id ='".$obj->program_id."'";
       if( !$mysqli->query($sql) ){
           echo $sql;
           break;
           die;
       }
      echo $calendar_id . "<br/>";
    }
    $result->close();
}
$mysqli->close();
