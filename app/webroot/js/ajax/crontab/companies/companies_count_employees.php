<?php

$basedir = dirname(dirname(dirname(dirname(dirname(__DIR__)))));
require_once $basedir.'/config/database.php';

$dbConfig = new DATABASE_CONFIG();
$mysqli = new mysqli( $dbConfig->default['host'] , 
                      $dbConfig->default['login'] ,
                      $dbConfig->default['password'] ,
                      $dbConfig->default['database'] );

$sql = "SELECT * FROM companies ORDER BY id;";
if ($result = $mysqli->query($sql)) {
    while($obj = $result->fetch_object()){
       $sql = "SELECT count(id) as total FROM participants WHERE company_id='".$obj->id."'";
       if( $resultTotal = $mysqli->query($sql) ){
          if( $resultTotal->num_rows > 0 ){
             $totalObject = $resultTotal->fetch_object();
             $sql =" UPDATE companies SET total_employee='".$totalObject->total."' WHERE id='".$obj->id."'";
             $mysqli->query($sql);
          }
       }
    }
    $result->close();
}
