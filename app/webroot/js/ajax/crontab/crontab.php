<?php

/*
 * This crontab will generate the latest companies,programs, participants and upcoming programs
 * this should be run every hour
 */
$basedir = dirname(dirname(dirname(dirname(__DIR__))));
require_once $basedir. '/config/database.php';

$dbConfig = new DATABASE_CONFIG();
$mysqli = new mysqli( $dbConfig->default['host'] , 
                      $dbConfig->default['login'] ,
                      $dbConfig->default['password'] ,
                      $dbConfig->default['database'] );
if( $mysqli->ping() ){
   $sSql = "SELECT DISTINCT(id) as id FROM programs WHERE status=1 ORDER BY id DESC LIMIT 0,10";
   if( $aRes = $mysqli->query($sSql) ){
      if( $aRes->num_rows > 0 ){
         $aProgramIds = array();
         while( $obj = $aRes->fetch_object() ){
            $aProgramIds[] = $obj->id;
         }
         $sSql = "TRUNCATE latest_programs";
         $mysqli->query($sSql);
         
         if( is_array($aProgramIds) && count($aProgramIds) > 0 ){
            foreach( $aProgramIds as $programId ){
                
                $sSql = "SELECT    SQL_CACHE MIN( `ps`.`start_date` ) AS `lastestDate` , 
                                  ps.program_id,
                                  ps.id
                         FROM     programs_schedules ps
                         JOIN     programs p ON (p.id = ps.program_id AND p.status=1 AND ps.status=1)
                         WHERE    ps.program_id = '".$programId."'
                         GROUP BY ps.program_id 
                         ORDER BY ps.`start_date`
                ";
               if( $aRes = $mysqli->query($sSql) ){
                  if( $aRes->num_rows > 0 ){
                     $aIds = array();
                     $obj = $aRes->fetch_object();
                  } 
               } 
               
               $sSql = "INSERT INTO latest_programs SET start_date='".$obj->lastestDate."',program_id='".$mysqli->escape_string($programId)."',date_created=NOW()";
               $mysqli->query($sSql); 
            }
         }
      }
      $aRes->free_result();
   }
   unset($sSql);

   $sSql = "SELECT DISTINCT(id) as id FROM companies WHERE status=1 ORDER BY id DESC LIMIT 0,10";
   if( $aRes = $mysqli->query($sSql) ){
      if( $aRes->num_rows > 0 ){
         $aCompanyIds = array();
         while( $obj = $aRes->fetch_object() ){
            $aCompanyIds[] = $obj->id;
         }
         $sSql = "TRUNCATE latest_companies;";
         $mysqli->query($sSql);

         if( is_array($aCompanyIds) && count($aCompanyIds) > 0 ){
            foreach( $aCompanyIds as $companyId ){

               $sSql = "INSERT INTO latest_companies SET company_id='".$mysqli->escape_string($companyId)."',date_created=NOW()";
               $mysqli->query($sSql);
            }
         }
      }
      $aRes->free_result();
   }
   unset($sSql);

   $sSql = "SELECT DISTINCT(id) as id FROM participants WHERE status=1 ORDER BY id DESC LIMIT 0,10";
   if( $aRes = $mysqli->query($sSql) ){
      if( $aRes->num_rows > 0 ){
         $aIds = array();
         while( $obj = $aRes->fetch_object() ){
            $aIds[] = $obj->id;
         }
         $sSql = "TRUNCATE latest_participants;";
         $mysqli->query($sSql);

         if( is_array($aIds) && count($aIds) > 0 ){
            foreach( $aIds as $Id ){

               $sSql = "INSERT INTO latest_participants SET participant_id='".$mysqli->escape_string($Id)."',date_created=NOW()";
               $mysqli->query($sSql);
            }
         }
      }
      $aRes->free_result();
   }

    $sSql = "TRUNCATE upcoming_programs;";
    $mysqli->query($sSql);
   
   $sSql = "SELECT    SQL_CACHE MIN( `ps`.`start_date` ) AS `lastestDate` , 
                      ps.program_id,
                      ps.id
             FROM     programs_schedules ps
             JOIN     programs p ON (p.id = ps.program_id AND p.status=1 AND ps.status=1)
             WHERE    (`start_date` >'".date("Y-m-d",strtotime("+10 days",strtotime('now')))."')
             GROUP BY ps.program_id 
             ORDER BY ps.`start_date`
             LIMIT 10
           ";
   //date("Y-m-d",strtotime("now"))
   //HAVING MIN( `start_date` ) < '".date("Y-m-d",strtotime("+30 days",strtotime('now')))."'
   //HAVING MIN( `start_date` ) < '".date("Y-m-d",strtotime("-500 days",strtotime('now')))."' 
   //echo $sSql;
   if( $aRes = $mysqli->query($sSql) ){
      if( $aRes->num_rows > 0 ){
         $aIds = array();
         while( $obj = $aRes->fetch_object() ){
             $aIds[] = $obj;
         }


         if( is_array($aIds) && count($aIds) > 0 ){
            foreach( $aIds as $Id ){
               $sSql = "INSERT INTO upcoming_programs SET program_id='".$mysqli->escape_string($Id->program_id)."',schedule='".$Id->lastestDate."', date_created=NOW()";
               $mysqli->query($sSql);
            }
         }
      }
      $aRes->free_result();
   } 
   
   //create upcoming events put schedule as well

}else{
   die;
}

$mysqli->close();