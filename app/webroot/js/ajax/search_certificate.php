<?php

require_once '../../../config/database.php';

$dbConfig = new DATABASE_CONFIG();
$mysqli = new mysqli( $dbConfig->default['host'] , 
                      $dbConfig->default['login'] ,
                      $dbConfig->default['password'] ,
                      $dbConfig->default['database'] );
if( $mysqli->ping() ){
  
   $limit = ( isset($_GET['limit']) && intval($_GET['limit']) > 0 ) ? intval($_GET['limit']) : 20;
   $q = trim($_GET['term']);
   $division_id = (isset($_GET['division_id']) && intval($_GET['division_id'])>0)? intval(trim($_GET['division_id'])):0;
   $sql = "	
SELECT 	SQL_CACHE DISTINCT(`Program`.`title`)
FROM `programs_certificates` AS `ProgramsCertificate` 
JOIN `programs` AS `Program` ON (`ProgramsCertificate`.`program_id` = `Program`.`id` AND `Program`.`status` = 1) 
JOIN `programs_division` AS `ProgramsDivision` ON (`Program`.`programs_division_id` = `ProgramsDivision`.`id`) 
JOIN `programs_types` AS `ProgramsType` ON (`Program`.`programs_type_id` = `ProgramsType`.`id` AND `ProgramsType`.`status` = 1)
	WHERE 
	";
   $sql .= ($division_id >0 )? " `Program`.`programs_division_id`=' ".intval($division_id)."' AND ": " ";
   $sql .=" 
		((( (
			MATCH(`Program`.`title`) AGAINST ('+".$mysqli->escape_string($q)."*' IN boolean MODE)) OR 
			(TRIM(UPPER(`Program`.`title`))) LIKE '%".$mysqli->escape_string($q)."%' )) AND 
			(( LENGTH(TRIM(`Program`.`title`))> 0 )) AND (`Program`.`status` = 1)) 
	ORDER BY MATCH(`Program`.`title`) AGAINST ('+".$mysqli->escape_string($q)."*' IN boolean MODE) DESC,TRIM(UPPER(Program.title))
   limit 20 
   		";
   header('Cache-Control: no-cache, must-revalidate');
   header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
   header('Content-type: application/json');
   $aData = array();
   if( $aRes = $mysqli->query($sql) ){
      while( $obj = $aRes->fetch_object() ){
      	 if(strlen($obj->title) > 80 ){
       	     $obj->title = substr($obj->title,0,80);		
       	 }	
         $aData[] = ucwords(trim(strtolower($obj->title))); 
      }
      $aRes->close();
   }

   if( strlen($q) > 0 ){
       echo json_encode($aData);
   }else{
       echo json_encode(null);
   }
}else{
   die;
}

$mysqli->close();
?>
