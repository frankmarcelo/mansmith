<?php

require_once '../../../config/database.php';
$dbConfig = new DATABASE_CONFIG();
$mysqli = new mysqli( $dbConfig->default['host'] , 
                      $dbConfig->default['login'] ,
                      $dbConfig->default['password'] ,
                      $dbConfig->default['database'] );
if( $mysqli->ping() && isset($_REQUEST['id']) && strlen($_REQUEST['id']) > 0 && strstr($_REQUEST['id'],'/') ){
   list( $id, $name ) = explode("/",$_REQUEST['id']);

   $sql = " SELECT SQL_CACHE company_id FROM participants WHERE id='".$mysqli->escape_string($id)."' AND `status`=1 LIMIT 1 ";
   $result = $mysqli->query($sql);
   if( $result->num_rows > 0 ){
       $obj = $result->fetch_object();
       $company_id = $obj->company_id;

       $sql = "UPDATE participants SET status=2, date_modified=NOW(),who_modified=".intval($_REQUEST['who_modified'])." WHERE id=".$mysqli->escape_string($id);
       header('Cache-Control: no-cache, must-revalidate');
       header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
       header('Content-type: application/json');
       $aData = array();
       if( $mysqli->query($sql) ){
            $sql = "UPDATE companies SET `total_employee`=(`total_employee`-1),date_modified=NOW(),who_modified=".intval($_REQUEST['who_modified'])." WHERE id=".$mysqli->escape_string($company_id);
            if( $mysqli->query($sql) ){
                echo json_encode(true);
            }else{
                echo json_encode(false);
            }
       }else{
            echo json_encode(false);
       }
   }else{
       die;
   }
}else{
   die;
}
$mysqli->close();
