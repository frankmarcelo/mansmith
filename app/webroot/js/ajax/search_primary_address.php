<?php

require_once '../../../config/database.php';

$dbConfig = new DATABASE_CONFIG();
$mysqli = new mysqli( $dbConfig->default['host'] , 
                      $dbConfig->default['login'] ,
                      $dbConfig->default['password'] ,
                      $dbConfig->default['database'] );
if( $mysqli->ping() ){
  
   $limit = ( isset($_GET['limit']) && intval($_GET['limit']) > 0 ) ? intval($_GET['limit']) : 20;
   $q = trim($_GET['term']);
   $sql = "SELECT SQL_CACHE DISTINCT(primary_street) FROM companies
   		   WHERE (MATCH(primary_street) AGAINST ('+".$mysqli->escape_string($q)."*' IN boolean MODE) OR 
   		          TRIM(primary_street) like '%".$mysqli->escape_string($q)."%') AND LENGTH(primary_street) > 0 
   		          AND status = 1
   		   GROUP BY TRIM(primary_street)
   		   ORDER BY MATCH(primary_street) AGAINST ('+".$mysqli->escape_string($q)."*' IN boolean MODE) DESC, UPPER(TRIM(primary_street)) ASC 
   		   LIMIT ".$limit;
   header('Cache-Control: no-cache, must-revalidate');
   header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
   header('Content-type: application/json');
   $aData = array();
   if( $aRes = $mysqli->query($sql) ){
      while( $obj = $aRes->fetch_object() ){
      	 if(strlen($obj->primary_street) > 80 ){
       	     $obj->primary_street = substr($obj->primary_street,0,80);		
       	 }	
         $aData[] = ucwords(trim(strtolower($obj->primary_street))); 
      }
      $aRes->close();
   }

   if( sizeof($aData) < 20 ){
       $sql = "SELECT SQL_CACHE DISTINCT(secondary_street) FROM companies
   		   WHERE (MATCH(secondary_street) AGAINST ('+".$mysqli->escape_string($q)."*' IN boolean MODE) OR 
   		          TRIM(secondary_street) like '%".$mysqli->escape_string($q)."%') AND LENGTH(secondary_street) > 0 
   		          AND status = 1
   		   GROUP BY TRIM(secondary_street)
   		   ORDER BY MATCH(secondary_street) AGAINST ('+".$mysqli->escape_string($q)."*' IN boolean MODE) DESC, UPPER(TRIM(secondary_street)) ASC 
   		   LIMIT ".$limit;
       if( $aRes = $mysqli->query($sql) ){
          while( $obj = $aRes->fetch_object() ){
      	     if(strlen($obj->secondary_street) > 80 ){
       	         $obj->secondary_street = substr($obj->secondary_street,0,80);		
       	     }	
             $aData[] = ucwords(trim(strtolower($obj->secondary_street))); 
          }
          $aRes->close();
       }
   }
  
   if( strlen($q) > 0 ){
       echo json_encode($aData);
   }else{
       echo json_encode(null);
   }
}else{
   die;
}

$mysqli->close();
?>
