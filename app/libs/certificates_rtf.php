<?php

final class CertificatesRtf{

	const characterlimit = '35';
	
	const characterset = 'Utf-8';
	const marginleft   = '1.799166667';
	const marginright  = '0.564444444';
	const margintop    = '2.716388889';
	const marginbottom = '2.52';
	
	const paperwidth   = '27.94';
	const paperheight  = '21.59';  
	
	const initialcount = 1;
	const columncount  = 2;
	const pageDivisor  = 18;
	const columnwidth  = '18.044583333';
	const columnwidth_bigger  = '18.767777778';
	
	const column_width_one   = '0.705555556';
	const column_width_two   = '0.705555556';
	const column_width_three = '1.058333333';
	const column_width_four  = '22.930555556';
	
	private $rtf;
	private $border;
	private $participantsInfo =array();
	private $programInfo =array();
	private $file_name;
	private $schedule_dates;
	
	public function __construct(PHPRtfLite $rtf,PHPRtfLite_Border $border, $file_name,$schedule_dates=array(),$programInfo =array(), $participantsInfo=array()){
		$this->rtf = &$rtf;
		$this->border = &$border;
		$this->programInfo = &$programInfo;
		$this->participantsInfo = &$participantsInfo;
		$this->schedule_dates = $schedule_dates;
		$this->file_name = $file_name;
		
		$this->rtf->setLandscape();
		$this->rtf->setMarginLeft(constant('CertificatesRtf::marginleft'));
		$this->rtf->setMarginRight(constant('CertificatesRtf::marginright'));
		$this->rtf->setMarginTop(constant('CertificatesRtf::margintop'));
		$this->rtf->setMarginBottom(constant('CertificatesRtf::marginbottom'));
		$this->rtf->setPaperWidth(constant('CertificatesRtf::paperwidth'));
		$this->rtf->setPaperHeight(constant('CertificatesRtf::paperheight'));
		$this->rtf->setCharset(constant('CertificatesRtf::characterset'));	
	}
	
	private function addSection(){
		return $this->rtf->addSection();
	}
	
	private function getTotalPages(){
		return $this->getTotalParticipants();
	}
	
	private function getTotalParticipants(){
		return intval(count($this->participantsInfo));
	} 
	
	public function generateRtf(){
		
		foreach( $this->participantsInfo as $participants_key => $participants_Info ){
			
			$oSection  = $this->addSection();	
			$oTable_one = $oSection->addTable();
	
			$company_name = $participants_Info['Company']['name'];
    		$program  = $this->programInfo['Program']['title'];
    		$venue = trim($this->programInfo['Program']['venue_name']);
    		if( strlen(trim($this->programInfo['Program']['address'])) >0 ){
    			$venue .= ', '.trim($this->programInfo['Program']['address']);	
    		}
			if( strlen(trim($this->programInfo['Program']['suburb'])) >0 ){
    			$venue .= ', '.trim($this->programInfo['Program']['suburb']);	
    		}
			if( strlen(trim($this->programInfo['Program']['city'])) >0 ){
    			$venue .= ', '.trim($this->programInfo['Program']['city']);	
    		}
    		
    		$full_name  = ucwords(strtolower($participants_Info['Participant']['first_name'])).' ';
   			$full_name .= (strlen(trim($participants_Info['Participant']['middle_name'])) > 0 )? ucwords(strtolower($participants_Info['Participant']['middle_name'])).' ':'';
    		$full_name .= (strlen(trim($participants_Info['Participant']['last_name'])) > 0 )? ucwords(strtolower(trim($participants_Info['Participant']['last_name']))).' ':'';
    		
    		$oTable_two = $oSection->addTable();
			$oTable_two->addRows(1);
			$oTable_two->addColumnsList(array(
					constant('CertificatesRtf::column_width_one'),
					constant('CertificatesRtf::column_width_two'),
					constant('CertificatesRtf::column_width_three'),
					constant('CertificatesRtf::column_width_four')	
				)
			);
			
			$oTable_two->writeToCell(1,4, '<br><br><br><br><br><br><br>',new PHPRtfLite_Font(12, 'Arial','#000000') );//start with name
			$cell_two = $oTable_two->getCell(1,4);
			$cell_two->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
			$cell_two->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);
	
			$oTable_one = $oSection->addTable();
			$oTable_one->addRows(1);
			$oTable_one->addColumnsList(array(
					constant('CertificatesRtf::column_width_one'),
					constant('CertificatesRtf::column_width_two'),
					constant('CertificatesRtf::column_width_three'),
					constant('CertificatesRtf::column_width_four')	
				)
			);
			
			$break_newline ='<br>';
			$program_display = null;
			if( strlen(trim($program)) > constant('CertificatesRtf::characterlimit') ){
				$program_display = wordwrap(trim($program), constant('CertificatesRtf::characterlimit')-5,"<br>");
				$program_display_list = explode("<br>",trim($program_display));
				$oTable_one->writeToCell(1,4, '<br><b>'.$full_name.'</b><br>',new PHPRtfLite_Font(28, 'calisto MT','#000000') );//start with name
				$oTable_one->writeToCell(1,4, $company_name.'<br><br><br>',new PHPRtfLite_Font(14, 'Arial Narrow','#000000') );//company name
				if( count($program_display_list)>0 ){
					foreach( $program_display_list as $program_name_key => $program_names ){
						if( strlen(trim($program_names))>0 ){ 
							if( $program_name_key == 0 ){
								$oTable_one->writeToCell(1,4, '<br><b>'.$program_names.'</b>',new PHPRtfLite_Font(24, 'calisto MT','#000000') );//company name
							}else{
								$oTable_one->writeToCell(1,4, '<br><b>'.$program_names.'</b>',new PHPRtfLite_Font(24, 'calisto MT','#000000') );//company name
							}
						}
					}	
				}
				$cell_one = $oTable_one->getCell(1,4);
				$cell_one->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
				$cell_one->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);
				
			}else{
				$program_display = $program.$break_newline;
				$oTable_one->writeToCell(1,4, '<br><b>'.$full_name.'</b><br>',new PHPRtfLite_Font(28, 'calisto MT','#000000') );//start with name
				$oTable_one->writeToCell(1,4, $company_name.'<br><br><br>',new PHPRtfLite_Font(14, 'Arial Narrow','#000000') );//company name
				$oTable_one->writeToCell(1,4, '<br><b>'.$program_display.'</b>',new PHPRtfLite_Font(24, 'calisto MT','#000000') );//company name
		
				$cell_one = $oTable_one->getCell(1,4);
				$cell_one->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
				$cell_one->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);
			}	
	
			$oTable_three = $oSection->addTable();
			if( strlen(trim($program)) > constant('CertificatesRtf::characterlimit') ){
				$oTable_three->addRows(1,4.5);
			}else{
				$oTable_three->addRows(1,5);
			}
			$oTable_three->addColumnsList(array(
				constant('CertificatesRtf::column_width_one'),
				constant('CertificatesRtf::column_width_two'),
				constant('CertificatesRtf::column_width_three'),
				constant('CertificatesRtf::column_width_four')
			));	
			$oTable_three->writeToCell(1,4, $break_newline,new PHPRtfLite_Font(14, 'Arial','#000000') );//start with name
			$cell = $oTable_three->getCell(1,4);
			$cell->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
			$cell->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);

			
			$oTable_four = $oSection->addTable();
			$oTable_four->addRows(1,0.4);
			$oTable_four->addColumnsList(array(8.466666667,7.055555556,9.877777778));
			$oTable_four->writeToCell(1,1, date("jS",strtotime($this->schedule_dates[0]['ProgramsSchedule']['start_date'])),new PHPRtfLite_Font(9.5, 'calisto MT','#000000'));
			$oTable_four->writeToCell(1,2, date("F",strtotime($this->schedule_dates[0]['ProgramsSchedule']['start_date'])).' '.date("Y",strtotime($this->schedule_dates[0]['ProgramsSchedule']['start_date'])),new PHPRtfLite_Font(9.5, 'calisto MT','#000000') );
			$oTable_four->writeToCell(1,3, $venue,new PHPRtfLite_Font(9.5, 'calisto MT','#000000') );
			
			$cell_column_one = $oTable_four->getCell(1,1);
			$cell_column_one->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_RIGHT);
			$cell_column_one->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);
			$cell_column_one->setCellPaddings(0, 0, 0.6, 0);
			
			$cell_column_two = $oTable_four->getCell(1,2);
			$cell_column_two->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_LEFT);
			$cell_column_two->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);
			$cell_column_two->setCellPaddings(2.5, 0, 0.0, 0);
			
			$cell_column_three = $oTable_four->getCell(1,3);
			$cell_column_three->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_LEFT);
			$cell_column_three->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);
			$cell_column_three->setCellPaddings(0.2, 0, 0.0, 0);
		}
	}
	
	public function save_rtf(){
        $old_mask = umask(0);
        $this->rtf->save($this->file_name);
        umask($old_mask);
        system('chmod 777 '.$this->file_name);
	}
}