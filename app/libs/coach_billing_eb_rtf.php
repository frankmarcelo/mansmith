<?php

final class CoachBillingEbRtf{
		
	const characterlimit = '35';
	
	const characterset = 'Utf-8';
	const marginleft   = '1.763888889';
	const marginright  = '1.763888889';
	const margintop    = '1.322916667';
	const marginbottom = '1.322916667)';
	const paperwidth   = '21.6';
	
	const initialcount = 1;
	const columncount  = 2;
	const pageDivisor  = 10;
	const columnwidth  = '18.062222222';
	
	const column_division = '8.95';
	
	const column_division_one   = '1.693333333';
	const column_division_two   = '7.355416667';
	const column_division_three = '3.986388889';
	const column_division_four  = '5.044722222';
	const column_division_five  = '16.368888922';
	
	const column_division_one_a = '6.018388889';
	const column_division_one_b = '6.014861111';
	
	//image dimensions
	const image_width  = '3';
	const image_height = '1.2';
	
	private $rtf;
	private $border;
	private $participantsInfo =array();
	private $primaryContacts =array();
	private $programInfo =array();
	private $file_name;
	private $schedule_dates;
		
	public function __construct(PHPRtfLite $rtf,PHPRtfLite_Border $border, $file_name,$schedule_dates=array(),$programInfo=array(),$participantsInfo=array(),$primaryContacts=array()){
		$this->rtf = &$rtf;
		$this->border = &$border;
		$this->programInfo = &$programInfo;
		$this->participantsInfo = &$participantsInfo;
		$this->schedule_dates = &$schedule_dates;
		$this->primaryContacts = &$primaryContacts;
		$this->file_name = $file_name;
		
		$this->rtf->setMarginLeft(constant('CoachBillingEbRtf::marginleft'));
		$this->rtf->setMarginRight(constant('CoachBillingEbRtf::marginright'));
		$this->rtf->setMarginTop(constant('CoachBillingEbRtf::margintop'));
		$this->rtf->setMarginBottom(constant('CoachBillingEbRtf::marginbottom'));
		$this->rtf->setPaperWidth(constant('CoachBillingEbRtf::paperwidth'));
		$this->rtf->setCharset(constant('CoachBillingEbRtf::characterset'));
	}

	private function addSection(){
		return $this->rtf->addSection();
	}

	private function getTotalPages(){
		return ceil($this->getTotalParticipants()/constant('CoachBillingEbRtf::pageDivisor'));
	}

	private function getTotalParticipants(){
		return count($this->participantsInfo);
	}

	public function generateRtf(){
		$oSection  = $this->addSection();
		$numOfPage = intval($this->getTotalPages());

		$oTable = $oSection->addTable();
		$oTable->addRows(1);
		$oTable->addColumnsList(array(constant('CoachBillingEbRtf::columnwidth')));//set to two columns

		/**start of image header **/
		$coach_image = Configure::read('Coach.image');
		$cell_one = $oTable->getCell(1, 1);
		$cell_one->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
		$cell_one->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_TOP);
		$cell_one->setCellPaddings(0.0, 0, 0, 0.4);
		$oImage   = $cell_one->addImage($coach_image);
		$oImage->setWidth(constant('CoachBillingEbRtf::image_width'));
		$oImage->setHeight(constant('CoachBillingEbRtf::image_height'));

		$oTable = $oSection->addTable();
		$oTable->addRows(1);
		$oTable->addColumnsList(array(constant('CoachBillingEbRtf::columnwidth')));//set to two columns


		$cell_one = $oTable->getCell(1, 1);
		$cell_one->writeText('<b>Corporate</b> ',new PHPRtfLite_Font(18, 'Century Gothic','#c00000'));
		$cell_one->writeText('<b>Achievers</b> ',new PHPRtfLite_Font(18, 'Century Gothic','#e36c0a'));
		$cell_one->writeText('<b>Institute</b><br>',new PHPRtfLite_Font(18, 'Century Gothic','#000000'));
		$cell_one->writeText('.',new PHPRtfLite_Font(3, 'Century Gothic','#000000'));
		$cell_one->writeText(Configure::read('Coach.Header'),new PHPRtfLite_Font(12, 'Century Gothic','#000000'));
		$cell_one->writeText(Configure::read('AttendanceSheet.HeaderAddress'),new PHPRtfLite_Font(10, 'Arial','#000000'));
		$cell_one->writeText(Configure::read('Billing.Info').'<br>',new PHPRtfLite_Font(10, 'Arial','#000000'));
		$cell_one->writeText('<br>',new PHPRtfLite_Font(12, 'PaquetteDisplaySSK','#000000'));
		$cell_one->writeText('<b>BILLING STATEMENT</b>',new PHPRtfLite_Font(10, 'Arial','#000000'));

		$cell = $oTable->getCell(1,1);
		$cell->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
		$cell->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);

		$oTable = $oSection->addTable();
		$oTable->addRows(2);

		$oTable->addColumnsList(array(
			constant('CoachBillingEbRtf::column_division_one'),
			constant('CoachBillingEbRtf::column_division_two'),
			constant('CoachBillingEbRtf::column_division_three'),
			constant('CoachBillingEbRtf::column_division_four'),
		));//set to four columns

		$oTable->writeToCell(1, 1, 'To:',new PHPRtfLite_Font(10, 'Arial','#000000'));
		$cell = $oTable->getCell(1,1);
		$cell->setCellPaddings(0.1, 0, 0, 0);

		$recipient  = $this->programInfo['BillingEb']['recipient'];
		if( strlen(trim($this->programInfo['BillingEb']['recipient_position']))>0 ){
			$recipient .= '<br>'.trim($this->programInfo['BillingEb']['recipient_position']);
		}

		if( strlen(trim($this->programInfo['Company']['name']))>0 ){
			$recipient .= '<br>'.trim($this->programInfo['Company']['name']);
		}

		$oTable->writeToCell(1, 2,$recipient,new PHPRtfLite_Font(10, 'Arial','#000000'));

		$cell = $oTable->getCell(1,2);
		$cell->setCellPaddings(0.1, 0, 0, 0);

		$recipient_fax = null;
		if( strlen(trim($this->programInfo['BillingEb']['recipient_fax_number']))>0 ){
			$recipient_fax = trim($this->programInfo['BillingEb']['recipient_fax_number']);
		}

		$oTable->writeToCell(1, 3, '');
		$oTable->writeToCell(1, 4, 'Fax No:'.$recipient_fax,new PHPRtfLite_Font(10, 'Arial','#000000'));

		$cell = $oTable->getCell(1,4);
		$cell->setCellPaddings(0.3, 0, 0, 0);

		$oTable->writeToCell(2, 1, '<br>From:',new PHPRtfLite_Font(10, 'Arial','#000000'));
		$cell = $oTable->getCell(2,1);
		$cell->setCellPaddings(0.1, 0, 0, 0);

		$oTable->writeToCell(2, 2, '<br>'.trim($this->programInfo['BillingEb']['from']),new PHPRtfLite_Font(10, 'Arial','#000000'));
		$cell = $oTable->getCell(2,2);
		$cell->setCellPaddings(0.1, 0, 0, 0);

		$oTable->writeToCell(2, 3, '');
		$oTable->writeToCell(2, 4, '<br>DATE:'.date("d-M-Y",strtotime($this->programInfo['BillingEb']['billing_date'])),new PHPRtfLite_Font(10, 'Arial','#000000'));

		$cell = $oTable->getCell(2,4);
		$cell->setCellPaddings(0.3, 0, 0, 0);


		$oTable = $oSection->addTable();
		$oTable->addRows(1);
		$oTable->addColumnsList(array(constant('CoachBillingEbRtf::columnwidth')));//set to two columns
		$oTable->writeToCell(1, 1, '<b>PARTICULARS</b>',new PHPRtfLite_Font(10, 'Arial','#000000'));

		$cell = $oTable->getCell(1,1);
		$cell->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
		$cell->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);

		$oTable = $oSection->addTable();
		$oTable->addRows(3);
		$oTable->addColumnsList(array(
			constant('CoachBillingEbRtf::column_division_one'),
			constant('CoachBillingEbRtf::column_division_five'),
		));

		$oTable->writeToCell(1, 1, 'Seminar:',new PHPRtfLite_Font(10, 'Arial','#000000'));
		$oTable->writeToCell(1, 2, trim($this->programInfo['BillingEb']['seminar']),new PHPRtfLite_Font(10, 'Arial','#000000'));

		$cell = $oTable->getCell(1,1);
		$cell->setCellPaddings(0.1, 0, 0, 0);
		$cell = $oTable->getCell(1,2);
		$cell->setCellPaddings(0.1, 0, 0, 0);

		$oTable->writeToCell(2, 1, 'Venue:',new PHPRtfLite_Font(10, 'Arial','#000000'));
		$oTable->writeToCell(2, 2, trim($this->programInfo['BillingEb']['venue']),new PHPRtfLite_Font(10, 'Arial','#000000'));

		$cell = $oTable->getCell(2,1);
		$cell->setCellPaddings(0.1, 0, 0, 0);
		$cell = $oTable->getCell(2,2);
		$cell->setCellPaddings(0.1, 0, 0, 0);

		$oTable->writeToCell(3, 1, 'Date:',new PHPRtfLite_Font(10, 'Arial','#000000'));
		$oTable->writeToCell(3, 2, trim($this->programInfo['BillingEb']['schedule']),new PHPRtfLite_Font(10, 'Arial','#000000'));

		$cell = $oTable->getCell(3,1);
		$cell->setCellPaddings(0.1, 0, 0, 0);
		$cell = $oTable->getCell(3,2);
		$cell->setCellPaddings(0.1, 0, 0, 0);

		$oTable = $oSection->addTable();
		$oTable->addRows(1);
		$oTable->addColumnsList(array(constant('CoachBillingEbRtf::columnwidth')));//set the width
		$oTable->writeToCell(1, 1, 'No of Participant/s: '.intval($this->programInfo['BillingEb']['number_of_participant']),new PHPRtfLite_Font(10, 'Arial','#000000'));
		$cell = $oTable->getCell(1,1);
		$cell->setCellPaddings(0.1, 0.2, 0, 0.2);

		$oTable = $oSection->addTable();
		$oTable->addRows($this->getTotalParticipants()+1);

		$oTable->addColumnsList(array(
			constant('CoachBillingEbRtf::column_division_one_a'),
			constant('CoachBillingEbRtf::column_division_one_a'),
			constant('CoachBillingEbRtf::column_division_one_b'),
		));//set to two columns

		$rowId = 0;
		foreach( $this->participantsInfo as $participants_key => $participants_Info ){

			if( strlen(trim($participants_Info['Participant']['middle_name']))>0 ){
				$fullName = ucwords(strtolower($participants_Info['Participant']['last_name'])).', '.ucwords(strtolower($participants_Info['Participant']['first_name'])).', '.strtoupper($participants_Info['Participant']['middle_name']);
			}else{
				$fullName = ucwords(strtolower($participants_Info['Participant']['last_name'])).', '.ucwords(strtolower($participants_Info['Participant']['first_name']));
			}

			$nickName = null;
			if( strlen(trim($participants_Info['Participant']['nick_name']))>0 ){
				$nickName = strtoupper(trim($participants_Info['Participant']['first_name']));
			}else{
				$nickName = strtoupper(trim($participants_Info['Participant']['first_name']));
			}

			$position = null;
			if( strlen(trim($participants_Info['ParticipantsPosition']['name']))>0 ){
				$position = trim($participants_Info['ParticipantsPosition']['name']);
			}
			$rowId = ((int)$participants_key + 1);

			$oTable->writeToCell($rowId, 1, '<b>'.$fullName.'</b>',new PHPRtfLite_Font(10, 'Arial','#000000'));
			$cell = $oTable->getCell($rowId,1);
			$cell->setCellPaddings(0.1, 0, 0, 0);

			$oTable->writeToCell($rowId, 2, '<b>'.$nickName.'</b>',new PHPRtfLite_Font(10, 'Arial','#000000'));
			$cell = $oTable->getCell($rowId,2);
			$cell->setCellPaddings(1.0, 0, 0, 0);

			$oTable->writeToCell($rowId, 3, '<b>'.$position.'</b>',new PHPRtfLite_Font(10, 'Arial','#000000'));
			$cell = $oTable->getCell($rowId,3);
			$cell->setCellPaddings(0.1, 0, 0, 0);
		}

		$oTable->writeToCell($rowId+1, 1, '<br>',new PHPRtfLite_Font(6, 'Arial','#000000'));
		$oTable->writeToCell($rowId+1, 2, '<br>',new PHPRtfLite_Font(6, 'Arial','#000000'));
		$oTable->writeToCell($rowId+1, 3, '<br>',new PHPRtfLite_Font(6, 'Arial','#000000'));

		$oTable = $oSection->addTable();
		$oTable->addRows(1);
		$oTable->addColumnsList(array(constant('CoachBillingEbRtf::columnwidth')));//set to two columns
		$oTable->writeToCell(1, 1, '<b>Seminar Investment:</b>',new PHPRtfLite_Font(10, 'Arial','#000000'));

		$cell = $oTable->getCell(1,1);
		$cell->setCellPaddings(0.1, 0, 0, 0);


  		$oTable = $oSection->addTable();
		$oTable->addRows(2);
		$oTable->addColumnsList(array(
			constant('CoachBillingEbRtf::column_division_one_a'),
			constant('CoachBillingEbRtf::column_division_one_a'),
			constant('CoachBillingEbRtf::column_division_one_b'),
		));//set to two columns
		$oTable->writeToCell(1, 1, '<b>Early Bird Rate:</b><br>',new PHPRtfLite_Font(9, 'Arial','#000000'));
		$oTable->writeToCell(1, 1, 'If paid on/before <b>'.date("Y/m/d",strtotime($this->programInfo['BillingEb']['early_bird_date'])).'</b>',new PHPRtfLite_Font(9, 'Arial','#000000'));

		$cell = $oTable->getCell(1,1);
		$cell->setCellPaddings(0.1, 0.1, 0, 0);

		$oTable->writeToCell(1, 2, '<b>Regular Rate:</b><br>',new PHPRtfLite_Font(9, 'Arial','#000000'));
		$oTable->writeToCell(1, 2, 'If paid after <b>'.date("Y/m/d",strtotime($this->programInfo['BillingEb']['early_bird_date'])).'</b> but before the seminar week',new PHPRtfLite_Font(9, 'Arial','#000000'));

		$cell = $oTable->getCell(1,2);
		$cell->setCellPaddings(0.1, 0.1, 0, 0);

		$oTable->writeToCell(1, 3, '<b>On-Site Rate:</b><br>',new PHPRtfLite_Font(9, 'Arial','#000000'));
		$oTable->writeToCell(1, 3, 'If paid during the seminar week',new PHPRtfLite_Font(9, 'Arial','#000000'));

		$cell = $oTable->getCell(1,3);
		$cell->setCellPaddings(0.1, 0.1, 0, 0);

		$oTable->writeToCell(2, 1, str_replace("|","<br>",trim($this->programInfo['BillingEb']['early_bird_details'])),new PHPRtfLite_Font(9, 'Arial','#000000'));
		$oTable->writeToCell(2, 2, str_replace("|","<br>",trim($this->programInfo['BillingEb']['regular_details'])),new PHPRtfLite_Font(9, 'Arial','#000000'));
		$oTable->writeToCell(2, 3, str_replace("|","<br>",trim($this->programInfo['BillingEb']['on_site_details'])),new PHPRtfLite_Font(9, 'Arial','#000000'));

		$cell = $oTable->getCell(2,1);
		$cell->setCellPaddings(0.1, 0.3, 0, 0);

		$cell = $oTable->getCell(2,2);
		$cell->setCellPaddings(0.1, 0.3, 0, 0);

		$cell = $oTable->getCell(2,3);
		$cell->setCellPaddings(0.1, 0.3, 0, 0);

		//$payment_policy = trim($this->programInfo['BillingEb']['payment_policy']);
		$payment_policy = Configure::read('Coach.Payment.Policy');
		$other_information = trim($this->programInfo['BillingEb']['other_information']);
		$cancellation_policy = trim($this->programInfo['BillingEb']['cancellation_policy']);

		$extra_info = $payment_policy.$other_information.$cancellation_policy;
		$parFormat = new PHPRtfLite_ParFormat('LEFT');
		$parFormat->setSpaceBetweenLines(1);

		$oTable = $oSection->addTable();
		$oTable->addRows(1);
		$oTable->addColumnsList(array(constant('CoachBillingEbRtf::columnwidth')));//set to two columns
		$oTable->writeToCell(1, 1,$extra_info,new PHPRtfLite_Font(10, 'Arial','#000000'),$parFormat);
		
		$cell = $oTable->getCell(1,1);
		$cell->setCellPaddings(0.1, 0.2, 0, 0);
		return $oSection;
	}

    public function save_rtf(){
        $old_mask = umask(0);
        $this->rtf->save($this->file_name);
        umask($old_mask);
        system('chmod 777 '.$this->file_name);
    }
}