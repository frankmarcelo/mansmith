<?php

final class CoachInvoiceRtf{

	const characterlimit = '37';
	const characterset = 'Utf-8';
	const marginleft   = '1.763888889';
	const marginright  = '1.763888889';
	const margintop    = '1.322916667';
	const marginbottom = '1.322916667';
	const paperwidth   = '21.6';

	const initialcount = 1;
	const columncount  = 2;
	const pageDivisor  = 10;
	const columnwidth_one  = '7.8';
	const columnwidth_two  = '10.262222222';

	const column_divions_one_rowa = '7.267222222';
    const column_divions_one_rowb = '10.795';

    const column_divions_one_second_rowa = '11.676944444';
    const column_divions_one_second_rowb = '6.385277778';

    //image dimensions
	const image_width  = '4';
	const image_height = '2';

	private $rtf;
	private $border;
	private $participantsInfo =array();
	private $primaryContacts =array();
	private $programInfo =array();
	private $file_name;
	private $schedule_dates;
	private $invoice_generator;

	public function __construct(PHPRtfLite $rtf,PHPRtfLite_Border $border, $file_name,$schedule_dates=array(),$programInfo=array(),$participantsInfo=array(),$primaryContacts=array(),$invoice_generator=null){
		$this->rtf = &$rtf;
		$this->border = &$border;
		$this->programInfo = &$programInfo;
		$this->participantsInfo = &$participantsInfo;
		$this->schedule_dates = &$schedule_dates;
		$this->primaryContacts = &$primaryContacts;
		$this->file_name = $file_name;
		$this->invoice_generator = $invoice_generator;

		$this->rtf->setMarginLeft(constant('CoachInvoiceRtf::marginleft'));
		$this->rtf->setMarginRight(constant('CoachInvoiceRtf::marginright'));
		$this->rtf->setMarginTop(constant('CoachInvoiceRtf::margintop'));
		$this->rtf->setMarginBottom(constant('CoachInvoiceRtf::marginbottom'));
		$this->rtf->setPaperWidth(constant('CoachInvoiceRtf::paperwidth'));
		$this->rtf->setCharset(constant('CoachInvoiceRtf::characterset'));
    }

	private function addSection(){
		return $this->rtf->addSection();
	}

	private function getTotalPages(){
		return ceil($this->getTotalParticipants()/constant('CoachInvoiceRtf::pageDivisor'));
	}

	private function getTotalParticipants(){
		return count($this->participantsInfo);
	}

	public function generateRtf(){

        $oSection  = $this->addSection();
		$numOfPage = intval($this->getTotalPages());

		$oTable = $oSection->addTable();
		$oTable->addRows(1);
		$oTable->addColumnsList(array(constant('CoachInvoiceRtf::columnwidth_one'),constant('CoachInvoiceRtf::columnwidth_two')));//set to two columns

        /**start of image header **/
        $coach_image = Configure::read('Coach.image');
        $cell_one = $oTable->getCell(1, 1);
        $cell_one->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_TOP);
        $oImage   = $cell_one->addImage($coach_image);
        $oImage->setWidth(constant('CoachInvoiceRtf::image_width'));
        $oImage->setHeight(constant('CoachInvoiceRtf::image_height'));

		/**end of image header **/

		/** Coach Invoice Header**/
		$right_paragraph = new PHPRtfLite_ParFormat('right');
		$right_paragraph->setSpaceBetweenLines(1.8);

		$center_paragraph = new PHPRtfLite_ParFormat('center');

		$cell = $oTable->getCell(1, 2);
		$cell->writeText('<b>Corporate</b> ',new PHPRtfLite_Font(18, 'Century Gothic','#c00000'));
		$cell->writeText('<b>Achievers</b>',new PHPRtfLite_Font(18, 'Century Gothic','#e36c0a'));
		$cell->writeText('<b>Institute</b>',new PHPRtfLite_Font(18, 'Century Gothic','#000000'),$right_paragraph);
		$cell->writeText('.',new PHPRtfLite_Font(3, 'Century Gothic','#000000'));
		$cell->writeText('<b>A Division of Mansmith and Fielders, Inc.</b><br>',new PHPRtfLite_Font(12, 'Century Gothic','#000000'), $center_paragraph);
		$cell->writeText('Waters Center, 14 Ilang‑Ilang St., New Manila, Quezon City<br>',new PHPRtfLite_Font(10, 'Arial','#000000'));
		$cell->writeText('<b>TIN # 000-128-440-000</b>',new PHPRtfLite_Font(12, 'Arial','#000000'));

        $cell->setCellPaddings(0, 0, 0.1, 0);
		$cell->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_RIGHT);

        /** Coach End of Invoice Header**/


        $oTableTwo = $oSection->addTable();
        $oTableTwo->addRows(1);
        $oTableTwo->addColumnsList(array(constant('CoachInvoiceRtf::column_divions_one_rowa'),constant('CoachInvoiceRtf::column_divions_one_rowb')));//set to two columns

		$recipient  = '<b>'.trim($this->programInfo['InvoiceDetails']['recipient']).'</b>';
        if( strlen(trim($this->programInfo['InvoiceDetails']['recipient_position']))>0 ){
			$recipient .= '<br>'.trim($this->programInfo['InvoiceDetails']['recipient_position']);
		}
        $recipient .= '<br>';

        $company = null;
        if( strlen(trim($this->programInfo['InvoiceDetails']['company']))>0 ){
			$company .= '<b>'.trim($this->programInfo['InvoiceDetails']['company']).'</b>';
		}

        if( strlen(trim($this->programInfo['InvoiceDetails']['recipient_address']))>0 ){
            $company .= '<br>'.trim($this->programInfo['InvoiceDetails']['recipient_address']).'<br>';
        }

        $oTableTwo->writeToCell(1,1,'TO:<br>',new PHPRtfLite_Font(9, 'Arial','#000000'));
        $oTableTwo->writeToCell(1,1,$recipient,new PHPRtfLite_Font(12, 'Times New Roman','#000000'));
        $oTableTwo->writeToCell(1,1,$company,new PHPRtfLite_Font(12, 'Times New Roman','#000000'));

        $cell_three = $oTableTwo->getCell(1,1);
        $cell_three->setCellPaddings(0.1, 0.3, 0, 0);
        $cell_three->setBorder($this->border);


        $subTotal = number_format($this->programInfo['InvoiceDetails']['total_amount_due'],2, '.',',');
        $invoice_date = date("F d, Y",strtotime($this->programInfo['InvoiceDetails']['payment_due_date']));
        $billing_date = date("F d, Y",strtotime($this->programInfo['Invoice']['billing_date']));

        $allowed_length = constant('CoachInvoiceRtf::characterlimit');

        $extra_spaces = $allowed_length - strlen($invoice_date);
        $extra_html = null;
        for( $i = 0; $i < $extra_spaces; $i++ ){
            $extra_html .= ' ';
        }

        $extra_billing_spaces = $allowed_length - strlen($billing_date);
        $extra_billing_html = null;
        for( $i = 0; $i < $extra_billing_spaces; $i++ ){
            $extra_billing_html .= ' ';
        }

        $oTableTwo->writeToCell(1,2,'<b>Billing Invoice Date</b>                <b>Billing Invoice No.:</b><br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));
        $oTableTwo->writeToCell(1,2, $billing_date.$extra_html.'<b>'.trim($this->programInfo['InvoiceDetails']['invoice_number']).'</b><br>',new PHPRtfLite_Font(9, 'Century Gothic','#000000'));
        $oTableTwo->writeToCell(1,2,'<br>',new PHPRtfLite_Font(9, 'Century Gothic','#FFFFFF'));
        $oTableTwo->writeToCell(1,2,'<b>Payment Due Date</b>                 <b>TOTAL AMOUNT DUE</b><br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));
        $oTableTwo->writeToCell(1,2, $invoice_date.$extra_billing_html.'<b>Php '.$subTotal.'</b><br>',new PHPRtfLite_Font(9, 'Century Gothic','#000000'));
        $oTableTwo->writeToCell(1,2,'<br>',new PHPRtfLite_Font(9, 'Century Gothic','#FFFFFF'));

        $oTableTwo->writeToCell(1,2,'<b>Please make the check payable to Corporate Achievers Institute</b><br>',new PHPRtfLite_Font(9, 'Century Gothic','#000000'));
		$oTableTwo->writeToCell(1,2,'<b>For Peso Remittance:</b><br>',new PHPRtfLite_Font(9, 'Century Gothic','#000000'));
		$oTableTwo->writeToCell(1,2,'Account Name: <b>Corporate Achievers Institute</b><br>',new PHPRtfLite_Font(9, 'Arial','#000000'));
		$oTableTwo->writeToCell(1,2,'Account No.: <b>1-220-05964-9</b><br>',new PHPRtfLite_Font(10, 'Arial','#000000'));
		$oTableTwo->writeToCell(1,2,'Bank Name: <b>RCBC (New Manila Branch)</b><br>',new PHPRtfLite_Font(10, 'Arial','#000000'));
		$oTableTwo->writeToCell(1,2,'<br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));
		$oTableTwo->writeToCell(1,2,'Kindly fax copy of deposit slip to (63-2) 722-2318',new PHPRtfLite_Font(9, 'Century Gothic','#000000'));

		$cell = $oTableTwo->getCell(1,2);
		$cell->setCellPaddings(0.1, 0.3, 0, 0);
		$cell->setBorder($this->border);

		$oSection->writeText('<br>', new PHPRtfLite_Font(3, 'Arial'));

        $oTableThree = $oSection->addTable();
        $oTableThree->addRows(4);

        $oTableThree->addColumnsList(array(
            constant('CoachInvoiceRtf::column_divions_one_second_rowa'),
            constant('CoachInvoiceRtf::column_divions_one_second_rowb'))
        );//set to two columns

        $oTableThree->writeToCell(1,1,'.',new PHPRtfLite_Font(3, 'Arial','#000000'),new PHPRtfLite_ParFormat('center'));
        $oTableThree->writeToCell(1,1,'<b>PARTICULARS</b>',new PHPRtfLite_Font(12, 'Arial','#000000'),new PHPRtfLite_ParFormat('center'));
        $oTableThree->writeToCell(1,1,'.',new PHPRtfLite_Font(6, 'Arial','#000000'),new PHPRtfLite_ParFormat('center'));
        $oTableThree->writeToCell(1,2,'.',new PHPRtfLite_Font(3, 'Arial','#000000'),new PHPRtfLite_ParFormat('center'));
        $oTableThree->writeToCell(1,2,'<b>AMOUNT</b>',new PHPRtfLite_Font(12, 'Arial','#000000'),new PHPRtfLite_ParFormat('center'));
        $oTableThree->writeToCell(1,2,'.',new PHPRtfLite_Font(6, 'Arial','#000000'),new PHPRtfLite_ParFormat('center'));

        $cell = $oTableThree->getCell(1,1);
        $cell->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
        $cell->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);

        $cell = $oTableThree->getCell(1,2);
        $cell->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
        $cell->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);

        $oTableThree->setBorderForCellRange($this->border, 1, 1, 2, 2);

        $participantsName = array();
        if( count($this->participantsInfo)>0 ){
            foreach( $this->participantsInfo as $participantInfo ){
                $last_names = strtolower(trim($participantInfo['Participant']['last_name']));
                $participantsName[$last_names] = ucfirst($participantInfo['Participant']['full_name']);
            }
            ksort($participantsName);
        }
        $program = trim($this->programInfo['Invoice']['seminar']);

        $oTableThree->writeToCell(2,1,'<br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name
        $oTableThree->writeToCell(2,1,'Public Seminar/Workshop for the following:<br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));
        $oTableThree->writeToCell(2,1,'<b>Course Investment:</b><br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));
        $oTableThree->writeToCell(2,1,'<b>'.$program.'</b><br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name
        $oTableThree->writeToCell(2,1,'<b>'.trim($this->programInfo['Invoice']['schedule']).'</b><br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name
        if( strlen(trim($this->programInfo['Invoice']['venue']))>0 ){
            $oTableThree->writeToCell(2,1,'<b>at '.trim($this->programInfo['Invoice']['venue']).'</b><br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name
        }
        $oTableThree->writeToCell(2,1,'<br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name
        $oTableThree->writeToCell(2,1,'<b>'.trim(implode(", ",$participantsName)).'</b><br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name
        $oTableThree->writeToCell(2,1,'<br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name

		if(!empty($this->programInfo['InvoiceDetails']['invoice_notes']) && !is_null($this->programInfo['InvoiceDetails']['invoice_notes'])){
			$invoice_notes = explode("|",$this->programInfo['InvoiceDetails']['invoice_notes']);
			foreach( $invoice_notes as $notes){
				$oTableThree->writeToCell(2,1,$notes.'<br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name
			}
		}

        $oTableThree->writeToCell(2,1,'<b>TOTAL Php                  '.$subTotal.'</b>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name
		$oTableThree->writeToCell(2,2,'<br>',new PHPRtfLite_Font(12, 'Arial','#000000'));//event name
        $oTableThree->writeToCell(2,2,'<br>',new PHPRtfLite_Font(12, 'Arial','#000000'));//event name
        $oTableThree->writeToCell(2,2,'<b>PHP '.$subTotal.'</b><br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name
        $oTableThree->writeToCell(2,2,'<br>',new PHPRtfLite_Font(12, 'Arial','#000000'));//event name
        $oTableThree->writeToCell(2,2,'<b>Amount in words:</b><br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name
        $oTableThree->writeToCell(2,2, trim($this->programInfo['InvoiceDetails']['amount_in_words']).'<br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name

        $cell = $oTableThree->getCell(2,1);
        $cell->setCellPaddings(0.1, 0, 0, 0);


        $cell = $oTableThree->getCell(2,2);
        $cell->setCellPaddings(0.1, 0, 0, 0);


		$oTableThree->writeToCell(3,1,'<br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,1,'<b>Note:</b><br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,1,'1) Please disregard this billing if payment has been made.<br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,1,'2) Once we receive registration, cancellations are accepted with full refund or no<br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,1,'   administrative charges provided adviced to us EIGHT (8) working days<br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,1,'   before the seminar. Cancellations made less than 8 days before or no-show<br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,1,'   during the seminar mean forfeiture or payment ( or will still be charged to<br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,1,'   client in case of special credit arrangement). Substitution is accepted at any<br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,1,'   point before the seminar. Only one person is expected to attend each program.<br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,1,'3) Should your company wish to withhold tax, please withhold only 2% as we<br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,1,'   are classified under suppliers or contractors of services. Please also attach<br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,1,'   creditable tax certificate or form 2307.',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name

		$cell = $oTableThree->getCell(3,1);
		$cell->setCellPaddings(0.1, 0, 0, 0);

		$oTableThree->writeToCell(3,2,'<b></b><br>',new PHPRtfLite_Font(10, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,2,'<b></b><br>',new PHPRtfLite_Font(10, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,2,'<b>PREPARED BY:</b>',new PHPRtfLite_Font(10, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,2,'<br>',new PHPRtfLite_Font(12, 'Arial','#000000'));//event name
		$oTableThree->writeToCell(3,2,'<br>',new PHPRtfLite_Font(12, 'Arial','#000000'));//event name
		$oTableThree->writeToCell(3,2,'<b>____________________________</b><br>',new PHPRtfLite_Font(10, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,2,'<b>'.$this->invoice_generator.'</b><br>',new PHPRtfLite_Font(10, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,2,'<b>Program Officer</b>',new PHPRtfLite_Font(10, 'Century Gothic','#000000'));//event name

		$cell = $oTableThree->getCell(3,2);
		$cell->setCellPaddings(0.7, 0, 0, 0);

		$oTableThree->writeToCell(4,1,'<b></b><br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
        $oTableThree->writeToCell(4,1,'<b></b><br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
        $oTableThree->writeToCell(4,1,'<b>Mansmith and Fielders, Inc. 14 Ilang-ilang St., GF Waters Central, New Manila, Quezon City</b><br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
        $oTableThree->writeToCell(4,1,'<b>Telefax Nos. (63-2) 584-5858 / 7227318 / 4120034</b>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name

		$cell = $oTableThree->getCell(4,1);
        $cell->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
        $oTableThree->mergeCellRange(4, 1, 4, 2);
        return $oSection;
	}

    public function save_rtf(){
        $old_mask = umask(0);
        $this->rtf->save($this->file_name);
        umask($old_mask);
        system('chmod 777 '.$this->file_name);
    }
}