<?php

final class InvoiceRtf{

    const characterlimit = '37';
    const characterset = 'Utf-8';
    const marginleft   = '1.763888889';
    const marginright  = '1.763888889';
    const margintop    = '1.322916667';
    const marginbottom = '1.322916667';
    const paperwidth   = '21.6';

    const initialcount = 1;
    const columncount  = 2;
    const pageDivisor  = 10;
    const columnwidth  = '9.031111111';

    const column_divions_one_rowa = '7.267222222';
    const column_divions_one_rowb = '10.795';

    const column_divions_one_second_rowa = '11.676944444';
    const column_divions_one_second_rowb = '6.385277778';

    //image dimensions
    const image_width  = '4.409722222';
    const image_height = '2.5';

    private $rtf;
    private $border;
    private $participantsInfo =array();
    private $primaryContacts =array();
    private $programInfo =array();
    private $file_name;
    private $schedule_dates;
    private $invoice_generator;

    public function __construct(PHPRtfLite $rtf,PHPRtfLite_Border $border, $file_name,$schedule_dates=array(),$programInfo=array(),$participantsInfo=array(),$primaryContacts=array(),$invoice_generator=null){
        $this->rtf = &$rtf;
        $this->border = &$border;
        $this->programInfo = &$programInfo;
        $this->participantsInfo = &$participantsInfo;
        $this->schedule_dates = &$schedule_dates;
        $this->primaryContacts = &$primaryContacts;
        $this->file_name = $file_name;
        $this->invoice_generator = $invoice_generator;

        $this->rtf->setMarginLeft(constant('InvoiceRtf::marginleft'));
        $this->rtf->setMarginRight(constant('InvoiceRtf::marginright'));
        $this->rtf->setMarginTop(constant('InvoiceRtf::margintop'));
        $this->rtf->setMarginBottom(constant('InvoiceRtf::marginbottom'));
        $this->rtf->setPaperWidth(constant('InvoiceRtf::paperwidth'));
        $this->rtf->setCharset(constant('InvoiceRtf::characterset'));
    }

    private function addSection(){
        return $this->rtf->addSection();
    }

    private function getTotalPages(){
        return ceil($this->getTotalParticipants()/constant('InvoiceRtf::pageDivisor'));
    }

    private function getTotalParticipants(){
        return count($this->participantsInfo);
    }

    public function generateRtf(){

        $oSection  = $this->addSection();
        $numOfPage = intval($this->getTotalPages());

        $oTable = $oSection->addTable();
        $oTable->addRows(1);
        $oTable->addColumnsList(array(constant('InvoiceRtf::columnwidth'),constant('InvoiceRtf::columnwidth')));//set to two columns

        /**start of image header **/
        $mansmith_image = Configure::read('Mansmith.image');
        $cell_one = $oTable->getCell(1, 1);
        $cell_one->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_TOP);
        $oImage   = $cell_one->addImage($mansmith_image);
        $oImage->setWidth(constant('InvoiceRtf::image_width'));
        $oImage->setHeight(constant('InvoiceRtf::image_height'));
        /**end of image header **/

        /** Invoice Header**/
        $oTable->writeToCell(1, 2, '<b>MANSMITH AND FIELDERS, INC</b><br>',new PHPRtfLite_Font(14, 'Verdana','#000000'));
        $oTable->writeToCell(1, 2, '<i>Helping your marketing and sales team soar!</i><br>',new PHPRtfLite_Font(12, 'Times New Roman','#000000'));
        $oTable->writeToCell(1, 2, '<i>info@mansmith.net www.mansmith.net</i><br>',new PHPRtfLite_Font(12, 'Times New Roman','#000000'));
        $oTable->writeToCell(1, 2, '<b>TIN: 000-128-440-000 VAT</b><br>',new PHPRtfLite_Font(10, 'Century Gothic','#000000'));

        $cell_two = $oTable->getCell(1,2);
        $cell_two->setCellPaddings(0, 0.1, 0, 0);
        $cell_two->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_RIGHT);
        $cell_two->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_TOP);
        /** End of Invoice Header**/

        $oTableTwo = $oSection->addTable();
        $oTableTwo->addRows(1);
        $oTableTwo->addColumnsList(array(constant('InvoiceRtf::column_divions_one_rowa'),constant('InvoiceRtf::column_divions_one_rowb')));//set to two columns

        $recipient  = '<b>'.trim($this->programInfo['InvoiceDetails']['recipient']).'</b>';
        if( strlen(trim($this->programInfo['InvoiceDetails']['recipient_position']))>0 ){
            $recipient .= '<br>'.trim($this->programInfo['InvoiceDetails']['recipient_position']);
        }
        $recipient .= '<br>';

        $company = null;
        if( strlen(trim($this->programInfo['InvoiceDetails']['company']))>0 ){
            $company .= '<b>'.trim($this->programInfo['InvoiceDetails']['company']).'</b>';
        }

        if( strlen(trim($this->programInfo['InvoiceDetails']['recipient_address']))>0 ){
            $company .= '<br>'.trim($this->programInfo['InvoiceDetails']['recipient_address']).'<br>';
        }

        $oTableTwo->writeToCell(1,1,'TO:<br>',new PHPRtfLite_Font(9, 'Arial','#000000'));
        $oTableTwo->writeToCell(1,1,$recipient,new PHPRtfLite_Font(12, 'Times New Roman','#000000'));
        $oTableTwo->writeToCell(1,1,$company,new PHPRtfLite_Font(12, 'Times New Roman','#000000'));

        $cell_three = $oTableTwo->getCell(1,1);
        $cell_three->setCellPaddings(0.1, 0.3, 0, 0);
        $cell_three->setBorder($this->border);

        $subTotal = number_format($this->programInfo['InvoiceDetails']['total_amount_due'],2, '.',',');
        $invoice_date = date("F d, Y",strtotime($this->programInfo['InvoiceDetails']['payment_due_date']));
        $billing_date = date("F d, Y",strtotime($this->programInfo['Invoice']['billing_date']));

        $allowed_length = constant('InvoiceRtf::characterlimit');

        $extra_spaces = $allowed_length - strlen($invoice_date);
        $extra_html = null;
        for( $i = 0; $i < $extra_spaces; $i++ ){
            $extra_html .= ' ';
        }

        $extra_billing_spaces = $allowed_length - strlen($billing_date);
        $extra_billing_html = null;
        for( $i = 0; $i < $extra_billing_spaces; $i++ ){
            $extra_billing_html .= ' ';
        }

        $oTableTwo->writeToCell(1,2,'<b>Billing Invoice Date</b>                <b>Billing Invoice No.:</b><br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));
        $oTableTwo->writeToCell(1,2, $billing_date.$extra_html.'<b>'.trim($this->programInfo['InvoiceDetails']['invoice_number']).'</b><br>',new PHPRtfLite_Font(9, 'Century Gothic','#000000'));
        $oTableTwo->writeToCell(1,2,'<br>',new PHPRtfLite_Font(9, 'Century Gothic','#FFFFFF'));
        $oTableTwo->writeToCell(1,2,'<b>Payment Due Date</b>                 <b>TOTAL AMOUNT DUE</b><br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));
        $oTableTwo->writeToCell(1,2, $invoice_date.$extra_billing_html.'<b>Php '.$subTotal.'</b><br>',new PHPRtfLite_Font(9, 'Century Gothic','#000000'));
        $oTableTwo->writeToCell(1,2,'<br>',new PHPRtfLite_Font(9, 'Century Gothic','#FFFFFF'));

        $oTableTwo->writeToCell(1,2,'<b>Please make the check payable to Mansmith and Fielders, Inc.</b><br>',new PHPRtfLite_Font(9, 'Century Gothic','#000000'));
        $oTableTwo->writeToCell(1,2,'<b>For Peso Remittance:</b><br>',new PHPRtfLite_Font(9, 'Century Gothic','#000000'));
        $oTableTwo->writeToCell(1,2,'Account Name: Mansmith and Fielders, Inc.<br>',new PHPRtfLite_Font(9, 'Century Gothic','#000000'));
        $oTableTwo->writeToCell(1,2,'Account No.: 0162-028106-001<br>',new PHPRtfLite_Font(9, 'Century Gothic','#000000'));
        $oTableTwo->writeToCell(1,2,'Bank Name: Security Bank Timog Branch<br>',new PHPRtfLite_Font(9, 'Century Gothic','#000000'));
        $oTableTwo->writeToCell(1,2,'Swift Code: SETCPHMMXXX<br>',new PHPRtfLite_Font(9, 'Century Gothic','#000000'));
        $oTableTwo->writeToCell(1,2,'OR<br>',new PHPRtfLite_Font(9, 'Century Gothic','#000000'));
        $oTableTwo->writeToCell(1,2,'Account Name: Mansmith and Fielders, Inc.<br>',new PHPRtfLite_Font(9, 'Century Gothic','#000000'));
        $oTableTwo->writeToCell(1,2,'Account No.: 1-220-01044-5<br>',new PHPRtfLite_Font(9, 'Century Gothic','#000000'));
        $oTableTwo->writeToCell(1,2,'Bank Name: RCBC New Manila Branch<br>',new PHPRtfLite_Font(9, 'Century Gothic','#000000'));
        $oTableTwo->writeToCell(1,2,'Swift Code:<br>',new PHPRtfLite_Font(9, 'Century Gothic','#000000'));
        $oTableTwo->writeToCell(1,2,'<br>',new PHPRtfLite_Font(9, 'Century Gothic','#000000'));
        $oTableTwo->writeToCell(1,2,'Kindly fax copy of deposit slip to (63-2) 722-2318',new PHPRtfLite_Font(9, 'Century Gothic','#000000'));

        $cell_three = $oTableTwo->getCell(1,2);
        $cell_three->setCellPaddings(0.1, 0.3, 0, 0);
        $cell_three->setBorder($this->border);

        $oSection->writeText('<br>', new PHPRtfLite_Font(3, 'Arial'));

        $oTableThree = $oSection->addTable();
        $oTableThree->addRows(4);

        $oTableThree->addColumnsList(array(
            constant('InvoiceRtf::column_divions_one_second_rowa'),
            constant('InvoiceRtf::column_divions_one_second_rowb'))
        );//set to two columns

        $oTableThree->writeToCell(1,1,'.',new PHPRtfLite_Font(3, 'Arial','#000000'),new PHPRtfLite_ParFormat('center'));
        $oTableThree->writeToCell(1,1,'<b>PARTICULARS</b>',new PHPRtfLite_Font(12, 'Arial','#000000'),new PHPRtfLite_ParFormat('center'));
        $oTableThree->writeToCell(1,1,'.',new PHPRtfLite_Font(6, 'Arial','#000000'),new PHPRtfLite_ParFormat('center'));
        $oTableThree->writeToCell(1,2,'.',new PHPRtfLite_Font(3, 'Arial','#000000'),new PHPRtfLite_ParFormat('center'));
        $oTableThree->writeToCell(1,2,'<b>AMOUNT</b>',new PHPRtfLite_Font(12, 'Arial','#000000'),new PHPRtfLite_ParFormat('center'));
        $oTableThree->writeToCell(1,2,'.',new PHPRtfLite_Font(6, 'Arial','#000000'),new PHPRtfLite_ParFormat('center'));

        $cell = $oTableThree->getCell(1,1);
        $cell->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
        $cell->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);

        $cell = $oTableThree->getCell(1,2);
        $cell->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
        $cell->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);

        $oTableThree->setBorderForCellRange($this->border, 1, 1, 2, 2);

        $participantsName = array();
        if( count($this->participantsInfo)>0 ){
            foreach( $this->participantsInfo as $participantInfo ){
                $last_names = strtolower(trim($participantInfo['Participant']['last_name']));
                $participantsName[$last_names] = ucfirst($participantInfo['Participant']['full_name']);
            }
            ksort($participantsName);
        }
        $program = trim($this->programInfo['Invoice']['seminar']);

        $oTableThree->writeToCell(2,1,'<br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name
        $oTableThree->writeToCell(2,1,'Public Seminar/Workshop for the following:<br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));
        $oTableThree->writeToCell(2,1,'<b>Course Investment:</b><br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));
        $oTableThree->writeToCell(2,1,'<b>'.$program.'</b><br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name
        $oTableThree->writeToCell(2,1,'<b>'.trim($this->programInfo['Invoice']['schedule']).'</b><br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name
        if( strlen(trim($this->programInfo['Invoice']['venue']))>0 ){
            $oTableThree->writeToCell(2,1,'<b>at '.trim($this->programInfo['Invoice']['venue']).'</b><br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name
        }
        $oTableThree->writeToCell(2,1,'<br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name
        $oTableThree->writeToCell(2,1,'<b>'.trim(implode(", ",$participantsName)).'</b><br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name
        $oTableThree->writeToCell(2,1,'<br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name

        if(!empty($this->programInfo['InvoiceDetails']['invoice_notes']) && !is_null($this->programInfo['InvoiceDetails']['invoice_notes'])){
            $invoice_notes = explode("|",$this->programInfo['InvoiceDetails']['invoice_notes']);
            foreach( $invoice_notes as $notes){
                $oTableThree->writeToCell(2,1,$notes.'<br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name
            }
        }

        $oTableThree->writeToCell(2,1,'<b>TOTAL Php                  '.$subTotal.'</b>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name

        $oTableThree->writeToCell(2,2,'<br>',new PHPRtfLite_Font(12, 'Arial','#000000'));//event name
        $oTableThree->writeToCell(2,2,'<br>',new PHPRtfLite_Font(12, 'Arial','#000000'));//event name
        $oTableThree->writeToCell(2,2,'<b>PHP '.$subTotal.'</b><br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name
        $oTableThree->writeToCell(2,2,'<br>',new PHPRtfLite_Font(12, 'Arial','#000000'));//event name
        $oTableThree->writeToCell(2,2,'<b>Amount in words:</b><br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name
        $oTableThree->writeToCell(2,2, trim($this->programInfo['InvoiceDetails']['amount_in_words']).'<br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));//event name

        $cell = $oTableThree->getCell(2,1);
        $cell->setCellPaddings(0.1, 0, 0, 0);

        $cell = $oTableThree->getCell(2,2);
        $cell->setCellPaddings(0.1, 0, 0, 0);

        $oTableThree->writeToCell(3,1,'<br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,1,'<b>Note:</b><br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,1,'1) Please disregard this billing if payment has been made.<br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,1,'2) Once we receive registration, cancellations are accepted with full refund or no<br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,1,'   administrative charges provided adviced to us EIGHT (8) working days<br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,1,'   before the seminar. Cancellations made less than 8 days before or no-show<br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,1,'   during the seminar mean forfeiture or payment ( or will still be charged to<br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,1,'   client in case of special credit arrangement). Substitution is accepted at any<br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,1,'   point before the seminar. Only one person is expected to attend each program.<br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,1,'3) Should your company wish to withhold tax, please withhold only 2% as we<br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,1,'   are classified under suppliers or contractors of services. Please also attach<br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,1,'   creditable tax certificate or form 2307.',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name

		$cell = $oTableThree->getCell(3,1);
		$cell->setCellPaddings(0.1, 0, 0, 0);

		$oTableThree->writeToCell(3,2,'<b></b><br>',new PHPRtfLite_Font(10, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,2,'<b></b><br>',new PHPRtfLite_Font(10, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,2,'<b>PREPARED BY:</b>',new PHPRtfLite_Font(10, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,2,'<br>',new PHPRtfLite_Font(12, 'Arial','#000000'));//event name
		$oTableThree->writeToCell(3,2,'<br>',new PHPRtfLite_Font(12, 'Arial','#000000'));//event name
		$oTableThree->writeToCell(3,2,'<b>____________________________</b><br>',new PHPRtfLite_Font(10, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,2,'<b>'.$this->invoice_generator.'</b><br>',new PHPRtfLite_Font(10, 'Century Gothic','#000000'));//event name
		$oTableThree->writeToCell(3,2,'<b>Program Officer</b>',new PHPRtfLite_Font(10, 'Century Gothic','#000000'));//event name

		$cell = $oTableThree->getCell(3,2);
		$cell->setCellPaddings(0.7, 0, 0, 0);

		$oTableThree->writeToCell(4,1,'<b></b><br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
        $oTableThree->writeToCell(4,1,'<b></b><br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
        $oTableThree->writeToCell(4,1,'<b>Mansmith and Fielders, Inc. 14 Ilang-ilang St., GF Waters Central, New Manila, Quezon City</b><br>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name
        $oTableThree->writeToCell(4,1,'<b>Telefax Nos. (63-2) 584-5858 / 7227318 / 4120034</b>',new PHPRtfLite_Font(8, 'Century Gothic','#000000'));//event name

		$cell = $oTableThree->getCell(4,1);
        $cell->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
        $oTableThree->mergeCellRange(4, 1, 4, 2);

        return $oSection;
    }

    public function save_rtf(){
        $old_mask = umask(0);
        $this->rtf->save($this->file_name);
        umask($old_mask);
        system('chmod 777 '.$this->file_name);
    }
}