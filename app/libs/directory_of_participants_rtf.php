<?php

final class DirectoryOfParticipantsRtf{

	const characterlimit = '35';
	
	const characterset = 'Utf-8';
	const marginleft   = '1.763888889';
	const marginright  = '1.763888889';
	const margintop    = '1.146527778';
	const marginbottom = '1.146527778';
	const paperwidth   = '21.4';
	
	const initialcount = 1;
	const columncount  = 2;
	const pageDivisor  = 10;
	const columnwidth  = '17.9';
	
	const column_division = '8.95';
	
	//image dimensions
	const image_width  = '3.427777778';
	const image_height = '1.663888889';
	
	private $rtf;
	private $border;
	private $participantsInfo =array();
	private $programInfo =array();
	private $file_name;
	private $schedule_dates;
	
	public function __construct(PHPRtfLite $rtf,PHPRtfLite_Border $border, $file_name,$schedule_dates,$programInfo =array(), $participantsInfo=array()){
		$this->rtf = &$rtf;
		$this->border = &$border;
		$this->programInfo = &$programInfo;
		$this->participantsInfo = &$participantsInfo;
		$this->schedule_dates = &$schedule_dates;
		$this->file_name = $file_name;
		
		$this->rtf->setMarginLeft(constant('DirectoryOfParticipantsRtf::marginleft'));
		$this->rtf->setMarginRight(constant('DirectoryOfParticipantsRtf::marginright'));
		$this->rtf->setMarginTop(constant('DirectoryOfParticipantsRtf::margintop'));
		$this->rtf->setMarginBottom(constant('DirectoryOfParticipantsRtf::marginbottom'));
		$this->rtf->setPaperWidth(constant('DirectoryOfParticipantsRtf::paperwidth'));
		$this->rtf->setCharset(constant('DirectoryOfParticipantsRtf::characterset'));	
	}
	
	private function addSection(){
		return $this->rtf->addSection();
	}
	
	private function getTotalPages(){
		return ceil($this->getTotalParticipants()/constant('DirectoryOfParticipantsRtf::pageDivisor'));
	}
	
	private function getTotalParticipants(){
		return count($this->participantsInfo);
	} 
	
	public function generateRtf(){
		$oSection  = $this->addSection();
		$numOfPage = intval($this->getTotalPages());
		
		for( $i =0 ; $i < $numOfPage; $i++){
			$oSection->writeText('<br>');
			
			//first table
			$oTable = $oSection->addTable();
			$oTable->addRows(1);
			$oTable->addColumnsList(array(constant('DirectoryOfParticipantsRtf::columnwidth')));//set column width
	
			/**start of image header **/	
			$mansmith_image = Configure::read('Mansmith.image');
			$cell_one = $oTable->getCell(1, 1);
			$cell_one->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
			$cell_one->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);
			$oImage   = $cell_one->addImage($mansmith_image);
			$oImage->setWidth(constant('DirectoryOfParticipantsRtf::image_width'));
			$oImage->setHeight(constant('DirectoryOfParticipantsRtf::image_height'));
			
			$cell_one->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
			$cell_one->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);
			/**end of image header **/
			
			//table one
			$oTable_one = $oSection->addTable();
			$oTable_one->addRows(1);
			$oTable_one->addColumnsList(array(constant('DirectoryOfParticipantsRtf::columnwidth')));//set to two columns
	
			$oTable_one->writeToCell(1, 1,Configure::read('AttendanceSheet.Header'),new PHPRtfLite_Font(11, 'Verdana','#000000'));
			$oTable_one->writeToCell(1, 1,Configure::read('AttendanceSheet.SubHeader'),new PHPRtfLite_Font(11, 'Times New Roman','#000000'));
	
			$cell_one = $oTable_one->getCell(1,1);
			$cell_one->setCellPaddings(0, 0.2, 0, 0);
			$cell_one->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
			$cell_one->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);
			
			//table two
			$oTable_two = $oSection->addTable();
			$oTable_two->addRows(1);
			$oTable_two->addColumnsList(array(constant('DirectoryOfParticipantsRtf::columnwidth')));//set to two columns
			
			$oTable_two->writeToCell(1,1,Configure::read('AttendanceSheet.HeaderAddress'),new PHPRtfLite_Font(9, 'Arial','#000000'));
			$oTable_two->writeToCell(1,1,Configure::read('AttendanceSheet.SubHeaderAddress'),new PHPRtfLite_Font(9, 'Arial','#000000'));
			
			$cell_two = $oTable_two->getCell(1,1);
			$cell_two->setCellPaddings(0, 0.2, 0, 0.2);
			$cell_two->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
			$cell_two->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);
			
			//table three
			$oTable_three = $oSection->addTable();
			$oTable_three->addRows(1);
			$oTable_three->addColumnsList(array(constant('DirectoryOfParticipantsRtf::columnwidth')));//set to two columns
			
			
    		$program  = trim($this->programInfo['Program']['title']);
    		$venue = trim($this->programInfo['Program']['venue_name']);
    		if( strlen(trim($this->programInfo['Program']['address'])) >0 ){
    			$venue .= ', '.trim($this->programInfo['Program']['address']);	
    		}
			if( strlen(trim($this->programInfo['Program']['suburb'])) >0 ){
    			$venue .= ', '.trim($this->programInfo['Program']['suburb']);	
    		}
			if( strlen(trim($this->programInfo['Program']['city'])) >0 ){
    			$venue .= ', '.trim($this->programInfo['Program']['city']);	
    		}
    		$schedule  = date("F d, Y",strtotime($this->schedule_dates[0]['ProgramsSchedule']['start_date']));
    		if( count($this->schedule_dates)>0 ){
    			$schedule .= ' to '.date("F d, Y",strtotime($this->schedule_dates[count($this->schedule_dates)-1]['ProgramsSchedule']['start_date']));
    		}  
    		
    		$oTable_three->writeToCell(1,1, 'Directory Of Participants<br />',new PHPRtfLite_Font(11, 'Arial','#000000'));
			$oTable_three->writeToCell(1,1, '<b>'.$program.'</b><br />',new PHPRtfLite_Font(12, 'Arial','#000000'));
			$oTable_three->writeToCell(1,1, $schedule.' ‑ '.$venue,new PHPRtfLite_Font(11, 'Arial','#000000'));
			
			$cell_three = $oTable_three->getCell(1,1);
			$cell_three->setCellPaddings(0, 0, 0, 0.1);
			$cell_three->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
			$cell_three->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);
			
			$oTable_four = $oSection->addTable();
	
			if( $i == 0 && $numOfPage > 1){// num of page is greater than 1 so automatically load 5 rows
				$oTable_four->addRows(5);
				$oTable_four->addColumnsList(array(constant('DirectoryOfParticipantsRtf::column_division'),constant('DirectoryOfParticipantsRtf::column_division')));//set to two columns
				$oTable_four->setBorderForCellRange($this->border, 1, 1, 5, 2);
				
			}elseif( $i > 0 && $numOfPage > 1  ){
				
				$max_display = ( $i * constant('DirectoryOfParticipantsRtf::pageDivisor') );
				$rows = $this->getTotalParticipants() - $max_display;
				$rows = ceil( $rows/2 );
				$oTable_four->addRows($rows);
				$oTable_four->addColumnsList(array(constant('DirectoryOfParticipantsRtf::column_division'),constant('DirectoryOfParticipantsRtf::column_division')));//set to two columns
				$oTable_four->setBorderForCellRange($this->border, 1, 1, $rows, 2);
				
			}else{
				
				$rows = ceil($this->getTotalParticipants()/2);
				$oTable_four->addRows($rows);
				$oTable_four->addColumnsList(array(constant('DirectoryOfParticipantsRtf::column_division'),constant('DirectoryOfParticipantsRtf::column_division')));//set to two columns
				$oTable_four->setBorderForCellRange($this->border, 1, 1, $rows, 2);
			}
    		
			$participantsFont = new PHPRtfLite_Font(10, 'Arial','#000000');
    
			$j = constant('DirectoryOfParticipantsRtf::initialcount');
			$minimum_key     = $i * constant('DirectoryOfParticipantsRtf::pageDivisor');
			$max_display_key = $minimum_key + constant('DirectoryOfParticipantsRtf::pageDivisor');
			
			foreach( $this->participantsInfo as $participants_key => $participants_Info ){
				
				if( $participants_key >= $minimum_key && $participants_key < $max_display_key ){
					
					$columnId = 1;
					if( ( $participants_key % 2 ) == 1 ){
						$columnId = 2;
					}else{
						$columnId = 1;
					}
					
					$company_name = $participants_Info['Company']['name'];
				
					$full_name  = ucwords(strtolower($participants_Info['Participant']['last_name']));
					$full_name .= (strlen(trim($participants_Info['Participant']['first_name'])) > 0 )? ', '.ucwords(strtolower(trim($participants_Info['Participant']['first_name']))).' ':'';
   					$full_name .= (strlen(trim($participants_Info['Participant']['middle_name'])) > 0 )? ucwords(strtolower($participants_Info['Participant']['middle_name'])).'.':'';
    				$nick_name  = (strlen(trim($participants_Info['Participant']['nick_name']))>0 ) ? strtoupper($participants_Info['Participant']['nick_name']) : strtoupper($participants_Info['Participant']['first_name']);	
					
    				$oTable_four->writeToCell($j,$columnId, '<b>'.$full_name.'</b><br>',$participantsFont);
    				$oTable_four->writeToCell($j,$columnId, '"'.$nick_name.'"',$participantsFont);
					if( strlen(trim($participants_Info['ParticipantsPosition']['name']))>0 ){
						$oTable_four->writeToCell($j,$columnId, '<br>'.trim($participants_Info['ParticipantsPosition']['name']),$participantsFont);
					}
					
					if( strlen(trim($participants_Info['Participant']['address']))>0 ){
						$oTable_four->writeToCell($j,$columnId, '<br>'.trim($participants_Info['Participant']['address']).' '.trim($participants_Info['Participant']['zip_code']),$participantsFont);
					}
					
					if( strlen(trim($participants_Info['Participant']['phone']))>0 ){
						$oTable_four->writeToCell($j,$columnId, '<br>'.trim($participants_Info['Participant']['phone']),$participantsFont);
					}
					
					if( strlen(trim($participants_Info['Participant']['mobile']))>0 ){
						//$msisdn = &new Msisdn(trim($participants_Info['Participant']['mobile']));
						//$oTable_four->writeToCell($j,$columnId, '<br />'.$msisdn->GetMsisdnAsIntl(),$participantsFont);
						$msisdn = (trim($participants_Info['Participant']['mobile']));
						$oTable_four->writeToCell($j,$columnId, '<br>'.$msisdn,$participantsFont);
					}
					
					if( strlen(trim($participants_Info['Participant']['email']))>0 ){
						$oTable_four->writeToCell($j,$columnId, '<br>'.trim($participants_Info['Participant']['email']),$participantsFont);
					}
					
					$cell_four = $oTable_four->getCell($j,$columnId);
					$cell_four->setCellPaddings(0.1, 0.1, 0, 0);

					if( $participants_key % 2 == 1 ){
						$j++;		
					}
				}
			}
			$oSection->insertPageBreak();
		}//end of for loop
		return $oSection;
	}
	
	public function save_rtf(){
        $old_mask = umask(0);
        $this->rtf->save($this->file_name);
        umask($old_mask);
        system('chmod 777 '.$this->file_name);
	}
}