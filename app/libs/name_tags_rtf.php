<?php

final class NameTagsRtf{
	
	const characterset = 'Utf-8';
	const marginleftright = '1.5875';
	const margintopbottom = '1.234722222';
	const paperwidth   = '21.4';
	const initialcount = 1;
	const columncount  = 2;
	const pageDivisor  = 10;
	const columnwidth  = '9.189861111';
	
	private $rtf;
	private $border;
	private $participantsInfo =array();
	private $programInfo =array();
	private $file_name;
	
	public function __construct(PHPRtfLite $rtf,PHPRtfLite_Border $border, $file_name, $programInfo =array(), $participantsInfo=array()){
		
		$this->rtf = &$rtf;
		$this->border = &$border;
		$this->programInfo = &$programInfo;
		$this->participantsInfo = &$participantsInfo;
		$this->file_name = $file_name;
		$this->rtf->setMarginLeft(constant('NameTagsRtf::marginleftright'));
		$this->rtf->setMarginRight(constant('NameTagsRtf::marginleftright'));
		$this->rtf->setMarginTop(constant('NameTagsRtf::margintopbottom'));
		$this->rtf->setMarginBottom(constant('NameTagsRtf::margintopbottom'));
		$this->rtf->setPaperWidth(constant('NameTagsRtf::paperwidth'));
		$this->rtf->setCharset(constant('NameTagsRtf::characterset'));	
	}
	
	private function addSection(){
		return $this->rtf->addSection();
	}
	
	private function getTotalPages(){
		return ceil($this->getTotalParticipants()/constant('NameTagsRtf::pageDivisor'));
	}
	
	private function getTotalParticipants(){
		return count($this->participantsInfo);
	} 
	
	public function generateRtf(){
		$oSection  = $this->addSection();
		$numOfPage = intval($this->getTotalPages());
		
		for( $i = 0; $i < $numOfPage; $i++ ){
		
			$oTable = $oSection->addTable();
			if( $i==0 && $numOfPage > 1 ){
				$oTable->addRows(constant('NameTagsRtf::pageDivisor'));
				$oTable->addColumnsList(array(constant('NameTagsRtf::columnwidth'),constant('NameTagsRtf::columnwidth')));//set to two columns
				$oTable->setBorderForCellRange($this->border,constant('NameTagsRtf::initialcount'),constant('NameTagsRtf::initialcount'),constant('NameTagsRtf::pageDivisor'),constant('NameTagsRtf::columncount'));
			}elseif( $i > 0 && $numOfPage > 1 ){
				$maxDisplay = ($i * constant('NameTagsRtf::pageDivisor') );
				$num_rows   = ($this->getTotalParticipants() - $maxDisplay);
				$rows       = (ceil($num_rows/constant('NameTagsRtf::columncount')));
				$rows       = ($rows * constant('NameTagsRtf::columncount'));
				$oTable->addRows($rows);
				$oTable->addColumnsList(array(constant('NameTagsRtf::columnwidth'),constant('NameTagsRtf::columnwidth')));//set to two columns
				$oTable->setBorderForCellRange($this->border,constant('NameTagsRtf::initialcount'),constant('NameTagsRtf::initialcount'),constant('NameTagsRtf::pageDivisor'),constant('NameTagsRtf::columncount'));
			}else{
				$num_rows   = (ceil($this->getTotalParticipants()/constant('NameTagsRtf::columncount')));
				$rows       = ($num_rows * constant('NameTagsRtf::columncount'));
				$oTable->addRows($rows);
				$oTable->addColumnsList(array(constant('NameTagsRtf::columnwidth'),constant('NameTagsRtf::columnwidth')));//set to two columns
				$oTable->setBorderForCellRange($this->border,constant('NameTagsRtf::initialcount'),constant('NameTagsRtf::initialcount'),$rows,constant('NameTagsRtf::columncount'));
			}
			
			$j = constant('NameTagsRtf::initialcount');
			$k = constant('NameTagsRtf::columncount');
			$minimum_key     = $i * constant('NameTagsRtf::pageDivisor');
			$max_display_key = $minimum_key + constant('NameTagsRtf::pageDivisor');

            foreach( $this->participantsInfo as $participants_key => $participants_Info ){
				if( $participants_key >= $minimum_key && $participants_key < $max_display_key ){
					
					$columnId = constant('NameTagsRtf::initialcount');
					if( $participants_key % 2 == 1 ){
						$columnId = constant('NameTagsRtf::columncount');
					}else{
						$columnId = constant('NameTagsRtf::initialcount');
					}
			
					$oTable->writeToCell($j,$columnId, Configure::read('NameTags.Header'),new PHPRtfLite_Font(16, 'PaquetteDisplaySSK','#000000'));
					$oTable->writeToCell($j,$columnId, Configure::read('NameTags.SubHeader'),new PHPRtfLite_Font(10, 'Times New Roman','#000000'));
					$column_one = $oTable->getCell($j,$columnId);
					$column_one->setBorder($this->border);
					$column_one->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
					$column_one->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);
					$column_one->setCellPaddings(0, 0.15, 0, 0.07);

                    $name = (strlen(trim($participants_Info['Participant']['nick_name'])) > 0) ? trim($participants_Info['Participant']['nick_name']) :
                        trim($participants_Info['Participant']['first_name']);

                    $oTable->writeToCell($k,$columnId, '<b>'.$name.'</b><br />',new PHPRtfLite_Font(65, 'Arial Narrow','#000000'));
					$oTable->writeToCell($k,$columnId, $participants_Info['Company']['name'].'<br />',new PHPRtfLite_Font(12, 'Arial','#000000'));
					$column_two = $oTable->getCell($k,$columnId);
					$column_two->setBorder($this->border);
					$column_two->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
					$column_two->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);
					$column_two->setCellPaddings(0, 0.5, 0, 0.1);
					
					if( $participants_key % 2 == 1 ){
						$j = $j + constant('NameTagsRtf::columncount');
						$k = $k + constant('NameTagsRtf::columncount');		
					}
				}
			}
			$oSection->insertPageBreak();
		}
		return $oSection;
	}
	
	public function save_rtf(){
        $old_mask = umask(0);
        $this->rtf->save($this->file_name);
        umask($old_mask);
        system('chmod 777 '.$this->file_name);
    }
}