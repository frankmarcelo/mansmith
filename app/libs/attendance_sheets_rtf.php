<?php

final class AttendanceSheetsRtf{

	const characterset = 'Utf-8';
	const marginleftright = '1.411111111';
	const margintopbottom = '1.411111111';
	const paperwidth   = '21.6';
	const initialcount = 1;
	const columncount  = 2;
	const pageDivisor  = 18;
	const columnwidth  = '18.044583333';
	const columnwidth_bigger  = '18.767777778';
	
	const image_width  = '3.427777778';
	const image_height = '1.663888889';
	
	const column_width_division = '4.691944444';
	
	private $rtf;
	private $border;
	private $participantsInfo =array();
	private $programInfo =array();
	private $file_name;
	private $start_date;
	private $end_date;
	
	public function __construct(PHPRtfLite $rtf,PHPRtfLite_Border $border, $file_name,$start_date,$programInfo =array(), $participantsInfo=array()){
		$this->rtf = &$rtf;
		$this->border = &$border;
		$this->programInfo = &$programInfo;
		$this->participantsInfo = &$participantsInfo;
		$this->start_date = $start_date;
		$this->file_name = $file_name;
		$this->rtf->setMarginLeft(constant('AttendanceSheetsRtf::marginleftright'));
		$this->rtf->setMarginRight(constant('AttendanceSheetsRtf::marginleftright'));
		$this->rtf->setMarginTop(constant('AttendanceSheetsRtf::margintopbottom'));
		$this->rtf->setMarginBottom(constant('AttendanceSheetsRtf::margintopbottom'));
		$this->rtf->setPaperWidth(constant('AttendanceSheetsRtf::paperwidth'));
		$this->rtf->setCharset(constant('AttendanceSheetsRtf::characterset'));	
	}
	
	private function addSection(){
		return $this->rtf->addSection();
	}
	
	private function getTotalPages(){
		return ceil($this->getTotalParticipants()/constant('AttendanceSheetsRtf::pageDivisor'));
	}
	
	private function getTotalParticipants(){
		return count($this->participantsInfo);
	} 
	
	public function generateRtf(){
		$oSection  = $this->addSection();
		$numOfPage = intval($this->getTotalPages());
		
		for( $i =0 ; $i < $numOfPage; $i++){
	
			//first table
			$oTable = $oSection->addTable();
			$oTable->addRows(1);
			$oTable->addColumnsList(array(constant('AttendanceSheetsRtf::columnwidth')));//set column width
	
			$mansmith_image = Configure::read('Mansmith.image');
			$cell_one = $oTable->getCell(1, 1);
			$cell_one->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
			$cell_one->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);
			$oImage   = $cell_one->addImage($mansmith_image);
			$oImage->setWidth(constant('AttendanceSheetsRtf::image_width'));
			$oImage->setHeight(constant('AttendanceSheetsRtf::image_height'));
			
			//second table
			$oTable_two = $oSection->addTable();
			$oTable_two->addRows(1);
			$oTable_two->addColumnsList(array(constant('AttendanceSheetsRtf::columnwidth')));
			
			$oTable_two->writeToCell(1, 1,Configure::read('AttendanceSheet.Header'),new PHPRtfLite_Font(11, 'Verdana','#000000'));
			$oTable_two->writeToCell(1, 1,Configure::read('AttendanceSheet.SubHeader'),new PHPRtfLite_Font(11, 'Times New Roman','#000000'));
		
			$cell_two = $oTable_two->getCell(1,1);
			$cell_two->setCellPaddings(0, 0.2, 0, 0);
			$cell_two->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
			$cell_two->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);
			
			//third table
			$oTable_three = $oSection->addTable();
			$oTable_three->addRows(1);
			$oTable_three->addColumnsList(array(constant('AttendanceSheetsRtf::columnwidth')));
			
			$oTable_three->writeToCell(1,1,Configure::read('AttendanceSheet.HeaderAddress'),new PHPRtfLite_Font(9, 'Arial','#000000'));
			$oTable_three->writeToCell(1,1,Configure::read('AttendanceSheet.SubHeaderAddress'),new PHPRtfLite_Font(9, 'Arial','#000000'));
		
			$cell_three = $oTable_three->getCell(1,1);
			$cell_three->setCellPaddings(0, 0.2, 0, 0.2);
			$cell_three->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
			$cell_three->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);
		
			//fourth table
			$oTable_four = $oSection->addTable();
			$oTable_four->addRows(1);
			$oTable_four->addColumnsList(array(constant('AttendanceSheetsRtf::columnwidth_bigger')));//set to two columns
		
			$oTable_four->writeToCell(1,1, '.<br>',new PHPRtfLite_Font(6, 'Arial','#000000'));
			$oTable_four->writeToCell(1,1, 'Attendance Sheet<br>',new PHPRtfLite_Font(11, 'Arial','#000000'));
			$oTable_four->writeToCell(1,1, '<b>'.$this->programInfo['Program']['title'].'</b><br>',new PHPRtfLite_Font(12, 'Arial','#000000'));
			$oTable_four->writeToCell(1,1, date("F d, Y",strtotime($this->start_date)).' - '.$this->programInfo['Program']['venue_name'].', '.$this->programInfo['Program']['city'].'<br>',new PHPRtfLite_Font(11, 'Arial','#000000'));
			$oTable_four->writeToCell(1,1, '.<br>',new PHPRtfLite_Font(6, 'Arial','#000000'));
			$oTable_four->writeToCell(1,1, Configure::read('AttendanceSheet.Instruction1'),new PHPRtfLite_Font(9, 'Arial','#000000'));
			$oTable_four->writeToCell(1,1, Configure::read('AttendanceSheet.Instruction2'),new PHPRtfLite_Font(9, 'Arial','#000000'));
			$oTable_four->writeToCell(1,1, '.',new PHPRtfLite_Font(6, 'Arial','#000000'));
			
			$cell_four = $oTable_four->getCell(1,1);
			$cell_four->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
			$cell_four->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);	
	
			//fifth table
			$oTable_five = $oSection->addTable();
			$oTable_five->addRows(1);
			
			//const column_width_division = '4.691944444';
			$oTable_five->addColumnsList(array(
				constant('AttendanceSheetsRtf::column_width_division'),
				constant('AttendanceSheetsRtf::column_width_division'),
				constant('AttendanceSheetsRtf::column_width_division'),
				constant('AttendanceSheetsRtf::column_width_division'))
			);//set to four columns
	
			$oTable_five->writeToCell(1,1, '<br><b>COMPANY</b>',new PHPRtfLite_Font(8.5, 'Arial','#000000'));
			$oTable_five->writeToCell(1,2, '<br><b>NAME</b>',new PHPRtfLite_Font(8.5, 'Arial','#000000'));
			$oTable_five->writeToCell(1,3, '<br><b>POSITION</b>',new PHPRtfLite_Font(8.5, 'Arial','#000000'));
			$oTable_five->writeToCell(1,4, '<br><b>SIGNATURE</b>',new PHPRtfLite_Font(8.5, 'Arial','#000000'));
			$oTable_five->setBorderForCellRange($this->border, 1, 1, 1, 4);
	
			$cell_column_one = $oTable_five->getCell(1,1);
			$cell_column_one->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
			$cell_column_one->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);
	
			$cell_column_two = $oTable_five->getCell(1,2);
			$cell_column_two->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
			$cell_column_two->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);
			
			$cell_column_three = $oTable_five->getCell(1,3);
			$cell_column_three->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
			$cell_column_three->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);
			
			$cell_column_four = $oTable_five->getCell(1,4);
			$cell_column_four->setTextAlignment(PHPRtfLite_Table_Cell::TEXT_ALIGN_CENTER);
			$cell_column_four->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);
	
			//sixth table
			$oTable_six = $oSection->addTable();
	
			$j = constant('AttendanceSheetsRtf::initialcount');
			$minimum_key     = $i * constant('AttendanceSheetsRtf::pageDivisor');
			$max_display_key = $minimum_key + constant('AttendanceSheetsRtf::pageDivisor');
	
			if( $this->getTotalParticipants() >= $max_display_key ){// num of page is greater than 1 so automatically load 5 rows
				$oTable_six->addRows(constant('AttendanceSheetsRtf::pageDivisor'),0.5);
				$oTable_six->addColumnsList(array(
					constant('AttendanceSheetsRtf::column_width_division'),
					constant('AttendanceSheetsRtf::column_width_division'),
					constant('AttendanceSheetsRtf::column_width_division'),
					constant('AttendanceSheetsRtf::column_width_division'))
				);//set to four columns
				$oTable_six->setBorderForCellRange($this->border, 1, 1, constant('AttendanceSheetsRtf::pageDivisor'), 4);
			}else{
				$rows = $max_display_key - $this->getTotalParticipants();
				$oTable_six->addRows($rows,0.5);
				$oTable_six->addColumnsList(array(
					constant('AttendanceSheetsRtf::column_width_division'),
					constant('AttendanceSheetsRtf::column_width_division'),
					constant('AttendanceSheetsRtf::column_width_division'),
					constant('AttendanceSheetsRtf::column_width_division'))
				);//set to four columns
				$oTable_six->setBorderForCellRange($this->border, 1, 1, $rows, 4);
			}
			
			foreach( $this->participantsInfo as $participants_key => $participants_Info ){
				if( $participants_key >= $minimum_key && $participants_key < $max_display_key ){
	
					
					$fullName = ucwords(strtolower($participants_Info['Participant']['first_name'])).' '.strtoupper($participants_Info['Participant']['middle_name']).' '.ucwords(strtolower($participants_Info['Participant']['last_name']));
					$oTable_six->writeToCell($j,1, $participants_Info['Company']['name'],new PHPRtfLite_Font(10, 'Arial','#000000'));
					$oTable_six->writeToCell($j,2, $fullName,new PHPRtfLite_Font(10, 'Arial','#000000'));
					$oTable_six->writeToCell($j,3, $participants_Info['ParticipantsPosition']['name'],new PHPRtfLite_Font(10, 'Arial','#000000'));
					$oTable_six->writeToCell($j,4, '',new PHPRtfLite_Font(10, 'Arial','#000000'));
					
					$cell_column_data = $oTable_six->getCell($j,1);
					$cell_column_data->setCellPaddings(0.1, 0, 0, 0);
					$cell_column_data->setBorder($this->border);
					$cell_column_data->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);	
						
					$cell_column_data = $oTable_six->getCell($j,2);
					$cell_column_data->setCellPaddings(0.1, 0, 0, 0);
					$cell_column_data->setBorder($this->border);
					$cell_column_data->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);	
					
					$cell_column_data = $oTable_six->getCell($j,3);
					$cell_column_data->setCellPaddings(0.1, 0, 0, 0);
					$cell_column_data->setBorder($this->border);
					$cell_column_data->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);	
					
					$cell_column_data = $oTable_six->getCell($j,4);
					$cell_column_data->setCellPaddings(0.1, 0, 0, 0);
					$cell_column_data->setBorder($this->border);
					$cell_column_data->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);
						
					$j++;
				}
			}
	
			if( $i == ($numOfPage -1) ){
				$oTable_seven = $oSection->addTable();
				$oTable_seven->addRows(1);
				$oTable_seven->addColumnsList(array(constant('AttendanceSheetsRtf::columnwidth_bigger')));//set to two columns
				$oTable_seven->writeToCell(1,1, '<br>Total Number of Participants: <b>'.$this->getTotalParticipants().'</b>',new PHPRtfLite_Font(10, 'Arial','#000000'));
				
				$cell_seven = $oTable_seven->getCell(1,1);
				$cell_seven->setCellPaddings(0.1, 0, 0, 0);
				$cell_seven->setVerticalAlignment(PHPRtfLite_Table_Cell::VERTICAL_ALIGN_CENTER);
				$cell_seven->setBorder($this->border);
			}else{
				
				$oTable_eight = $oSection->addTable();
				$oTable_eight->addRows(1);
				$oTable_eight->addColumnsList(array(constant('AttendanceSheetsRtf::columnwidth')));//set to two columns
				$oTable_eight->writeToCell(1,1, '<br>',new PHPRtfLite_Font(11, 'Times New Roman','#000000'));
			}
			$oSection->insertPageBreak();
		}//end of for loop
		return $oSection;
	}
	
	public function save_rtf(){
		$old_mask = umask(0);
        $this->rtf->save($this->file_name);
		umask($old_mask);
		system('chmod 777 '.$this->file_name);
	}
}
