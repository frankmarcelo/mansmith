<?php
$cacheConfig = array(
        'duration' => '+1 hour',
        'path' => CACHE.'queries',
        'engine' => 'File',
);

Configure::write('User.failed_login_limit', 5);
Configure::write('User.failed_login_duration', 300);
Cache::config('users_login', array_merge($cacheConfig, array(
    'duration' => '+' . Configure::read('User.failed_login_duration') . ' seconds',
)));

Configure::write('UrlCache.pageFiles', true);
require_once(CONFIGS . DS . 'mansmith_bootstrap.php' );
require_once(APP . DS . 'plugins' . DS . 'acl' . DS . 'config' . DS . 'bootstrap.php');
#require_once(ROOT . DS . 'plugins' . DS . 'acl' . DS . 'config' . DS . 'bootstrap.php');
?>
