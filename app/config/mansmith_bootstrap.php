<?php

function headersDownloadFile( $filetype=null, $downloadFile=null,$filename=null){
	
	if(ini_get('zlib.output_compression')){
    	ini_set('zlib.output_compression', 'Off');
	} 
	header("HTTP/1.1 200 OK");
	header("Content-Description: File Transfer");
	header("Accept-Ranges: bytes");
						
	foreach (getallheaders() as $name => $value) {
		if( strstr(strtolower($name),'range')){
 			header("Content-Range: bytes ".$value."/".filesize($downloadFile));
		}
	}

    header('Expires: 0');
    header("Cache-Control: no-cache, must-revalidate");
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Pragma: public");
    header("Content-disposition: attachment; filename=".$filename);
    header('Content-Transfer-Encoding: binary');
    header("Content-Length: ".filesize($downloadFile));
    if( $filetype == 'rtf'){
    	header('Content-type: application/vnd.ms-word');
    }elseif( $filetype == 'xls' || $filetype =='csv'){
        header('Content-type: application/vnd.ms-excel');
    }
    flush();
    readfile($downloadFile);
    exit;
}

function billingEbFormat($cost,$noOfParticipantDiscount=0,$amount=0,$VAT,$discountedVAT=0){
    $display  = "PHP ".number_format($cost,2,'.',',')." Seminar Investment <br>";
    $display .= "x ".$noOfParticipantDiscount." No. of pax <br>";
    $display .= "------------------<br>";
    $display .= "<b>".number_format($amount,2,".",",")."</b><br>";
    $display .= "+ ".number_format($VAT,2,".",",")." ".Configure::read('VAT_PERCENTAGE')."% VAT<br>";
    $display .= "------------------<br>";
    $display .= "<b>PHP ".number_format($discountedVAT,2,".",",")."</b>";
    return $display;
}

function billingInvoiceFormat(
    $cost,
    $padParticipants=0,
    $actualAmount=0,
    $nonEbVATX=0
){
    $display  = 'Seminar Investment Php         '.number_format($cost,2,'.',',').'|';
    $display .= 'No. of pax                   x   '.$padParticipants.'|';
    $display .= '                                ------------------|';
    $display .= '                               '.number_format($actualAmount,2,'.',',').'|';
    $display .= '+ 12% VAT                 +  '.$nonEbVATX.'|';
    $display .= '                                ------------------';
    return $display;
}

function billingInvoiceDataInsert(
    $insertId=null,
    $billingTypeId=1,
    $invoiceNumber,
    $programBillingInfo=array(),
    $participants=array(),
    $sourceFile,
    $amount,
    $amountInWords,
    $totalParticipants,
    $invoice_notes,
    $notes,
    $status,
    $who_created,
    $payment_date
){
    /**
     * @TODO
     * $insertId replace a proper Info
     */
    $invoiceDetails = array();
    $invoiceDetails['InvoiceDetails']['invoice_id'] = $insertId;
    $invoiceDetails['InvoiceDetails']['billing_type_id'] = $billingTypeId;
    $invoiceDetails['InvoiceDetails']['invoice_number'] = $invoiceNumber;

    if( $programBillingInfo && isset($programBillingInfo[0]['ProgramBillingInfo']) ){
        $invoiceDetails['InvoiceDetails']['company'] =  $participants[0]['Company']['name'];
        $invoiceDetails['InvoiceDetails']['recipient'] = $programBillingInfo[0]['Participant']['last_name'].', '.$programBillingInfo[0]['Participant']['first_name'].' '.$programBillingInfo[0]['Participant']['middle_name'];
        $invoiceDetails['InvoiceDetails']['recipient_position'] = $programBillingInfo[0]['ParticipantsPosition']['name'];
        /**
         * TODO
         * fix the payment date
         * invoice number
         */
    }else{
        $invoiceDetails['InvoiceDetails']['company'] =  $participants[0]['Company']['name'];
        $invoiceDetails['InvoiceDetails']['recipient'] = 'non assigned';
        $invoiceDetails['InvoiceDetails']['recipient_position'] = '';
    }

    $address  = $participants[0]['Company']['primary_bldg_name'].' ';
    $address .= $participants[0]['Company']['primary_street'].' ';
    $address .= $participants[0]['Company']['primary_suburb'].', ';
    $address .= $participants[0]['Company']['primary_city'];

    $invoiceDetails['InvoiceDetails']['payment_due_date'] = $payment_date;
    $invoiceDetails['InvoiceDetails']['recipient_address'] = $address;
    $invoiceDetails['InvoiceDetails']['source_file'] = $sourceFile;
    $invoiceDetails['InvoiceDetails']['total_amount_due'] = str_replace(",","",$amount);
    $invoiceDetails['InvoiceDetails']['amount_in_words'] = $amountInWords;
    $invoiceDetails['InvoiceDetails']['number_of_participant'] = $totalParticipants;
    $invoiceDetails['InvoiceDetails']['invoice_notes'] = $invoice_notes;
    $invoiceDetails['InvoiceDetails']['notes'] = $notes;//TODO set the text area notes
    $invoiceDetails['InvoiceDetails']['status'] = $status;
    $invoiceDetails['InvoiceDetails']['date_created'] = date("Y-m-d H:i:s");
    $invoiceDetails['InvoiceDetails']['date_modified'] = date("Y-m-d H:i:s");
    $invoiceDetails['InvoiceDetails']['who_created'] = $who_created;

    return $invoiceDetails;
}

/**
 *
 *  MSISDN is Mobile Station International ISDN Number  
 *	MSISDN = CC + NDC + SN
 *	CC = Country Code
 * 	NDC = National Destination Code, identifies one or part of a PLMN
 *	SN = Subscriber Number
 *	http://en.wikipedia.org/wiki/Telephone_numbers_in_New_Zealand#International_Number_Lengths
 *
 */
 
define( "COUNTRY_CODE_LEN" , 2 ); //this will be the length of the country code. As for New Zealand 64 is the country code so it will be string length of 2
define( "PROVIDER_LEN"     , 2 ); //this will be the network provider code as for vodafone[021 => 21],telecom [027 => 27], 2degrees [022=>22] . It will still be a length of 2.
define( "PREFIX_SN_LEN"    , 3 ); //Subscriber number as normally it will be three. Number length for vodafone is 6-8 digits. We need make the mobile num flexible as possible

class Msisdn{
	
	public $country_code = "";
	public $area_code = "";
	public $nxx = "";
	public $station_number = "";
	public $error;
	
	public function __construct( $msisdn = "" ){
		if( strlen($msisdn) > 0 ){
			$this->SetMsisdn( $msisdn );	
		}
	}
	
	public function GetError(){
		if( isset($this->error) && strlen($this->error) > 0){
			return $this->error;
		}
		return false;
	}
	
	public function SetError($error = null){
		$this->error = $error;	
	}
	
	public function SetMsisdn( $msisdn ){
		$msisdn = trim($msisdn);
		$msisdn = str_replace( " " , "" , $msisdn );
		$msisdn = str_replace( "-" , "" , $msisdn );
		$msisdn = preg_replace( "/^(\+)/i","", $msisdn);
		$msisdn = preg_replace( "/^(09|9)/i","639", $msisdn);
		$msisdn = preg_replace( "/^(6309)/i","639", $msisdn);
		if( ctype_digit($msisdn) && preg_match("/^(639)/i",$msisdn) && strlen($msisdn) >= 10 ){		
			$this->SetCountryCode( substr($msisdn , 0, COUNTRY_CODE_LEN ) );
			$this->SetAreaCode( substr($msisdn, strlen($this->country_code), PROVIDER_LEN)	);
			$this->SetNxx( substr($msisdn, strlen($this->country_code) + strlen($this->area_code),PREFIX_SN_LEN) );
			$this->SetStationNumber( substr($msisdn,strlen($this->country_code) + strlen($this->area_code)+ strlen($this->nxx)));
		}else{
			$this->error = "Invalid mobile msisdn:[".$msisdn."] gettype:[".gettype($msisdn)."]";
			$this->SetError($this->error);
		}
	}
	
	public function GetMsisdn(){
		return $this->country_code . $this->area_code . $this->nxx . $this->station_number;
	}
	
	public function GetStandardMsisdn(){
		return "0" . $this->area_code . $this->nxx . $this->station_number;
	}
	
	public function GetMsisdnAsIntl(){
		if( $this->GetMsisdn() ){
			return "+" . $this->GetMsisdn();
		}else{
			return false;
		}
	}
	
	public function GetSubscriberNumber(){
		return $this->area_code . $this->nxx . $this->station_number;
	}
	
	public function SetCountryCode( $aCountryCode ){
		$this->country_code = trim($aCountryCode);
	}
	
	public function GetCountryCode(){
		return $this->country_code;
	}
	
	public function SetAreaCode( $aAreaCode ){
		$this->area_code = trim($aAreaCode);
	}
	
	public function GetAreaCode(){
		return $this->area_code;
	}
	
	public function SetNxx($aNxx){
		$this->nxx = trim($aNxx);
	}
	
	public function GetNxx(){
		return $this->nxx;	
	}
	
	public function SetStationNumber($aStationNumber){
		$this->station_number = trim($aStationNumber);
	}
	
	public function GetStationNumber(){
		return $this->station_number;
	}
}

function invoicePad($invoiceNumber, $billingTypeId){

    $basedir = dirname(dirname(__DIR__));
    require_once $basedir. '/app/config/database.php';

    $dbConfig = new DATABASE_CONFIG();
    $mysqli = new mysqli( $dbConfig->default['host'] ,
        $dbConfig->default['login'] ,
        $dbConfig->default['password'] ,
        $dbConfig->default['database'] );

    $returnString = $invoiceNumber.rand(100,999).$billingTypeId;
    $sql = " SELECT id FROM invoice_details WHERE invoice_number='".$returnString."' ";
    if( $res = $mysqli->query($sql) ){
        if( $res->num_rows > 0 ){
            invoicePad($billingTypeId,$length);
        }else{
            return $returnString;
        }
    }
    $mysqli->close();
}

function generatePassword($algorithm, $chars){

    $pwd = $algorithm(microtime());
    $pwd= substr($pwd, 0, $chars - 1);

    return $pwd;
}

function convertAmountToWords( $amount=0 ){
	
    $options = array(
		'1'   => 'one',    
    	'2'   => 'two',
    	'7'   => 'seven',
    	'10'  => 'ten',      
    	'20'  => 'twenty', 
     	'70'  => 'seventy',
    	'11'  => 'eleven',   
     	'3'   => 'three',  
     	'8'   => 'eight',
    	'12'  => 'twelve',   
     	'30'  => 'thirty', 
     	'80'  => 'eighty',
    	'13'  => 'thirteen',  
    	'4'   => 'four', 
    	'9'   => 'nine',
    	'14'  => 'fourteen',  
    	'40'  => 'fourty',
    	'90'  => 'ninety',
    	'15'  => 'fifteen',   
    	'5'   => 'five',
    	'16'  => 'sixteen',   
    	'50'  => 'fifty',
    	'17'  => 'seventeen', 
    	'6'   => 'six',
    	'18'  => 'eighteen',  
    	'60'  => 'sixty'
    );
    
    $options_denomination = array( 
    	'3' => 'million',
    	'2' => 'thousand',
    	'1' => ''
    );

	$comparisonOne = explode( '.', $amount );
	$comparisonTwo = explode( ',', $comparisonOne[0] );
	 
	$intARef = count($comparisonTwo);
	$amountInWords = null;

    if(	$intARef > 1 ){
    	foreach( $comparisonTwo as $key => $segment ){
      		
        	if( strlen($segment) == 3 ){
        		$amountInWords = $amountInWords.' '.processThrees($segment,$options).' '.$options_denomination[$intARef];
        	}else{
          		$amountInWords = $amountInWords.' '.processTwosAndOnes($segment,$options).' '.$options_denomination[$intARef];
        	}

        	$intARef = $intARef - 1;
      	}
    }else{
    	
    	if(strlen($comparisonTwo[0]) == 3){
        	$amountInWords = $amountInWords.' '.processThrees($comparisonTwo[0],$options);
      	}else{
      	  	$amountInWords = $amountInWords.' '.processTwosAndOnes($comparisonTwo[0],$options);
      	}
    }

    $cents = null;
    if( $comparisonOne[1] > 0 ){
      	$cents = 'and '.processTwosAndOnes($comparisonOne[1],$options).' cents';
    }

    $returnString = trim($amountInWords).' '.trim($cents);
    $returnString = trim($returnString);
    $returnString = str_replace(array("/  /","/^ /"),array(" ",""),$returnString); 
    $returnString = ucfirst($returnString);
 	return $returnString;
}

function processThrees( $amount =0 , $options = array() ){
	
    $segment_one = substr($amount,0,1);
    $segment_two = substr($amount,1,2);
	return $options[$segment_one].' hundred '.processTwosAndOnes( $segment_two,$options );
}

function processTwosAndOnes( $amount=0, $options = array() ){
	
    $returnString = null;
    
    if( preg_match("/^0/",$amount) ){
        if( isset($options[preg_replace("/^0/","",$amount)]) ){
		    $returnString = $options[preg_replace("/^0/","",$amount)];
        }else{
            $returnString = '';
        }
    }else{
    	
      	if( preg_match("/0/",$amount) || $amount < 20 ) {
      		$returnString = (isset($options[$amount])) ? $options[$amount]:'';
        	
      	}else{
      		
        	$segment_one = substr( $amount,0,1).'0';
        	$segment_two = substr( $amount,1,1);
			$returnString = $options[$segment_one].' '.$options[$segment_two];
      	}
	}
	return $returnString;
}

function getFullAddress($program){
    $sAddress = null;
    if( isset($program['Program']['venue_name']) ){
        $sAddress .= $program['Program']['venue_name'].', ';
    }

    if( isset($program['Program']['address']) ){
        $sAddress .= ucwords($program['Program']['address']).', ';
    }

    if( isset($program['Program']['suburb']) ){
        $sAddress .= ucwords($program['Program']['suburb']).' ';
    }
    $sAddress .=' ';

    if( isset($program['Program']['city']) ){
        $sAddress .= ucwords($program['Program']['city']);
    }
    return $sAddress;
}

function getSchedule($program,$shortcut=false){
    $sSchedule = null;
    if( isset($program['ProgramsSchedule']) && is_array($program['ProgramsSchedule']) && count($program['ProgramsSchedule']) > 0 ){
        if( $shortcut == true ){
            return date("d M Y",strtotime($program['ProgramsSchedule'][0]['start_date']));
        }else{
            foreach( $program['ProgramsSchedule'] as $program_schedule_key  => $program_schedule ){
                if( is_array($program_schedule) && $program_schedule){
                    $sSchedule .= date("D",strtotime($program_schedule['start_date'])).' '.
                        date("d",strtotime($program_schedule['start_date'])).' '.
                        date("M Y",strtotime($program_schedule['start_date']));
                }
            }
        }
    }
    return $sSchedule.'  ';
}

class DirectoryProperty{
	
	public $documentDirectory;
}
	
class DocumentDirectory{
	
	private $directoryProperty;
	private $programId = 0;
	private $programDirectory;
	private $normalizedDirectory;
	
	public function __construct( DirectoryProperty $oDirectoryProperty, $programId =0 ){
		if( !is_object($oDirectoryProperty) && $programId > 0){
			return false;
		}
		$this->oDirectoryProperty = &$oDirectoryProperty;

        if( intval($programId)>0 ){
            $this->programId = $programId;
            $this->convertProgramToDirectory($this->programId);
        }
	}
	
	public function getDocumentsDirectory(){
		return (isset($this->oDirectoryProperty->documentDirectory)) ? $this->oDirectoryProperty->documentDirectory : null; 
	}
	
	public function getProgramId(){
		return $this->programId;
	}
	
	private function convertProgramToDirectory($programId=0){//return void
		if( $programId <1 ){
			return;
		}
		
		$uniformDirectory = null;
		$padded_programId = str_pad($programId, 9, 0, STR_PAD_LEFT);
		for( $i = strlen($padded_programId) ; $i > 0; $i--){
			$uniformDirectory = (($i % 3) == 0 && $i < strlen($padded_programId)) ? '/'.$uniformDirectory : $uniformDirectory;
			$uniformDirectory = substr($padded_programId, $i - 1, 1).$uniformDirectory;
		}
	
		$normalizeDirectory = $this->getProgramDirectory(). $uniformDirectory;
		if( !is_dir($normalizeDirectory) ){
			$this->createProgramDirectory( $this->getProgramDirectory() , $uniformDirectory );
		}
		$this->normalizedDirectory = $normalizeDirectory;
	}
	
	private function createProgramDirectory( $documentRoot , $uniformDirectory = null){
		
		$programDirectory = $documentRoot . $uniformDirectory;
		
		$old_mask = umask(0);
		$tmp = explode('/', $uniformDirectory); 
		$tmp_path = '';
		
		for( $i = 0; $i < count($tmp); $i++ ){
			if( $i == 0 ){
				$tmp_path .= $tmp[$i];
			}else{
				$tmp_path .= '/'.$tmp[$i];
			}
			if (!is_dir( $documentRoot.$tmp_path) ){ 
				mkdir( $documentRoot.$tmp_path, 0777); 
			}
		}

        if( !is_dir( $programDirectory. DS ."attendance-sheets" . DS ) ){
			mkdir( $programDirectory. DS ."attendance-sheets" , 0777,true);
		}
			
		if( !is_dir( $programDirectory. DS ."billing-eb" . DS ) ){
			mkdir( $programDirectory. DS ."billing-eb" , 0777,true);
		}
			
		if( !is_dir( $programDirectory. DS ."billing-noneb" . DS ) ){
			mkdir( $programDirectory. DS ."billing-noneb" , 0777,true);
		}
			
		if( !is_dir( $programDirectory. DS ."billing-osp" . DS ) ){
			mkdir( $programDirectory. DS ."billing-osp" , 0777,true);
		}
			
		if( !is_dir( $programDirectory. DS ."billing-regular" . DS ) ){
			mkdir( $programDirectory. DS ."billing-regular" , 0777,true);
		}
		
		if( !is_dir( $programDirectory. DS ."invoice" . DS ) ){
        	mkdir( $programDirectory. DS ."invoice".DS."eb", 0777,true);
            mkdir( $programDirectory. DS ."invoice".DS."non-eb", 0777,true);
            mkdir( $programDirectory. DS ."invoice".DS."osp", 0777,true);
            mkdir( $programDirectory. DS ."invoice".DS."regular", 0777,true);
        }
			
		if( !is_dir( $programDirectory. DS ."certificates" . DS ) ){
			mkdir( $programDirectory. DS ."certificates" , 0777,true);
		}
			
		if( !is_dir( $programDirectory. DS ."name-tags" . DS ) ){
			mkdir( $programDirectory. DS ."name-tags" , 0777,true);
		}
			
		if( !is_dir( $programDirectory. DS ."participants-directory" . DS ) ){
			mkdir( $programDirectory. DS ."participants-directory" , 0777,true);
		}
		umask($old_mask);
	}
	
	public function getProgramDirectory(){
		return trim($this->getDocumentsDirectory().DS.$this->programDirectory);
	}
	
	public function getNormalizeProgramDirectory(){
		return trim($this->normalizedDirectory); 
	}
	
	public function getProgramAttendanceDirectory(){
		return trim($this->getNormalizeProgramDirectory() . DS . 'attendance-sheets'. DS);
	}
	
	public function getBillingEbDirectory(){
		return trim($this->getNormalizeProgramDirectory() . DS . 'billing-eb'. DS);
	}
	
	public function getBillingNonEbDirectory(){
		return trim($this->getNormalizeProgramDirectory() . DS . 'billing-noneb'. DS);
	}
	
	public function getBillingOspDirectory(){
		return trim($this->getNormalizeProgramDirectory() . DS . 'billing-osp'. DS);
	}
	
	public function getBillingRegularDirectory(){
		return trim($this->getNormalizeProgramDirectory() . DS . 'billing-regular'. DS);
	}
	
	public function getCertificatesDirectory(){
		return trim($this->getNormalizeProgramDirectory() . DS . 'certificates'. DS);
	}
	
	public function getNameTagsDirectory(){
		return trim($this->getNormalizeProgramDirectory() . DS . 'name-tags'. DS);
	}
	
	public function getParticipantsDirectory(){
		return trim($this->getNormalizeProgramDirectory() . DS . 'participants-directory'. DS);
	}
	
	public function getInvoiceDirectory(){
		return trim($this->getNormalizeProgramDirectory() . DS . 'invoice'. DS);
	}
	
	public function getSalesReportsDirectory($year_month){
        $directory = $this->oDirectoryProperty->documentDirectory.DS.'sales_reports'.DS.$year_month.DS;
		if( !is_dir($directory) ){
			mkdir( $directory, 0777 );
		}

        return $directory;
	}
}
//debug(convertAmountToWords('1,101,194.56'));