<?php
	
Configure::write('debug', 0);
Configure::write('log', true);
Configure::write('log', E_ALL);
Configure::write('App.encoding', 'UTF-8');
Configure::write('Routing.prefixes', array('admin'));
Configure::write('Cache.check', true);
define('LOG_ERROR', 3);
Configure::write('Session.save','cake');
Configure::write('Session.cookie', 'CAKEPHP');
Configure::write('latitude','14.62021249417380');//default latitude
Configure::write('longitude','121.03447342340698');//default longitude
Configure::write('gCalendar.user', 'mansmith.net@gmail.com');
Configure::write('gCalendar.pass', 'm4nsmith');
Configure::write('status_onhold', '0');
Configure::write('status_live', '1');
Configure::write('status_deleted', '2');
Configure::write('administrator', '1');
Configure::write('docs_directory', APP .  'document_data' );
Configure::write('Session.timeout', '1000');
Configure::write('Session.start', true);
Configure::write('Session.checkAgent', true);
Configure::write('Security.level', 'low');
Configure::write('Security.salt', '8690cf97dc5a210795d89856f3c4c8d3c3290cbd');
Configure::write('Security.cipherSeed', '13630382842097025292');
Configure::write('Asset.timestamp', true);
Configure::write('Acl.classname','DbAcl');
Configure::write('Acl.database','default');
date_default_timezone_set('UTC');

Configure::write('Mansmith.image',APP .  'webroot' . DS . 'img' . DS .'mansmith_financial.png');
Configure::write('Coach.image',APP .  'webroot' . DS . 'img' . DS .'coach_financial.jpg');
Configure::write('Company.Name','Mansmith and Fielders, Inc.');
Configure::write('Coach.Company.Name','A Division of '.Configure::read('Company.Name'));
/**
 *  NAME TAGS
 */
Configure::write('NameTags.Header',Configure::read('Company.Name').'<br/>');
Configure::write('NameTags.SubHeader','<b><i>Helping your marketing and sales team soar!</i></b>');
$coach_payment_policy  = '<b>PAYMENT METHODS: COACH TIN: 000‑128‑440‑000 VAT</b><br><b><bullet></b> Seminar fees must be paid before the seminar and addressed to Corporate Achievers Institute';
$coach_payment_policy .= '<br><b><bullet></b> Payment collection or check pick-up (within selected GMA areas only) may be done provided with<br>  at least 24 hr notice.<br>';
$coach_payment_policy .= '<b><bullet></b> Over‑the‑counter payments may also be made at any Security Bank Branch to the following account:<br>';
$coach_payment_policy .= '         <b>Security Bank</b><br>';
$coach_payment_policy .= '         <b>RCBC (New Manila Branch)</b><br>';
$coach_payment_policy .= '         <b>Savings Account # 1-220-05964-9</b><br>';
$coach_payment_policy .= '<b><bullet></b> Please make sure to fax us the deposit slip with your company name and title of seminar to notify<br>';
$coach_payment_policy .= '  us of your payment<br><br>';
Configure::write('Coach.Payment.Policy',$coach_payment_policy);
/** ATTENDANCE SHEET **
 * 
 */
Configure::write('AttendanceSheet.Header','<b>'.strtoupper(Configure::read('Company.Name')).'</b><br/>');
Configure::write('Coach.Header','<b>'.strtoupper(Configure::read('Coach.Company.Name')).'</b><br/>');
Configure::write('AttendanceSheet.SubHeader','<i>Helping your marketing and sales team soar!</i>');
Configure::write('AttendanceSheet.HeaderAddress','Waters Center, 14 Ilang-Ilang St., New Manila, Quezon City<br />');
Configure::write('AttendanceSheet.SubHeaderAddress','7222318  4120034   info@mansmith.net    www.mansmith.net');
Configure::write('Billing.Info','7236671 7222318 info@mansmith.net');
Configure::write('Invoice.Info','<i>info@mansmith.net www.mansmith.net</i><br>');
Configure::write('Invoice.TIN','<b>TIN: 000-128-440-000 VAT</b><br>');
Configure::write('AttendanceSheet.Instruction1','<b><i>All materials and contents of handouts are owned by and copyrighted for exclusive use in Mansmith/Coach seminars.</i></b><br>');
Configure::write('AttendanceSheet.Instruction2','<b><i>These may not be used by participants to conduct other seminars in part or in entirety to other parties.</i></b><br>');
Configure::write('js_directory','/js/ajax/');
Configure::write('js','/js/');
Configure::write('VAT','0.12');
Configure::write('VAT_PERCENTAGE',Configure::read('VAT') * 100);
Configure::write('Invoice.Payment.Instructions',"<b>Please make the check payable to Mansmith and Fielders, Inc.</b><br>|<b>For Peso Remittance:</b><br>|Account Name: Mansmith and Fielders, Inc.<br>|Account No.: 0162-028106-001<br>|Bank Name: Security Bank Timog Branch<br>|Swift Code: SETCPHMMXXX<br>|OR<br>|Account Name: Mansmith and Fielders, Inc.<br>|Account No.: 1-220-01044-5<br>|Bank Name: RCBC New Manila Branch<br>|Swift Code:<br>|<br>|Kindly fax copy of deposit slip to (63-2) 722-2318|");
Configure::write('Invoice.Payment.Policy',"<br>|<b>Note:</b><br>|1) Please disregard this billing if payment has been made.<br>|2) Once we receive registration, cancellations are accepted with full refund or no<br>|   administrative charges provided adviced to us EIGHT (8) working days<br>|   before the seminar. Cancellations made less than 8 days before or no-show<br>|   during the seminar mean forfeiture or payment ( or will still be charged to<br>|   client in case of special credit arrangement). Substitution is accepted at any<br>|   point before the seminar. Only one person is expected to attend each program.<br>|3) Should your company wish to withhold tax, please withhold only 2% as we<br>|   are classified under suppliers or contractors of services. Please also attach<br>|   creditable tax certificate or form 2307.|");

Cache::config('default', array(
    'engine' => 'File', //[required]
    'duration'=> '3600', //[optional]
    'probability'=> 100, //[optional]
    'path' => CACHE, //[optional] use system tmp directory - remember to use absolute path
    'prefix' => 'cake_', //[optional]  prefix every cache file with this string
    'lock' => true, //[optional]  use file locking
    'serialize' => true
));

Cache::config('short', array(
    'engine' => 'File', //[required]
    'duration'=> '+5 minutes', //[optional]
    'probability'=> 100, //[optional]
    'path' => CACHE, //[optional] use system tmp directory - remember to use absolute path
    'prefix' => 'cake_', //[optional]  prefix every cache file with this string
    'lock' => true, //[optional]  use file locking
    'serialize' => true
));

Cache::config('long', array(
    'engine' => 'File', //[required]
    'duration'=> '+1 week', //[optional]
    'probability'=> 100, //[optional]
    'path' => CACHE, //[optional] use system tmp directory - remember to use absolute path
    'prefix' => 'cake_', //[optional]  prefix every cache file with this string
    'lock' => true, //[optional]  use file locking
    'serialize' => true
));
