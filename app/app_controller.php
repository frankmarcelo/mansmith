<?php

class AppController extends Controller {

    public $components = array(
        'Acl',
    	'Auth',
        'Security',
        'Session',
        'RequestHandler',
        'Cookie',
       //'DebugKit.Toolbar'
    );

    public $helpers = array(
    	'Cache',
        'Html',
        'Form',
        'Session',
        'Text',
        'Js',
        'Time',
        'Layout',
        'Custom',
        'Ajax',
        'Javascript',
		'TidyFilter'
    );
    
    public $uses = array(
        'Setting',
        'Role',
        'User',
        
    );
    public $usePaginationCache = true;
    public $view = 'Theme';
    public $theme;
    
    public function __construct() {
        parent::__construct();
    }
        
    public function beforeFilter() {
        if( ($roles = Cache::read('roles','long')) === false) {
            $roles = $this->Role->find('list',array('cache' => 'ProgramsDivision', 'cacheConfig' => 'cache_queries'));
            Cache::write('roles', $roles,'long');
        }
        
        if( ($users = Cache::read('users','short')) === false) {
            $users = $this->User->find('list',array('cache' => 'User', 'cacheConfig' => 'cache_queries'));
            Cache::write('users', $users,'short');
        }
        $this->set( compact('roles','users') );
        if (isset($this->params['admin']) && $this->name != 'CakeError') {
            $this->layout = 'admin';
        }
    }
}
