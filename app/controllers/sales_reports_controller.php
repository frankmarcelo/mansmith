<?php

class SalesReportsController extends AppController {

    public $name = 'SalesReports';
    public $uses = array(
    	'Program',
    	'User',
    	'ProgramsDivision',
    	'ProgramsType',
    	'ProgramsSpeaker',
    	'Speaker', 
    	'ProgramsRatePlan', 
    	'ProgramsSchedule',
    	'ProgramsSpeaker',
    	'MansmithProgram',
    	'Day8Program',
    	'CoachProgram',
    	'Participant',
    	'SalesReports',
        'SalesReportsDetails'
    );
    
    public $helpers = array(
    	'Program',
        'ProgramExtraInfo',
    	'SalesReports',
        'SalesReportsDetails'
    );
    
    public $paginate = array(
        'limit' => 20,
    	'order' => array('SalesReports.id' => 'DESC')
    );
    

    public function beforeFilter() {
        $this->Security->validatePost = false;
        parent::beforeFilter();
    }

    public function getSalesReportsDirectory($year_month=null){
        $oDirectoryProperty = new DirectoryProperty();
        $oDirectoryProperty->documentDirectory = Configure::read('docs_directory');
        $oDocumentDirectory = new DocumentDirectory( $oDirectoryProperty, $programId=null);
        return $oDocumentDirectory->getSalesReportsDirectory(date("Y_m",strtotime($year_month)));
    }

    public function admin_download($downloadId=null){
        Configure::write('debug',2);
        $this->autoRender = false;
        $this->layout = 'ajax';
    	if( isset($downloadId) && intval($downloadId) > 0 ){
            $sales_reports = $this->SalesReports->findById($downloadId);
            if( $sales_reports ){
                $downloadDirectory = $this->getSalesReportsDirectory($sales_reports['SalesReports']['report_date']);
                $downloadFile = trim($downloadDirectory.$sales_reports['SalesReports']['source_file']);
                if( file_exists($downloadFile) ){
                    unlink($downloadFile);
                }
                if( file_exists($downloadFile) ){
                    //headersDownloadFile('xls',$downloadFile,$sales_reports['SalesReports']['source_file']);
                }else{
                    if( isset($sales_reports['SalesReportsDetails']) && count($sales_reports['SalesReportsDetails']) ){

                        App::import('Vendor', 'PHPExcel', array('file' =>'PHPExcel/PHPExcel.php'));
                        /* Create a new workbook with just the supplied sheet */

                        $this->Program = ClassRegistry::init('Program');
                        $this->Company = ClassRegistry::init('Company');
                        $this->Company->contain();

                        $excelFiles = array();
                        $k = 0;

                        foreach( $sales_reports['SalesReportsDetails'] as $program_key => $program_id ){

                            $program = $this->Program->findById($program_id['program_id']);
                            if( $program['Program']['status'] == Configure::read('status_live') ){
                                $companies = array();
                                if( isset($program['ProgramsParticipant']) ){
                                    foreach( $program['ProgramsParticipant'] as $key => $participant ){
                                        if( $participant['status'] == Configure::read('status_live') && $participant['programs_participant_status_id'] == Configure::read('status_live') )
                                        {
                                            $companies[$participant['company_id']] = 1;
                                        }
                                    }
                                }

                                if( count($companies) >0 && $program &&
                                    (
                                        $program['ProgramsRatePlan'][0]['eb_cost'] ||
                                        $program['ProgramsRatePlan'][0]['reg_cost']||
                                        $program['ProgramsRatePlan'][0]['osp_cost']
                                    )
                                ){
                                    $excelReader = PHPExcel_IOFactory::createReader('Excel5');
                                    if (count($companies) >1){
                                        $objPHPExcel = $excelReader->load(dirname(__DIR__)."/document_data/sales_reports/BDNA_Template.xls");
                                    } else {
                                        $objPHPExcel = $excelReader->load(dirname(__DIR__)."/document_data/sales_reports/BDNA_Template_single.xls");
                                    }
                                    $objPHPExcel->getProperties()->setCreator($this->Session->read('Auth.User.name'));
                                    $objPHPExcel->setActiveSheetIndex(0);//always zero
                                    $objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri (Body)');//set font size
                                    $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
                                    $objPHPExcel->getActiveSheet()->getStyle('A1:Z1')->getFill()->getStartColor()->setRGB('FFFFFF');

                                    $objPHPExcel->getActiveSheet()->setCellValue('J9', number_format($program['ProgramsRatePlan'][0]['eb_cost'],2,'.',','));
                                    $objPHPExcel->getActiveSheet()->setCellValue('L9', number_format($program['ProgramsRatePlan'][0]['reg_cost'],2,'.',','));
                                    $objPHPExcel->getActiveSheet()->setCellValue('N9', number_format($program['ProgramsRatePlan'][0]['osp_cost'],2,'.',','));

                                    $discountFlag = ($program['Program']['discount_applies']==1) ? true:false;
                                    $sheet = date("M Y",strtotime($sales_reports['SalesReports']['report_date'])).'-'.substr($program['Program']['title'],0,10).rand(1,9);

                                    // Create a first sheet, representing sales data

                                    $objPHPExcel->getActiveSheet()->setTitle($sheet);
                                    $totalRows = count($companies)-1;

                                    $schedule = null;
                                    if( count($program['ProgramsSchedule']) >0 ){
                                        $schedule = date("F d, Y",strtotime($program['ProgramsSchedule'][0]['start_date']));
                                    }
                                    if( count($program['ProgramsSchedule']) >1 ){

                                        $schedule = date("F d",strtotime($program['ProgramsSchedule'][0]['start_date'])).'-'.date("d, Y",strtotime($program['ProgramsSchedule'][count($program['ProgramsSchedule'])-1]['start_date']));
                                    }
                                    $schedule .= ',  '.getFullAddress($program);


                                    $objPHPExcel->getActiveSheet()->setCellValue('D5', $program['Program']['title']);
                                    $objPHPExcel->getActiveSheet()->setCellValue('D6', $schedule );//'November 10-11, 2011, Mandarin Oriental Hotel, Makati City'
                                    $objPHPExcel->getActiveSheet()->setCellValue('D7', date("d/m/y",strtotime($program['ProgramsRatePlan'][0]['cut_off_date'])));//'27/10/11'


                                    $objPHPExcel->getActiveSheet()->setCellValue('J9', number_format($program['ProgramsRatePlan'][0]['eb_cost'],2,'.',','));
                                    $objPHPExcel->getActiveSheet()->setCellValue('L9', number_format($program['ProgramsRatePlan'][0]['reg_cost'],2,'.',','));
                                    $objPHPExcel->getActiveSheet()->setCellValue('N9', number_format($program['ProgramsRatePlan'][0]['osp_cost'],2,'.',','));

                                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('U2', 'TOP');
                                    $objPHPExcel->getActiveSheet()->getCell('U2')->getHyperlink()->setUrl("sheet://'".$sheet."'!B2");
                                    $objPHPExcel->getActiveSheet()->insertNewRowBefore(11,$totalRows);

                                    $startRow = 10;
                                    $initialRow = $startRow;
                                    $counter = 1;
                                    if (count($companies) > 1){ 

                                    foreach( $companies as $company_id => $company ){

                                        $this->Company->contain();
                                        $companyInfo = $this->Company->findById($company_id);

                                        $this->ProgramBillingInfo = &ClassRegistry::init('ProgramBillingInfo');
                                        $this->ProgramBillingInfo->contain();

                                        $programBillingInfo = $this->ProgramBillingInfo->find('all',array(
                                            'conditions' => array(
                                                'ProgramBillingInfo.program_id' => intval($program_id['program_id']) ,
                                                'ProgramBillingInfo.company_id' => intval($company_id),
                                                'ProgramBillingInfo.status' => Configure::read('status_live'),
                                            )
                                        ));

                                        if( $companyInfo['Company']['status'] == Configure::read('status_live') ){//if company is active
                                            $total_company_participant = 0;
                                            foreach( $program['ProgramsParticipant'] as $key => $participant ){
                                                if( $participant['status'] == Configure::read('status_live') &&
                                                    $participant['programs_participant_status_id'] == Configure::read('status_live') &&
                                                    $companyInfo['Company']['id'] == $participant['company_id']
                                                )
                                                {
                                                    $total_company_participant++;
                                                }
                                            }
                                            $objPHPExcel->getActiveSheet()->setCellValue('B'.$initialRow, $counter );
                                            $objPHPExcel->getActiveSheet()->setCellValue('C'.$initialRow, $companyInfo['Company']['name']);
                                            $objPHPExcel->getActiveSheet()->setCellValue('D'.$initialRow, 0);//always zero(0) on Special Rate

                                            if( empty($programBillingInfo) || count($programBillingInfo) < 1 ||
                                                (isset($programBillingInfo[0]['ProgramBillingInfo']['billing_type_id']) &&
                                                    $programBillingInfo[0]['ProgramBillingInfo']['billing_type_id'] ==1 )
                                            ){
                                                if( $discountFlag )
                                                {
                                                    $companyParticipantDiscount = floor($total_company_participant/4);
                                                    if( $companyParticipantDiscount > 0 )
                                                    {
                                                        $companyParticipant = $companyParticipantDiscount * 0.5;
                                                        $companyParticipantTotalDiscount = $total_company_participant - $companyParticipant;
                                                        if( $companyParticipantTotalDiscount < 1 ){
                                                            $companyParticipantDiscountFlag =1;
                                                        }else{
                                                            $companyParticipantDiscountFlag = floor($companyParticipantTotalDiscount);
                                                        }
                                                        $total_company_participant = $companyParticipantTotalDiscount;
                                                    }
                                                    $objPHPExcel->getActiveSheet()->setCellValue('E'.$initialRow, $total_company_participant);
                                                }else{
                                                    $objPHPExcel->getActiveSheet()->setCellValue('E'.$initialRow, $total_company_participant);
                                                }
                                                $objPHPExcel->getActiveSheet()->setCellValue('F'.$initialRow, 0);
                                                $objPHPExcel->getActiveSheet()->setCellValue('G'.$initialRow, 0);
                                            }

                                            if ( isset($programBillingInfo[0]['ProgramBillingInfo']['billing_type_id']) && 
                                                 $programBillingInfo[0]['ProgramBillingInfo']['billing_type_id'] ==3 )
                                            {// on site rate
                                                $objPHPExcel->getActiveSheet()->setCellValue('G'.$initialRow, $total_company_participant);
                                                $objPHPExcel->getActiveSheet()->setCellValue('E'.$initialRow, 0);
                                                $objPHPExcel->getActiveSheet()->setCellValue('F'.$initialRow, 0);
                                            }

                                            if ( isset($programBillingInfo[0]['ProgramBillingInfo']['billing_type_id']) && 
                                                 $programBillingInfo[0]['ProgramBillingInfo']['billing_type_id'] ==4 )
                                            {// regular rate
                                                $objPHPExcel->getActiveSheet()->setCellValue('F'.$initialRow, $total_company_participant);
                                                $objPHPExcel->getActiveSheet()->setCellValue('E'.$initialRow, 0);
                                                $objPHPExcel->getActiveSheet()->setCellValue('G'.$initialRow, 0);
                                            }

                                            if ( isset($programBillingInfo[0]['ProgramBillingInfo']['status']) && 
                                                $programBillingInfo[0]['ProgramBillingInfo']['status'] ==Configure::read('status_live')){
                                                $objPHPExcel->getActiveSheet()->setCellValue('T'.$initialRow, date("F d Y",strtotime($programBillingInfo[0]['ProgramBillingInfo']['payment_date'])));
                                            }
                                            $objPHPExcel->getActiveSheet()->setCellValue('H'.$initialRow, '=(H$9*D'.$initialRow.')/I'.$initialRow);
                                            $objPHPExcel->getActiveSheet()->setCellValue('I'.$initialRow, 1);//default values
                                            $objPHPExcel->getActiveSheet()->setCellValue('J'.$initialRow, '=J$9*E'.$initialRow.'*K'.$initialRow);
                                            $objPHPExcel->getActiveSheet()->setCellValue('K'.$initialRow, 1);//default values
                                            $objPHPExcel->getActiveSheet()->setCellValue('L'.$initialRow, '=L$9*F'.$initialRow.'*M'.$initialRow);
                                            $objPHPExcel->getActiveSheet()->setCellValue('M'.$initialRow, 1);//default values
                                            $objPHPExcel->getActiveSheet()->setCellValue('N'.$initialRow, '=N$9*G'.$initialRow.'*O'.$initialRow);
                                            $objPHPExcel->getActiveSheet()->setCellValue('O'.$initialRow, 1);//default values
                                            $objPHPExcel->getActiveSheet()->setCellValue('P'.$initialRow, '=(H'.$initialRow.'+J'.$initialRow.'+L'.$initialRow.'+N'.$initialRow.')*P$9');
                                            $objPHPExcel->getActiveSheet()->setCellValue('Q'.$initialRow, '=(H'.$initialRow.'+J'.$initialRow.'+L'.$initialRow.'+N'.$initialRow.'+P'.$initialRow.')');
                                            $initialRow++;
                                            $counter++;
                                        }
                                    }//end of foreach companies
                                    }

                                    if (count($companies) == 1){
                                        $objPHPExcel->getActiveSheet()->setCellValue('B'.$initialRow, '' );
                                        $objPHPExcel->getActiveSheet()->setCellValue('C'.$initialRow, '');
                                        $objPHPExcel->getActiveSheet()->setCellValue('D'.$initialRow, 0);//always zero(0) on Special Rate
                                        $objPHPExcel->getActiveSheet()->setCellValue('E'.$initialRow, 0);
                                        $objPHPExcel->getActiveSheet()->setCellValue('F'.$initialRow, 0);
                                        $objPHPExcel->getActiveSheet()->setCellValue('G'.$initialRow, 0);
                                        $objPHPExcel->getActiveSheet()->setCellValue('H'.$initialRow, '=(H$9*D'.$initialRow.')/I'.$initialRow);
                                        $objPHPExcel->getActiveSheet()->setCellValue('I'.$initialRow, 1);//default values
                                        $objPHPExcel->getActiveSheet()->setCellValue('J'.$initialRow, '=J$9*E'.$initialRow.'*K'.$initialRow);
                                        $objPHPExcel->getActiveSheet()->setCellValue('K'.$initialRow, 1);//default values
                                        $objPHPExcel->getActiveSheet()->setCellValue('L'.$initialRow, '=L$9*F'.$initialRow.'*M'.$initialRow);
                                        $objPHPExcel->getActiveSheet()->setCellValue('M'.$initialRow, 1);//default values
                                        $objPHPExcel->getActiveSheet()->setCellValue('N'.$initialRow, '=N$9*G'.$initialRow.'*O'.$initialRow);
                                        $objPHPExcel->getActiveSheet()->setCellValue('O'.$initialRow, 1);//default values
                                        $objPHPExcel->getActiveSheet()->setCellValue('P'.$initialRow, '=(H'.$initialRow.'+J'.$initialRow.'+L'.$initialRow.'+N'.$initialRow.')*P$9');
                                        $objPHPExcel->getActiveSheet()->setCellValue('Q'.$initialRow, '=(H'.$initialRow.'+J'.$initialRow.'+L'.$initialRow.'+N'.$initialRow.'+P'.$initialRow.')');
                                        $initialRow++;
                                        $counter++;
                                        die;
                                    }
                                       
                                    /* 
                                    $summationRow = $startRow + $counter - 1;
                                    $endRow = $initialRow - 1;
                                    $objPHPExcel->getActiveSheet()->setCellValue('D'.$summationRow,'=SUM(D'.$startRow.':D'.$endRow.')');
                                    $objPHPExcel->getActiveSheet()->setCellValue('E'.$summationRow,'=SUM(E'.$startRow.':E'.$endRow.')');
                                    $objPHPExcel->getActiveSheet()->setCellValue('F'.$summationRow,'=SUM(F'.$startRow.':F'.$endRow.')');
                                    $objPHPExcel->getActiveSheet()->setCellValue('G'.$summationRow,'=SUM(G'.$startRow.':G'.$endRow.')');
                                    $summationTotalPax = $summationRow +1;
                                    //echo 'D'.$summationTotalPax,'=SUM(D'.$summationRow.':G'.$summationRow.')'."<br>";
                                    $objPHPExcel->getActiveSheet()->setCellValue('D'.$summationTotalPax,'=SUM(D'.$summationRow.':G'.$summationRow.')');
                                    $objPHPExcel->getActiveSheet()->setCellValue('H'.$summationTotalPax,'=SUM(H'.$startRow.':H'.$endRow.')');
                                    $objPHPExcel->getActiveSheet()->setCellValue('J'.$summationTotalPax,'=SUM(J'.$startRow.':J'.$endRow.')');
                                    $objPHPExcel->getActiveSheet()->setCellValue('L'.$summationTotalPax,'=SUM(L'.$startRow.':L'.$endRow.')');
                                    $objPHPExcel->getActiveSheet()->setCellValue('N'.$summationTotalPax,'=SUM(N'.$startRow.':N'.$endRow.')');
                                    if( isset($programBillingInfo[0]['ProgramBillingInfo']['notes']) && strlen(trim($programBillingInfo[0]['ProgramBillingInfo']['notes'])) ){
                                        $notesRow = $summationTotalPax + 2;
                                        $objPHPExcel->getActiveSheet()->setCellValue('D'.$notesRow,trim($programBillingInfo[0]['ProgramBillingInfo']['notes']));
                                    }

                                    $overallAmountRow = $summationTotalPax+1;
                                    $objPHPExcel->getActiveSheet()->setCellValue('U'.$overallAmountRow,'=(H'.$summationTotalPax.'+J'.$summationTotalPax.'+L'.$summationTotalPax.'+N'.$summationTotalPax.')');//=H24+J24+L24+N24

                                    $booksLinkRow = 64+ (count($companies)-1);
                                    $objPHPExcel->getActiveSheet()->getCell('U'.$booksLinkRow)->getHyperlink()->setUrl("sheet://'".$sheet."'!B2");


                                    $bags = $summationTotalPax+11;
                                    $objPHPExcel->getActiveSheet()->setCellValue('R'.$bags,'=PRODUCT(N'.$bags.':Q'.$bags.')');

                                    $handouts = $bags+1;
                                    $objPHPExcel->getActiveSheet()->setCellValue('R'.$handouts,'=PRODUCT(N'.$handouts.':Q'.$handouts.')');

                                    $materials = $handouts+1;
                                    $objPHPExcel->getActiveSheet()->setCellValue('U'.$materials,'=SUM(R'.$bags.':R'.$handouts.')');

                                    $transportation = $materials + 2;
                                    $objPHPExcel->getActiveSheet()->setCellValue('R'.$transportation,'=PRODUCT(N'.$transportation.':Q'.$transportation.')');

                                    $meal = $transportation+1;
                                    $objPHPExcel->getActiveSheet()->setCellValue('R'.$meal,'=PRODUCT(N'.$meal.':Q'.$meal.')');

                                    $flyer = $meal+1;
                                    $objPHPExcel->getActiveSheet()->setCellValue('R'.$flyer,'=PRODUCT(N'.$flyer.':Q'.$flyer.')');

                                    $fax = $flyer+1;
                                    $objPHPExcel->getActiveSheet()->setCellValue('R'.$fax,'=PRODUCT(N'.$fax.':Q'.$fax.')');

                                    $brochures = $fax+1;
                                    $objPHPExcel->getActiveSheet()->setCellValue('R'.$brochures,'=PRODUCT(N'.$brochures.':Q'.$brochures.')');

                                    $plastic_n_gum_labels = $brochures+1;
                                    $objPHPExcel->getActiveSheet()->setCellValue('R'.$plastic_n_gum_labels,'=PRODUCT(N'.$plastic_n_gum_labels.':Q'.$plastic_n_gum_labels.')');


                                    $postage = $plastic_n_gum_labels+1;
                                    $objPHPExcel->getActiveSheet()->setCellValue('R'.$postage,'=PRODUCT(N'.$postage.':Q'.$postage.')');

                                    $ads = $postage+1;
                                    $objPHPExcel->getActiveSheet()->setCellValue('R'.$ads,'=PRODUCT(N'.$ads.':Q'.$ads.')');

                                    $photocopy = $ads+1;
                                    $objPHPExcel->getActiveSheet()->setCellValue('R'.$photocopy,'=PRODUCT(N'.$photocopy.':Q'.$photocopy.')');

                                    $books = $photocopy+1;
                                    $objPHPExcel->getActiveSheet()->setCellValue('R'.$books,'=PRODUCT(N'.$books.':Q'.$books.')');

                                    $others = $books+1;
                                    $objPHPExcel->getActiveSheet()->setCellValue('R'.$others,'=PRODUCT(N'.$others.':Q'.$others.')');

                                    $totalOtherExpenses = $others+1;
                                    $objPHPExcel->getActiveSheet()->setCellValue('U'.$totalOtherExpenses,'=SUM(R'.$transportation.':R'.$others.')');


                                    $food_dayone = $totalOtherExpenses+2;
                                    $objPHPExcel->getActiveSheet()->setCellValue('R'.$food_dayone,'=PRODUCT(N'.$food_dayone.':Q'.$food_dayone.')');

                                    $food_daytwo = $food_dayone+1;
                                    $objPHPExcel->getActiveSheet()->setCellValue('R'.$food_daytwo,'=PRODUCT(N'.$food_daytwo.':Q'.$food_daytwo.')');

                                    $tip = $food_daytwo+1;
                                    //$objPHPExcel->getActiveSheet()->setCellValue('R'.$tip,'=PRODUCT(N'.$tip.':Q'.$tip.')');

                                    $total_meals = $tip+1;
                                    $objPHPExcel->getActiveSheet()->setCellValue('U'.$total_meals,'=SUM(R'.$food_dayone.':R'.$tip.')');


                                    $foc_dayone = $total_meals+2;
                                    $objPHPExcel->getActiveSheet()->setCellValue('R'.$foc_dayone,'=PRODUCT(N'.$foc_dayone.':Q'.$foc_dayone.')');

                                    $foc_daytwo = $foc_dayone+1;
                                    $objPHPExcel->getActiveSheet()->setCellValue('R'.$foc_daytwo,'=PRODUCT(N'.$foc_daytwo.':Q'.$foc_daytwo.')');

                                    $total_focmeals = $foc_daytwo+1;
                                    $objPHPExcel->getActiveSheet()->setCellValue('U'.$total_focmeals,'=SUM(R'.$foc_dayone.':R'.$foc_daytwo.')');


                                    $total_expenses_without_foc_meals = $total_focmeals+1;
                                    $objPHPExcel->getActiveSheet()->setCellValue('U'.$total_expenses_without_foc_meals,'=(U'.$materials.'+U'.$totalOtherExpenses.'+U'.$total_meals.')-(U'.$total_focmeals.')');

                                    $total_expenses = $total_expenses_without_foc_meals+1;
                                    $objPHPExcel->getActiveSheet()->setCellValue('U'.$total_expenses,'=(U'.$materials.'+U'.$totalOtherExpenses.'+U'.$total_meals.')');

                                    $total_revenues = $total_expenses + 3;
                                    $objPHPExcel->getActiveSheet()->setCellValue('U'.$total_revenues,'=(U'.$overallAmountRow.'-U'.$total_expenses.')');
                                    */  
                                    $objPHPExcel->getActiveSheet()->setShowGridlines(true);
                                    $excelFiles[] = clone $objPHPExcel;
                                }
                            }//end if program status === 1
                        }//end of foreach

                        $bigExcel = new PHPExcel();
                        $bigExcel->removeSheetByIndex(0);
                        $reader = PHPExcel_IOFactory::createReader('Excel5');

                        foreach ($excelFiles as $filename) {
                            foreach ($filename->getAllSheets() as $sheet) {
                                $bigExcel->addExternalSheet($sheet);
                            }
                        }

                        $writer = PHPExcel_IOFactory::createWriter($bigExcel, 'Excel5');
                        $writer->save($downloadFile);
                        headersDownloadFile('xls',$downloadFile,$sales_reports['SalesReports']['source_file']);
                    }
                }
            }
        }
        exit;
    }
    
    public function admin_index(){
    	//if( $this->Session->read('Auth.User.role_id') == Configure::read('administrator') ){
            $this->paginate['SalesReports'] = array(
                'order' => array('SalesReports.id' => 'desc'),
        	    'conditions'=>  array('SalesReports.status'=> Configure::read('status_live'))
            );
            $this->set('sales_reports',$this->paginate('SalesReports'));
        //}
        /*else{
            $salesReportsId = array();
            $this->SalesReportsDetails->contain(array('Program.id'));
            $programs = $this->SalesReportsDetails->find('all', array(
                   'conditions' => array(
                            'Program.id = SalesReportsDetails.program_id',
                            'Program.programs_division_id' => $this->Session->read('Auth.User.role_id'),
                            'Program.status' => Configure::read('status_live')		
                   ),
	           'fields' => array('SalesReportsDetails.sales_reports_id','Program.id')
                )
            );
            
            if( $programs && is_array($programs) && count($programs)>0 ){
                foreach( $programs as $program ){
                    $salesReportsId[] = intval($program['SalesReportsDetails']['sales_reports_id']);
                }
                
                $this->paginate['SalesReports'] = array(
                    'order' => array('SalesReports.id' => 'desc'),
                    'conditions'=>  array(
                        'SalesReports.status'=> Configure::read('status_live'),
                        'SalesReports.id IN ('.implode(",",$salesReportsId).')'
                    )
                );
            	$this->set('sales_reports',$this->paginate('SalesReports'));
	        }
        }*/
        
	    $types = $this->ProgramsType->find('list',array('cache' => 'ProgramsType', 'cacheConfig' => 'cache_queries'));
        $divisions = $this->ProgramsDivision->find('list',array('cache' => 'ProgramsDivision', 'cacheConfig' => 'cache_queries'));
        $this->set( compact('divisions','types') );
    }

    public function admin_add_generate_sales_reports()
    {
        Configure::write('debug',0);
        $this->autoRender = false;
        $this->layout = 'ajax';

        if( isset($this->data) &&
            $this->data['SalesReports']['id'] &&
            $this->data['SalesReports']['year'] &&
            $this->data['SalesReports']['month']
        ){
            $id = mktime(date('h'),date('i'),date('s'),date('m'),date('d'),date('Y'));
            $insertId = null;
            $data = array();
            $this->SalesReports->begin();
            $this->SalesReports->id = null;
            $data['SalesReports']['source_file'] = date('Ymd').'--'.$this->data['SalesReports']['year'].'-'.$this->data['SalesReports']['month'].'--'.$id.'-sr.xls';
            $data['SalesReports']['reference_code'] = strtoupper(generatePassword($algorithm='md5','12'));
            $data['SalesReports']['report_date'] = date("Y-m",strtotime($this->data['SalesReports']['year'].'-'.$this->data['SalesReports']['month'].'-01')).'-01';
            $data['SalesReports']['status'] = Configure::read('status_live');
            $data['SalesReports']['date_created'] = date("Y-m-d H:i:s");
            $data['SalesReports']['date_modified'] = date("Y-m-d H:i:s");
            $data['SalesReports']['who_created'] = $this->Session->read('Auth.User.id');

            if( $this->SalesReports->save($data) ){
                $this->SalesReports->commit();
                $insertId = $this->SalesReports->getLastInsertID();
            }

            foreach( $this->data['SalesReports']['id'] as $key => $program_id ){
                $dataInfo = array();
                $dataInfo['SalesReportsDetails']['sales_reports_id'] = $insertId;
                $dataInfo['SalesReportsDetails']['program_id'] = $program_id;
                $this->SalesReportsDetails->begin();
                $this->SalesReportsDetails->id = null;
                if( $this->SalesReportsDetails->save($dataInfo) ){
                    $this->SalesReportsDetails->commit();
                }
            }
            $this->Session->setFlash(__('Add sales reports successful.', true), 'default',array('class' => 'success'));
        }
        $this->redirect(array('admin'=>true,'controller'=>'sales_reports','action'=>'generate_sales_reports','?month='.$this->data['SalesReports']['month'].'&year='.$this->data['SalesReports']['year']));
        die;
    }

    public function admin_generate_sales_reports(){
        Configure::write('debug',2);

        $query = null;
        $sOrder = null;
        if( isset($this->params['url']['month']) && strlen(trim($this->params['url']['year']))> 0 ){
            $query = date("Y-m",strtotime(trim($this->params['url']['year'].'-'.trim($this->params['url']['month']))));
        }

        $conditions = array();

        if( !empty($query) && strlen($query)> 0 ){

            App::import('Sanitize');
            $schedule_date = $query;

            if( !empty($schedule_date) && $schedule_date ){
                $this->Program = &ClassRegistry::init('Program');
                $this->ProgramsSchedule = &ClassRegistry::init('ProgramsSchedule');
                $this->ProgramsSchedule->contain();
                $program_ids = $this->ProgramsSchedule->find('list',array(
                        'fields' => array( 'ProgramsSchedule.program_id' ),
                        'conditions' => array("ProgramsSchedule.start_date LIKE '%".$schedule_date."%'"),
                        'group' => array('ProgramsSchedule.program_id'), //fields to GROUP BY
                        'Program.status' => Configure::read('status_live')
                    )
                );


                $this->set('year', trim($this->params['url']['year']) );
                $this->set('month', trim($this->params['url']['month']) );

                if( $program_ids ){
                    //if( $this->Session->read('Auth.User.role_id') == Configure::read('administrator')){
                        $this->paginate['Program'] = array(
                            'conditions'=> array(
                                'Program.id IN ('.implode(",",$program_ids).')'
                            ),
                            'order' => array ('Program.title' => 'asc'),
                            'limit' => $this->paginate['limit']
                        );
                    /*}else{
                        $this->paginate['Program'] = array(
                            'conditions'=> array(
                                'Program.id IN ('.implode(",",$program_ids).')',
                                'Program.programs_division_id' => $this->Session->read('Auth.User.role_id'),
                            ),
                            'limit' => $this->paginate['limit']
                        );
                    }*/
                    $this->set('programs',$this->paginate('Program'));
                }else{
                    $this->set('programs',false);
                }

                $this->paginate['SalesReports'] = array(
                    'order' => array('SalesReports.id' => 'desc'),
                    'conditions'=>  array(
                        'SalesReports.status'=> Configure::read('status_live'),
                        "SalesReports.report_date LIKE '%".date("Y-m",strtotime($query))."-01%'"
                    )
                );
                $this->set('sales_reports',$this->paginate('SalesReports'));

                $log = $this->SalesReports->getDataSource()->getLog(false, false);
                //debug($log);

            }
        }
    }

    public function admin_search(){

        $query = null;
        $sOrder = null;
        if( isset($this->params['url']['q']) && strlen(trim($this->params['url']['q']))> 0 ){
            $query = trim($this->params['url']['q']);
        }

        $conditions = array();
        if( !empty($query) && strlen($query)> 0 ){
            App::import('Sanitize');
            $full_text_search = explode (" ", $query );
            foreach ($full_text_search as $wordindex => $wordvalue) {
                if( strlen(trim($wordvalue))==0) {
                    unset($full_text_search[$wordindex]);
                }
            }

            $fulltext_array = array();
            $like_array = array();
            $original_search = array();
            foreach ($full_text_search as $wordindex => $wordvalue) {
                if( strlen ($wordvalue)<=3 ) {
                    $like_array[] = Sanitize::clean($wordvalue);
                }else{
                    $fulltext_array[] = "+".Sanitize::clean($wordvalue)."*";
                }
            }

            $original_search = array_merge($like_array,$fulltext_array);
            $boolSearchByLike = $this->SalesReports->getLikeFullName($query);

            if( $boolSearchByLike == true){
                $conditions[] = array(
                    "(
                        UPPER(`SalesReports`.`source_file`) LIKE '%".addslashes($query)."%' AND
                        (`SalesReports`.`source_file` IS NOT NULL) AND
                        (LENGTH(TRIM(`SalesReports`.`source_file`))) > 0
                     )
                     OR
                     (
                        UPPER(`SalesReports`.`reference_code`) LIKE '%".addslashes($query)."%' AND
                        (`SalesReports`.`reference_code` IS NOT NULL) AND
                        (LENGTH(TRIM(`SalesReports`.`reference_code`))) > 0

                     )
                     AND SalesReports.status = '".intval(Configure::read('status_live'))."'
                    "
                );
                $sOrder = " TRIM(UPPER(`SalesReports`.`source_file`)) ASC,TRIM(UPPER(`SalesReports`.`reference_code`)) ASC ";
            }else{
                $fullword_query = trim(implode(' ', $original_search));
                $conditions[] = array(
                    "(
					(   MATCH(
                            SalesReports.source_file,
							SalesReports.reference_code

						) AGAINST ('".addslashes(trim($fullword_query))."' IN boolean MODE)
					) OR  TRIM(UPPER(`SalesReports`.`source_file`)) LIKE '%".addslashes($query)."%'
					  OR  TRIM(UPPER(`SalesReports`.`reference_code`)) LIKE '%".addslashes($query)."%'
				)"
                );
                $sOrder = " ";
            }

        }else{//NO SEARCH QUERIES
            $sOrder = 'SalesReports.id desc';
        }

        $schedule_date = null;
        if( isset($this->params['url']['schedule_date']) && strlen($this->params['url']['schedule_date']) > 0 ){
            $schedule_date = trim($this->params['url']['schedule_date']);
            list($month, $year)= explode("/",$schedule_date);
            $sScheduleDate = date("Y-m",strtotime($year."-".$month."-01"));
            $conditions[] = array(
                "SalesReports.report_date LIKE '%".date("Y-m",strtotime($sScheduleDate))."-01%'"
            );
            $this->set('schedule_date',date("M Y",strtotime($sScheduleDate)));
        }

        $sPage = 1;
        if( isset($this->params['url']['page']) ){
            if( is_numeric($this->params['url']['page']) ){
                $sPage = $this->params['url']['page'];
            }
        }

        $sOrder = 'SalesReports.id desc';

        $order = array();
        $recursive = 1;
        if( !empty($query) && $query ){
            $order = array($sOrder);
        }else{
            $order = array("SalesReports.id DESC");
        }

        if( $this->Session->read('Auth.User.role_id') != Configure::read('administrator')){
             /*  $conditions[] = array(
                    "Program.programs_division_id" => $this->Session->read('Auth.User.role_id'),
                    "Program.status" => Configure::read('status_live')
                );*/

            $conditions[]=array(
                'AND' => array(
                    'SalesReports.status'=> Configure::read('status_live'),

                )
            );
        }else{
            $conditions[]=array(
                'AND' => array(
                    'SalesReports.status'=> Configure::read('status_live')
                )
            );
        }

        $this->paginate['SalesReports'] = array(
            'recursive' => $recursive,
            'conditions'=> $conditions,
            'page'  => $sPage,
            'limit' => 20,
            'order' => $order,
            'group' => array('SalesReports.id')
        );

        $this->set('sales_reports',$this->paginate('SalesReports'));
        $types = $this->ProgramsType->find('all',array('cache' => 'ProgramsType', 'cacheConfig' => 'cache_queries'));
        $divisions = $this->ProgramsDivision->find('list',array('cache' => 'ProgramsDivision', 'cacheConfig' => 'cache_queries'));
        $this->set( compact('divisions','types') );
    }
}
