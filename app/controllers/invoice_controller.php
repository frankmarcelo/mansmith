<?php

class InvoiceController extends AppController {

	public $name = 'Invoice';
    public $uses = array(
    	'ProgramsNameTag',
    	'Program',
    	'User',
    	'ProgramsDivision',
    	'ProgramsType',
    	'ProgramsSpeaker',
    	'Speaker', 
    	'ProgramsRatePlan', 
    	'ProgramsSchedule',
    	'ProgramsSpeaker',
    	'MansmithProgram',
    	'Day8Program',
    	'CoachProgram',
    	'Participant',
    	'ProgramsCompanyContact',
    	'ProgramsParticipant',
    	'Invoice',
        'InvoiceDetails'
    );
    
    public $helpers = array(
    	'Program',
        'ProgramExtraInfo',
    	'Invoice'
    );
    
    public $paginate = array(
        'limit' => 20,
    	'group' => array('Invoice.program_id'),
    	'order' => array('Invoice.id' => 'DESC')
    );
    
	public function beforeFilter() {
        parent::beforeFilter();
    }

	public function getInvoiceDirectory($programId=null){
    	$oDirectoryProperty = new DirectoryProperty();
        $oDirectoryProperty->documentDirectory = Configure::read('docs_directory');
        $oDocumentDirectory = new DocumentDirectory( $oDirectoryProperty, $programId );
        return $oDocumentDirectory->getInvoiceDirectory();
    }

    public function getInvoiceType($billingTypeId=1){
        $extraDirectory = null;
        if( $billingTypeId == 1){
            $extraDirectory = 'eb';
        }elseif( $billingTypeId == 2){
            $extraDirectory = 'non-eb';
        }elseif( $billingTypeId == 3){
            $extraDirectory = 'osp';
        }elseif( $billingTypeId == 4){
            $extraDirectory = 'regular';
        }
        return $extraDirectory.DIRECTORY_SEPARATOR;
    }

    public function admin_download($downloadId=null){
        Configure::write('debug',0);
        if( isset($downloadId) && intval($downloadId) > 0 ){

    		$this->InvoiceDetails->bindModel(array(
                'hasOne' => array(
                    'Program' => array(
                        'foreignKey' => false,
                        'conditions' => array('Program.id = Invoice.program_id')
                    ),
                    'ProgramsDivision' => array(
                        'foreignKey' => false,
                        'conditions' => array('Program.programs_division_id = ProgramsDivision.id')
                    ),
                )
        	));

            $invoice_details = $this->InvoiceDetails->findById($downloadId);
            $this->autoRender = false;
            $this->layout = 'ajax';


            $downloadDirectory = $this->getInvoiceDirectory(intval($invoice_details['Invoice']['program_id'])).$this->getInvoiceType(intval($invoice_details['InvoiceDetails']['billing_type_id']));
            if( !file_exists($downloadDirectory) ){
                mkdir($downloadDirectory,0777,$recursive=true);
            }
            $downloadFile = trim($downloadDirectory.$invoice_details['InvoiceDetails']['source_file']);
            if( file_exists($downloadFile) ){
    			headersDownloadFile('rtf',$downloadFile,$invoice_details['InvoiceDetails']['source_file']);
			}else{

        		$this->ProgramsCompanyContact->bindModel(array(
        			'hasOne' => array(
        				'ParticipantsPosition' => array(
            				'foreignKey' => false,
            				'conditions' => array('ParticipantsPosition.id = Participant.participants_position_id')
        				)
        			)
        		));
            
        		$this->ProgramsCompanyContact->contain(array('Company.name','Company.fax','Participant','ParticipantsPosition.name'));
            	$primary_contacts = $this->ProgramsCompanyContact->find('first',array(
            		'conditions' => array(
            			'ProgramsCompanyContact.program_id' => intval($invoice_details['Program']['id']),
            			'ProgramsCompanyContact.company_id' => intval($invoice_details['Invoice']['company_id'])
            		)
            	));

                $programsSchedules = $this->ProgramsSchedule->find('all',
					array(
						'fields' => array('ProgramsSchedule.*'),
						'conditions' => array(
						'ProgramsSchedule.program_id' => intval($invoice_details['Program']['id'])
					),'order' => array('ProgramsSchedule.start_date'))
				);
				//

				App::import('Vendor', 'phprtf', array('file' =>'phprtf/PHPRtfLite.php'));
                if( $invoice_details['ProgramsDivision']['id'] != 3 ){//not coach
            		App::import('Lib', 'InvoiceRtf');
				}else{
					App::import('Lib', 'CoachInvoiceRtf');
				}
            	PHPRtfLite::registerAutoloader();
				$rtf = new PHPRtfLite();
				$tableBorder = new PHPRtfLite_Border(
					$rtf,//PHPRtfLite instance
					new PHPRtfLite_Border_Format(0.7,'#000000'),//left border: 2pt, green color
					new PHPRtfLite_Border_Format(0.7,'#000000'),//top border: 1pt, yellow color
					new PHPRtfLite_Border_Format(0.7,'#000000'),//right border: 2pt, red color
					new PHPRtfLite_Border_Format(0.7,'#000000')//bottom border: 1pt, blue color
				);

				$this->ProgramsParticipant->contain(array(
						'Participant.full_name',
						'Participant.last_name',
						'Participant.first_name',
						'Participant.middle_name',
						'Participant.nick_name',
						'Participant.phone',
						'Participant.email',
						'Participant.address',
						'Participant.zip_code',
						'Participant.mobile',
						'ParticipantsPosition.name',
						'Company.name',
					)
				);

				$this->ProgramsParticipant->bindModel(array(
    				'hasOne' => array(
        				'ParticipantsPosition' => array(
            				'foreignKey' => false,
            				'conditions' => array('ParticipantsPosition.id = Participant.participants_position_id')
        				)
        			),
        		));
				$aProgramsParticipant = $this->ProgramsParticipant->find('all',array(
						'conditions' => array(
							'ProgramsParticipant.program_id'=> intval($invoice_details['Program']['id']),
							'ProgramsParticipant.company_id'=> intval($invoice_details['Invoice']['company_id']),
                            'ProgramsParticipant.status' => Configure::read('status_live'),
                            'ProgramsParticipant.programs_participant_status_id' => Configure::read('status_live')
						),
						'order' => array('Participant.last_name')
					)
				);

				if( isset($aProgramsParticipant) && is_array($aProgramsParticipant) && count($aProgramsParticipant)>0 ){
                    if( $invoice_details['ProgramsDivision']['id'] != 3 ){//not coach
                        $oInvoiceRtf = new InvoiceRtf($rtf,$tableBorder,$downloadFile,$programsSchedules,$invoice_details,$aProgramsParticipant,$primary_contacts,$invoice_details['Invoice']['prepared_by']);
        			}else{
        				$oInvoiceRtf = new CoachInvoiceRtf($rtf,$tableBorder,$downloadFile,$programsSchedules,$invoice_details,$aProgramsParticipant,$primary_contacts,$invoice_details['Invoice']['prepared_by']);
        			}
        			$oInvoiceRtf->generateRtf();
					$oInvoiceRtf->save_rtf();
					headersDownloadFile('rtf',$downloadFile,$invoice_details['InvoiceDetails']['source_file']);
				}
			}
		}
	}
    
    public function admin_index(){
    	if( $this->Session->read('Auth.User.role_id') == Configure::read('administrator') ){
    		$this->paginate['Invoice'] = array(
    				'order' => array('Invoice.id' => 'desc'),
        			'conditions'=>  array(
           				'AND' => array(
        					'Program.status'=> Configure::read('status_live'),
           					'Invoice.status'=> Configure::read('status_live')
    					)
           			),
           			'group' => array('Invoice.program_id')
           	);
            $this->set('invoice',$this->paginate('Invoice'));
        }else{
        	$this->paginate['Invoice'] = array(
           			'order' => array('Invoice.id' => 'desc'),
        			'conditions'=>  array(
           							"AND" => array(
           									'Program.programs_division_id'=> $this->Session->read('Auth.User.role_id'),
           									'Program.status'=> Configure::read('status_live'),
           									'Invoice.status'=> Configure::read('status_live')
        							)
           			),
           			'group' => array('Invoice.program_id')
           	);
            $this->set('invoice',$this->paginate('Invoice'));
        }
        
		$types = $this->ProgramsType->find('list',array('cache' => 'ProgramsType', 'cacheConfig' => 'cache_queries'));
        $divisions = $this->ProgramsDivision->find('list',array('cache' => 'ProgramsDivision', 'cacheConfig' => 'cache_queries'));
        $this->set( compact('divisions','types') );
    }

    public function admin_list($searchType=null){
    	$oDivision = $this->ProgramsDivision->findByTitle($searchType);
        if( !isset($oDivision['ProgramsDivision']['id']) && ($oDivision['ProgramsDivision']['id']) == Configure::read('status_live') ) {
            $this->Session->setFlash(__('Invalid program or program might have been deleted.', true), 'default', array('class' => 'error'));
            $this->redirect('/');
        }

        if( $this->Session->read('Auth.User.role_id') != Configure::read('administrator') && $oDivision['ProgramsDivision']['id'] != $this->Session->read('Auth.User.role_id')  ){
            $this->Session->setFlash(__('Invalid program or you are not allowed to access this information.', true), 'default', array('class' => 'error'));
            $this->redirect('/');
        }

        $this->paginate['Invoice'] = array(
    			'order' => array('Invoice.id' => 'desc'),
        		'conditions'=>  array(
         			'AND' => array(
    					'Program.programs_division_id'=> intval($oDivision['ProgramsDivision']['id']),
        				'Program.status'=> Configure::read('status_live'),
           				'Invoice.status'=> Configure::read('status_live')
           			)
           		),
           		'group' => array('Invoice.program_id')
        );
        $this->set('invoice',$this->paginate('Invoice'));
        $types = $this->ProgramsType->find('list',array('cache' => 'ProgramsType', 'cacheConfig' => 'cache_queries'));
        $divisions = $this->ProgramsDivision->find('list',array('cache' => 'ProgramsDivision', 'cacheConfig' => 'cache_queries'));
        $speakers  = $this->Speaker->find('all',array('cache' => 'Speaker', 'cacheConfig' => 'cache_queries'));
        $this->set('division', strtolower($searchType) );
        $this->set( compact('divisions','types') );
    }

    public function admin_search(){

    	$query = null;
        $sOrder = null;
        if( isset($this->params['url']['q']) && strlen(trim($this->params['url']['q']))> 0 ){
        	$query = trim($this->params['url']['q']);
        }

        $conditions = array();
        if( !empty($query) && strlen($query)> 0 ){
        	App::import('Sanitize');
        	$full_text_search = explode (" ", $query );
     	   	foreach ($full_text_search as $wordindex => $wordvalue) {
				if( strlen(trim($wordvalue))==0) {
					unset($full_text_search[$wordindex]);
            	}
			}

			$fulltext_array = array();
			$like_array = array();
			$original_search = array();
			foreach ($full_text_search as $wordindex => $wordvalue) {
				if( strlen ($wordvalue)<=3 ) {
					$like_array[] = Sanitize::clean($wordvalue);
            	}else{
            	    $fulltext_array[] = "+".Sanitize::clean($wordvalue)."*";
            	}
            }

			$original_search = array_merge($like_array,$fulltext_array);
            $boolSearchByLike = $this->Invoice->getLikeFullName($query);
            if( $boolSearchByLike == true){
                $this->InvoiceDetails->contain();
                $invoice_details = $this->InvoiceDetails->find('all',array(
                    'conditions' => array(

                            "
                            (
                               TRIM(UPPER(`InvoiceDetails`.`source_file`)) LIKE '%".addslashes($query)."%' AND
                               `InvoiceDetails`.`source_file` IS NOT NULL  AND
                               LENGTH(TRIM(`InvoiceDetails`.`source_file`)) > 0
                            )
                            OR
                            (
                                TRIM(UPPER(`InvoiceDetails`.`invoice_number`)) LIKE '%".addslashes($query)."%' AND
                                `InvoiceDetails`.`invoice_number` IS NOT NULL AND
                                LENGTH(TRIM(`InvoiceDetails`.`invoice_number`)) > 0
                            )
                            OR
                            (
                                TRIM(UPPER(`InvoiceDetails`.`company`)) LIKE '%".addslashes($query)."%' AND
                                `InvoiceDetails`.`company` IS NOT NULL AND
                                LENGTH(TRIM(`InvoiceDetails`.`company`)) > 0
                            )
                            OR
                            (
                                TRIM(UPPER(`InvoiceDetails`.`recipient`)) LIKE '%".addslashes($query)."%' AND
                                `InvoiceDetails`.`recipient` IS NOT NULL AND
                                LENGTH(TRIM(`InvoiceDetails`.`recipient`)) > 0
                            )
                            OR
                            (
                                TRIM(UPPER(`InvoiceDetails`.`recipient_address`)) LIKE '%".addslashes($query)."%' AND
                                `InvoiceDetails`.`recipient_address` IS NOT NULL AND
                                LENGTH(TRIM(`InvoiceDetails`.`recipient_address`)) > 0
                            )"
                        ),
                        'group' => array('InvoiceDetails.invoice_id')
                     )
                );

                $invoice_id = array(0);
                if( count($invoice_details)>0 ){
                    foreach($invoice_details as $invoices => $invoice ){
                        $invoice_id[] = $invoice['InvoiceDetails']['invoice_id'];
                    }
                    $invoice_id = array_unique($invoice_id);
                }

                $conditions[] = array(
									"(
                                        TRIM(UPPER(`Program`.`title`)) LIKE '%".addslashes($query)."%' AND
                                        `Program`.`title` IS NOT NULL AND
                                        LENGTH(TRIM(`Program`.`title`)) > 0
                                     )
                                     OR
                                     (
                                        TRIM(UPPER(`Invoice`.`seminar`)) LIKE '%".addslashes($query)."%' AND
                                        `Invoice`.`seminar` IS NOT NULL AND
                                        LENGTH(TRIM(`Invoice`.`seminar`)) > 0
									 )
									 OR
									 (
                                        `Invoice`.id IN (".implode(",",$invoice_id).")
                                     )
									 AND Program.status = '".intval(Configure::read('status_live'))."'"
				);
				$sOrder = " TRIM(UPPER(`Program`.`title`)) ASC,TRIM(UPPER(`Invoice`.`seminar`)) ASC";
            }else{

                $fullword_query = trim(implode(' ', $original_search));
                $this->InvoiceDetails->contain();
                $invoice_details = $this->InvoiceDetails->find('all',array(
                        'conditions' => array(
                           "
                           (
                                (   MATCH(
                                        `InvoiceDetails`.invoice_number,
                                        `InvoiceDetails`.source_file,
                                        `InvoiceDetails`.company,
                                        `InvoiceDetails`.recipient,
                                        `InvoiceDetails`.recipient_position,
                                        `InvoiceDetails`.notes
                                    ) AGAINST ('".addslashes(trim($fullword_query))."' IN boolean MODE)
                                )
                                OR  TRIM(UPPER(`InvoiceDetails`.`invoice_number`))
                                OR  TRIM(UPPER(`InvoiceDetails`.`source_file`)) LIKE '%".addslashes($query)."%'
                            )
                           "
                        ),
                        'order' => array("MATCH(`InvoiceDetails`.invoice_number,`InvoiceDetails`.source_file,`InvoiceDetails`.company,`InvoiceDetails`.recipient,`InvoiceDetails`.recipient_position,`InvoiceDetails`.notes) AGAINST ('".addslashes(trim($query))."') DESC"),
                        'group' => array('InvoiceDetails.invoice_id')
                     )
                );

                $invoice_id = array(0);
                if( count($invoice_details)>0 ){
                    foreach($invoice_details as $invoices => $invoice ){
                        $invoice_id[] = $invoice['InvoiceDetails']['invoice_id'];
                    }
                    $invoice_id = array_unique($invoice_id);
                }

                $conditions[] = array(
				"
                    `Invoice`.id IN (".implode(",",$invoice_id).") AND
                    LENGTH(TRIM(`Program`.`title`))> 0 AND
                    `Program`.`status` = '".intval(Configure::read('status_live'))."'
                "
				);
				$sOrder = " TRIM(UPPER(`Program`.`title`)) ASC,TRIM(UPPER(`Invoice`.`seminar`)) ASC ";
            }

		}else{//NO SEARCH QUERIES
			$sOrder = '`Invoice`.id desc';
		}

	    if( isset($this->params['url']['type']) && strlen($this->params['url']['type']) > 0 ){
            if( strtolower($this->params['url']['type']) != 'all' ){
	    		$oProgramsType = $this->ProgramsType->findBySeoName($this->params['url']['type']);
	    		$conditions[] = array(
	    				"Program.programs_type_id" => $oProgramsType['ProgramsType']['id'],
	    				"Program.status" => Configure::read('status_live')
	    		);
	    		$this->set('program_type',$oProgramsType['ProgramsType']['name']);
            }
	    }

	    $schedule_date = null;
	    if( isset($this->params['url']['schedule_date']) && strlen($this->params['url']['schedule_date']) > 0 ){
    		$schedule_date = trim($this->params['url']['schedule_date']);
			list($month, $year)= explode("/",$schedule_date);
    		$sScheduleDate = date("Y-m",strtotime($year."-".$month."-01"));
			$conditions[] = array(
                "Invoice.schedule LIKE '%".date("F",strtotime($sScheduleDate))."%'",
                "Invoice.schedule LIKE '%".date("Y",strtotime($sScheduleDate))."%'"
            );
            $this->set('schedule_date',date("M Y",strtotime($sScheduleDate)));
    	}

    	if( isset($this->params['url']['division']) && strlen($this->params['url']['division']) > 0 ){
    		if( strtolower($this->params['url']['division']) != 'all' ){
    		  	$oDivision = $this->ProgramsDivision->findByTitle($this->params['url']['division']);
        		if( !empty($oDivision) && isset($oDivision['ProgramsDivision']['id']) ){
	        		$conditions[] = array('Program.programs_division_id'=> $oDivision['ProgramsDivision']['id']);
		    	}
            }
    	}

        if( $this->Session->read('Auth.User.role_id') != Configure::read('administrator')){
    		if( isset($this->params['url']['q']) ){
	    		$conditions[] = array("Program.programs_division_id" => $this->Session->read('Auth.User.role_id'));
    		}else{
    			$conditions = array(
	        		array("Program.programs_division_id" => $this->Session->read('Auth.User.role_id'))
	    		);
    		}
    	}

    	$sPage = 1;
    	if( isset($this->params['url']['page']) ){
    		if( is_numeric($this->params['url']['page']) ){
    			$sPage = $this->params['url']['page'];
    		}
    	}

    	$order = array();
    	$recursive = 1;
    	if( !empty($query) && $query ){
    		$order = array($sOrder);
    	}else{
            $order = array("Program.id DESC");
        }

    	$conditions[]=array(
    					'AND' => array(
        					'Program.status'=> Configure::read('status_live'),
           					'Invoice.status'=> Configure::read('status_live'),

                        )
           			);
        $this->paginate['Invoice'] = array(
    		'recursive' => $recursive,
    		'conditions'=> $conditions,
    		'page'  => $sPage,
            'limit' => 20,
    		'order' => $order,
    		'group' => array('Invoice.program_id')
        );

	    $this->set('invoice',$this->paginate('Invoice'));
		$types = $this->ProgramsType->find('all',array('cache' => 'ProgramsType', 'cacheConfig' => 'cache_queries'));
		$divisions = $this->ProgramsDivision->find('list',array('cache' => 'ProgramsDivision', 'cacheConfig' => 'cache_queries'));
        $this->set( compact('divisions','types') );
    }
    
	public function paginate($object = null, $scope = array(), $whitelist = array(), $key = null) {
      	$results = parent::paginate($object, $scope, $whitelist);
      	if ($key) {
       		$this->params['paging'][$key] = $this->params['paging'][$object];
       		unset($this->params['paging'][$object]);
      	}
		return $results;
    }
}