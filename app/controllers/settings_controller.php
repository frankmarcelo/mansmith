<?php

class SettingsController extends AppController {

    public $components = array(
        //'DebugKit.Toolbar'
    );

    public $name = 'Settings';
    public $uses = array('LatestCompany','LatestProgram','LatestParticipant','UpcomingProgram');

    public $helpers = array(
        'Program',
        'ProgramExtraInfo',
        'Company',
        'CompanyExtraInfo',
        'Participant',
        'ParticipantExtraInfo'
    );

	public function beforeFilter() {
        parent::beforeFilter();
    }

    public function admin_dashboard() {
    	$latest_programs = $this->LatestProgram->find('all',array('order'=>'LatestProgram.start_date DESC'));
    	$latest_companies = $this->LatestCompany->find('all',array('order'=>'Company.date_created DESC'));
    	$latest_participants = $this->LatestParticipant->find('all',array('order'=>'Participant.date_created DESC'));
        $upcoming_programs = $this->UpcomingProgram->find('all',array('order'=>'UpcomingProgram.schedule DESC'));
        $this->set('title_for_layout', __('Dashboard', true));
        $this->set( compact('latest_programs','latest_companies','latest_participants','upcoming_programs'));
    }
}