<?php

class BillingRegularController extends AppController {

	const billing_type_id = 4;	
    public $name = 'BillingRegular';
    public $uses = array(
    	'ProgramsNameTag',
    	'Program',
    	'User',
    	'ProgramsDivision',
    	'ProgramsType',
    	'ProgramsSpeaker',
    	'Speaker', 
    	'ProgramsRatePlan', 
    	'ProgramsSchedule',
    	'ProgramsSpeaker',
    	'MansmithProgram',
    	'Day8Program',
    	'CoachProgram',
    	'Participant',
    	'ProgramsCompanyContact',
    	'ProgramsParticipant',
    	'BillingRegular'
    );
    
    public $helpers = array(
    	'Program',
        'ProgramExtraInfo',
    	'BillingRegular'
    );
    
    public $paginate = array(
        'limit' => 20,
    	'group' => array('BillingRegular.program_id'),
    	'order' => array('BillingRegular.id' => 'DESC')
    );
    
	public function beforeFilter() {
        parent::beforeFilter();
    }

	public function getBillingRegularDirectory($programId=null){
    	$oDirectoryProperty = new DirectoryProperty();
        $oDirectoryProperty->documentDirectory = Configure::read('docs_directory');
        $oDocumentDirectory = new DocumentDirectory( $oDirectoryProperty, $programId );
        return $oDocumentDirectory->getBillingRegularDirectory();
    }

    public function admin_download($downloadId=null){
    	if( isset($downloadId) && intval($downloadId) > 0 ){
			
			$this->BillingRegular->bindModel(array(
    				'hasOne' => array(
        				'ProgramsDivision' => array(
            				'foreignKey' => false,
            				'conditions' => array('Program.programs_division_id = ProgramsDivision.id')
        				),
        			)
        	));			
			
    		$billing_regular = $this->BillingRegular->findById($downloadId);
            $this->autoRender = false;
            $this->layout = 'ajax';
            $downloadDirectory = $this->getBillingRegularDirectory($billing_regular['BillingRegular']['program_id']);
            $downloadFile = trim($downloadDirectory.$billing_regular['BillingRegular']['source_file']);

            if( file_exists($downloadFile) ){
    			headersDownloadFile('rtf',$downloadFile,$billing_regular['BillingRegular']['source_file']);
			}else{
			
            	$this->BillingRegular->bindModel(array(
    				'hasOne' => array(
        				'ProgramsDivision' => array(
            				'foreignKey' => false,
            				'conditions' => array('Program.programs_division_id = ProgramsDivision.id')
        				),
        			)
        		));
        		
        		$this->ProgramsCompanyContact->bindModel(array(
        			'hasOne' => array(
        				'ParticipantsPosition' => array(
            				'foreignKey' => false,
            				'conditions' => array('ParticipantsPosition.id = Participant.participants_position_id')
        				)
        			)
        		));
        		$this->ProgramsCompanyContact->contain(array('Company.name','Company.fax','Participant','ParticipantsPosition.name'));
            	$primary_contacts = $this->ProgramsCompanyContact->find('first',array(
            		'conditions' => array(	
            			'ProgramsCompanyContact.program_id' => intval($billing_regular['Program']['id']),
            			'ProgramsCompanyContact.company_id' => intval($billing_regular['Company']['id'])			
            		)
            	));
            	$programsSchedules = $this->ProgramsSchedule->find('all',
					array(
						'fields' => array('ProgramsSchedule.*'),
						'conditions' => array(
						'ProgramsSchedule.program_id' => intval($billing_regular['Program']['id'])
					),'order' => array('ProgramsSchedule.start_date'))
				);

                App::import('Vendor', 'phprtf', array('file' =>'phprtf/PHPRtfLite.php'));
				if( $billing_regular['ProgramsDivision']['id'] != 3 ){
            		App::import('Lib', 'BillingRegularRtf');
				}else{
					App::import('Lib', 'CoachBillingRegularRtf');
				}
				
            	PHPRtfLite::registerAutoloader();
				$rtf = new PHPRtfLite();
				$tableBorder = new PHPRtfLite_Border(
					$rtf,//PHPRtfLite instance
					new PHPRtfLite_Border_Format(0.7,'#000000'),//left border: 2pt, green color
					new PHPRtfLite_Border_Format(0.7,'#000000'),//top border: 1pt, yellow color
					new PHPRtfLite_Border_Format(0.7,'#000000'),//right border: 2pt, red color
					new PHPRtfLite_Border_Format(0.7,'#000000')//bottom border: 1pt, blue color
				);
				
				$this->ProgramsParticipant->contain(array(
						'Participant.full_name',
						'Participant.last_name',
						'Participant.first_name',
						'Participant.middle_name',
						'Participant.nick_name',
						'Participant.phone',
						'Participant.email',
						'Participant.address',
						'Participant.zip_code',
						'Participant.mobile',
						'ParticipantsPosition.name',
						'Company.name',
					)
				);
				
				$this->ProgramsParticipant->bindModel(array(
    				'hasOne' => array(
        				'ParticipantsPosition' => array(
            				'foreignKey' => false,
            				'conditions' => array('ParticipantsPosition.id = Participant.participants_position_id')
        				)
        			),
        		));
				$aProgramsParticipant = $this->ProgramsParticipant->find('all',array(
						'conditions' => array(
							'ProgramsParticipant.program_id'=> intval($billing_regular['BillingRegular']['program_id']),
							'ProgramsParticipant.company_id'=> intval($billing_regular['BillingRegular']['company_id']),
                            'ProgramsParticipant.status' => Configure::read('status_live'),
                            'ProgramsParticipant.programs_participant_status_id' => Configure::read('status_live')
						),
						'order' => array('Participant.last_name')
					)
				);
				
				if( isset($aProgramsParticipant) && is_array($aProgramsParticipant) && count($aProgramsParticipant)>0 ){
                    if( $billing_regular['ProgramsDivision']['id'] != 3 ){
        				$oBillingRegularRtf = new BillingRegularRtf($rtf,$tableBorder,$downloadFile,$programsSchedules,$billing_regular,$aProgramsParticipant,$primary_contacts);
        			}else{
        				$oBillingRegularRtf = new CoachBillingRegularRtf($rtf,$tableBorder,$downloadFile,$programsSchedules,$billing_regular,$aProgramsParticipant,$primary_contacts);
        			}
        			$oBillingRegularRtf->generateRtf();
					$oBillingRegularRtf->save_rtf();
					headersDownloadFile('rtf',$downloadFile,$billing_regular['BillingRegular']['source_file']);
				}
			}
		}
	}
    
	public function admin_index(){
    	if( $this->Session->read('Auth.User.role_id') == Configure::read('administrator') ){
    		$this->paginate['BillingRegular'] = array(
    				'order' => array('BillingRegular.id' => 'desc'),
        			'conditions'=>  array(
           				'AND' => array(
        					'Program.status'=> Configure::read('status_live'),
           					'BillingRegular.status'=> Configure::read('status_live'),
    						'BillingRegular.billing_type_id'=> constant('self::billing_type_id')
           				)
           			),
           			'group' => array('BillingRegular.program_id')
           	);
            $this->set('billing_regular',$this->paginate('BillingRegular'));
        }else{
        	$this->paginate['BillingRegular'] = array(
           			'order' => array('BillingRegular.id' => 'desc'),
        			'conditions'=>  array(
           							"AND" => array(
           									'Program.programs_division_id'=> $this->Session->read('Auth.User.role_id'),
           									'Program.status'=> Configure::read('status_live'),
           									'BillingRegular.status'=> Configure::read('status_live'),
        									'BillingRegular.billing_type_id'=> constant('self::billing_type_id')	
           							)
           			),
           			'group' => array('BillingRegular.program_id')
           	);
            $this->set('billing_regular',$this->paginate('BillingRegular'));	
        }
        
		$types = $this->ProgramsType->find('list',array('cache' => 'ProgramsType', 'cacheConfig' => 'cache_queries'));
        $divisions = $this->ProgramsDivision->find('list',array('cache' => 'ProgramsDivision', 'cacheConfig' => 'cache_queries'));
        $this->set( compact('divisions','types') );
    }

    public function admin_list($searchType=null){
    	$oDivision = $this->ProgramsDivision->findByTitle($searchType);
        if( !isset($oDivision['ProgramsDivision']['id']) && ($oDivision['ProgramsDivision']['id']) == Configure::read('status_live') ) {
            $this->Session->setFlash(__('Invalid program or program might have been deleted.', true), 'default', array('class' => 'error'));
            $this->redirect('/');
        }

        if( $this->Session->read('Auth.User.role_id') != Configure::read('administrator') && $oDivision['ProgramsDivision']['id'] != $this->Session->read('Auth.User.role_id')  ){
            $this->Session->setFlash(__('Invalid program or you are not allowed to access this information.', true), 'default', array('class' => 'error'));
            $this->redirect('/');
        }

        $this->paginate['BillingRegular'] = array(
    			'order' => array('BillingRegular.id' => 'desc'),
        		'conditions'=>  array(
         			'AND' => array(
    					'Program.programs_division_id'=> intval($oDivision['ProgramsDivision']['id']),
        				'Program.status'=> Configure::read('status_live'),
                        'BillingRegular.billing_type_id'=> constant('self::billing_type_id'),
           				'BillingRegular.status'=> Configure::read('status_live')
           			)
           		),
           		'group' => array('BillingRegular.program_id')
        );
        $this->set('billing_regular',$this->paginate('BillingRegular'));
        $types = $this->ProgramsType->find('list',array('cache' => 'ProgramsType', 'cacheConfig' => 'cache_queries'));
        $divisions = $this->ProgramsDivision->find('list',array('cache' => 'ProgramsDivision', 'cacheConfig' => 'cache_queries'));
        $speakers  = $this->Speaker->find('all',array('cache' => 'Speaker', 'cacheConfig' => 'cache_queries'));
        $this->set('division', strtolower($searchType) );
        $this->set( compact('divisions','types') );
    }

    public function admin_search(){

    	$query = null;
        $sOrder = null;
        if( isset($this->params['url']['q']) && strlen(trim($this->params['url']['q']))> 0 ){
        	$query = trim($this->params['url']['q']);
        }

        $conditions = array();
        if( !empty($query) && strlen($query)> 0 ){
        	App::import('Sanitize');
        	$full_text_search = explode (" ", $query );
     	   	foreach ($full_text_search as $wordindex => $wordvalue) {
				if( strlen(trim($wordvalue))==0) {
					unset($full_text_search[$wordindex]);
            	}
			}

			$fulltext_array = array();
			$like_array = array();
			$original_search = array();
			foreach ($full_text_search as $wordindex => $wordvalue) {
				if( strlen ($wordvalue)<=3 ) {
					$like_array[] = Sanitize::clean($wordvalue);
            	}else{
            	    $fulltext_array[] = "+".Sanitize::clean($wordvalue)."*";
            	}
            }

			$original_search = array_merge($like_array,$fulltext_array);
			$boolSearchByLike = $this->BillingRegular->getLikeFullName($query);
            if( $boolSearchByLike == true){
                $conditions[] = array(
									"((
							 TRIM(UPPER(`Program`.`title`)) LIKE '%".addslashes($query)."%' AND
							 (`Program`.`title` IS NOT NULL) AND
							 (LENGTH(TRIM(`Program`.`title`))) > 0
									 ))
									 OR
									 ((
							 TRIM(UPPER(`BillingRegular`.`seminar`)) LIKE '%".addslashes($query)."%' AND
							 (`BillingRegular`.`seminar` IS NOT NULL) AND
							 (LENGTH(TRIM(`BillingRegular`.`seminar`))) > 0
									 ))
									 OR
									 ((
							 TRIM(UPPER(`BillingRegular`.`source_file`)) LIKE '%".addslashes($query)."%' AND
							 (`BillingRegular`.`source_file` IS NOT NULL) AND
							 (LENGTH(TRIM(`BillingRegular`.`source_file`))) > 0
									 ))
									 OR
									((
							 TRIM(UPPER(`BillingRegular`.`billing_reference_code`)) LIKE '%".addslashes($query)."%' AND
							 (`BillingRegular`.`billing_reference_code` IS NOT NULL) AND
							 (LENGTH(TRIM(`BillingRegular`.`billing_reference_code`))) > 0
									 ))
                                     OR
									((
							 TRIM(UPPER(`Company`.`name`)) LIKE '%".addslashes($query)."%' AND
							 (`Company`.`name` IS NOT NULL) AND
							 (LENGTH(TRIM(`Company`.`name`))) > 0
									 ))
									 AND Program.status = '".intval(Configure::read('status_live'))."'
									 AND BillingRegular.status = '".intval(Configure::read('status_live'))."'
									"
				);
				$sOrder = " TRIM(UPPER(`Program`.`title`)) ASC,TRIM(UPPER(`BillingRegular`.`seminar`)) ASC,TRIM(UPPER(`BillingRegular`.`source_file`)) ASC,TRIM(UPPER(`BillingRegular`.`billing_reference_code`)) ASC ";
            }else{
                $fullword_query = trim(implode(' ', $original_search));
				$conditions[] = array(
				"(
					(   MATCH(
							BillingRegular.seminar,
							BillingRegular.source_file,
							BillingRegular.billing_reference_code,
							BillingRegular.company,
							BillingRegular.recipient,
							BillingRegular.recipient_position,
							BillingRegular.recipient_fax_number,
							BillingRegular.venue,
							BillingRegular.notes
						) AGAINST ('".addslashes(trim($fullword_query))."' IN boolean MODE)
					) OR  TRIM(UPPER(Program.title)) LIKE '%".addslashes($query)."%'
					  OR  TRIM(UPPER(`BillingRegular`.`seminar`))
                      OR  TRIM(UPPER(`BillingRegular`.`source_file`)) LIKE '%".addslashes($query)."%'
					  OR  TRIM(UPPER(`BillingRegular`.`billing_reference_code`)) LIKE '%".addslashes($query)."%'
				)AND(
					LENGTH(TRIM(Program.title))> 0
				)AND(
					Program.status = '".intval(Configure::read('status_live'))."'
				)"
				);
				$sOrder = "
                MATCH(
					BillingRegular.seminar,
                    BillingRegular.source_file,
                    BillingRegular.billing_reference_code,
                    BillingRegular.company,
                    BillingRegular.recipient,
                    BillingRegular.recipient_position,
                    BillingRegular.recipient_fax_number,
                    BillingRegular.venue,
                    BillingRegular.notes
				) AGAINST ('".addslashes(trim($query))."') DESC,TRIM(UPPER(Program.title)) ASC";
            }

		}else{//NO SEARCH QUERIES
			$sOrder = 'BillingRegular.id desc';
		}

	    if( isset($this->params['url']['type']) && strlen($this->params['url']['type']) > 0 ){
            if( strtolower($this->params['url']['type']) != 'all' ){
	    		$oProgramsType = $this->ProgramsType->findBySeoName($this->params['url']['type']);
	    		$conditions[] = array(
	    				"Program.programs_type_id" => $oProgramsType['ProgramsType']['id'],
	    				"Program.status" => Configure::read('status_live')
	    		);
	    		$this->set('program_type',$oProgramsType['ProgramsType']['name']);
            }
	    }

	    $schedule_date = null;
	    if( isset($this->params['url']['schedule_date']) && strlen($this->params['url']['schedule_date']) > 0 ){
    		$schedule_date = trim($this->params['url']['schedule_date']);
			list($month, $year)= explode("/",$schedule_date);
    		$sScheduleDate = date("Y-m",strtotime($year."-".$month."-01"));
			$conditions[] = array(
                "BillingRegular.schedule LIKE '%".date("F",strtotime($sScheduleDate))."%'",
                "BillingRegular.schedule LIKE '%".date("Y",strtotime($sScheduleDate))."%'"
            );
            $this->set('schedule_date',date("M Y",strtotime($sScheduleDate)));
    	}

    	if( isset($this->params['url']['division']) && strlen($this->params['url']['division']) > 0 ){
    		if( strtolower($this->params['url']['division']) != 'all' ){
    		  	$oDivision = $this->ProgramsDivision->findByTitle($this->params['url']['division']);
        		if( !empty($oDivision) && isset($oDivision['ProgramsDivision']['id']) ){
	        		$conditions[] = array('Program.programs_division_id'=> $oDivision['ProgramsDivision']['id']);
		    	}
            }
    	}

        if( $this->Session->read('Auth.User.role_id') != Configure::read('administrator')){
    		if( isset($this->params['url']['q']) ){
	    		$conditions[] = array(
	    			"Program.programs_division_id" => $this->Session->read('Auth.User.role_id'),
	    			"Program.status" => Configure::read('status_live')
	    		);
    		}else{
    			$conditions = array(
	        		array(
	        			"Program.programs_division_id" => $this->Session->read('Auth.User.role_id'),
	        			"Program.status" => Configure::read('status_live')
	        		)
	    		);
    		}
    	}

    	$sPage = 1;
    	if( isset($this->params['url']['page']) ){
    		if( is_numeric($this->params['url']['page']) ){
    			$sPage = $this->params['url']['page'];
    		}
    	}

    	$order = array();
    	$recursive = 1;
    	if( !empty($query) && $query ){
    		$order = array($sOrder);
    	}else{
            $order = array("Program.id DESC");
        }

    	$conditions[]=array(
    					'AND' => array(
        					'Program.status'=> Configure::read('status_live'),
           					'BillingRegular.status'=> Configure::read('status_live'),
                            'BillingRegular.billing_type_id'=> constant('self::billing_type_id')
           				)
           			);

    	$this->paginate['BillingRegular'] = array(
    		'recursive' => $recursive,
    		'conditions'=> $conditions,
    		'page'  => $sPage,
            'limit' => 20,
    		'order' => $order,
    		'group' => array('BillingRegular.program_id')
        );

	    $this->set('billing_regular',$this->paginate('BillingRegular'));
		$types = $this->ProgramsType->find('all',array('cache' => 'ProgramsType', 'cacheConfig' => 'cache_queries'));
		$divisions = $this->ProgramsDivision->find('list',array('cache' => 'ProgramsDivision', 'cacheConfig' => 'cache_queries'));
        $this->set( compact('divisions','types') );
    }
    
	public function paginate($object = null, $scope = array(), $whitelist = array(), $key = null) {
      	$results = parent::paginate($object, $scope, $whitelist);
      	if ($key) {
       		$this->params['paging'][$key] = $this->params['paging'][$object];
       		unset($this->params['paging'][$object]);
      	}
		return $results;
    }
}