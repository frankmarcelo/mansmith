<?php

class CompaniesController extends AppController {
    
    public $name = 'Companies';
    public $uses = array('Company','User','ProgramsDivision','CompaniesAffiliation','CompaniesIndustry','Program','Speaker');
    public $paginate = array(
        'limit' => 20,
        'order' => array('Company.name' => 'ASC')
    );

    public $helpers = array(
    	'Html',
    	'Company',
        'CompanyExtraInfo',
    	'Program',
    	'ParticipantsTitle',
    	'ParticipantsGrouping',
    	'ParticipantsPosition',
    	'ProgramExtraInfo',
    	'Participant',
    	'ParticipantExtraInfo',
        'BillingEb',
        'BillingNonEb',
        'BillingOsp',
        'BillingRegular',
        'Invoice'
    );

    public function beforeFilter() {
        parent::beforeFilter();
        
        $divisions = $this->ProgramsDivision->find('list',array('cache' => 'ProgramsDivision', 'cacheConfig' => 'cache_queries'));
        $this->CompaniesAffiliation->recursive = -1;
    	$this->CompaniesIndustry->recursive = -1;
        $company_affiliations = $this->CompaniesAffiliation->find('list',array('cache' => 'CompaniesAffiliation', 'cacheConfig' => 'cache_queries'));
        $company_industry = $this->CompaniesIndustry->find('list',array("order"=>"UPPER(TRIM(CompaniesIndustry.name)) ASC"));
        $this->set(compact('divisions','company_affiliations','company_industry'));

        
    }

	public function admin_index(){
    	$conditions[] = array( 
	        	'AND' => array (
	    			'Company.name IS NOT NULL',
        			'LENGTH(TRIM(Company.name)) > 0',
    				'Company.status = '.Configure::read('status_live')
        		)
	    );
	    
	    $this->paginate['Company'] = array(
                'conditions'=> $conditions,
                'limit' => 20,
                'order' => array("TRIM(UPPER(Company.name)) ASC") 
        );

        
        $this->set('companies',$this->paginate('Company'));	
        $this->set('title_for_layout','All Companies');
        $divisions = $this->ProgramsDivision->find('list',array('cache' => 'ProgramsDivision', 'cacheConfig' => 'cache_queries'));
        $this->set(compact('divisions'));
    }

    public function admin_create(){
        $this->cacheAction = '5 minutes';
    	$this->set('title_for_layout','Add Company');
        $this->CompaniesAffiliation->recursive = -1;
        $this->CompaniesIndustry->recursive = -1;
        $company_affiliations = $this->CompaniesAffiliation->find('list',array('cache' => 'CompaniesAffiliation', 'cacheConfig' => 'cache_queries'));
        $company_industry = $this->CompaniesIndustry->find('list',array("order"=>"UPPER(TRIM(CompaniesIndustry.name)) ASC"));
        $this->set('title_for_layout','Add Company');
        $this->set(compact('divisions','company_affiliations','company_industry'));
    }
    
	public function admin_edit( $company_id=0, $seo_name=null){
    	$seo_name = trim($seo_name);
        $company_id = ( is_numeric($company_id) && $company_id > 0 ) ? intval($company_id): 0;
    	$oCompany = $this->Company->findById($company_id);
        if( ( 
        	  strtolower($seo_name) != strtolower($oCompany['Company']['seo_name']) )|| 
        	  $oCompany['Company']['status'] != Configure::read('status_live') 
        ) {
            $this->Session->setFlash(__('Invalid company or company has been deleted.', true), 'default', array('class' => 'error'));
            $this->redirect('/');
        }
        
        $this->set('company',$oCompany);
        $this->set('title_for_layout','Update Company Information for '.ucwords(strtolower($oCompany['Company']['name'])));
        $this->CompaniesAffiliation->recursive = -1;
        $this->CompaniesIndustry->recursive = -1;
        $company_affiliations = $this->CompaniesAffiliation->find('list',array('cache' => 'CompaniesAffiliation', 'cacheConfig' => 'cache_queries'));
        $company_industry = $this->CompaniesIndustry->find('list',array("order"=>"UPPER(TRIM(CompaniesIndustry.name)) ASC"));
        $this->set(compact('company_affiliations','company_industry'));
    }
    
    public function admin_search(){

        $divisions = $this->ProgramsDivision->find('list',array('cache' => 'ProgramsDivision', 'cacheConfig' => 'cache_queries'));
        $this->CompaniesAffiliation->recursive = -1;
        $this->CompaniesIndustry->recursive = -1;
        $company_affiliations = $this->CompaniesAffiliation->find('all',array('cache' => 'CompaniesAffiliation', 'cacheConfig' => 'cache_queries'));
        $company_industry = $this->CompaniesIndustry->find('all',array('order' => array('CompaniesIndustry.name'), 'cache' => 'CompaniesIndustry', 'cacheConfig' => 'cache_queries'));
        $this->set(compact('divisions','company_affiliations','company_industry'));

    	
     	$query = null;
        $sOrder = null;
        if( isset($this->params['url']['q']) && strlen(trim($this->params['url']['q']))> 0 ){
        	$query = trim($this->params['url']['q']);
        }
        
        $conditions = array();
        if( !empty($query) && strlen($query)> 0 ){
        	
        	App::import('Sanitize');
        	
 	       	$full_text_search = explode (" ", $query );
     	   	foreach ($full_text_search as $wordindex => $wordvalue) {
				if( strlen(trim($wordvalue))==0) {
					unset($full_text_search[$wordindex]);
            	}
			}
    
			$fulltext_array = array();
			$like_array = array();
			$original_search = array();
			foreach ($full_text_search as $wordindex => $wordvalue) {
				if( strlen ($wordvalue)<=3 ) {
					$like_array[] = Sanitize::clean($wordvalue);
            	}else{
            	    $fulltext_array[] = "+".Sanitize::clean($wordvalue)."*";
            	}
            }
        	
			$original_search = array_merge($like_array,$fulltext_array);
			$boolSearchByLike = $this->Company->getLikeFullName($query);
        	if( $boolSearchByLike == true){
        		$conditions[] = array(
	                           		"(TRIM(UPPER(Company.name))) LIKE '%".addslashes($query)."%'",
        							"Company.name IS NOT NULL",
        							"LENGTH(TRIM(Company.name)) > 0",			
	        						"Company.status"=> Configure::read('status_live')
	        	);
	        	$sOrder = "TRIM(UPPER(Company.name)) ASC";
	        	
        	}else{
        		
        		$fullword_query = trim(implode(' ', $original_search));
        		$conditions[] = array(
	        					"( 
									(MATCH(Company.name,Company.email,Company.primary_bldg_name,Company.primary_street,Company.primary_suburb,Company.primary_city,Company.secondary_bldg_name,Company.secondary_street,Company.secondary_suburb,Company.secondary_city,Company.notes,Company.primary_phone,Company.mobile,Company.fax,Company.website) AGAINST ('".addslashes(trim($fullword_query))."' IN boolean MODE)) OR 
	                       			(TRIM(UPPER(Company.name))) LIKE '%".addslashes($query)."%'
	                       		)",
	        					"( Company.name IS NOT NULL )",
        						"( LENGTH(TRIM(Company.name))> 0 )",
	        					"Company.status"=> Configure::read('status_live')
	        	);
	        	$sOrder = "MATCH(Company.name,Company.email,Company.primary_bldg_name,Company.primary_street,Company.primary_suburb,Company.primary_city,Company.secondary_bldg_name,Company.secondary_street,Company.secondary_suburb,Company.secondary_city,Company.notes,Company.primary_phone,Company.mobile,Company.fax,Company.website) AGAINST ('".addslashes(trim($query))."') DESC,TRIM(UPPER(Company.name)) ASC";
	        	
        	}
	    }else{
            $conditions[] = array(
            					"( Company.name IS NOT NULL )",
        						"( LENGTH(TRIM(Company.name))> 0 )",
            					"Company.status" => Configure::read('status_live')
            				);
        }
        
      	if( isset($this->params['url']['affiliation']) && strlen($this->params['url']['affiliation']) > 0 ){
            if( strtolower($this->params['url']['affiliation']) != 'all' ){
	    		$oCompaniesAffiliation = $this->CompaniesAffiliation->findBySeoName($this->params['url']['affiliation']);
	    		$conditions[] = array(
	    			"( Company.name IS NOT NULL )",
        			"( LENGTH(TRIM(Company.name))> 0 )",
	    			"Company.companies_affiliation_id" => $oCompaniesAffiliation['CompaniesAffiliation']['id'],
	    			"Company.status" => Configure::read('status_live')
	    		);
	    		$this->set('affiliation',$oCompaniesAffiliation['CompaniesAffiliation']['name']);
            }
	    }
	    
    	if( isset($this->params['url']['industry']) && strlen($this->params['url']['industry']) > 0 ){
            if( strtolower($this->params['url']['industry']) != 'all' ){
	    		$oCompaniesIndustry = $this->CompaniesIndustry->findBySeoName($this->params['url']['industry']);
	    		$conditions[] = array(
	    			"( Company.name IS NOT NULL )",
        			"( LENGTH(TRIM(Company.name))> 0 )",
	    			"Company.companies_industry_id" => $oCompaniesIndustry['CompaniesIndustry']['id'],
	    			"Company.status" => Configure::read('status_live')
	    		);
	    		$this->set('industry',$oCompaniesIndustry['CompaniesIndustry']['name']);
            }
	    }
	    
    	$sPage = 1;
    	if( isset($this->params['url']['page']) ){
    		if( is_numeric($this->params['url']['page']) ){
    			$sPage = $this->params['url']['page'];
    		}
    	}
    	
    	$order = array();
    	$recursive = 1;
    	if( !empty($query) && $query ){
    		$order = array($sOrder);
    	}else{
            $order = array( "TRIM(UPPER(Company.name)) ASC" );
        }
    	
        $this->paginate['Company'] = array(
    				'recursive' => $recursive,
    				'conditions'=> $conditions,
    				'page'  => $sPage,
                    'limit' => 20,
    				'order' => $order
        );
        $this->set('title_for_layout','Search Companies');	
    	$this->set('companies',$this->paginate('Company'));
	}

    public function admin_employees($company_id,$page){
        Configure::write('debug',0);
        if( intval($company_id) > 0 ){
            $this->layout = 'iframe';
            if( isset($page) && intval($page)>0 && is_numeric($page) ){
                $sPage = intval($page);
            }else{
                $sPage = 1;
            }
            $order = 'TRIM(UPPER(`Participant`.`full_name`)) ASC';
            $conditions = array( 
                'Participant.company_id' => intval($company_id) ,
                'Participant.status' => Configure::read('status_live')
            );
            $this->Participants = &ClassRegistry::init('Participants');
            $this->paginate['Participant'] = array(
    				'conditions'=> $conditions,
    				'page'  => $sPage,
                    'limit' => $this->paginate['limit'],
    				'order' => $order
            );

            $this->set('participants',$this->paginate('Participant'));
            
        }else{
            $this->redirect('/');
        }
    }

    public function admin_billing_eb($company_id,$page){
        $this->layout = 'iframe';
        Configure::write('debug',0);

        if( intval($company_id) >0 ){
            $this->BillingEb = &ClassRegistry::init('BillingEb');
            $this->paginate['BillingEb'] = array(
    				'order' => array('BillingEb.id' => 'desc'),
        			'conditions'=>  array(
           				'AND' => array(
        					'Program.status'=> Configure::read('status_live'),
           					'BillingEb.status'=> Configure::read('status_live'),
    						'BillingEb.billing_type_id'=> 1,
                            'BillingEb.company_id' => intval($company_id)
           				)
           			),
           			'limit' => $this->paginate['limit'],
           			'group' => array('BillingEb.program_id')
           	);
            $this->set(compact('company_id'));
            $this->set('billing_eb',$this->paginate('BillingEb'));
        }else{
            $this->redirect('/');
        }
    }

    public function admin_billing_non_eb($company_id,$page){
        $this->layout = 'iframe';
        Configure::write('debug',0);

        if( intval($company_id) >0 ){
            $this->BillingNonEb = &ClassRegistry::init('BillingNonEb');
            $this->paginate['BillingNonEb'] = array(
    				'order' => array('BillingNonEb.id' => 'desc'),
        			'conditions'=>  array(
           				'AND' => array(
        					'Program.status'=> Configure::read('status_live'),
           					'BillingNonEb.status'=> Configure::read('status_live'),
    						'BillingNonEb.billing_type_id'=> 2,
                            'BillingNonEb.company_id' => intval($company_id)
           				)
           			),
           			'limit' => $this->paginate['limit'],
           			'group' => array('BillingNonEb.program_id')
           	);
            $this->set(compact('company_id'));
            $this->set('billing_non_eb',$this->paginate('BillingNonEb'));
        }else{
            $this->redirect('/');
        }
    }

    public function admin_billing_osp($company_id,$page){
        $this->layout = 'iframe';
        Configure::write('debug',0);

        if( intval($company_id) >0 ){
            $this->BillingOsp = &ClassRegistry::init('BillingOsp');
            $this->paginate['BillingOsp'] = array(
    				'order' => array('BillingOsp.id' => 'desc'),
        			'conditions'=>  array(
           				'AND' => array(
        					'Program.status'=> Configure::read('status_live'),
           					'BillingOsp.status'=> Configure::read('status_live'),
    						'BillingOsp.billing_type_id'=> 3,
                            'BillingOsp.company_id' => intval($company_id)
           				)
           			),
           			'limit' => $this->paginate['limit'],
           			'group' => array('BillingOsp.program_id')
           	);
            $this->set(compact('company_id'));
            $this->set('billing_osp',$this->paginate('BillingOsp'));
        }else{
            $this->redirect('/');
        }
    }

    public function admin_billing_regular($company_id,$page){
        $this->layout = 'iframe';
        Configure::write('debug',0);

        if( intval($company_id) >0 ){
            $this->BillingRegular = &ClassRegistry::init('BillingRegular');
            $this->paginate['BillingRegular'] = array(
    				'order' => array('BillingRegular.id' => 'desc'),
        			'conditions'=>  array(
           				'AND' => array(
        					'Program.status'=> Configure::read('status_live'),
           					'BillingRegular.status'=> Configure::read('status_live'),
    						'BillingRegular.billing_type_id'=> 4,
                            'BillingRegular.company_id' => intval($company_id)
           				)
           			),
           			'limit' => $this->paginate['limit'],
           			'group' => array('BillingRegular.program_id')
           	);
            $this->set(compact('company_id'));
            $this->set('billing_regular',$this->paginate('BillingRegular'));
        }else{
            $this->redirect('/');
        }
    }

    public function admin_invoices($company_id,$page){
        $this->layout = 'iframe';
        Configure::write('debug',0);

        if( intval($company_id) >0 ){
            $this->Invoice = &ClassRegistry::init('Invoice');
            $this->paginate['Invoice'] = array(
    				'order' => array('Invoice.id' => 'desc'),
        			'conditions'=>  array(
           				'AND' => array(
        					'Program.status'=> Configure::read('status_live'),
           					'Invoice.status'=> Configure::read('status_live'),
    						'Invoice.company_id' => intval($company_id)
           				)
           			),
           			'limit' => $this->paginate['limit'],
           			'group' => array('Invoice.program_id')
           	);
            $this->set(compact('company_id'));
            $this->set('invoice',$this->paginate('Invoice'));
        }else{
            $this->redirect('/');
        }
    }

    public function admin_attended_programs($company_id,$page){
        $speakers = $this->Speaker->find('all',array('cache' => 'Speaker', 'cacheConfig' => 'cache_queries'));
        Cache::write('speakers', $speakers,'short');
        $this->set( compact('speakers') );
        Configure::write('debug',2);
        if( intval($company_id) > 0 ){
            $this->layout = 'iframe';
            if( isset($page) && intval($page)>0 && is_numeric($page) ){
                $sPage = intval($page);
            }else{
                $sPage = 1;
            }
            $conditions = array(
                'ProgramsParticipant.company_id' => intval($company_id) ,
                'ProgramsParticipant.status' => Configure::read('status_live') ,
                'Program.status' => Configure::read('status_live')
            );
            $group = array('ProgramsParticipant.program_id');
            $order = array('TRIM(UPPER(`Program`.`title`)) ASC');
            $this->ProgramsParticipant = &ClassRegistry::init('ProgramsParticipant');
            $this->ProgramsParticipant->contain(array('Program.id','Program.programs_type_id','Program.programs_division_id','Program.status'));
            $program_participants = $this->ProgramsParticipant->find("all",array(
                    'conditions' => $conditions,
                    'order' => $order,
                    'group' => $group
                )
            );

            $programId = array();
            if( count($program_participants) > 0 ){
                foreach( $program_participants as $program_participant ){
                    $programId[] = intval($program_participant['Program']['id']);
                }
            }
            $programId = array_unique($programId);

            if( count($programId)>0  ){
                $this->Program = &ClassRegistry::init('Program');
                $this->paginate['Program'] = array(
           			'order' => array('Program.id' => 'desc'),
        			'conditions'=>  array(
           							"AND" => array(
           									'Program.id IN ('.implode(",",$programId).')',
           									'Program.status'=> Configure::read('status_live')
           							)
           			),
           			'limit' => $this->paginate['limit']
                );
                $programs  = $this->paginate('Program');
                $this->set(compact('programs'));
            }
            $sPage = 1;
            if( isset($this->params['url']['page']) ){
                if( is_numeric($this->params['url']['page']) ){
                    $sPage = $this->params['url']['page'];
                }
            }
            $order = 'TRIM(UPPER(`Participant`.`full_name`)) ASC';
            $conditions = array( 'company_id' => intval($company_id) );
            $this->Participants = &ClassRegistry::init('Participants');
            $this->paginate['Participant'] = array(
    				'conditions'=> $conditions,
    				'page'  => $sPage,
                    'order' => $order
            );

            $this->set('participants',$this->paginate('Participant'));

        }else{
            $this->redirect('/');
        }
    }
	
	public function admin_info( $company_id=0, $seo_name=null ){
    	
        $seo_name = trim($seo_name);
        $company_id = ( is_numeric($company_id) && $company_id > 0 ) ? intval($company_id): 0;
        $this->Company->contain(array('ProgramsDivision','CompaniesAffiliation','CompaniesIndustry'));
        $oCompany = $this->Company->findById($company_id);
        if( (strtolower($seo_name) != strtolower($oCompany['Company']['seo_name'])) || $oCompany['Company']['status'] != Configure::read('status_live') ) {
            $this->Session->setFlash(__('Invalid company or company has been deleted.', true), 'default', array('class' => 'error'));
            $this->redirect('/');
        }
        
        $this->set('title_for_layout','Company Information for '.ucwords(strtolower($oCompany['Company']['name'])));
        $this->set('company',$oCompany);
    }

    public function paginate($object = null, $scope = array(), $whitelist = array(), $key = null) {
      	$results = parent::paginate($object, $scope, $whitelist);
      	if ($key) {
       		$this->params['paging'][$key] = $this->params['paging'][$object];
       		unset($this->params['paging'][$object]);
      	}
		return $results;
    }
}