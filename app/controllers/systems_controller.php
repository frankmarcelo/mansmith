<?php

class SystemsController extends AppController {

    public $name = 'Systems';
    public $uses = array('BackUps','ParticipantsGroupingDumps','ParticipantsGrouping','Participant','ParticipantsPosition','CompaniesAffiliation','CompaniesIndustry');

    public $helpers = array(
        'Program',
        'ProgramExtraInfo',
        'Company',
        'CompanyExtraInfo',
        'Participant',
        'ParticipantExtraInfo'
    );

    public function beforeFilter() {
        parent::beforeFilter();
    }

    public function admin_index(){
        $this->set('title_for_layout','Manage Look Ups');
        Configure::write('debug',2);
        $this->ParticipantsGrouping->contain();
        $this->CompaniesAffiliation->contain();
        $this->CompaniesIndustry->contain(); 
    
        $participantsGrouping = $this->ParticipantsGrouping->find('list', array('order' => array('name' => 'ASC')));
        $companiesAffiliation = $this->CompaniesAffiliation->find('list', array('order' => array('name' => 'ASC')));
        $companiesIndustry = $this->CompaniesIndustry->find('list', array('order' => array('name' => 'ASC')));
        $this->set(compact('participantsGrouping', 'companiesAffiliation', 'companiesIndustry'));
    }

    public function admin_add_company_industry() {
        Configure::write('debug', 2);
    }

    public function admin_add_participants_grouping() {
        Configure::write('debug', 2);
    }

    public function admin_add_company_affiliation() {
        Configure::write('debug', 2);
    }

    public function admin_generate_participants_grouping() {
        Configure::write('debug', 0);
	    $groupingId = isset($this->params['url']['grouping_id']) ? $this->params['url']['grouping_id'] : false;
        if ($this->RequestHandler->isGet() && intval($groupingId) > 0){
            $this->ParticipantsGrouping->contain();
            $participantsGrouping = $this->ParticipantsGrouping->findById($groupingId);
            $strFile = date('Ymd-His').'--'.$participantsGrouping['ParticipantsGrouping']['seo_name'].'--gd.csv';
            $this->ParticipantsGroupingDumps->create();
            $this->data['ParticipantsGroupingDumps']['participants_grouping_id'] = intval($groupingId);
            $this->data['ParticipantsGroupingDumps']['source_file'] = $strFile;
            $this->data['ParticipantsGroupingDumps']['date_created'] = date("Y-m-d H:i:s");
            $this->data['ParticipantsGroupingDumps']['date_modified'] = date("Y-m-d H:i:s");
            $this->data['ParticipantsGroupingDumps']['who_created'] = $this->Session->read('Auth.User.id');
            if ($this->ParticipantsGroupingDumps->save($this->data)) {
                $this->Session->setFlash(__('Participant grouping save Successful', true), 'default', array('class' => 'success'));
            }
        }
        $this->redirect(array('controller'=>'systems',"action"=>"data_dumps"));
        exit;
    }

    private function getDataDumpDirectory(){
        $oDirectoryProperty = new DirectoryProperty();
        $oDirectoryProperty->documentDirectory = Configure::read('docs_directory').DIRECTORY_SEPARATOR.'data_dumps'.DIRECTORY_SEPARATOR.'groupings'.DIRECTORY_SEPARATOR;
        return $oDirectoryProperty;
    }

    public function admin_download_data_dumps($downloadId=null){
        if( isset($downloadId) && intval($downloadId) > 0 ){
            $participantGroupingDump = $this->ParticipantsGroupingDumps->findById($downloadId);
            $this->autoRender = false;
            $this->layout = 'ajax';
            $downloadDirectory = $this->getDataDumpDirectory()->documentDirectory;
            $downloadFile = trim($downloadDirectory.$participantGroupingDump['ParticipantsGroupingDumps']['source_file']);

            if( file_exists($downloadFile) ){
                unlink($downloadFile);
            }

            if( file_exists($downloadFile) ){
                headersDownloadFile('xls',$downloadFile,$participantGroupingDump['ParticipantsGroupingDumps']['source_file']);
            }else{
 
                App::import('Vendor', 'PHPExcel', array('file' =>'PHPExcel/PHPExcel.php'));
                $objPHPExcel = new PHPExcel();//start phpExcel
                $objPHPExcel->getProperties()->setCreator($this->Session->read('Auth.User.name'));
                $objPHPExcel->setActiveSheetIndex(0);//always zero
                $objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri (Body)');//set font size
                $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');

                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(60);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(60);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(60);
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(60);
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(60);
                $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
                $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);

                $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Email');
                $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Last Name');
                $objPHPExcel->getActiveSheet()->setCellValue('C1', 'First Name');
                $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Middle Name');
                $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Nick Name');
                $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Company');
                $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Position');
                $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Participants Grouping');
                $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Address');
                $objPHPExcel->getActiveSheet()->setCellValue('J1', 'Telephone');
                $objPHPExcel->getActiveSheet()->setCellValue('K1', 'Mobile');
                $sheet = str_replace(array(".xls",".csv"),array("",""),$participantGroupingDump['ParticipantsGroupingDumps']['source_file']);
                // Create a first sheet, representing sales data

                $objPHPExcel->getActiveSheet()->setTitle($sheet);

                /* Create a new workbook with just the supplied sheet */
                $participants = $this->Participant->find('all',array(
                    'conditions' => array(
                        'Participant.participants_grouping_id' => $participantGroupingDump['ParticipantsGroupingDumps']['participants_grouping_id'],
                        'Participant.status' => Configure::read('status_live')
                    ),
                    'order' => array('Participant.last_name')
                ));

                if( $participants ){
                    $rowIndex = 2;
                    foreach( $participants as $participant ){
                        //email, last , first , middle name, nick, company, position,grouping,address,telephone,mobile
                        $participant['Participant']['email'] = (strlen($participant['Participant']['email'])>0) ? trim($participant['Participant']['email']) : ' ';
                        $participant['Participant']['last_name'] = (strlen($participant['Participant']['last_name'])>0) ? trim($participant['Participant']['last_name']) : ' ';
                        $participant['Participant']['first_name'] = (strlen($participant['Participant']['first_name'])>0) ? trim($participant['Participant']['first_name']) : ' ';
                        $participant['Participant']['middle_name'] = (strlen($participant['Participant']['middle_name'])>0) ? trim($participant['Participant']['middle_name']) : ' ';
                        $participant['Participant']['nick_name'] = (strlen($participant['Participant']['nick_name'])>0) ? trim($participant['Participant']['nick_name']) : ' ';
                        $participant['Company']['name'] = (strlen($participant['Company']['name'])>0) ? trim($participant['Company']['name']) : ' ';
                        $participant['ParticipantsPosition']['name'] = (strlen($participant['ParticipantsPosition']['name'])>0) ? trim($participant['ParticipantsPosition']['name']) : ' ';
                        $participant['ParticipantsGrouping']['name'] = (strlen($participant['ParticipantsGrouping']['name'])>0) ? trim($participant['ParticipantsGrouping']['name']) : ' ';
                        $participant['Participant']['address'] = (strlen($participant['Participant']['address'])>0) ? trim($participant['Participant']['address']) : ' ';
                        $participant['Participant']['phone'] = (strlen($participant['Participant']['phone'])>0) ? trim($participant['Participant']['phone']) : ' ';
                        $participant['Participant']['mobile'] = (strlen($participant['Participant']['mobile'])>0) ? trim($participant['Participant']['mobile']) : ' ';

                        $objPHPExcel->getActiveSheet()->setCellValue('A'.$rowIndex, $participant['Participant']['email'] );
                        $objPHPExcel->getActiveSheet()->setCellValue('B'.$rowIndex, $participant['Participant']['last_name'] );
                        $objPHPExcel->getActiveSheet()->setCellValue('C'.$rowIndex, $participant['Participant']['first_name'] );
                        $objPHPExcel->getActiveSheet()->setCellValue('D'.$rowIndex, $participant['Participant']['middle_name'] );
                        $objPHPExcel->getActiveSheet()->setCellValue('E'.$rowIndex, $participant['Participant']['nick_name'] );
                        $objPHPExcel->getActiveSheet()->setCellValue('F'.$rowIndex, $participant['Company']['name'] );
                        $objPHPExcel->getActiveSheet()->setCellValue('G'.$rowIndex, $participant['ParticipantsPosition']['name'] );
                        $objPHPExcel->getActiveSheet()->setCellValue('H'.$rowIndex, $participant['ParticipantsGrouping']['name'] );
                        $objPHPExcel->getActiveSheet()->setCellValue('I'.$rowIndex, $participant['Participant']['address'] );
                        $objPHPExcel->getActiveSheet()->setCellValue('J'.$rowIndex, $participant['Participant']['phone'] );
                        $objPHPExcel->getActiveSheet()->setCellValue('K'.$rowIndex, $participant['Participant']['mobile'] );
                        $rowIndex++;
                    }

                    $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                    $writer->save($downloadFile);
                    headersDownloadFile('xls',$downloadFile,$participantGroupingDump['ParticipantsGroupingDumps']['source_file']);
                }
            }
        }
        die;
    }

    public function admin_data_dumps(){
        $this->set('title_for_layout','Manage Data Dumps');
        $this->paginate['ParticipantsGroupingDumps'] = array(
            'order' => array('ParticipantsGroupingDumps.date_created'=>'DESC')
        );

        $participantsGroupingDumps  = $this->paginate('ParticipantsGroupingDumps');
        $this->ParticipantsGrouping->contain();
        $participantsGrouping = $this->ParticipantsGrouping->find('all',
            array(
                'order' => array('ParticipantsGrouping.name'=>'ASC')
            )
        );
        $this->set(compact('participantsGroupingDumps','participantsGrouping'));
    }

    public function admin_backups(){
        /*
 * #!/usr/bin/perl

use strict;
use Tochi::Utils::Date;

my $oD = new Tochi::Utils::Date;

my $sStorageDir = '/home/mmuser/';
my $sDumpFile   = $sStorageDir.$oD->FgetDate('mon').'-mansmith-ems-dbdumps.dat';
my $sZIPFile    = $sStorageDir.$oD->FgetDate('mon').'-mansmith-ems-dbdumps.dat.zip';

system("mysqldump -e -u mmdbuser --password=mmdbus3r db_mm >> $sDumpFile");
system("zip $sZIPFile $sDumpFile");
system("rm -f $sDumpFile");
 *
 */
        $this->set('title_for_layout','Manage Back Ups');
        $this->paginate['BackUps'] = array(
            'order' => array('BackUps.source_file'=>'DESC')
        );
        $this->set('back_ups',$this->paginate('BackUps'));
    }
}
