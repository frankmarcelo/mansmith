<?php

class ParticipantsController extends AppController {

    public $name = 'Participants';
    public $uses = array(
        'Participant',
        'Company',
        'ProgramsParticipant',
        'User',
        'ProgramsDivision',
        'ParticipantsGrouping',
        'ParticipantsTitle',
        'ParticipantsPosition',
        'Speaker'
    );
    
    public $paginate = array(
        'limit' => 20,
        'order' => array('Participant.first_name' => 'ASC')
    );
        
    public $helpers = array(
        'Program',
    	'ProgramExtraInfo',
    	'Participant',
        'ParticipantExtraInfo',
    	'Company',
        'AttendanceSheets',
        'Certificates',
        'ParticipantDirectory',
        'NameTags'
    );

    public function beforeFilter() {
        parent::beforeFilter();
    }

    public function admin_index(){
    	$conditions[] = array( 
            'Participant.status'=>Configure::read('status_live'),
            'LENGTH(Participant.full_name)>0'
        );
    	        
        $this->paginate['Participant'] = array(
            'conditions'=> $conditions,
            'limit' => 20,
            'order' => array("TRIM(UPPER(Participant.full_name)) ASC") 
        );

        $divisions = $this->ProgramsDivision->find('list');
        $this->set('participants',$this->paginate('Participant'));
        $this->set(compact('divisions'));
    }
    
    public function admin_create(){
        $this->cacheAction = '5 minutes';
        if( isset($this->params['url']['company']) && strlen(trim($this->params['url']['company']))> 0 ){
    		$seo_name = trim($this->params['url']['company']);
    	 	App::import('Sanitize');
        	list( $companyId, $companySeo ) = explode("/", $seo_name );
        	if( intval($companyId) > 0 && strlen(trim($companySeo)) > 0 ){
        		$this->Company->contain();
        		$oCompany = $this->Company->findById($companyId);
        		if( strtolower(trim($companySeo)) == strtolower(trim($oCompany['Company']['seo_name'])) &&
        		    $oCompany['Company']['status'] == Configure::read('status_live') ) {
        		    	
            		$this->set('companies', $oCompany['Company']['name']);
                    $this->set('company_id', $oCompany['Company']['id']);
        		}
        	}
        }
        
    	
    	$this->ParticipantsTitle->recursive = -1;
    	$this->ParticipantsTitle->find('list');
    	
    	$this->ParticipantsPosition->recursive = -1;
    	$participant_position = $this->ParticipantsPosition->find('list',
    		array('order'=>'TRIM(UPPER(ParticipantsPosition.name)) ASC')
    	);
    	
    	$this->ParticipantsTitle->recursive = -1;
    	$participant_title = $this->ParticipantsTitle->find('list',
    		array('order'=>'TRIM(UPPER(ParticipantsTitle.name)) ASC')
    	);
    	
    	$this->ParticipantsGrouping->recursive = -1;
    	$participant_grouping = $this->ParticipantsGrouping->find('list',
    		array('order'=>'TRIM(UPPER(ParticipantsGrouping.name)) ASC')
    	);
    	
    	$this->ProgramsDivision->recursive = -1;
    	$divisions = $this->ProgramsDivision->find('list');
    		
		$this->set(compact('divisions','participant_title','participant_grouping','participant_position'));    	
    }
    
    public function admin_edit( $participant_id=0, $seo_name=null){
    	$seo_name = trim($seo_name);
        $participant_id = ( is_numeric($participant_id) && $participant_id > 0 ) ? intval($participant_id): 0;
    	$oParticipant = $this->Participant->findById($participant_id);
        if( ( 
        	  strtolower($seo_name) != strtolower($oParticipant['Participant']['seo_name']) )|| 
        	  $oParticipant['Participant']['status'] != Configure::read('status_live') 
        ) {
            $this->Session->setFlash(__('Invalid participant or participant has been deleted.', true), 'default', array('class' => 'error'));
            $this->redirect('/');
        }
        
        $this->set('participant', $oParticipant);
        
        $this->ParticipantsTitle->find('list');
    	$this->ParticipantsPosition->contain();
    	$participant_position = $this->ParticipantsPosition->find('list',
    		array('order'=>'TRIM(UPPER(ParticipantsPosition.name)) ASC')
    	);
    	
    	$this->ParticipantsTitle->contain();
    	$participant_title = $this->ParticipantsTitle->find('list',
    		array('order'=>'TRIM(UPPER(ParticipantsTitle.name)) ASC')
    	);
    	
    	$this->ParticipantsGrouping->contain();
    	$participant_grouping = $this->ParticipantsGrouping->find('list',
    		array('order'=>'TRIM(UPPER(ParticipantsGrouping.name)) ASC')
    	);
    	
    	$divisions = $this->ProgramsDivision->find('list');
    	$this->set(compact('divisions','participant_title','participant_grouping','participant_position'));
    }
   
    public function admin_search(){
    	
    	$divisions = $this->ProgramsDivision->find('list');
    	$this->ParticipantsGrouping->recursive = -1;
    	$participantsgrouping = $this->ParticipantsGrouping->find('all',array("order"=>"UPPER(TRIM(ParticipantsGrouping.name)) ASC"));
    	$this->set(compact('divisions','participantsgrouping'));
        
        $query = null;
        $sOrder = null;
        if( isset($this->params['url']['q']) && strlen(trim($this->params['url']['q']))> 0 ){
        	$query = trim($this->params['url']['q']);
        }
        
        $conditions = array();
        
        if( !empty($query) && strlen($query)> 0 ){
        	App::import('Sanitize');
        	
 	       	$full_text_search = explode (" ", $query );
     	   	foreach ($full_text_search as $wordindex => $wordvalue) {
				if( strlen(trim($wordvalue))==0) {
					unset($full_text_search[$wordindex]);
            	}
			}
    
			$fulltext_array = array();
			$like_array = array();
			$original_search = array();
			foreach ($full_text_search as $wordindex => $wordvalue) {
				if( strlen ($wordvalue)<=3 ) {
					$like_array[] = Sanitize::clean($wordvalue);
            	}else{
            	    $fulltext_array[] = "+".Sanitize::clean($wordvalue)."*";
            	}
            }
        	
			$original_search = array_merge($like_array,$fulltext_array);
			$boolSearchByLike = $this->Participant->getLikeFullName($query);
        	if( $boolSearchByLike == true){
        		$conditions[] = array(
	                           		"(TRIM(UPPER(Participant.full_name))) LIKE '%".addslashes($query)."%'",
	        						"(LENGTH(TRIM(Participant.full_name)) > 0 || LENGTH(TRIM(Participant.email)) > 0)",	
	        						"Participant.status"=> Configure::read('status_live')
	        	);
	        	$sOrder = "TRIM(UPPER(Participant.full_name)) ASC";
	        	
        	}else{
        		
        		$fullword_query = trim(implode(' ', $original_search));
				$conditions[] = array(
	        					"( 
	                       			(MATCH(Participant.full_name,Participant.nick_name,Participant.email,Participant.address,Participant.phone,Participant.mobile,Participant.fax) AGAINST ('".addslashes(trim($fullword_query))."' IN boolean MODE)) OR 
	                      			(TRIM(UPPER(Participant.full_name))) LIKE '%".addslashes($query)."%'
	                      		)",
	        					"(LENGTH(TRIM(Participant.full_name)) > 0 || LENGTH(TRIM(Participant.email)) > 0)",	
	        					"Participant.status"=> Configure::read('status_live')
	        	);
	        	$sOrder = "MATCH(Participant.full_name,Participant.nick_name,Participant.email,Participant.address,Participant.phone,Participant.mobile,Participant.fax) AGAINST ('".addslashes(trim($query))."') DESC,TRIM(UPPER(Participant.full_name)) ASC";
	        }
	        
        	$aPositionIds = array();
	    	$aAdditionalSearch = array("MATCH(ParticipantsPosition.name) AGAINST ('+".$query."*' IN boolean MODE) OR TRIM(ParticipantsPosition.name) LIKE '%".$query."%'");
	    	$aParticipantsPosition = $this->ParticipantsPosition->find('list',array('conditions'=> $aAdditionalSearch));
	    	if(is_array($aParticipantsPosition) && sizeof($aParticipantsPosition)>0){
	    		foreach( $aParticipantsPosition as $position_key => $position_name ){
				    $aPositionIds[] = $position_key;			
	    		}
	    		//$conditions["OR"] = " Participant.participants_position_id IN (".implode(",",$aPositionIds).")";
	    	}
	    }else{
            $conditions[] = array(
            					'Participant.status'=> Configure::read('status_live')
            				);
        }
        
        if( isset($this->params['url']['grouping']) && strlen($this->params['url']['grouping']) > 0 ){
            if( strtolower($this->params['url']['grouping']) != 'all' ){
        		$oParticipantsGrouping = $this->ParticipantsGrouping->findBySeoName($this->params['url']['grouping']);
        		$conditions[] = array(
	    			"Participant.participants_grouping_id" => $oParticipantsGrouping['ParticipantsGrouping']['id'],
	    			"Participant.status" => Configure::read('status_live')
	    		);
	    		$this->set('grouping',$oParticipantsGrouping['ParticipantsGrouping']['name']);
            }
	    }
	    
    	if( isset($this->params['url']['position']) && strlen($this->params['url']['position']) > 0 ){
            if( strtolower($this->params['url']['position']) != 'all' ){
	    		$oCompaniesIndustry = $this->CompaniesIndustry->findBySeoName($this->params['url']['industry']);
	    		$conditions[] = array(
	    			"Company.companies_industry_id" => $oCompaniesIndustry['CompaniesIndustry']['id'],
	    			"Company.status" => Configure::read('status_live')
	    		);
	    		$this->set('industry',$oCompaniesIndustry['CompaniesIndustry']['name']);
            }
	    }
	    
    	$sPage = 1;
    	if( isset($this->params['url']['page']) ){
    		if( is_numeric($this->params['url']['page']) ){
    			$sPage = $this->params['url']['page'];
    		}
    	}
    	
    	$order = array();
    	$recursive = 1;
    	if( !empty($query) && $query ){
    		$order = array($sOrder);
    	}else{
            $order = array( "TRIM(UPPER(Participant.full_name)) ASC" );
        }
        
        $this->paginate['Participant'] = array(
    				'recursive' => $recursive,
    				'conditions'=> $conditions,
    				'page'  => $sPage,
                    'limit' => $this->paginate['limit'],
    				'order' => $order
        );	
    	$this->set('participants',$this->paginate('Participant'));
    }
    
    public function admin_info( $participant_id=0, $seo_name=null ){
        Configure::write('debug',3);
        $seo_name = trim($seo_name);
        $participant_id = ( is_numeric($participant_id) && $participant_id > 0 ) ? intval($participant_id): 0;
        $oParticipant = $this->Participant->findById($participant_id);
        if( (
                  strtolower($seo_name) != strtolower($oParticipant['Participant']['seo_name']) )||
                  $oParticipant['Participant']['status'] != Configure::read('status_live')
        ) {
            $this->Session->setFlash(__('Invalid participant or participant has been deleted.', true), 'default', array('class' => 'error'));
            $this->redirect('/');
        }
        $this->set('title_for_layout','Participant Information for '.ucwords(strtolower($oParticipant['Participant']['first_name'].' '.$oParticipant['Participant']['last_name'])));
        $this->set('participant',$oParticipant);
    }
    
    public function admin_attended_programs($participant_id,$page){
        $speakers = $this->Speaker->find('all',array('cache' => 'Speaker', 'cacheConfig' => 'cache_queries'));
        Cache::write('speakers', $speakers,'short');
        $this->set( compact('speakers') );
        Configure::write('debug',0);
        if( intval($participant_id) > 0 ){
            $this->layout = 'iframe';
            if( isset($page) && intval($page)>0 && is_numeric($page) ){
                $sPage = intval($page);
            }else{
                $sPage = 1;
            }
            
            $conditions = array(
                'ProgramsParticipant.participant_id' => intval($participant_id) ,
                'ProgramsParticipant.status' => Configure::read('status_live') ,
                'Program.status' => Configure::read('status_live')
            );
            
            $group = array('ProgramsParticipant.program_id');
            $order = array('TRIM(UPPER(`Program`.`title`)) ASC');
            $this->ProgramsParticipant = &ClassRegistry::init('ProgramsParticipant');
            $this->ProgramsParticipant->contain(array('Program.id','Program.programs_type_id','Program.programs_division_id','Program.status'));
            $program_participants = $this->ProgramsParticipant->find("all",array(
                    'conditions' => $conditions,
                    'order' => $order,
                    'group' => $group
                )
            );
            
            $programId = array();
            if( count($program_participants) > 0 ){
                foreach( $program_participants as $program_participant ){
                    $programId[] = intval($program_participant['Program']['id']);
                }
            }
            $programId = array_unique($programId);
            if( count($programId)>0  ){
                $this->Program = &ClassRegistry::init('Program');
                $this->paginate['Program'] = array(
                    'order' => array('Program.id' => 'desc'),
                	'limit' => $this->paginate['limit'],
                    'conditions'=>  array(
                        "AND" => array(
                            'Program.id IN ('.implode(",",$programId).')',
                            'Program.status'=> Configure::read('status_live')
           				)
                    )
                );
                $programs  = $this->paginate('Program');
                $this->set(compact('programs'));
            }
        }else{
            $this->redirect('/');
        }
    }
    
    public function admin_attendance_sheets($participant_id,$page){
        if( intval($participant_id) > 0 ){
            
            $this->layout = 'iframe';
            if( isset($page) && intval($page)>0 && is_numeric($page) ){
                $sPage = intval($page);
            }else{
                $sPage = 1;
            }
            
            $conditions = array('AttendanceSheetsParticipants.participant_id' => intval($participant_id));
            
            $this->AttendanceSheetsParticipants = &ClassRegistry::init('AttendanceSheetsParticipants');
            $this->AttendanceSheetsParticipants->contain();
            $attendance_participants = $this->AttendanceSheetsParticipants->find("all",array(
                    'fields' => array('AttendanceSheetsParticipants.programs_attendance_sheet_id'),
                    'conditions' => $conditions,
                    'group' => array('AttendanceSheetsParticipants.id')
                )
            );
            
            $attendanceSheetId = array();
            if( is_array($attendance_participants) && count($attendance_participants) ){
                foreach( $attendance_participants as $attendance_participant ){
                    $attendanceSheetId[] = intval($attendance_participant['AttendanceSheetsParticipants']['programs_attendance_sheet_id']);
                }
            }
            
            $attendanceSheetId = array_unique($attendanceSheetId);
            $conditions = array(
                'ProgramsAttendanceSheet.id IN ('.implode(",",$attendanceSheetId).')' ,
                'ProgramsAttendanceSheet.status ="'.Configure::read('status_live').'"'
            );
            
            $group = array('ProgramsAttendanceSheet.program_id');
            $order = array('TRIM(UPPER(`Program`.`title`)) ASC');
            $this->ProgramsAttendanceSheet = &ClassRegistry::init('ProgramsAttendanceSheet');
            $this->paginate['ProgramsAttendanceSheet'] = array(
            		'conditions' => $conditions,
                    'order' => $order,
                    'group' => $group,
            		'limit' => $this->paginate['limit']
            );
            $this->set('participantId',$participant_id);
            $this->set('programsAttendanceSheet',$this->paginate('ProgramsAttendanceSheet'));
            
        }else{
            $this->redirect('/');
        }
    }
    
    public function admin_certificates($participant_id,$page){
        Configure::write('debug',0);
        if( intval($participant_id) > 0 ){
            
            $this->layout = 'iframe';
            if( isset($page) && intval($page)>0 && is_numeric($page) ){
                $sPage = intval($page);
            }else{
                $sPage = 1;
            }
            
            $conditions = array('CertificatesParticipants.participant_id' => intval($participant_id));
            
            $this->CertificatesParticipants = &ClassRegistry::init('CertificatesParticipants');
            $this->CertificatesParticipants->contain();
            $certificates_participants = $this->CertificatesParticipants->find("all",array(
                    'fields' => array('CertificatesParticipants.programs_certificate_id'),
                    'conditions' => $conditions,
                    'group' => array('CertificatesParticipants.id')
                )
            );
            
            $certificateId = array();
            if( is_array($certificates_participants) && count($certificates_participants) ){
                foreach( $certificates_participants as $certificates_participant ){
                    $certificateId[] = intval($certificates_participant['CertificatesParticipants']['programs_certificate_id']);
                }
            }
            
            $certificateId = array_unique($certificateId);
            $conditions = array(
                'ProgramsCertificate.id IN ('.implode(",",$certificateId).')' ,
                'ProgramsCertificate.status ="'.Configure::read('status_live').'"'
            );
            $group = array('ProgramsCertificate.program_id');
            $order = array('TRIM(UPPER(`Program`.`title`)) ASC');
            $this->ProgramsCertificate = &ClassRegistry::init('ProgramsCertificate');
            $this->paginate['ProgramsCertificate'] = array(
                    'conditions' => $conditions,
                    'order' => $order,
                    'group' => $group,
            		'limit' => $this->paginate['limit']
            );
            
            $this->set('participantId',$participant_id);
            $this->set('programsCertificates',$this->paginate('ProgramsCertificate'));
        }else{
            $this->redirect('/');
        }
    }
    
    public function admin_directory_of_participants($participant_id,$page){
        Configure::write('debug',3);
        if( intval($participant_id) > 0 ){
            
            $this->layout = 'iframe';
            if( isset($page) && intval($page)>0 && is_numeric($page) ){
                $sPage = intval($page);
            }else{
                $sPage = 1;
            }
            
            $conditions = array('DirectoriesOfParticipants.participant_id' => intval($participant_id));
            
            $this->DirectoriesOfParticipants = &ClassRegistry::init('DirectoriesOfParticipants');
            $this->DirectoriesOfParticipants->contain();
            $directories_participants = $this->DirectoriesOfParticipants->find("all",array(
                    'fields' => array('DirectoriesOfParticipants.programs_directories_of_participant_id'),
                    'conditions' => $conditions,
                    'group' => array('DirectoriesOfParticipants.id')
                )
            );
            
            $directoryOfParticipantId = array();
            if( is_array($directories_participants) && count($directories_participants) ){
                foreach( $directories_participants as $directories_participant ){
                    $directoryOfParticipantId[] = intval($directories_participant['DirectoriesOfParticipants']['programs_directories_of_participant_id']);
                }
            }
            
            $directoryOfParticipantId = array_unique($directoryOfParticipantId);
            $conditions = array(
                'ProgramsParticipantDirectory.id IN ('.implode(",",$directoryOfParticipantId).')' ,
                'ProgramsParticipantDirectory.status ="'.Configure::read('status_live').'"'
            );
            $group = array('ProgramsParticipantDirectory.program_id');
            $order = array('TRIM(UPPER(`Program`.`title`)) ASC');
            $this->ProgramsParticipantDirectory = &ClassRegistry::init('ProgramsParticipantDirectory');
            $this->paginate['ProgramsParticipantDirectory'] = array(
                    'conditions' => $conditions,
                    'order' => $order,
                    'group' => $group,
            		'limit' => $this->paginate['limit']
                
            );
            
            $this->set('participantId',$participant_id);
            $this->set('programsParticipantDirectories',$this->paginate('ProgramsParticipantDirectory'));
        }else{
            $this->redirect('/');
        }
    }
    
    public function admin_name_tags($participant_id,$page){
        Configure::write('debug',0);
        if( intval($participant_id) > 0 ){
            
            $this->layout = 'iframe';
            if( isset($page) && intval($page)>0 && is_numeric($page) ){
                $sPage = intval($page);
            }else{
                $sPage = 1;
            }
            
            $conditions = array('NameTagsParticipants.participant_id' => intval($participant_id));
            
            $this->NameTagsParticipants = &ClassRegistry::init('NameTagsParticipants');
            $this->NameTagsParticipants->contain();
            $name_tags = $this->NameTagsParticipants->find("all",array(
                    'fields' => array('NameTagsParticipants.programs_name_tag_id'),
                    'conditions' => $conditions,
                    'group' => array('NameTagsParticipants.id')
                )
            );
            
            $nametagId = array();
            if( is_array($name_tags) && count($name_tags) ){
                foreach( $name_tags as $name_tag ){
                    $nametagId[] = intval($name_tag['NameTagsParticipants']['programs_name_tag_id']);
                }
            }
            
            $nametagId = array_unique($nametagId);
            $conditions = array(
                'ProgramsNameTag.id IN ('.implode(",",$nametagId).')' ,
                'ProgramsNameTag.status ="'.Configure::read('status_live').'"'
            );
            $group = array('ProgramsNameTag.program_id');
            $order = array('TRIM(UPPER(`Program`.`title`)) ASC');
            $this->ProgramsNameTag = &ClassRegistry::init('ProgramsNameTag');
            $this->paginate['ProgramsNameTag'] = array(
                    'conditions' => $conditions,
                    'order' => $order,
                    'group' => $group,
            		'limit' => $this->paginate['limit']
            );
            $this->set('participantId',$participant_id);
            $this->set('nametags',$this->paginate('ProgramsNameTag'));
        }else{
            $this->redirect('/');
        }
    }
    
    public function paginate($object = null, $scope = array(), $whitelist = array(), $key = null) {
      	$results = parent::paginate($object, $scope, $whitelist);
      	if ($key) {
       		$this->params['paging'][$key] = $this->params['paging'][$object];
       		unset($this->params['paging'][$object]);
      	}
		return $results;
    }
}
