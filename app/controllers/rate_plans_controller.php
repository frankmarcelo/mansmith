<?php

class RatePlansController extends AppController {
/**
 * Controller name
 *
 * @var string
 * @access public
 */
    public $name = 'RatePlans';
/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
    public $uses = array('Setting', 'User','RatePlan');
/**
 * Helpers used by the Controller
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html', 'Form', 'Filemanager');

    public $deletablePaths = array();

    public function beforeFilter() {
        parent::beforeFilter();
    }
}