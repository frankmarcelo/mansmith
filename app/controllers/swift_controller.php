<?php // File -> app/controllers/employees_controller.php

class SwiftController extends AppController {
    var $name = 'Swift';
    var $components = array('SwiftMailer');
    
    function mail() {      
        $this->SwiftMailer->smtpType = 'tls';
        $this->SwiftMailer->smtpHost = 'smtp.gmail.com';
        $this->SwiftMailer->smtpPort = 465;
        $this->SwiftMailer->smtpUsername = 'frankitoy@gmail.com';
        $this->SwiftMailer->smtpPassword = 'hard_to_guess';

        $this->SwiftMailer->sendAs = 'html';
        $this->SwiftMailer->from = 'frankitoy@gmail.com';
        $this->SwiftMailer->fromName = 'New bakery component';
        $this->SwiftMailer->to = 'frankitoy@gmail.com';
        //set variables to template as usual
        $this->set('message', 'My message');
        
        try {
            if(!$this->SwiftMailer->send('im_excited', 'My subject')) {
                $this->log("Error sending email");
            }
        }
        catch(Exception $e) {
              $this->log("Failed to send email: ".$e->getMessage());
        }
        $this->redirect($this->referer(), null, true);
    }
}