<?php

class CertificatesController extends AppController {
	
    public $name = 'Certificates';
    public $uses = array(
    	'Program',
    	'User',
    	'ProgramsDivision',
    	'ProgramsType',
    	'ProgramsSpeaker',
    	'Speaker', 
    	'ProgramsRatePlan', 
    	'ProgramsSchedule',
    	'ProgramsSpeaker',
    	'MansmithProgram',
    	'Day8Program',
    	'CoachProgram',
    	'Participant',
    	'ProgramsCertificate',
    	'CertificatesParticipants'
    );
    
    public $helpers = array(
    	'Program',
        'ProgramExtraInfo',
    	'Certificates'
    );
    
    public $paginate = array(
        'limit' => 20,
        'group' => array('ProgramsCertificate.program_id'),
        'order' => array('ProgramsCertificate.id' => 'DESC')
    );

    public function beforeFilter() {
    	parent::beforeFilter();
    }

	public function getCertificatesDirectory($programId=null){
    	$oDirectoryProperty = new DirectoryProperty();
        $oDirectoryProperty->documentDirectory = Configure::read('docs_directory');
        $oDocumentDirectory = new DocumentDirectory( $oDirectoryProperty, $programId );
        return $oDocumentDirectory->getCertificatesDirectory();
    }
    
    public function admin_list($searchType=null){
    	$oDivision = $this->ProgramsDivision->findByTitle($searchType);
        if( !isset($oDivision['ProgramsDivision']['id']) && ($oDivision['ProgramsDivision']['id']) == Configure::read('status_live') ) {
            $this->Session->setFlash(__('Invalid program or program might have been deleted.', true), 'default', array('class' => 'error'));
            $this->redirect('/');
        }

        if( $this->Session->read('Auth.User.role_id') != Configure::read('administrator') && $oDivision['ProgramsDivision']['id'] != $this->Session->read('Auth.User.role_id')  ){
            $this->Session->setFlash(__('Invalid program or you are not allowed to access this information.', true), 'default', array('class' => 'error'));
            $this->redirect('/');
        }
        
        $this->paginate['ProgramsCertificate'] = array(
    			'order' => array('ProgramsCertificate.id' => 'desc'),
        		'conditions'=>  array(
         			'AND' => array(
    					'Program.programs_division_id'=> intval($oDivision['ProgramsDivision']['id']),
        				'Program.status'=> Configure::read('status_live'),
           				'ProgramsCertificate.status'=> Configure::read('status_live')
           			)
           		),
           		'group' => array('ProgramsCertificate.program_id')
        );
        $this->set('programsCertificates',$this->paginate('ProgramsCertificate'));
        $types = $this->ProgramsType->find('list',array('cache' => 'ProgramsType', 'cacheConfig' => 'cache_queries'));
        $divisions = $this->ProgramsDivision->find('list',array('cache' => 'ProgramsDivision', 'cacheConfig' => 'cache_queries'));
        $speakers  = $this->Speaker->find('all',array('cache' => 'Speaker', 'cacheConfig' => 'cache_queries'));
        $this->set('division', strtolower($searchType) );
        $this->set( compact('divisions','types') );
    }
    
    public function admin_download($downloadId=null){
    	
    	if( isset($downloadId) && intval($downloadId) > 0 ){
    		$programsCertificate = $this->ProgramsCertificate->findById($downloadId);
    		$this->autoRender = false;
            $this->layout = 'ajax';	
    	    $downloadDirectory = $this->getCertificatesDirectory($programsCertificate['Program']['id']);
    		$downloadFile = trim($downloadDirectory.$programsCertificate['ProgramsCertificate']['source_file']);
    		if( file_exists($downloadFile) ){
    			headersDownloadFile('rtf',$downloadFile,$programsCertificate['ProgramsCertificate']['source_file']);
			}else{
				$programsSchedules = $this->ProgramsSchedule->find('all',
					array(
						'fields' => array('ProgramsSchedule.*'),
						'conditions' => array(
						'ProgramsSchedule.program_id' => intval($programsCertificate['Program']['id'])
					),'order'=>array('ProgramsSchedule.start_date'))
				);	
				//debug($this->ProgramsSchedule->lastQuery());
				App::import('Vendor', 'phprtf', array('file' =>'phprtf/PHPRtfLite.php'));
            	App::import('Lib', 'CertificatesRtf'); 
            	PHPRtfLite::registerAutoloader();
				$rtf = new PHPRtfLite();
				$tableBorder = new PHPRtfLite_Border(
					$rtf,//PHPRtfLite instance
					new PHPRtfLite_Border_Format(0.7,'#000000'),//left border: 2pt, green color
					new PHPRtfLite_Border_Format(0.7,'#000000'),//top border: 1pt, yellow color
					new PHPRtfLite_Border_Format(0.7,'#000000'),//right border: 2pt, red color
					new PHPRtfLite_Border_Format(0.7,'#000000')//bottom border: 1pt, blue color
				);
				
				$this->CertificatesParticipants->contain(
					array(
						'Participant.last_name',
						'Participant.first_name',
						'Participant.middle_name',
						'ParticipantsPosition.name',
						'Company.name',
					)
				);
				$this->CertificatesParticipants->bindModel(array(
    				'hasOne' => array(
        				'Company' => array(
            				'foreignKey' => false,
            				'conditions' => array('Company.id = Participant.company_id')
        				),
        				'ParticipantsPosition' => array(
            				'foreignKey' => false,
            				'conditions' => array('ParticipantsPosition.id = Participant.participants_position_id')
        				)
        			),
        		));
        		
        		$aCertificatesParticipants = $this->CertificatesParticipants->find('all',array(
        			'conditions' => array('CertificatesParticipants.programs_certificate_id'=>intval($programsCertificate['ProgramsCertificate']['id'])),
        			'order' => array('UPPER(Participant.first_name)'))
        		);
        		if( isset($aCertificatesParticipants) && is_array($aCertificatesParticipants) && count($aCertificatesParticipants)>0 ){
					$oCertificatesRtf = new CertificatesRtf($rtf,$tableBorder,$downloadFile,$programsSchedules,$programsCertificate,$aCertificatesParticipants);
					$oCertificatesRtf->generateRtf();
					$oCertificatesRtf->save_rtf();
				}
                headersDownloadFile('rtf',$downloadFile,$programsCertificate['ProgramsCertificate']['source_file']);
			}
		}
	}
    
	public function admin_search(){
    	
    	$query = null;
        $sOrder = null;
        if( isset($this->params['url']['q']) && strlen(trim($this->params['url']['q']))> 0 ){
        	$query = trim($this->params['url']['q']);
        }
        
        $conditions = array();
        
        if( !empty($query) && strlen($query)> 0 ){
        	App::import('Sanitize');
        	$full_text_search = explode (" ", $query );
     	   	foreach ($full_text_search as $wordindex => $wordvalue) {
				if( strlen(trim($wordvalue))==0) {
					unset($full_text_search[$wordindex]);
            	}
			}
    
			$fulltext_array = array();
			$like_array = array();
			$original_search = array();
			foreach ($full_text_search as $wordindex => $wordvalue) {
				if( strlen ($wordvalue)<=3 ) {
					$like_array[] = Sanitize::clean($wordvalue);
            	}else{
            	    $fulltext_array[] = "+".Sanitize::clean($wordvalue)."*";
            	}
            }
        	
			$original_search = array_merge($like_array,$fulltext_array);
            $boolSearchByLike = $this->ProgramsCertificate->getLikeFullName($query);

            $this->CertificatesParticipants->contain(array('Participant.company_id','Company.name'));
            $this->CertificatesParticipants->bindModel(array(
                'hasOne' => array(
                    'Company' => array(
                        'foreignKey' => false,
                        'conditions' => array('Company.id = Participant.company_id')
                    ),
                ),
        	));
        
            $find_company = $this->CertificatesParticipants->find('all',array(
               'conditions' => array('Company.name LIKE'=> "%".$query."%"),
               'group' => array('Company.id')
            ));

            if( $boolSearchByLike == true || count($find_company)>0){
                if( $boolSearchByLike ){
                    $conditions[] = array(
                                        "((
                                 TRIM(UPPER(`Program`.`title`)) LIKE '%".addslashes($query)."%' AND
                                 (`Program`.`title` IS NOT NULL) AND
                                 (LENGTH(TRIM(`Program`.`title`))) > 0
                                         ))
                                         OR
                                         ((
                                 TRIM(UPPER(`ProgramsCertificate`.`source_file`)) LIKE '%".addslashes($query)."%' AND
                                 (`ProgramsCertificate`.`source_file` IS NOT NULL) AND
                                 (LENGTH(TRIM(`ProgramsCertificate`.`source_file`))) > 0
                                         ))

                                         AND Program.status = '".intval(Configure::read('status_live'))."'
                                         AND ProgramsCertificate.status = '".intval(Configure::read('status_live'))."'
                                        "
                    );
                }else{
                    $certificateIds = array();
                    foreach( $find_company as $company_info ){
                        $certificateIds[] = $company_info['CertificatesParticipants']['programs_certificate_id'];
                    }
                    $conditions[] = array( "ProgramsCertificate.id IN (".implode(",",$certificateIds).")");
                }
                $sOrder = " TRIM(UPPER(`Program`.`title`)) ASC,TRIM(UPPER(`ProgramsCertificate`.`source_file`)) ASC ";
            }else{

                $fullword_query = trim(implode(' ', $original_search));
                $conditions[] = array(
                "(
                    (
                        MATCH(
                            Program.title,
                            Program.notes,
                            Program.venue_name,
                            Program.address,
                            Program.suburb,
                            Program.city,
                            Program.notes,
                            Program.website,
                            Program.content
                        ) AGAINST ('".addslashes(trim($fullword_query))."' IN boolean MODE)
                    )
                    OR
                        (TRIM(UPPER(Program.title))) LIKE '%".addslashes($query)."%'
                    OR
                        (TRIM(UPPER(`ProgramsCertificate`.`source_file`))) LIKE '%".addslashes($query)."%'
                )
                AND
                ( LENGTH(TRIM(Program.title))> 0) AND
                  Program.status = '".intval(Configure::read('status_live'))."'"
                );
                $sOrder = "MATCH(Program.title,Program.notes,Program.venue_name,Program.address,Program.suburb,Program.city,Program.notes,Program.website,Program.content) AGAINST ('".addslashes(trim($query))."') DESC,TRIM(UPPER(Program.title)) ASC";
		        
		    }
		}else{//NO SEARCH QUERIES
			$sOrder = 'ProgramsCertificate.id desc';
		}
	    
	    if( isset($this->params['url']['type']) && strlen($this->params['url']['type']) > 0 ){
            if( strtolower($this->params['url']['type']) != 'all' ){
	    		$oProgramsType = $this->ProgramsType->findBySeoName($this->params['url']['type']);
	    		$conditions[] = array(
	    				"Program.programs_type_id" => $oProgramsType['ProgramsType']['id'],
	    				"Program.status" => Configure::read('status_live')
	    		);
	    		$this->set('program_type',$oProgramsType['ProgramsType']['name']);
            }
	    }
	    
	    $schedule_date = null;
	    if( isset($this->params['url']['schedule_date']) && strlen($this->params['url']['schedule_date']) > 0 ){
    		$schedule_date = trim($this->params['url']['schedule_date']);
    	}
    	
    	if( isset($this->params['url']['division']) && strlen($this->params['url']['division']) > 0 ){
    		if( strtolower($this->params['url']['division']) != 'all' ){
    		  	$oDivision = $this->ProgramsDivision->findByTitle($this->params['url']['division']);
        		if( !empty($oDivision) && isset($oDivision['ProgramsDivision']['id']) ){ 
	        		$conditions[] = array('Program.programs_division_id'=> $oDivision['ProgramsDivision']['id']);
		    	}
            }
    	}
        
        if( $this->Session->read('Auth.User.role_id') != Configure::read('administrator')){
    		if( isset($this->params['url']['q']) ){
	    		$conditions[] = array(
	    			"Program.programs_division_id" => $this->Session->read('Auth.User.role_id'),
	    			"Program.status" => Configure::read('status_live')
	    		);
    		}else{
    			$conditions = array( 
	        		array( 
	        			"Program.programs_division_id" => $this->Session->read('Auth.User.role_id'),
	        			"Program.status" => Configure::read('status_live')
	        		) 
	    		);
    		}
    	}
    	
    	$sPage = 1;
    	if( isset($this->params['url']['page']) ){
    		if( is_numeric($this->params['url']['page']) ){
    			$sPage = $this->params['url']['page'];
    		}
    	}
    	
    	$order = array();
    	$recursive = 1;
    	if( !empty($query) && $query ){
    		$order = array($sOrder);
    	}else{
            $order = array("Program.id DESC");
        }
    	
    	if( !empty($schedule_date) && $schedule_date ){
    		list($month, $year)= explode("/",$schedule_date);
    		$sScheduleDate = date("Y-m",strtotime($year."-".$month."-01"));
        	$program_ids = $this->Program->ProgramsSchedule->find('list',array(
            	'fields' => array( 'ProgramsSchedule.program_id' ),
                'recursive'  => -1,
			  	'conditions' => array("ProgramsSchedule.start_date LIKE '%".date("Y-m",strtotime($sScheduleDate))."%'"),
                'group' => array('ProgramsSchedule.program_id') //fields to GROUP BY
			));
            
			if( sizeof($program_ids) > 0 && $program_ids ){
            	$conditions[] = array("Program.id IN (".implode(",",$program_ids).") ");
            }else{
            	$conditions[] = array("Program.id =0 ");
            }
            $conditions[] = array("Program.status" => Configure::read('status_live'));
            $this->set('schedule_date',date("M Y",strtotime($sScheduleDate)));
    	}
    	
    	$this->paginate['ProgramsCertificate'] = array(
    		'recursive' => $recursive,
    		'conditions'=> $conditions,
    		'page'  => $sPage,
            'limit' => 20,
    		'order' => $order,
    		'group' => array('ProgramsCertificate.program_id')
    	);
	    
	    $this->set('programsCertificates',$this->paginate('ProgramsCertificate'));
		$types = $this->ProgramsType->find('all',array('cache' => 'ProgramsType', 'cacheConfig' => 'cache_queries'));
		$divisions = $this->ProgramsDivision->find('list',array('cache' => 'ProgramsDivision', 'cacheConfig' => 'cache_queries'));
        $this->set( compact('divisions','types') );
    }
    
    public function admin_index(){
    	if( $this->Session->read('Auth.User.role_id') == Configure::read('administrator') ){
    		$this->paginate['ProgramsCertificate'] = array(
    				'order' => array('ProgramsCertificate.id' => 'desc'),
        			'conditions'=>  array(
           				'AND' => array(
        					'Program.status'=> Configure::read('status_live'),
           					'ProgramsCertificate.status'=> Configure::read('status_live')
           				)
           			),
           			'group' => array('ProgramsCertificate.program_id')
           	);
            $this->set('programsCertificates',$this->paginate('ProgramsCertificate'));
        }else{
        	$this->paginate['ProgramsCertificate'] = array(
           			'order' => array('ProgramsCertificate.id' => 'desc'),
        			'conditions'=>  array(
           							"AND" => array(
           									'Program.programs_division_id'=> $this->Session->read('Auth.User.role_id'),
           									'Program.status'=> Configure::read('status_live'),
           									'ProgramsCertificate.status'=> Configure::read('status_live')				
           							)
           			),
           			'group' => array('ProgramsCertificate.program_id')
        	);
            $this->set('programsCertificates',$this->paginate('ProgramsCertificate'));	
        }
        
		$types = $this->ProgramsType->find('list',array('cache' => 'ProgramsType', 'cacheConfig' => 'cache_queries'));
        $divisions = $this->ProgramsDivision->find('list',array('cache' => 'ProgramsDivision', 'cacheConfig' => 'cache_queries'));
        $this->set( compact('divisions','types') );
    }
    
    public function paginate($object = null, $scope = array(), $whitelist = array(), $key = null) {
      	$results = parent::paginate($object, $scope, $whitelist);
      	if ($key) {
            $this->params['paging'][$key] = $this->params['paging'][$object];
            unset($this->params['paging'][$object]);
      	}
	return $results;
    }
}