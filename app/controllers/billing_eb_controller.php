<?php

class BillingEbController extends AppController {

	const billing_type_id = 1;
    public $name = 'BillingEb';
    public $uses = array(
    	'Program',
    	'User',
    	'ProgramsDivision',
    	'ProgramsType',
    	'ProgramsSpeaker',
    	'Speaker', 
    	'ProgramsRatePlan', 
    	'ProgramsSchedule',
    	'ProgramsSpeaker',
    	'MansmithProgram',
    	'Day8Program',
    	'CoachProgram',
    	'Participant',
    	'ProgramsCompanyContact',
    	'ProgramsParticipant',
    	'BillingEb'
    );
    
    public $helpers = array(
    	'Program',
        'ProgramExtraInfo',
    	'BillingEb'
    );
    
    public $paginate = array(
        'limit' => 20,
    	'group' => array('BillingEb.program_id'),
    	'order' => array('BillingEb.id' => 'DESC')
    );

    public function beforeFilter() {
        parent::beforeFilter();
    }

	public function getBillingEbDirectory($programId=null){
    	$oDirectoryProperty = new DirectoryProperty();
        $oDirectoryProperty->documentDirectory = Configure::read('docs_directory');
        $oDocumentDirectory = new DocumentDirectory( $oDirectoryProperty, $programId );
        return $oDocumentDirectory->getBillingEbDirectory();
    }
    
	public function admin_download($downloadId=null){
    	if( isset($downloadId) && intval($downloadId) > 0 ){
    		
    		$this->BillingEb->bindModel(array(
    				'hasOne' => array(
        				'ProgramsDivision' => array(
            				'foreignKey' => false,
            				'conditions' => array('Program.programs_division_id = ProgramsDivision.id')
        				),
        			)
        		));
    		
    		$billing_eb = $this->BillingEb->findById($downloadId);
    		$this->autoRender = false;
            $this->layout = 'ajax';
            $downloadDirectory = $this->getBillingEbDirectory($billing_eb['BillingEb']['program_id']);
            $downloadFile = trim($downloadDirectory.$billing_eb['BillingEb']['source_file']);

            if( file_exists($downloadFile) ){
    			headersDownloadFile('rtf',$downloadFile,$billing_eb['BillingEb']['source_file']);
			}else{
				$this->BillingEb->bindModel(array(
    				'hasOne' => array(
        				'ProgramsDivision' => array(
            				'foreignKey' => false,
            				'conditions' => array('Program.programs_division_id = ProgramsDivision.id')
        				),
        			)
        		));
        		
        		$this->ProgramsCompanyContact->bindModel(array(
        			'hasOne' => array(
        				'ParticipantsPosition' => array(
            				'foreignKey' => false,
            				'conditions' => array('ParticipantsPosition.id = Participant.participants_position_id')
        				)
        			)
        		));
        		$this->ProgramsCompanyContact->contain(array('Company.name','Company.fax','Participant','ParticipantsPosition.name'));
            	$primary_contacts = $this->ProgramsCompanyContact->find('first',array(
            		'conditions' => array(	
            			'ProgramsCompanyContact.program_id' => intval($billing_eb['Program']['id']),
            			'ProgramsCompanyContact.company_id' => intval($billing_eb['Company']['id'])			
            		)
            	));
            	/*
            	debug($this->ProgramsCompanyContact->lastQuery());
				debug($billing_eb);
            	debug($primary_contacts);*/
    			$programsSchedules = $this->ProgramsSchedule->find('all',
					array(
						'fields' => array('ProgramsSchedule.*'),
						'conditions' => array(
						'ProgramsSchedule.program_id' => intval($billing_eb['Program']['id'])
					),'order' => array('ProgramsSchedule.start_date'))
				);	
				//
				
				App::import('Vendor', 'phprtf', array('file' =>'phprtf/PHPRtfLite.php'));
				if( $billing_eb['ProgramsDivision']['id'] != 3 ){//not coach
            		App::import('Lib', 'BillingEbRtf');
				}else{
					App::import('Lib', 'CoachBillingEbRtf');
				}
                PHPRtfLite::registerAutoloader();
                $rtf = new PHPRtfLite();
				$tableBorder = new PHPRtfLite_Border(
					$rtf,//PHPRtfLite instance
					new PHPRtfLite_Border_Format(0.7,'#000000'),//left border: 2pt, green color
					new PHPRtfLite_Border_Format(0.7,'#000000'),//top border: 1pt, yellow color
					new PHPRtfLite_Border_Format(0.7,'#000000'),//right border: 2pt, red color
					new PHPRtfLite_Border_Format(0.7,'#000000')//bottom border: 1pt, blue color
				);
				
				$this->ProgramsParticipant->contain(array(
						'Participant.full_name',
						'Participant.last_name',
						'Participant.first_name',
						'Participant.middle_name',
						'Participant.nick_name',
						'Participant.phone',
						'Participant.email',
						'Participant.address',
						'Participant.zip_code',
						'Participant.mobile',
						'ParticipantsPosition.name',
						'Company.name',
					)
				);
				
				$this->ProgramsParticipant->bindModel(array(
    				'hasOne' => array(
        				'ParticipantsPosition' => array(
            				'foreignKey' => false,
            				'conditions' => array('ParticipantsPosition.id = Participant.participants_position_id')
        				)
        			),
        		));

				$aProgramsParticipant = $this->ProgramsParticipant->find('all',array(
						'conditions' => array(
							'ProgramsParticipant.program_id'=> intval($billing_eb['BillingEb']['program_id']),
							'ProgramsParticipant.company_id'=> intval($billing_eb['BillingEb']['company_id']),
                            'ProgramsParticipant.status' => Configure::read('status_live'),
                            'ProgramsParticipant.programs_participant_status_id' => Configure::read('status_live')
						),
						'order' => array('Participant.last_name')
					)
				);

				if( isset($aProgramsParticipant) && is_array($aProgramsParticipant) && count($aProgramsParticipant)>0 ){
        			if( $billing_eb['ProgramsDivision']['id'] != 3 ){//not coach
        				$oBillingEbRtf = new BillingEbRtf($rtf,$tableBorder,$downloadFile,$programsSchedules,$billing_eb,$aProgramsParticipant,$primary_contacts);
        			}else{
        				$oBillingEbRtf = new CoachBillingEbRtf($rtf,$tableBorder,$downloadFile,$programsSchedules,$billing_eb,$aProgramsParticipant,$primary_contacts);
        			}
        			$oBillingEbRtf->generateRtf();
					$oBillingEbRtf->save_rtf();
                    headersDownloadFile('rtf',$downloadFile,$billing_eb['BillingEb']['source_file']);
				}
			}
		}
	}

    public function admin_list($searchType=null){
    	$oDivision = $this->ProgramsDivision->findByTitle($searchType);
        if( !isset($oDivision['ProgramsDivision']['id']) && ($oDivision['ProgramsDivision']['id']) == Configure::read('status_live') ) {
            $this->Session->setFlash(__('Invalid program or program might have been deleted.', true), 'default', array('class' => 'error'));
            $this->redirect('/');
        }

        if( $this->Session->read('Auth.User.role_id') != Configure::read('administrator') && $oDivision['ProgramsDivision']['id'] != $this->Session->read('Auth.User.role_id')  ){
            $this->Session->setFlash(__('Invalid program or you are not allowed to access this information.', true), 'default', array('class' => 'error'));
            $this->redirect('/');
        }

        $this->paginate['BillingEb'] = array(
    			'order' => array('BillingEb.id' => 'desc'),
        		'conditions'=>  array(
         			'AND' => array(
    					'Program.programs_division_id'=> intval($oDivision['ProgramsDivision']['id']),
        				'Program.status'=> Configure::read('status_live'),
                        'BillingEb.billing_type_id'=> constant('self::billing_type_id'),
           				'BillingEb.status'=> Configure::read('status_live')
           			)
           		),
           		'group' => array('BillingEb.program_id')
        );
        $this->set('billing_eb',$this->paginate('BillingEb'));
        $types = $this->ProgramsType->find('list',array('cache' => 'ProgramsType', 'cacheConfig' => 'cache_queries'));
        $divisions = $this->ProgramsDivision->find('list',array('cache' => 'ProgramsDivision', 'cacheConfig' => 'cache_queries'));
        $speakers  = $this->Speaker->find('all',array('cache' => 'Speaker', 'cacheConfig' => 'cache_queries'));
        $this->set('division', strtolower($searchType) );
        $this->set( compact('divisions','types') );
    }

    public function admin_search(){

    	$query = null;
        $sOrder = null;
        if( isset($this->params['url']['q']) && strlen(trim($this->params['url']['q']))> 0 ){
        	$query = trim($this->params['url']['q']);
        }

        $conditions = array();
        if( !empty($query) && strlen($query)> 0 ){
        	App::import('Sanitize');
        	$full_text_search = explode (" ", $query );
     	   	foreach ($full_text_search as $wordindex => $wordvalue) {
				if( strlen(trim($wordvalue))==0) {
					unset($full_text_search[$wordindex]);
            	}
			}

			$fulltext_array = array();
			$like_array = array();
			$original_search = array();
			foreach ($full_text_search as $wordindex => $wordvalue) {
				if( strlen ($wordvalue)<=3 ) {
					$like_array[] = Sanitize::clean($wordvalue);
            	}else{
            	    $fulltext_array[] = "+".Sanitize::clean($wordvalue)."*";
            	}
            }

			$original_search = array_merge($like_array,$fulltext_array);
			$boolSearchByLike = $this->BillingEb->getLikeFullName($query);

            if( $boolSearchByLike == true){
                $conditions[] = array(
									"((
							 TRIM(UPPER(`Program`.`title`)) LIKE '%".addslashes($query)."%' AND
							 (`Program`.`title` IS NOT NULL) AND
							 (LENGTH(TRIM(`Program`.`title`))) > 0
									 ))
									 OR
									 ((
							 TRIM(UPPER(`BillingEb`.`seminar`)) LIKE '%".addslashes($query)."%' AND
							 (`BillingEb`.`seminar` IS NOT NULL) AND
							 (LENGTH(TRIM(`BillingEb`.`seminar`))) > 0
									 ))
									 OR
									 ((
							 TRIM(UPPER(`BillingEb`.`source_file`)) LIKE '%".addslashes($query)."%' AND
							 (`BillingEb`.`source_file` IS NOT NULL) AND
							 (LENGTH(TRIM(`BillingEb`.`source_file`))) > 0
									 ))
									 OR
									((
							 TRIM(UPPER(`BillingEb`.`billing_reference_code`)) LIKE '%".addslashes($query)."%' AND
							 (`BillingEb`.`billing_reference_code` IS NOT NULL) AND
							 (LENGTH(TRIM(`BillingEb`.`billing_reference_code`))) > 0
									 ))
                                     OR
									((
							 TRIM(UPPER(`Company`.`name`)) LIKE '%".addslashes($query)."%' AND
							 (`Company`.`name` IS NOT NULL) AND
							 (LENGTH(TRIM(`Company`.`name`))) > 0
									 ))
									 AND Program.status = '".intval(Configure::read('status_live'))."'
									 AND BillingEb.status = '".intval(Configure::read('status_live'))."'
									"
				);
				$sOrder = " TRIM(UPPER(`Program`.`title`)) ASC,TRIM(UPPER(`BillingEb`.`seminar`)) ASC,TRIM(UPPER(`BillingEb`.`source_file`)) ASC,TRIM(UPPER(`BillingEb`.`billing_reference_code`)) ASC ";
            }else{
                $fullword_query = trim(implode(' ', $original_search));
				$conditions[] = array(
				"(
					(   MATCH(
							
							BillingEb.seminar,
							BillingEb.source_file,
							BillingEb.billing_reference_code,
							BillingEb.company,
							BillingEb.recipient,
							BillingEb.recipient_position,
							BillingEb.recipient_fax_number,
							BillingEb.venue,
							BillingEb.notes
						) AGAINST ('".addslashes(trim($fullword_query))."' IN boolean MODE)
					) OR  TRIM(UPPER(Program.title)) LIKE '%".addslashes($query)."%'
					  OR  TRIM(UPPER(`BillingEb`.`seminar`))
                      OR  TRIM(UPPER(`BillingEb`.`source_file`)) LIKE '%".addslashes($query)."%'
					  OR  TRIM(UPPER(`BillingEb`.`billing_reference_code`)) LIKE '%".addslashes($query)."%'
				)AND(
					LENGTH(TRIM(Program.title))> 0
				)AND(
					Program.status = '".intval(Configure::read('status_live'))."'
				)"
				);
				$sOrder = "
                MATCH(
					BillingEb.seminar,
					BillingEb.source_file,
					BillingEb.billing_reference_code,
					BillingEb.company,
					BillingEb.recipient,
					BillingEb.recipient_position,
					BillingEb.recipient_fax_number,
					BillingEb.venue,
					BillingEb.notes
				) AGAINST ('".addslashes(trim($query))."') DESC,TRIM(UPPER(Program.title)) ASC";
            }
		    
		}else{//NO SEARCH QUERIES
			$sOrder = 'BillingEb.id desc';
		}

	    if( isset($this->params['url']['type']) && strlen($this->params['url']['type']) > 0 ){
            if( strtolower($this->params['url']['type']) != 'all' ){
	    		$oProgramsType = $this->ProgramsType->findBySeoName($this->params['url']['type']);
	    		$conditions[] = array(
	    				"Program.programs_type_id" => $oProgramsType['ProgramsType']['id'],
	    				"Program.status" => Configure::read('status_live')
	    		);
	    		$this->set('program_type',$oProgramsType['ProgramsType']['name']);
            }
	    }

	    $schedule_date = null;
	    if( isset($this->params['url']['schedule_date']) && strlen($this->params['url']['schedule_date']) > 0 ){
    		$schedule_date = trim($this->params['url']['schedule_date']);
			list($month, $year)= explode("/",$schedule_date);
    		$sScheduleDate = date("Y-m",strtotime($year."-".$month."-01"));
			$conditions[] = array(
                "BillingEb.schedule LIKE '%".date("F",strtotime($sScheduleDate))."%'",
                "BillingEb.schedule LIKE '%".date("Y",strtotime($sScheduleDate))."%'"
            );
            $this->set('schedule_date',date("M Y",strtotime($sScheduleDate)));
    	}

    	if( isset($this->params['url']['division']) && strlen($this->params['url']['division']) > 0 ){
    		if( strtolower($this->params['url']['division']) != 'all' ){
    		  	$oDivision = $this->ProgramsDivision->findByTitle($this->params['url']['division']);
        		if( !empty($oDivision) && isset($oDivision['ProgramsDivision']['id']) ){
	        		$conditions[] = array('Program.programs_division_id'=> $oDivision['ProgramsDivision']['id']);
		    	}
            }
    	}

        if( $this->Session->read('Auth.User.role_id') != Configure::read('administrator')){
    		if( isset($this->params['url']['q']) ){
	    		$conditions[] = array(
	    			"Program.programs_division_id" => $this->Session->read('Auth.User.role_id'),
	    			"Program.status" => Configure::read('status_live')
	    		);
    		}else{
    			$conditions = array(
	        		array(
	        			"Program.programs_division_id" => $this->Session->read('Auth.User.role_id'),
	        			"Program.status" => Configure::read('status_live')
	        		)
	    		);
    		}
    	}

    	$sPage = 1;
    	if( isset($this->params['url']['page']) ){
    		if( is_numeric($this->params['url']['page']) ){
    			$sPage = $this->params['url']['page'];
    		}
    	}

    	$order = array();
    	$recursive = 1;
    	if( !empty($query) && $query ){
    		$order = array($sOrder);
    	}else{
            $order = array("Program.id DESC");
        }

    	$conditions[]=array(
    					'AND' => array(
        					'Program.status'=> Configure::read('status_live'),
           					'BillingEb.status'=> Configure::read('status_live'),
                            'BillingEb.billing_type_id'=> constant('self::billing_type_id')
           				)
           			);

    	$this->paginate['BillingEb'] = array(
    		'recursive' => $recursive,
    		'conditions'=> $conditions,
    		'page'  => $sPage,
            'limit' => 20,
    		'order' => $order,
    		'group' => array('BillingEb.program_id')
        );

	    $this->set('billing_eb',$this->paginate('BillingEb'));
		$types = $this->ProgramsType->find('all',array('cache' => 'ProgramsType', 'cacheConfig' => 'cache_queries'));
		$divisions = $this->ProgramsDivision->find('list',array('cache' => 'ProgramsDivision', 'cacheConfig' => 'cache_queries'));
        $this->set( compact('divisions','types') );
    }
    
    public function admin_index(){
    	if( $this->Session->read('Auth.User.role_id') == Configure::read('administrator') ){
    		$this->paginate['BillingEb'] = array(
    				'order' => array('BillingEb.id' => 'desc'),
        			'conditions'=>  array(
           				'AND' => array(
        					'Program.status'=> Configure::read('status_live'),
           					'BillingEb.status'=> Configure::read('status_live'),
    						'BillingEb.billing_type_id'=> constant('self::billing_type_id')
           				)
           			),
           			'group' => array('BillingEb.program_id')
           	);
            $this->set('billing_eb',$this->paginate('BillingEb'));
        }else{
        	$this->paginate['BillingEb'] = array(
           			'order' => array('BillingEb.id' => 'desc'),
        			'conditions'=>  array(
           							"AND" => array(
           									'Program.programs_division_id'=> $this->Session->read('Auth.User.role_id'),
           									'Program.status'=> Configure::read('status_live'),
           									'BillingEb.status'=> Configure::read('status_live'),
        									'BillingEb.billing_type_id'=> constant('self::billing_type_id')	
           							)
           			),
           			'group' => array('BillingEb.program_id')
           	);
            $this->set('billing_eb',$this->paginate('BillingEb'));	
        }
        
		$types = $this->ProgramsType->find('list',array('cache' => 'ProgramsType', 'cacheConfig' => 'cache_queries'));
        $divisions = $this->ProgramsDivision->find('list',array('cache' => 'ProgramsDivision', 'cacheConfig' => 'cache_queries'));
        $this->set( compact('divisions','types') );
    }
    
	public function paginate($object = null, $scope = array(), $whitelist = array(), $key = null) {
      	$results = parent::paginate($object, $scope, $whitelist);
      	if ($key) {
       		$this->params['paging'][$key] = $this->params['paging'][$object];
       		unset($this->params['paging'][$object]);
      	}
		return $results;
    }
}