<?php
/**
 * Filemanager Controller
 *
 * PHP version 5
 *
 * @category Controller
 * @package  Croogo
 * @version  1.0
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class DocumentCertificatesController extends AppController {
/**
 * Controller name
 *
 * @var string
 * @access public
 */
    public $name = 'DocumentsCertificates';
	
    public $uses = array('DocumentsCertificate', 'User');
	
	public function beforeFilter() {
        parent::beforeFilter();
    }

    public function admin_index() {
    }
}