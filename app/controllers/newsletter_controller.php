<?php

class NewsletterController extends AppController {
    
    public $name = 'Newsletter';
    public $uses = array('Company','User','ProgramsDivision','CompaniesAffiliation','CompaniesIndustry','Program','Speaker');
    public $paginate = array(
        'limit' => 20,
        'order' => array('Company.name' => 'ASC')
    );


    public $helpers = array(
    	'Company',
        'CompanyExtraInfo',
    	'Program',
    	'ParticipantsTitle',
    	'ParticipantsGrouping',
    	'ParticipantsPosition',
    	'ProgramExtraInfo',
    	'Participant',
    	'ParticipantExtraInfo'
    );
    
    public function beforeFilter() {
        parent::beforeFilter();
	}

	public function admin_index(){
    
    }
}