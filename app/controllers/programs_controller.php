<?php

/**
 *
 */
class ProgramsController extends AppController {

    public $name = 'Programs';
    public $cacheQueries = true;
	public $uses = array(
    	'Program',
        'Speaker',
        'RatePlan',
    	'ProgramsDivision',
    	'ProgramsType',
    	'ProgramsSpeaker',
    	'ProgramsRatePlan', 
    	'ProgramsSchedule',
    	'ProgramsSpeaker',
    	'MansmithProgram',
    	'Day8Program',
    	'CoachProgram'
    );

    public $components = array('Zend');
    public $paginate = array(
        'Program' => array(
            'conditions' => array('Program.status'=> 1),
            'limit'	=> 20,
            'page'	=> 1,
            'order'	=> array(
                    'Program.id'	=> 'DESC')
            )
    );
    public $helpers = array(
        'Html',
    	'Program',
        'ProgramExtraInfo',
        'Participant',
        'ParticipantExtraInfo',
        'ParticipantsPosition',
        'AttendanceSheets',
        'Certificates',
        'ParticipantDirectory',
        'NameTags'
    );

    private $gCalendarClient = null;
    public function beforeFilter() {
        $this->Security->validatePost = false;
        parent::beforeFilter();

        if( ($types = Cache::read('types','long')) === false) {
            $types = $this->ProgramsType->find('list',array('cache' => 'ProgramsType', 'cacheConfig' => 'cache_queries'));
            Cache::write('types', $types,'long');
        }

        if( ($divisions = Cache::read('divisions','long')) === false) {
            $divisions = $this->ProgramsDivision->find('list',array('cache' => 'ProgramsDivision', 'cacheConfig' => 'cache_queries'));
            Cache::write('divisions', $divisions,'long');
        }

        if( ($rateplans  = Cache::read('rateplans','long')) === false) {
            $rateplans = $this->RatePlan->find('list',array('cache' => 'RatePlan', 'cacheConfig' => 'cache_queries'));
            Cache::write('rateplans', $rateplans,'long');
        }

        //if( ($speakers  = Cache::read('speakers','short')) === false) {
        $speakers = $this->Speaker->find('all',array('cache' => 'Speaker', 'cacheConfig' => 'cache_queries'));
        Cache::write('speakers', $speakers,'short');
        //}
        $this->set( compact('divisions','speakers','rateplans','types') );
    }
//
    /**
     *
     */
    private function invokeGoogleCalendar(){
		$this->Zend->loadClass('Zend_Gdata');
        $this->Zend->loadClass('Zend_Gdata_ClientLogin');
        $this->Zend->loadClass('Zend_Gdata_Calendar');
        $this->Zend->loadClass('Zend_Http_Client');

        // Parameters for ClientAuth authentication
        $service = Zend_Gdata_Calendar::AUTH_SERVICE_NAME;
        $sGCalendarLogin = Configure::read('gCalendar.user');
        $sGCalendarPass  = Configure::read('gCalendar.pass');
        $oGHttpClient = Zend_Gdata_ClientLogin::getHttpClient($sGCalendarLogin, $sGCalendarPass, $service);
        $oGHttpClient->setConfig(array( 'timeout' => 180 )); 
        try{
        	$this->gCalendarClient = new Zend_Gdata_Calendar($oGHttpClient);	
        }catch( Exception $e){
        	/**
			TODO send error to frank
        	 */
        	$this->gCalendarClient = false;
        }
    }

    /*
     *
     */
    private function setEventToGoogleCalendar( array $program_schedule , array $program ){

        if( is_object($this->gCalendarClient) && $this->gCalendarClient ){
        	$startDate = $program_schedule['start_date'];
            $startTime = date("H:i" , strtotime($startDate.' '.$program_schedule['start_time']) );
            $endDate   = $program_schedule['end_date'];
            $endTime   = date("H:i" , strtotime($endDate.' '.$program_schedule['end_time']) );
            $tzOffset  = '+08';
            $newEntry = $this->gCalendarClient->newEventEntry();
            $newEntry->title = $this->gCalendarClient->newTitle(trim($program['Program']['title']));
            $where  = trim($program['Program']['venue_name']);
            $where .= (strlen($program['Program']['address'])>0 ) ? ', '.trim($program['Program']['address']):'';
            $where .= (strlen($program['Program']['suburb'])>0 ) ? ', '.trim($program['Program']['suburb']):'';
            $where .= (strlen($program['Program']['city'])>0 ) ? ', '.trim($program['Program']['city']):'';
            $where .= (strlen($program['Program']['country'])>0 ) ? ', '.trim($program['Program']['country']):'';
            $where .= (strlen($program['Program']['zip_code'])>0 ) ? ' '.trim($program['Program']['zip_code']):'';

            $newEntry->where  = array($this->gCalendarClient->newWhere($where));

            $content  = (strlen($program['Program']['notes'])>0) ? trim($program['Program']['notes']):'';
            $content .= "For complete course outlines, enquiries and reservations please check our website http://www.mansmith.net";
            $content .= "<br /> or contact ".$program['Program']['contact_name']." at Office: ".$program['Program']['contact_phone'];
            $content .= "\nMobile: 0918-81-168-88 \nEmail: ".$program['Program']['contact_email'];

            $newEntry->content = $this->gCalendarClient->newContent($content);
            $newEntry->content->type = 'text';

            $when = $this->gCalendarClient->newWhen();
            $when->startTime = "{$startDate}T{$startTime}:00.000{$tzOffset}:00";
            $when->endTime = "{$endDate}T{$endTime}:00.000{$tzOffset}:00";
            $newEntry->when = array($when);
            $createdEntry = $this->gCalendarClient->insertEvent($newEntry);
            $event_id = $createdEntry->id->text;

            $this->ProgramsSchedule->id = $program_schedule['id'];
            $this->ProgramsSchedule->saveField('gCalendar_eventId', $event_id);
        }
    }

    public function admin_index($sFilter=null){
        if( $this->Session->read('Auth.User.role_id') != Configure::read('administrator') ){
           	$this->paginate['Program'] = array(
           			'order' => array('Program.id' => 'desc'),
        			'conditions'=>  array(
           							"AND" => array(
           									'Program.programs_division_id'=> $this->Session->read('Auth.User.role_id'),
           									'Program.status'=> Configure::read('status_live')
           							)
           			)
        	);
            $programs  = $this->paginate('Program');
            $this->set(compact('programs'));
        }else{
           $this->paginate['Program'] = array(
		   			'recursive' =>1,
		   			'conditions'=>  array('Program.status'=> Configure::read('status_live')),
		   			'order' => array('Program.id' => 'desc')
           );
           $this->set('programs',$this->paginate('Program'));
           
           $aProgramDivisions = $this->ProgramsDivision->find('all',array('cache' => 'ProgramsDivision', 'cacheConfig' => 'cache_queries'));
           $iLoop = 1;
           foreach( $aProgramDivisions as $division_key => $division ){
           		$iDivisionId = intval($division['ProgramsDivision']['id']);
                $sModelName = ucfirst($division['ProgramsDivision']['title']).'Program';
                
                $this->paginate[$sModelName] = array(
                		'recursive' =>1,
                		'conditions'=>array(
                						"AND" => array(
                									$sModelName.'.programs_division_id'=> $iDivisionId,
                									$sModelName.'.status' => Configure::read('status_live')
                								)
                					  ),
	                	'limit' => 20,
                		'order' => array($sModelName.'.id' => 'desc')
                );
                
                if( isset($sFilter) && !is_null($sFilter)){
	                if( strtolower($sFilter) == strtolower($division['ProgramsDivision']['title']) ){
	                	$this->set('selected_program',$iLoop);
	                	$this->set('selected_program_name',$division['ProgramsDivision']['title']);		
	                }
	            }
	            
                $iLoop++;
                $division_alias = strtolower($division['ProgramsDivision']['alias']);
                $this->set($division_alias,$this->paginate($sModelName));
            }
        }
    }

    public function admin_list($searchType=null){
        $oDivision = $this->ProgramsDivision->findByTitle($searchType);
        if( !isset($oDivision['ProgramsDivision']['id']) && ($oDivision['ProgramsDivision']['id']) == Configure::read('status_live') ) {
            $this->Session->setFlash(__('Invalid program or program might have been deleted.', true), 'default', array('class' => 'error'));
            $this->redirect('/');
        }

         
        if( $this->Session->read('Auth.User.role_id') != Configure::read('administrator') && $oDivision['ProgramsDivision']['id'] != $this->Session->read('Auth.User.role_id')  ){
            $this->Session->setFlash(__('Invalid program or you are not allowed to access this information.', true), 'default', array('class' => 'error'));
            $this->redirect('/');
        }
        
        $this->paginate['Program'] = array(
					'order' => array('Program.id' => 'desc')	,
        			'conditions'=>array(
        							"AND" => array(
        								'Program.programs_division_id'=> $oDivision['ProgramsDivision']['id'],
										'Program.status'=> Configure::read('status_live')
									) 
								  )
				);
		$this->set('programs',$this->paginate('Program'));
        $this->set('division', strtolower($searchType) );
    } 

    public function admin_create(){
    }
    
    public function admin_edit( $program_id=0, $seo_name=null){
    	$seo_name = trim($seo_name);
        $program_id = ( is_numeric($program_id) && $program_id > 0 ) ? intval($program_id): 0;
    	$oProgram = $this->Program->findById($program_id);
        if( ( 
        	  strtolower($seo_name) != strtolower($oProgram['Program']['seo_name']) )|| 
        	  $oProgram['Program']['status'] != Configure::read('status_live') 
        ) {
            $this->Session->setFlash(__('Invalid program or program has been deleted.', true), 'default', array('class' => 'error'));
            $this->redirect('/');
        }
        
        if( $this->Session->read('Auth.User.role_id') != Configure::read('administrator')  && $oProgram['Program']['programs_division_id'] != $this->Session->read('Auth.User.role_id')  ){
            $this->Session->setFlash(__('Invalid program or you are not allowed to access this information.', true), 'default', array('class' => 'error'));
            $this->redirect('/');
        }
		
        $this->set('program',$oProgram);
        if( $this->Session->read('Auth.User.role_id') == Configure::read('administrator') ){
           $speakers = $this->Speaker->find('all',array('cache' => 'Speaker', 'cacheConfig' => 'cache_queries'));
        }else{
           $speakers = $this->Speaker->findByProgramsDivisionId($this->Session->read('Auth.User.role_id'));
        }
        
        $this->set('title_for_layout','Update Program Information for '.ucwords(strtolower($oProgram['Program']['title'])));	
        //$this->set( compact('speakers') );
    }
    
	public function admin_search(){
		
		$query = null;
        $sOrder = null;
        if( isset($this->params['url']['q']) && strlen(trim($this->params['url']['q']))> 0 ){
        	$query = trim($this->params['url']['q']);
        }
        
        $conditions = array();
        
        if( !empty($query) && strlen($query)> 0 ){
        	
        	App::import('Sanitize');
        	
 	       	$full_text_search = explode (" ", $query );
     	   	foreach ($full_text_search as $wordindex => $wordvalue) {
				if( strlen(trim($wordvalue))==0) {
					unset($full_text_search[$wordindex]);
            	}
			}
    
			$fulltext_array = array();
			$like_array = array();
			$original_search = array();
			foreach ($full_text_search as $wordindex => $wordvalue) {
				if( strlen ($wordvalue)<=3 ) {
					$like_array[] = Sanitize::clean($wordvalue);
            	}else{
            	    $fulltext_array[] = "+".Sanitize::clean($wordvalue)."*";
            	}
            }
        	
			$original_search = array_merge($like_array,$fulltext_array);
			$boolSearchByLike = $this->Program->getLikeTitleName($query);
        	if( $boolSearchByLike == true){
        		$conditions[] = array(
	                           		"(TRIM(UPPER(Program.title))) LIKE '%".addslashes($query)."%'",
	        						"Program.status"=> Configure::read('status_live')
	        	);
	        	$sOrder = "TRIM(UPPER(Program.title)) ASC";
	        	
        	}else{
        		
        		$fullword_query = trim(implode(' ', $original_search));
				$conditions[] = array(
	        		"( 
						(MATCH(Program.title,Program.notes,Program.venue_name,Program.address,Program.suburb,Program.city,Program.notes,Program.website,Program.content) AGAINST ('".addslashes(trim($fullword_query))."' IN boolean MODE)) OR 
	                    (TRIM(UPPER(Program.title))) LIKE '%".addslashes($query)."%'
	                )",
	        		"( LENGTH(TRIM(Program.title))> 0 )",	
	        		"Program.status"=> Configure::read('status_live')
	        	);
	        	$sOrder = "MATCH(Program.title,Program.notes,Program.venue_name,Program.address,Program.suburb,Program.city,Program.notes,Program.website,Program.content) AGAINST ('".addslashes(trim($query))."') DESC,TRIM(UPPER(Program.title)) ASC";
	        }
        }else{//end of if query
            $conditions[] = array("Program.status" => Configure::read('status_live'));
        }	
        
	    if( isset($this->params['url']['type']) && strlen($this->params['url']['type']) > 0 ){
            if( strtolower($this->params['url']['type']) != 'all' ){
	    		$oProgramsType = $this->ProgramsType->findBySeoName($this->params['url']['type']);
	    		$conditions[] = array(
	    				"Program.programs_type_id" => $oProgramsType['ProgramsType']['id'],
	    				"Program.status" => Configure::read('status_live')
	    		);
	    		$this->set('program_type',$oProgramsType['ProgramsType']['name']);
            }
	    }
	    
	    $schedule_date = null;
    	if( isset($this->params['url']['schedule_date']) && strlen($this->params['url']['schedule_date']) > 0 ){
    		$schedule_date = trim($this->params['url']['schedule_date']);
    	}
    	
		if( isset($this->params['url']['division']) && strlen($this->params['url']['division']) > 0 ){
    		if( strtolower($this->params['url']['division']) != 'all' ){
    		  	$oDivision = $this->ProgramsDivision->findByTitle($this->params['url']['division']);
        		if( !empty($oDivision) && isset($oDivision['ProgramsDivision']['id']) ){ 
	        		$conditions[] = array('Program.programs_division_id'=> $oDivision['ProgramsDivision']['id']);
		    	}
            }
    	}
    	
    	if( $this->Session->read('Auth.User.role_id') != Configure::read('administrator') ){
    		$conditions[] = array('Program.programs_division_id'=> $this->Session->read('Auth.User.role_id'));
    	}
    	
    	$sPage = 1;
    	if( isset($this->params['url']['page']) ){
    		if( is_numeric($this->params['url']['page']) ){
    			$sPage = $this->params['url']['page'];
    		}
    	}
    	
    	$order = array();
    	$recursive = 1;
    	if( !empty($query) && $query ){
    		$order = array($sOrder);
    	}else{
            $order = array( "Program.id DESC" );
        }
    	
        if( !empty($schedule_date) && $schedule_date ){
    			list($month, $year)= explode("/",$schedule_date);
    			$sScheduleDate = date("Y-m",strtotime($year."-".$month."-01"));
                $program_ids = $this->Program->ProgramsSchedule->find('list',array(
                          	'fields' => array( 'ProgramsSchedule.program_id' ),
                          	'recursive'  => -1,
			  				'conditions' => array("ProgramsSchedule.start_date LIKE '%".date("Y-m",strtotime($sScheduleDate))."%'"),
                          	'group' => array('ProgramsSchedule.program_id') //fields to GROUP BY
					)
                );
                if( sizeof($program_ids) > 0 && $program_ids ){
                    $conditions[] = array("Program.id IN (".implode(",",$program_ids).") ");
                }else{
                    $conditions[] = array("Program.id =0 ");
                }
                $conditions[] = array("Program.status" => Configure::read('status_live'));
                $this->set('schedule_date',date("M Y",strtotime($sScheduleDate)));
    	}
    	
    	
    	$this->paginate['Program'] = array(
    					'recursive' => $recursive,
    					'conditions'=> $conditions,
    					'page'  => $sPage,
                        'limit' => 20,
    			   		'order' => $order
                );	
        $this->set('programs',$this->paginate('Program'));
        $types = $this->ProgramsType->find('all',array('cache' => 'ProgramsType', 'cacheConfig' => 'cache_queries'));
        $this->set(compact('types'));
    }

    public function admin_info( $program_id=0, $seo_name=null){
        Configure::write('debug',0);
    	$seo_name = trim($seo_name);
    	if(strstr($seo_name,'#program_participants')){
    		$seo_name = str_replace('#program_participants', '', $seo_name);
    	}
    	
        $program_id = ( is_numeric($program_id) && $program_id > 0 ) ? intval($program_id): 0;
        $oProgram = $this->Program->findById($program_id);
        if( $oProgram['Program']['status'] != Configure::read('status_live') ) {
            $this->Session->setFlash(__('Invalid program or program has been deleted.', true), 'default', array('class' => 'error'));
            $this->redirect('/');
        }
        
        if( $this->Session->read('Auth.User.role_id') != Configure::read('administrator')  && $oProgram['Program']['programs_division_id'] != $this->Session->read('Auth.User.role_id')  ){
            $this->Session->setFlash(__('Invalid program or you are not allowed to access this information.', true), 'default', array('class' => 'error'));
            $this->redirect('/');
        }
		
        $this->set('program',$oProgram);
        $this->Speaker->recursive = -1;
        if( $this->Session->read('Auth.User.role_id') == Configure::read('administrator') ){
           $speakers = $this->Speaker->find('all',array('cache' => 'Speaker', 'cacheConfig' => 'cache_queries'));
        }else{
           $speakers = $this->Speaker->findByProgramsDivisionId($this->Session->read('Auth.User.role_id'));
        }

        $this->ProgramsScheduleType = &ClassRegistry::init('ProgramsScheduleType');
        $this->ProgramsScheduleType->contain();
        $programsScheduleType = $this->ProgramsScheduleType->find('all');
        $this->set('title_for_layout','Program Information for '.ucwords(strtolower($oProgram['Program']['title'])));
        $this->set( compact('programsScheduleType') );
    }

    public function admin_programs_participants(){
        $this->layout = false;
        $this->autoRender = false;
        if( isset($this->data)  ){
            if( isset($this->data['ProgramsParticipant']['id']) ){
                $totalRemoved = 0;
                $this->ProgramsParticipant = &ClassRegistry::init('ProgramsParticipant');
                foreach( $this->data['ProgramsParticipant']['id'] as $key => $program_participant_id ){
                    $data = array();
                    //$this->ProgramsParticipant->begin();
                    $sql = "DELETE FROM programs_participants WHERE id=".intval($program_participant_id);
                    $this->ProgramsParticipant->query($sql);
                    /*
                    $this->ProgramsParticipant->id = $program_participant_id;
                    $data['ProgramsParticipant']['status'] = 2;
                    $data['ProgramsParticipant']['date_modified'] = date("Y-m-d H:i:s");
                    $data['ProgramsParticipant']['who_modified'] = $this->Session->read('Auth.User.id');
                    if( $this->ProgramsParticipant->delete($data) ){
                        $this->ProgramsParticipant->commit();
                        $totalRemoved++;
                    }*/
                }
            }
            $this->Session->setFlash(__('Delete successful.', true), 'default',array('class' => 'success'));
            $this->redirect(array('admin'=>true,'controller'=>'programs','action'=>'program_participants',intval($this->data['Program']['id'])));
        }else{
            $this->Session->setFlash(__('Delete unsuccessful.', true), 'default',array('class' => 'error'));
            $this->redirect("/");
        }
        die;
    }

    public function admin_add_program_participants(){
        Configure::write('debug',0);
        $this->layout = 'iframe';
        $this->autoRender = false;
        if( isset($this->data) ){
            if( isset($this->data['ProgramsParticipant']['participant_id']) ){
                $this->ProgramsParticipant = &ClassRegistry::init('ProgramsParticipant');
                foreach( $this->data['ProgramsParticipant']['participant_id'] as $participant_id ){
                    $this->ProgramsParticipant->begin();
                    $data = array();
                    $this->ProgramsParticipant->id = null;
                    $data['ProgramsParticipant']['program_id'] = $this->data['ProgramsParticipant']['program_id'];
                    $data['ProgramsParticipant']['company_id'] = $this->data['ProgramsParticipant']['company_id'];
                    $data['ProgramsParticipant']['participant_id'] = $participant_id;
                    $data['ProgramsParticipant']['status'] = 1;
                    $data['ProgramsParticipant']['programs_participant_status_id'] = 1;
                    $data['ProgramsParticipant']['date_created'] = date("Y-m-d H:i:s");
                    $data['ProgramsParticipant']['date_modified'] = date("Y-m-d H:i:s");
                    $data['ProgramsParticipant']['who_modified'] = $this->Session->read('Auth.User.id');
                    if( $this->ProgramsParticipant->save($data) ){
                        $this->ProgramsParticipant->commit();
                    }
                }

            }

            if( isset($this->data['ProgramsCompanyContact']['participant_id']) ){

                $this->ProgramsCompanyContact = &ClassRegistry::init('ProgramsCompanyContact');
                $sql  = " DELETE FROM  programs_company_contacts WHERE program_id=".intval($this->data['ProgramsParticipant']['program_id']);
                $sql .= " AND company_id =".intval($this->data['ProgramsParticipant']['company_id']);
                $this->ProgramsCompanyContact->query($sql);

                $data = array();
                $this->ProgramsCompanyContact->begin();
                $data['ProgramsCompanyContact']['program_id'] = $this->data['ProgramsParticipant']['program_id'];
                $data['ProgramsCompanyContact']['company_id'] = $this->data['ProgramsParticipant']['company_id'];
                $data['ProgramsCompanyContact']['participant_id'] = $this->data['ProgramsCompanyContact']['participant_id'];
                $data['ProgramsCompanyContact']['date_created'] = date("Y-m-d H:i:s");
                $data['ProgramsCompanyContact']['date_modified'] = date("Y-m-d H:i:s");
                $data['ProgramsCompanyContact']['who_modified'] = $this->Session->read('Auth.User.id');
                if( $this->ProgramsCompanyContact->save($data) ){
                    $this->ProgramsCompanyContact->commit();
                }
            }
            $this->Session->setFlash(__('Add program participants successful.', true), 'default',array('class' => 'success'));
            $this->redirect(array('admin'=>true,'controller'=>'programs','action'=>'add_programs_participants',intval($this->data['ProgramsParticipant']['program_id'])));
        }
    }

    public function admin_add_programs_participants($program_id=null){
        Configure::write('debug',0);
        if( intval($program_id) > 0 ){
            $this->layout = 'iframe';
            $this->set('program_id',$program_id);
        }else{
            //$this->redirect("/");
        }
    }

    public function admin_programs_schedule($program_id=null){
        Configure::write('debug',0);
        if( intval($program_id) > 0 ){
            $this->layout = 'iframe';
            if( isset($page) && intval($page)>0 && is_numeric($page) ){
                $sPage = intval($page);
            }else{
                $sPage = 1;
            }

            $this->ProgramsSchedule = &ClassRegistry::init('ProgramsSchedule');
            $this->ProgramsScheduleType = &ClassRegistry::init('ProgramsScheduleType');
            $this->ProgramsScheduleType->contain();
            $order = 'ProgramsSchedule.start_date ASC';
            $conditions = array(
                'ProgramsSchedule.program_id' => $program_id,
                'ProgramsSchedule.status' => Configure::read('status_live'),
                'Program.status' => Configure::read('status_live')
            );
            $this->paginate['ProgramsSchedule'] = array(
                'conditions'=> $conditions,
                'page'  => $sPage,
                'limit' => $this->paginate['Program']['limit'],
                'order' => $order
            );


            $this->set('program_id',$program_id);
            $this->set('program_schedule_types', $this->ProgramsScheduleType->find('all'));
            $this->set('programs_schedules',$this->paginate('ProgramsSchedule'));

        }else{
            $this->redirect('/');
        }
    }

    public function admin_programs_document($program_id){
        $this->layout = 'iframe';
        Configure::write('debug',0);
        if( intval($program_id) > 0 ){
            $this->layout = 'iframe';
            if( isset($page) && intval($page)>0 && is_numeric($page) ){
                $sPage = intval($page);
            }else{
                $sPage = 1;
            }

            $this->ProgramsParticipantStatus = &ClassRegistry::init('ProgramsParticipantStatus');
            $this->ProgramsParticipant = &ClassRegistry::init('ProgramsParticipant');
            $this->ProgramsParticipant->bindModel(array(
                'hasOne' => array(
                    'ParticipantsTitle' => array(
                        'foreignKey' => false,
                        'conditions' => array('Participant.participants_title_id = ParticipantsTitle.id')
                    ),
                )
            ));
            $this->ProgramsParticipant->contain(array('Company','Participant','ParticipantsTitle'));
            $order = 'TRIM(UPPER(`Participant`.`full_name`)) ASC';
            $conditions = array(
                'ProgramsParticipant.program_id' => intval($program_id) ,
                'ProgramsParticipant.status' => Configure::read('status_live')
            );
            $this->paginate['ProgramsParticipant'] = array(
                'conditions'=> $conditions,
                'page'  => $sPage,
                'limit' => $this->paginate['Program']['limit'],
                'order' => $order
            );

            /** @var ProgramsAttendanceSheet */
            $this->ProgramsAttendanceSheet = &ClassRegistry::init('ProgramsAttendanceSheet');
            $this->paginate['ProgramsAttendanceSheet'] = array(
                'order' => array('ProgramsAttendanceSheet.id' => 'desc'),
                'conditions'=>  array(
                    'AND' => array(
                        'Program.id' => intval($program_id),
                        'Program.status'=> Configure::read('status_live'),
                        'ProgramsAttendanceSheet.status'=> Configure::read('status_live')
                    )
                )
            );

            $this->set('attendancesheets',$this->paginate('ProgramsAttendanceSheet'));

            /** @var ProgramsCertificate */
            $this->paginate['ProgramsCertificate'] = array(
                'order' => array('ProgramsCertificate.id' => 'desc'),
                'conditions'=>  array(
                    'AND' => array(
                        'Program.id' => intval($program_id),
                        'Program.status'=> Configure::read('status_live'),
                        'ProgramsCertificate.status'=> Configure::read('status_live')
                    )
                ),
            );
            $this->set('certificates',$this->paginate('ProgramsCertificate'));

            /** @var ProgramsParticipantDirectory */
            $this->paginate['ProgramsParticipantDirectory'] = array(
                'order' => array('ProgramsParticipantDirectory.id' => 'desc'),
                'conditions'=>  array(
                    'AND' => array(
                        'Program.id' => intval($program_id),
                        'Program.status'=> Configure::read('status_live'),
                        'ProgramsParticipantDirectory.status'=> Configure::read('status_live')
                    )
                ),
            );
            $this->set('directoryparticipants',$this->paginate('ProgramsParticipantDirectory'));

            /** @var ProgramsNameTag  */
            $this->ProgramsNameTag = &ClassRegistry::init('ProgramsNameTag');
            $this->ProgramsNameTag->contain();
            $this->paginate['ProgramsNameTag'] = array(
                'order' => array('ProgramsNameTag.id' => 'desc'),
                'conditions'=>  array(
                    'AND' => array(
                        'Program.id' => intval($program_id),
                        'Program.status'=> Configure::read('status_live'),
                        'ProgramsNameTag.status'=> Configure::read('status_live')
                    )
                )
            );
            $this->set('nametags',$this->paginate('ProgramsNameTag'));


            $this->set('program_id',$program_id);
            $this->set('program_participant_status', $this->ProgramsParticipantStatus->find('all'));
            $this->set('participants',$this->paginate('ProgramsParticipant'));

        }else{
            $this->redirect('/');
        }
    }


    /*
     *
     */
    public function admin_programs_billing($program_id){
        $this->layout = 'iframe';
        Configure::write('debug',0);
        if( intval($program_id) > 0 ){
            $this->layout = 'iframe';

            $this->ProgramsParticipant = &ClassRegistry::init('ProgramsParticipant');
            $this->ProgramsParticipant->contain(array('Company'));
            $companies = $this->ProgramsParticipant->find('all',array(
                'conditions' => array(
                    'ProgramsParticipant.program_id' => intval($program_id) ,
                    'ProgramsParticipant.status' => Configure::read('status_live')
                ),
                'group' => array('ProgramsParticipant.company_id'),
                'order' => array('UPPER(TRIM(Company.name))')
            ));
            $this->set('program_id',$program_id);
            $this->set(compact('companies'));
        }else{
            $this->redirect('/');
        }
    }

    /*
     *
     */
    public function admin_programs_billing_summary($program_id=null,$company_id=null){
        $this->layout = 'iframe';
        Configure::write('debug',0);
        if( intval($program_id) > 0 ){
            $this->layout = 'iframe';

            $this->BillingStatus = &ClassRegistry::init('BillingStatus');
            $this->BillingType = &ClassRegistry::init('BillingType');
            $this->PaymentStatus = &ClassRegistry::init('PaymentStatus');


            $payment_status = $this->PaymentStatus->find('list',array(
                    'cache' => 'PaymentStatus', 'cacheConfig' => 'cache_queries',
                    'fields' => array('PaymentStatus.id','PaymentStatus.status'),
                    'recursive' => 0
                )
            );

            $this->ProgramBillingInfo = &ClassRegistry::init('ProgramBillingInfo');
            $program_billing_info = $this->ProgramBillingInfo->find('all',array(
                'conditions' => array(
                    'ProgramBillingInfo.program_id' => intval($program_id) ,
                    'ProgramBillingInfo.company_id' => intval($company_id)
                ),
            ));

            if( is_array($program_billing_info) && count($program_billing_info) > 0  ){
                $program_billing_info = $program_billing_info[0];
            }else{
                $program_billing_info = false;
            }

            $this->ProgramsParticipant = &ClassRegistry::init('ProgramsParticipant');
            $this->ProgramsParticipant->contain(array('Participant'));
            $participants = $this->ProgramsParticipant->find('all',array(
                'conditions' => array(
                    'ProgramsParticipant.program_id' => intval($program_id) ,
                    'ProgramsParticipant.status' => Configure::read('status_live'),
                    'ProgramsParticipant.company_id' => intval($company_id)
                ),
                'order' => array('UPPER(TRIM(Participant.last_name))')
            ));

            $billing_status = $this->BillingStatus->find('list',array(
                'cache' => 'BillingStatus', 'cacheConfig' => 'cache_queries',
                'fields' => array('BillingStatus.id','BillingStatus.status'),
                'order' => array('BillingStatus.id' => 'DESC')
            ));
            $billing_type = $this->BillingType->find('list',array('cache' => 'BillingType', 'cacheConfig' => 'cache_queries'
            ));
            $this->set('program_id',$program_id);
            $this->set(compact('participants','program_billing_info','company_id','billing_status','billing_type','payment_status'));
        }else{
            $this->redirect('/');
        }
    }

    public function admin_add_programs_billing_summary(){
        Configure::write('debug',0);
        $this->layout = false;
        $this->autoRender = false;
        if( isset($this->data) && $this->data['ProgramBillingInfo'] ){
            $this->ProgramBillingInfo = &ClassRegistry::init('ProgramBillingInfo');
            $this->ProgramBillingInfo->begin();
            $data = array();
            if( !$this->data['ProgramBillingInfo']['id'] ){
                $this->ProgramBillingInfo->id = null;
                $data['ProgramBillingInfo']['date_created'] = date("Y-m-d H:i:s");
                $data['ProgramBillingInfo']['who_created'] = $this->data['Program']['who_created'];

            }else{

                $this->ProgramBillingInfo->id = intval($this->data['ProgramBillingInfo']['id']);
                $data['ProgramBillingInfo']['date_modified'] = date("Y-m-d H:i:s");
                $data['ProgramBillingInfo']['who_modified'] = $this->Session->read('Auth.User.id');
            }
            list( $participantId, $companyId ) = explode("_",$this->data['ProgramBillingInfo']['participant_id']);

            $billing_hour  = ($this->data['ProgramBillingInfo']['billed_date_ss']=='pm') ? ($this->data['ProgramBillingInfo']['billed_date_hh']+12):$this->data['ProgramBillingInfo']['billed_date_hh'];
            $billing_date  = $this->data['ProgramBillingInfo']['billed_date_year'].'-';
            $billing_date .= $this->data['ProgramBillingInfo']['billed_date_month'].'-';
            $billing_date .= $this->data['ProgramBillingInfo']['billed_date_day'].' ';
            $billing_date .= $billing_hour.':'.$this->data['ProgramBillingInfo']['billed_date_mm'].':00';

            $payment_hour  = ($this->data['ProgramBillingInfo']['payment_date_ss']=='pm') ? ($this->data['ProgramBillingInfo']['payment_date_hh']+12):$this->data['ProgramBillingInfo']['payment_date_hh'];
            $payment_date  = $this->data['ProgramBillingInfo']['payment_date_year'].'-';
            $payment_date .= $this->data['ProgramBillingInfo']['payment_date_month'].'-';
            $payment_date .= $this->data['ProgramBillingInfo']['payment_date_day'].' ';
            $payment_date .= $payment_hour.':'.$this->data['ProgramBillingInfo']['payment_date_mm'].':00';

            $data['ProgramBillingInfo']['program_id'] = $this->data['Program']['id'];
            $data['ProgramBillingInfo']['company_id'] = $companyId;
            $data['ProgramBillingInfo']['participant_id'] = $participantId;
            $data['ProgramBillingInfo']['billing_type_id'] = $this->data['ProgramBillingInfo']['billing_type_id'];
            $data['ProgramBillingInfo']['billed_date'] = date("Y-m-d H:i:s",strtotime($billing_date));
            $data['ProgramBillingInfo']['payment_date'] = date("Y-m-d H:i:s",strtotime($payment_date));
            $data['ProgramBillingInfo']['billing_status_id'] = $this->data['ProgramBillingInfo']['billing_status_id'];
            $data['ProgramBillingInfo']['notes'] = $this->data['ProgramBillingInfo']['notes'];
            $data['ProgramBillingInfo']['status'] = $this->data['ProgramBillingInfo']['status'];
            if( $this->ProgramBillingInfo->save($data) ){
                $this->ProgramBillingInfo->commit();
                $this->Session->setFlash(__('Program billing info successful.', true), 'default',array('class' => 'success'));
            }
        }
        $this->redirect(array('admin'=>true,'controller'=>'programs','action'=>'admin_programs_billing_summary',intval($this->data['Program']['id']).'/'.intval($companyId)));
        die;
    }

    private function billingDocumentDetails($oProgram,$company_id=null,$documentType='eb',$display=true){
        $totalParticipants = 0;
        if( $company_id && isset($oProgram['ProgramsParticipant']) ){
            foreach( $oProgram['ProgramsParticipant'] as $oParticipant ){
                if(
                    $oParticipant['programs_participant_status_id'] == Configure::read('status_live') &&
                    $oParticipant['status'] == Configure::read('status_live') &&
                    $oParticipant['company_id'] == $company_id
                ){
                    $totalParticipants++;
                }
            }
        }

        if( $company_id > 0 ){
            $this->set('company_id',$company_id);
            $this->set('totalParticipants',$totalParticipants);
        }

        $this->set('group_discount','Off');
        $discount_applies = false;
        $ebDisplay = null;
        $regularDisplay = null;
        $ospDisplay = null;
        if( intval($oProgram['Program']['discount_applies'])==1 ){//discount applies equal 1
            $totalParticipantsDimensions = floor($totalParticipants/4);//if more than 4 group discount applies
            if( $totalParticipantsDimensions > 0 ){//group discount if participants > 4
                $discount_applies = true;
                $this->set('group_discount','On');

                $totalParticipantsPercentage = $totalParticipantsDimensions * 0.5;
                $totalParticipantsDiscount = $totalParticipants - $totalParticipantsPercentage;
                /**
                 * Discounted Eb
                 */
                if( $totalParticipantsDiscount < 1){
                    $totalParticipantsDiscountFlag = 1;
                }else{
                    $totalParticipantsDiscountFlag = floor($totalParticipantsDiscount);
                }

                /**
                 * Implementation changes as of 2008 08 05 by Tochi
                 * I don't know the previous version
                 */
                $participantsNDCF = $totalParticipants - $totalParticipantsDimensions;

                if( $documentType == 'eb' ){
                    $totalEbNTotalDiscount = $oProgram['ProgramsRatePlan'][0]['eb_cost'] * $participantsNDCF;//total discount people
                    $totalEbNTotalDiscountFlag = $oProgram['ProgramsRatePlan'][0]['reg_cost'] * $totalParticipantsPercentage;


                    $totalEbNwv = number_format($totalEbNTotalDiscount + $totalEbNTotalDiscountFlag,2,'.','');
                    $totalEbNVAT = $totalEbNwv * Configure::read('VAT');
                    $totalEbNDTotal = $totalEbNwv + $totalEbNVAT;

                    $totalEbDiscount = $totalParticipantsDiscountFlag * $oProgram['ProgramsRatePlan'][0]['eb_cost'];
                    $totalEbDiscountFlag = ( $oProgram['ProgramsRatePlan'][0]['reg_cost'] * $totalParticipantsDimensions ) * $totalParticipantsPercentage;

                    $totalEbMV = $totalEbDiscount + $totalEbNTotalDiscountFlag;
                    $totalEbVAT = $totalEbMV * Configure::read('VAT');

                    $ebDisplay  = 'PHP '.number_format($oProgram['ProgramsRatePlan'][0]['eb_cost'],2,'.',',')." Seminar Investment <br>";
                    $ebDisplay .= 'x '.$participantsNDCF." No. of pax <br>";
                    $ebDisplay .= "------------------<br>";
                    $ebDisplay .= "<b>".number_format($totalEbNTotalDiscount,2,'.',',')."</b><br>";
                    $ebDisplay .= "+ ".number_format($oProgram['ProgramsRatePlan'][0]['reg_cost'],2,'.',',').$totalParticipantsDimensions."<br>";
                    $ebDisplay .= "x ".$totalParticipantsPercentage." No. of discounted pax.<br>";
                    $ebDisplay .= "------------------<br>";
                    $ebDisplay .= "<b>".number_format($totalEbNTotalDiscountFlag,2,'.',',')."</b><br>";
                    $ebDisplay .= "<b>".number_format($totalEbNwv,2,'.',',')."</b><br>";
                    $ebDisplay .= "+ ".number_format($totalEbNVAT,2,'.',',')." ".Configure::read('VAT_PERCENTAGE')."% VAT<br>";
                    $ebDisplay .= "------------------<br>";
                    $ebDisplay .= "<b>PHP ".number_format($totalEbNDTotal,2,'.',',')."</b>";
                }elseif ( $documentType =='non_eb' ){

                    $totalEbDiscount = $oProgram['ProgramsRatePlan'][0]['eb_cost'] * $totalParticipantsDiscountFlag;
                    $totalEbDiscountFlag = ( $oProgram['ProgramsRatePlan'][0]['reg_cost'] * $totalParticipantsDimensions ) * $totalParticipantsPercentage;

                    $totalEbWV = $totalEbDiscount + $totalEbDiscountFlag;
                    $totalEbVAT = $totalEbWV * Configure::read('VAT');

                    $totalEbDiscount = $totalEbWV + $totalEbVAT;
                    $ebDisplay  = 'PHP '.number_format($oProgram['ProgramsRatePlan'][0]['eb_cost'],2,'.',',')." Seminar Investment <br>";
                    $ebDisplay .= 'x '.$totalParticipantsDiscountFlag." No. of pax <br>";
                    $ebDisplay .= "------------------<br>";
                    $ebDisplay .= "<b>".number_format($totalEbDiscount,2,'.',',')."</b><br>";
                    $ebDisplay .= "+ ".number_format($oProgram['ProgramsRatePlan'][0]['reg_cost'],2,'.',',').' x '.$totalParticipantsDimensions."<br>";
                    $ebDisplay .= "x ".$totalParticipantsPercentage." No. of discounted pax.<br>";
                    $ebDisplay .= "------------------<br>";
                    $ebDisplay .= "<b>".number_format($totalEbDiscountFlag,2,'.',',')."</b><br>";
                    $ebDisplay .= "<b>".number_format($totalEbWV,2,'.',',')."</b><br>";
                    $ebDisplay .= "+ ".number_format($totalEbVAT,2,'.',',')." ".Configure::read('VAT_PERCENTAGE')."% VAT<br>";
                    $ebDisplay .= "------------------<br>";
                    $ebDisplay .= "<b>PHP ".number_format($totalEbDiscount,2,'.',',')."</b>";

                }

                $this->set(compact('ebDisplay'));

                /* discounted regular [s] */
                $regularWV = $oProgram['ProgramsRatePlan'][0]['reg_cost'] * $totalParticipantsDiscount;
                $regularVAT = $regularWV * Configure::read('VAT');
                $regularDiscountedVAT = $regularWV + $regularVAT;

                $regularDisplay = billingEbFormat(
                    $oProgram['ProgramsRatePlan'][0]['reg_cost'],
                    $totalParticipantsDiscount,
                    $regularWV,
                    $regularVAT,
                    $regularDiscountedVAT
                );//this function can be found under app/config/mansmith_bootstrap.php
                $this->set(compact('regularDisplay'));

                /* discounted osp [s] */
                $ospWV = $oProgram['ProgramsRatePlan'][0]['osp_cost'] * $totalParticipantsDiscount;
                $ospVAT = $ospWV * Configure::read('VAT');
                $ospDiscountedVAT = $ospWV + $ospVAT;

                $ospDisplay = billingEbFormat(
                    $oProgram['ProgramsRatePlan'][0]['osp_cost'],
                    $totalParticipantsDiscount,
                    $ospWV,
                    $ospVAT,
                    $ospDiscountedVAT

                );
                $this->set(compact('ospDisplay'));

                if( $display == false ){
                    return array(
                        'eb' => str_replace("<br>","|",$ebDisplay),
                        'regular' => str_replace("<br>","|",$regularDisplay),
                        'osp' => str_replace("<br>","|",$ospDisplay)
                    );
                }
            }//group discount
        }

        if( intval($oProgram['Program']['discount_applies'])==0 || $discount_applies == false )//discount applies equal 1
        {//non discount

            /** @var $totalNonEbWV
             *  non discounted NonEb
             */
            $totalNonEbWV= $oProgram['ProgramsRatePlan'][0]['eb_cost'] * $totalParticipants;//total amount people
            $totalNonEbVAT = $totalNonEbWV * Configure::read('VAT');
            $totalNonEbDVAT = $totalNonEbWV + $totalNonEbVAT;

            $ebDisplay = billingEbFormat(
                $oProgram['ProgramsRatePlan'][0]['eb_cost'],
                $totalParticipants,
                $totalNonEbWV,
                $totalNonEbVAT,
                $totalNonEbDVAT
            );
            $this->set(compact('ebDisplay'));

            /** @var $totalNonEbWV
             *  non discounted NonEb
             */
            /* non-discounted regular */
            $totalRegularWV= $oProgram['ProgramsRatePlan'][0]['reg_cost'] * $totalParticipants;//total amount people
            $totalRegularVAT = $totalRegularWV * Configure::read('VAT');
            $totalRegularDVAT = $totalRegularWV + $totalRegularVAT;

            $regularDisplay = billingEbFormat(
                $oProgram['ProgramsRatePlan'][0]['reg_cost'],
                $totalParticipants,
                $totalRegularWV,
                $totalRegularVAT,
                $totalRegularDVAT
            );
            $this->set(compact('regularDisplay'));

            /** @var $ospDisplay
             * non discounted osp
             */
            $totalOspWV= $oProgram['ProgramsRatePlan'][0]['osp_cost'] * $totalParticipants;//total amount people
            $totalOspVAT = $totalOspWV * Configure::read('VAT');
            $totalOspDVAT = $totalOspWV + $totalOspVAT;
            $ospDisplay = billingEbFormat(
                $oProgram['ProgramsRatePlan'][0]['osp_cost'],
                $totalParticipants,
                $totalOspWV,
                $totalOspVAT,
                $totalOspDVAT
            );
            $this->set(compact('ospDisplay'));
            if( $display == false ){
                return array(
                    'eb' => str_replace("<br>","|",$ebDisplay),
                    'regular' => str_replace("<br>","|",$regularDisplay),
                    'osp' => str_replace("<br>","|",$ospDisplay)
                );
            }
        }

        $this->set(compact('oProgram'));
        if( $display == false ){
            return false;
        }
    }

    private function admin_invoice_details($oProgram,$company_id=null){

        $totalParticipants = 0;
        if( $company_id && isset($oProgram['ProgramsParticipant']) ){
            foreach( $oProgram['ProgramsParticipant'] as $oParticipant ){
                if(
                    $oParticipant['programs_participant_status_id'] == Configure::read('status_live') &&
                    $oParticipant['status'] == Configure::read('status_live') &&
                    $oParticipant['company_id'] == $company_id
                ){
                    $totalParticipants++;
                }
            }
        }

        $discount_applies = false;
        if( intval($oProgram['Program']['discount_applies'])==1 ){//discount applies equal 1
            $totalParticipantsDimensions = floor($totalParticipants/4);//if more than 4 group discount applies
            if( $totalParticipantsDimensions > 0 ){//group discount if participants > 4
                $discount_applies = true;
                $this->set('group_discount','On');

                $totalParticipantsPercentage = $totalParticipantsDimensions * 0.5;
                $totalParticipantsDiscount = $totalParticipants - $totalParticipantsPercentage;
                /**
                 * Discounted Eb
                 */
                if( $totalParticipantsDiscount < 1){
                    $totalParticipantsDiscountFlag = 1;
                }else{
                    $totalParticipantsDiscountFlag = floor($totalParticipantsDiscount);
                }

                $participantsNDCF = $totalParticipants - $totalParticipantsDimensions;

                /* first part */
                $totalEbNTotalDiscount = $oProgram['ProgramsRatePlan'][0]['eb_cost'] * $participantsNDCF;//total discount people
                $totalEbNTotalDiscountFlag = $oProgram['ProgramsRatePlan'][0]['reg_cost'] * $totalParticipantsPercentage;
                $totalEbNwv = number_format($totalEbNTotalDiscount + $totalEbNTotalDiscountFlag,2,'.','');
                $totalEbNVAT = $totalEbNwv * Configure::read('VAT');
                $totalEbNDTotal = $totalEbNwv + $totalEbNVAT;

                /* second part */
                $totalEbDiscount = $totalParticipantsDiscountFlag * $oProgram['ProgramsRatePlan'][0]['eb_cost'];
                $totalEbDiscountFlag = ( $oProgram['ProgramsRatePlan'][0]['reg_cost'] * $totalParticipantsDimensions ) * $totalParticipantsPercentage;
                $totalEbWV = $totalEbDiscount + $totalEbNTotalDiscountFlag;
                $totalEbVAT = $totalEbWV * Configure::read('VAT');

                /** @var $totalEbDiscountTotal eb */
                $totalEbDiscountTotal = $totalEbWV + $totalEbVAT;
                $totalEbDiscountTotal = number_format($totalEbDiscountTotal,2,'.',',');
                $amountToWordsEbDiscountTotal = convertAmountToWords($totalEbDiscountTotal);

                $ebDisplay  = 'Seminar Investment Php    '.number_format($oProgram['ProgramsRatePlan'][0]['eb_cost'],2,'.',',').'|';
                $ebDisplay .= 'No. of pax                x  '.$participantsNDCF.'|';
                $ebDisplay .= '                              ------------------|';
                $ebDisplay .= '                             '.number_format($totalEbDiscount,2,'.',',').'|';
                $ebDisplay .= '                          +  '.number_format($oProgram['ProgramsRatePlan'][0]['reg_cost'],2,'.',',').' x '.$totalParticipantsDimensions.'|';
                $ebDisplay .= 'No. of discounted pax     x  '.$totalParticipantsPercentage.'|';
                $ebDisplay .= '                              ------------------|';
                $ebDisplay .= '                             '.number_format($totalEbDiscountFlag,2,'.',',').'|';
                $ebDisplay .= '                             '.number_format($totalEbWV,2,'.',',').'|';
                $ebDisplay .= '+ 12% VAT                    '.number_format($totalEbVAT,2,'.',',').'|';
                $ebDisplay .= '                              ------------------';
                /**
                 *
                 */
                $regularWV = $oProgram['ProgramsRatePlan'][0]['reg_cost'] * $totalParticipantsDiscount;
                $regularVAT = $regularWV * Configure::read('VAT');
                $regularDiscountedVAT = $regularWV + $regularVAT;
                $regularDiscountedVAT = number_format($regularDiscountedVAT,2,'.',',');
                $amountToWordsRegularDiscountTotal = convertAmountToWords($regularDiscountedVAT);

                $regularDisplay  = 'Seminar Investment Php       '.number_format($oProgram['ProgramsRatePlan'][0]['reg_cost'],2,'.',',').'|';
                $regularDisplay .= 'No. of pax               x   '.$totalParticipantsDiscount.'|';
                $regularDisplay .= '                             ------------------|';
                $regularDisplay .= '                             '.number_format($regularWV,2,'.',',').'|';
                $regularDisplay .= '+ 12% VAT                +   '.number_format($regularVAT,2,'.',',').'|';
                $regularDisplay .= '                              ------------------';

                /**
                 *
                 */
                $ospWV = $oProgram['ProgramsRatePlan'][0]['osp_cost'] * $totalParticipantsDiscount;
                $ospVAT = $ospWV * Configure::read('VAT');
                $ospDiscountedVAT = $ospWV + $ospVAT;
                $ospDiscountedVAT = number_format($ospDiscountedVAT,2,'.',',');
                $amountToWordsOspDiscountTotal = convertAmountToWords($ospDiscountedVAT);


                $ospDisplay = 'Seminar Investment Php      '.number_format($oProgram['ProgramsRatePlan'][0]['osp_cost'],2,'.',',').'|';
                $ospDisplay.= 'No. of pax                    x  '.$totalParticipantsDiscount.'|';
                $ospDisplay.= '                                  ------------------|';
                $ospDisplay.= '                                 '.number_format($ospWV,2,'.',',').'|';
                $ospDisplay.= '+ 12% VAT                     +  '.number_format($ospVAT,2,'.',',').'|';
                $ospDisplay.= '                                  ------------------';
                return array(
                    'eb' => array(
                        'display' => $ebDisplay,
                        'amount' => $totalEbDiscountTotal,
                        'amountInWords' => $amountToWordsEbDiscountTotal
                    ),
                    'regular' => array(
                        'display' => $regularDisplay,
                        'amount' => $regularDiscountedVAT,
                        'amountInWords' => $amountToWordsRegularDiscountTotal
                    ),
                    'osp' => array(
                        'display' => $ospDisplay,
                        'amount' => $ospDiscountedVAT,
                        'amountInWords' => $amountToWordsOspDiscountTotal
                    )
                );
            }//group discount
        }

        if( intval($oProgram['Program']['discount_applies'])==0 || $discount_applies == false )//discount applies equal 1
        {//non discount
            /* non-discounted eb [s] */
            /** @var $totalNonEbWV
             *  non discounted NonEb
             */
            $totalNonEbWV= $oProgram['ProgramsRatePlan'][0]['eb_cost'] * $totalParticipants;//total amount people
            $totalNonEbVAT = $totalNonEbWV * Configure::read('VAT');
            $totalNonEbDVAT = $totalNonEbWV + $totalNonEbVAT;
            $totalNonEbDVAT = number_format($totalNonEbDVAT,2,'.',',');
            $amountToWordsNonEbDiscountTotal = convertAmountToWords($totalNonEbDVAT);

            $strLength = strlen(number_format($oProgram['ProgramsRatePlan'][0]['eb_cost'],2,'.',','));
            $strPadParticipants = str_pad($totalParticipants,$strLength," ",STR_PAD_LEFT);
            $strLengthB = strlen(number_format($totalNonEbWV,2,'.',','));
            $strNonEbVATX = str_pad(number_format($totalNonEbVAT,2,'.',','),$strLengthB," ",STR_PAD_LEFT);

            $ebDisplay = billingInvoiceFormat($oProgram['ProgramsRatePlan'][0]['eb_cost'],$strPadParticipants,$totalNonEbWV,$strNonEbVATX);

            /** @var $totalNonEbWV
             *  non discounted NonEb
             */
            /* non-discounted regular */
            $totalRegularWV= $oProgram['ProgramsRatePlan'][0]['reg_cost'] * $totalParticipants;//total amount people
            $totalRegularVAT = $totalRegularWV * Configure::read('VAT');
            $totalRegularDVAT = $totalRegularWV + $totalRegularVAT;
            $totalRegularDVAT = number_format($totalRegularDVAT,2,'.',',');
            $amountToWordsNonRegularDiscountTotal = convertAmountToWords($totalRegularDVAT);

            $strLength = strlen(number_format($oProgram['ProgramsRatePlan'][0]['reg_cost'],2,'.',','));
            $strPadParticipants = str_pad($totalParticipants,$strLength," ",STR_PAD_LEFT);
            $strLengthB = strlen(number_format($totalRegularWV,2,'.',','));
            $strNonRegularVATX = str_pad(number_format($totalRegularVAT,2,'.',','),$strLengthB," ",STR_PAD_LEFT);
            $regularDisplay = billingInvoiceFormat($oProgram['ProgramsRatePlan'][0]['reg_cost'],$strPadParticipants,$totalRegularWV,$strNonRegularVATX);


            /** @var $ospDisplay
             * non discounted osp
             */
            $totalOspWV= $oProgram['ProgramsRatePlan'][0]['osp_cost'] * $totalParticipants;//total amount people
            $totalOspVAT = $totalOspWV * Configure::read('VAT');
            $totalOspDVAT = $totalOspWV + $totalOspVAT;
            $totalOspDVAT = number_format($totalOspDVAT,2,'.',',');
            $amountToWordsNonOspDiscountTotal = convertAmountToWords($totalOspDVAT);

            $strLength = strlen(number_format($oProgram['ProgramsRatePlan'][0]['osp_cost'],2,'.',','));
            $strPadParticipants = str_pad($totalParticipants,$strLength," ",STR_PAD_LEFT);
            $strLengthB = strlen(number_format($totalOspWV,2,'.',','));
            $strOspVATX = str_pad(number_format($totalOspVAT,2,'.',','),$strLengthB," ",STR_PAD_LEFT);
            $ospDisplay = billingInvoiceFormat($oProgram['ProgramsRatePlan'][0]['osp_cost'],$strPadParticipants,$totalOspWV,$strOspVATX);
            return array(
                'eb' => array(
                    'display' => $ebDisplay,
                    'amount' => $totalNonEbDVAT,
                    'amountInWords' => $amountToWordsNonEbDiscountTotal
                ),
                'regular' => array(
                    'display' => $regularDisplay,
                    'amount' => $totalRegularDVAT,
                    'amountInWords' => $amountToWordsNonRegularDiscountTotal
                ),
                'osp' => array(
                    'display' => $ospDisplay,
                    'amount' => $totalOspDVAT,
                    'amountInWords' => $amountToWordsNonOspDiscountTotal
                )
            );
        }
        return false;
    }

    public function admin_add_programs_billing_eb(){
        $this->layout = false;
        Configure::write('debug',0);
        if( isset($this->data) && intval($this->data['BillingEb']['company_id']) > 0 ){
            $this->admin_add_programs_billing($billingType='BillingEb');
            $this->Session->setFlash(__('Add program billing eb successful.', true), 'default',array('class' => 'success'));
            $this->redirect(array('admin'=>true,'controller'=>'programs','action'=>'programs_billing_eb',intval($this->data['Program']['id']).'/'.intval($this->data['BillingEb']['company_id'])));
        }
        die;
    }

    public function admin_add_programs_billing_non_eb(){
        $this->layout = false;
        Configure::write('debug',0);
        if( isset($this->data) && intval($this->data['BillingNonEb']['company_id']) > 0 ){
            $this->admin_add_programs_billing($billingType='BillingNonEb');
            $this->Session->setFlash(__('Add program billing non eb successful.', true), 'default',array('class' => 'success'));
            $this->redirect(array('admin'=>true,'controller'=>'programs','action'=>'programs_billing_non_eb',intval($this->data['Program']['id']).'/'.intval($this->data['BillingNonEb']['company_id'])));
        }
        die;
    }

    public function admin_add_programs_billing_regular(){
        $this->layout = false;
        Configure::write('debug',0);
        if( isset($this->data) && intval($this->data['BillingRegular']['company_id']) > 0 ){
            $this->admin_add_programs_billing($billingType='BillingRegular');
            $this->Session->setFlash(__('Add program billing regular successful.', true), 'default',array('class' => 'success'));
            $this->redirect(array('admin'=>true,'controller'=>'programs','action'=>'programs_billing_regular',intval($this->data['Program']['id']).'/'.intval($this->data['BillingRegular']['company_id'])));
        }
        die;
    }

    public function admin_add_programs_billing_osp(){
        $this->layout = false;
        Configure::write('debug',0);
        if( isset($this->data) && intval($this->data['BillingOsp']['company_id']) > 0 ){
            $this->admin_add_programs_billing($billingType='BillingOsp');
            $this->Session->setFlash(__('Add program billing osp successful.', true), 'default',array('class' => 'success'));
            $this->redirect(array('admin'=>true,'controller'=>'programs','action'=>'programs_billing_on_site',intval($this->data['Program']['id']).'/'.intval($this->data['BillingOsp']['company_id'])));
        }
        die;
    }


    private function admin_add_programs_billing($billingType='BillingEb'){

        $this->ProgramsParticipant = &ClassRegistry::init('ProgramsParticipant');
        $participants = $this->ProgramsParticipant->find('all',array(
            'conditions' => array(
                'ProgramsParticipant.program_id' => intval($this->data['Program']['id']) ,
                'ProgramsParticipant.status' => Configure::read('status_live'),
                'ProgramsParticipant.company_id' => intval($this->data[$billingType]['company_id']),
                'ProgramsParticipant.programs_participant_status_id' => Configure::read('status_live')
            ),
            'order' => array('UPPER(TRIM(Participant.last_name))'),
        ));

        $program = $this->Program->findById(intval($this->data['Program']['id']));
        $this->BillingMisc = &ClassRegistry::init('BillingMisc');
        $this->BillingMisc->contain();
        $billingMisc = $this->BillingMisc->findByProgramsDivisionId($program['Program']['programs_division_id']);

        $this->ProgramBillingInfo = &ClassRegistry::init('ProgramBillingInfo');
        $this->ProgramBillingInfo->bindModel(array(
            'hasOne' => array(
                'ParticipantsPosition' => array(
                    'foreignKey' => false,
                    'conditions' => array('Participant.participants_position_id = ParticipantsPosition.id')
                ),
            )
        ));
        $programBillingInfo = $this->ProgramBillingInfo->find('all',array(
           'conditions' => array(
               'ProgramBillingInfo.program_id' => intval($this->data['Program']['id']) ,
               'ProgramBillingInfo.company_id' => intval($this->data[$billingType]['company_id']),
           )
        ));

        $data = array();
        $this->$billingType = &ClassRegistry::init($billingType);
        $this->$billingType->begin();
        $data[$billingType]['program_id'] = $this->data['Program']['id'];
        $data[$billingType]['company_id'] = $this->data[$billingType]['company_id'];

        if( $programBillingInfo && isset($programBillingInfo[0]['ProgramBillingInfo']) ){
            $data[$billingType]['program_billing_info_id'] = $programBillingInfo[0]['ProgramBillingInfo']['id'];
            $data[$billingType]['recipient'] = $programBillingInfo[0]['Participant']['last_name'].', '.$programBillingInfo[0]['Participant']['first_name'].' '.$programBillingInfo[0]['Participant']['middle_name'];
            $data[$billingType]['recipient_position'] = $programBillingInfo[0]['ParticipantsPosition']['name'];
            $data[$billingType]['recipient_fax_number'] = $programBillingInfo[0]['Company']['fax'];
        }else{
            $data[$billingType]['program_billing_info_id'] = 0;
            $data[$billingType]['recipient'] = 'non assigned';
            $data[$billingType]['recipient_position'] = '';
            $data[$billingType]['recipient_fax_number'] = '';
        }
        $schedule = date("F d, Y",strtotime($program['ProgramsSchedule'][0]['start_date']));
        if( count($program['ProgramsSchedule']) >1 ){
            $schedule .= ' to '.date("F d, Y",strtotime($program['ProgramsSchedule'][count($program['ProgramsSchedule'])-1]['start_date']));
        }

        $data[$billingType]['company'] = $participants[0]['Company']['name'];

        $documentType = 'eb';
        if( $billingType == 'BillingEb' ){
            $data[$billingType]['source_file'] = $program['ProgramsDivision']['alias'].'-'.date('Ymd-His').'-billing-eb--'.$this->Session->read('Auth.User.id').'.rtf';
            $data[$billingType]['billing_type_id'] = 1;
        }elseif( $billingType == 'BillingNonEb' ){
            $data[$billingType]['source_file'] = $program['ProgramsDivision']['alias'].'-'.date('Ymd-His').'-billing-noneb--'.$this->Session->read('Auth.User.id').'.rtf';
            $data[$billingType]['billing_type_id'] = 2;
            $documentType ='non_eb';
        }elseif( $billingType == 'BillingRegular' ){
            $data[$billingType]['source_file'] = $program['ProgramsDivision']['alias'].'-'.date('Ymd-His').'-billing-regular--'.$this->Session->read('Auth.User.id').'.rtf';
            $data[$billingType]['billing_type_id'] = 4;
            $documentType ='regular';
        }elseif( $billingType == 'BillingOsp' ){
            $data[$billingType]['source_file'] = $program['ProgramsDivision']['alias'].'-'.date('Ymd-His').'-billing-regular--'.$this->Session->read('Auth.User.id').'.rtf';
            $data[$billingType]['billing_type_id'] = 3;
            $documentType ='osp';
        }


        $data[$billingType]['billing_reference_code'] = strtoupper(uniqid()).rand(1,3);
        $data[$billingType]['from'] = 'Chiqui Escareal-Go';
        $data[$billingType]['billing_date'] = date("Y-m-d");
        $data[$billingType]['seminar'] = $program['Program']['title'];
        $data[$billingType]['venue'] = $program['Program']['venue_name'].', '.$program['Program']['city'];
        $data[$billingType]['schedule'] = $schedule;
        $data[$billingType]['number_of_participant'] = count($participants);
        $data[$billingType]['early_bird_date'] = $program['ProgramsRatePlan'][0]['cut_off_date'];
        $data[$billingType]['status'] = Configure::read('status_live');

        $display_rates = $this->billingDocumentDetails($program,$this->data[$billingType]['company_id'],$documentType,$display=false);//generate the eb, regular and osp rate
        $data[$billingType]['early_bird_details'] = $display_rates['eb'];
        $data[$billingType]['regular_details'] = $display_rates['regular'];
        $data[$billingType]['on_site_details'] = $display_rates['osp'];

        $data[$billingType]['early_bird_charges'] = $program['ProgramsRatePlan'][0]['eb_cost'];
        $data[$billingType]['regular_rate_charges'] = $program['ProgramsRatePlan'][0]['reg_cost'];
        $data[$billingType]['on_site_rate_charges'] = $program['ProgramsRatePlan'][0]['osp_cost'];
        $data[$billingType]['payment_policy'] = $billingMisc['BillingMisc']['payment_policy'];
        $data[$billingType]['other_information'] = $billingMisc['BillingMisc']['other_information'];
        $data[$billingType]['cancellation_policy'] = $billingMisc['BillingMisc']['cancellation_policy'];
        $data[$billingType]['notes'] = $this->data[$billingType]['notes'];
        $data[$billingType]['date_created'] = date("Y-m-d H:i:s");
        $data[$billingType]['date_modified'] = date("Y-m-d H:i:s");
        $data[$billingType]['who_created'] = $this->data['Program']['who_created'];
        $data[$billingType]['who_modified'] = $this->data['Program']['who_created'];
        if( $this->$billingType->save($data) ){
            $this->$billingType->commit();
        }
    }

    public function admin_add_programs_invoice(){
        if( isset($this->data) && intval($this->data['Invoice']['company_id']) > 0 ){

            $this->ProgramsParticipant = &ClassRegistry::init('ProgramsParticipant');
            $participants = $this->ProgramsParticipant->find('all',array(
                'conditions' => array(
                    'ProgramsParticipant.program_id' => intval($this->data['Program']['id']) ,
                    'ProgramsParticipant.status' => Configure::read('status_live'),
                    'ProgramsParticipant.company_id' => intval($this->data['Invoice']['company_id']),
                    'ProgramsParticipant.programs_participant_status_id' => Configure::read('status_live')
                ),
                'order' => array('UPPER(TRIM(Participant.last_name))'),
            ));

            $program = $this->Program->findById(intval($this->data['Program']['id']));
            $billing_invoice = null;
            if( $program ){
                $billing_invoice = $this->admin_invoice_details($program,$this->data['Invoice']['company_id']);
            }

            $this->BillingMisc = &ClassRegistry::init('BillingMisc');
            $this->BillingMisc->contain();
            $billingMisc = $this->BillingMisc->findByProgramsDivisionId($program['Program']['programs_division_id']);

            $this->ProgramBillingInfo = &ClassRegistry::init('ProgramBillingInfo');
            $this->ProgramBillingInfo->bindModel(array(
                'hasOne' => array(
                    'ParticipantsPosition' => array(
                        'foreignKey' => false,
                        'conditions' => array('Participant.participants_position_id = ParticipantsPosition.id')
                    ),
                )
            ));

            $programBillingInfo = $this->ProgramBillingInfo->find('all',array(
                'conditions' => array(
                    'ProgramBillingInfo.program_id' => intval($this->data['Program']['id']) ,
                    'ProgramBillingInfo.company_id' => intval($this->data['Invoice']['company_id']),
                )
            ));

            $data = array();
            $this->Invoice = &ClassRegistry::init('Invoice');
            $this->Invoice->begin();
            $data['Invoice']['program_id'] = $this->data['Program']['id'];
            $data['Invoice']['company_id'] = $this->data['Invoice']['company_id'];
            $data['Invoice']['invoice_reference_code'] = strtoupper(generatePassword($algorithm='md5','12'));
            if( $programBillingInfo && isset($programBillingInfo[0]['ProgramBillingInfo']) ){
                $data['Invoice']['program_billing_info_id'] = $programBillingInfo[0]['ProgramBillingInfo']['id'];
                $data['Invoice']['billing_date'] = date("Y-m-d",strtotime($programBillingInfo[0]['ProgramBillingInfo']['billed_date']));
            }else{
                $data['Invoice']['program_billing_info_id'] = 0;
                $data['Invoice']['billing_date'] = date("Y-m-d");
            }

            $schedule = date("F d, Y",strtotime($program['ProgramsSchedule'][0]['start_date']));
            if( count($program['ProgramsSchedule']) >1 ){
                $schedule .= ' to '.date("F d, Y",strtotime($program['ProgramsSchedule'][count($program['ProgramsSchedule'])-1]['start_date']));
            }

            $data['Invoice']['seminar'] = $program['Program']['title'];
            $data['Invoice']['venue'] = $program['Program']['venue_name'].', '.$program['Program']['city'];
            $data['Invoice']['schedule'] = $schedule;
            $data['Invoice']['vat'] = Configure::read('VAT');
            $data['Invoice']['payment_instructions'] = Configure::read('Invoice.Payment.Instructions');
            $data['Invoice']['payment_policy'] = Configure::read('Invoice.Payment.Policy');
            $data['Invoice']['prepared_by'] = $program['Program']['contact_name'];
            $data['Invoice']['status'] = Configure::read('status_live');
            $data['Invoice']['date_created'] = date("Y-m-d H:i:s");
            $data['Invoice']['date_modified'] = date("Y-m-d H:i:s");
            $data['Invoice']['who_created'] = $this->data['Program']['who_created'];
            if( $this->Invoice->save($data) ){
                if($this->Invoice->commit() ){

                    if ($billing_invoice && is_array($billing_invoice) ){

                        $insertId = $this->Invoice->getLastInsertID();
                        $this->InvoiceDetails = &ClassRegistry::init('InvoiceDetails');

                        /**
                         * Eb
                         */
                        $billingTypeId=1;
                        $invoiceGenerator = '0'.date("y").'-'.date("md",strtotime("now")).'-';
                        $invoiceNumber = invoicePad($invoiceGenerator, $billingTypeId);
                        $sourceFile = $program['ProgramsDivision']['alias'].'-'.date("Ymd-His").'-invoice-eb--'.$this->Session->read('Auth.User.id').'.rtf';
                        $amount = $billing_invoice['eb']['amount'];
                        $amountInWords =  $billing_invoice['eb']['amountInWords'];
                        $invoice_notes = $billing_invoice['eb']['display'];
                        $totalParticipants = count($participants);
                        $notes = '';
                        $status = Configure::read('status_live');
                        $who_created = $this->Session->read('Auth.User.id');
                        $payment_date = date("Y-m-d",strtotime(
                                $this->data['Invoice']['year'].'-'.
                                $this->data['Invoice']['month'].'-'.
                                $this->data['Invoice']['day']
                            )
                        );

                        $ebInvoiceDetails = billingInvoiceDataInsert(
                            $insertId,
                            $billingTypeId,
                            $invoiceNumber,
                            $programBillingInfo,
                            $participants,
                            $sourceFile,
                            $amount,
                            $amountInWords,
                            $totalParticipants,
                            $invoice_notes,
                            $notes,
                            $status,
                            $who_created,
                            $payment_date
                        );
                        $this->InvoiceDetails->begin();
                        if( $this->InvoiceDetails->save($ebInvoiceDetails) ){
                            $this->InvoiceDetails->commit();
                        }

                        /** @var regular */
                        $billingTypeId=4;
                        $invoiceNumber = invoicePad($invoiceGenerator, $billingTypeId);
                        $sourceFile = $program['ProgramsDivision']['alias'].'-'.date("Ymd-His").'-invoice-reg--'.$this->Session->read('Auth.User.id').'.rtf';
                        $amount = $billing_invoice['regular']['amount'];
                        $amountInWords =  $billing_invoice['regular']['amountInWords'];
                        $invoice_notes = $billing_invoice['regular']['display'];

                        $regularInvoiceDetails = billingInvoiceDataInsert(
                            $insertId,
                            $billingTypeId,
                            $invoiceNumber,
                            $programBillingInfo,
                            $participants,
                            $sourceFile,
                            $amount,
                            $amountInWords,
                            $totalParticipants,
                            $invoice_notes,
                            $notes,
                            $status,
                            $who_created,
                            $payment_date
                        );
                        $this->InvoiceDetails->id = null;
                        $this->InvoiceDetails->begin();
                        if( $this->InvoiceDetails->save($regularInvoiceDetails) ){
                            $this->InvoiceDetails->commit();
                        }

                        //osp
                        $billingTypeId=3;
                        $invoiceNumber = invoicePad($invoiceGenerator, $billingTypeId);
                        $sourceFile = $program['ProgramsDivision']['alias'].'-'.date("Ymd-His").'-invoice-osp--'.$this->Session->read('Auth.User.id').'.rtf';
                        $amount = $billing_invoice['osp']['amount'];
                        $amountInWords =  $billing_invoice['osp']['amountInWords'];
                        $invoice_notes = $billing_invoice['osp']['display'];

                        $ospInvoiceDetails = billingInvoiceDataInsert(
                            $insertId,
                            $billingTypeId,
                            $invoiceNumber,
                            $programBillingInfo,
                            $participants,
                            $sourceFile,
                            $amount,
                            $amountInWords,
                            $totalParticipants,
                            $invoice_notes,
                            $notes,
                            $status,
                            $who_created,
                            $payment_date
                        );

                        $this->InvoiceDetails->id = null;
                        $this->InvoiceDetails->begin();
                        if( $this->InvoiceDetails->save($ospInvoiceDetails) ){
                            $this->InvoiceDetails->commit();
                        }
                        $this->Session->setFlash(__('Add program invoice successful.', true), 'default',array('class' => 'success'));
                        //end of eb
                    }
                }
            }

            $this->redirect(array('admin'=>true,'controller'=>'programs','action'=>'programs_invoice',intval($this->data['Program']['id']).'/'.intval($this->data['Invoice']['company_id'])));
        }
    }


    public function admin_programs_billing_eb($program_id=null,$company_id=null){
        $this->layout = 'iframe';
        Configure::write('debug',0);
        $this->Program->contain(array('ProgramsType','ProgramsSchedule','ProgramsRatePlan','ProgramsSpeaker','ProgramsParticipant','ProgramsCompanyContact'));
        $oProgram = $this->Program->findById($program_id);

        if( $oProgram ){
            $this->set(compact('program_id'));
            if( $company_id && intval($company_id)>0 ){

                $this->BillingEb = &ClassRegistry::init('BillingEb');
                $this->BillingEb->contain();
                $this->BillingEb->bindModel(array(
                    'hasOne' => array(
                        'ProgramsDivision' => array(
                            'foreignKey' => false,
                            'conditions' => array('Program.programs_division_id = ProgramsDivision.id')
                        ),
                    )
                ));

                $billing_eb = $this->BillingEb->find('all',array(
                        'conditions' => array(
                            'BillingEb.program_id' => $program_id ,
                            'BillingEb.company_id' =>  $company_id,
                            'BillingEb.billing_type_id' => 1,
                            'BillingEb.status'     => Configure::read('status_live')
                        )
                    )
                );

                if( $billing_eb && count($billing_eb) >0 ){
                     $this->set(compact('billing_eb'));
                }

            }
            $this->billingDocumentDetails($oProgram,$company_id,$documentType='eb',$display=true);
        }else{
            $this->redirect('/');
        }
    }

    public function admin_programs_billing_non_eb($program_id=null,$company_id=null){
        $this->layout = 'iframe';
        Configure::write('debug',0);
        $this->Program->contain(array('ProgramsType','ProgramsSchedule','ProgramsRatePlan','ProgramsSpeaker','ProgramsParticipant','ProgramsCompanyContact'));
        $oProgram = $this->Program->findById($program_id);

        if( $oProgram ){
            $this->set(compact('program_id'));
            if( $company_id && intval($company_id)>0 ){
                $this->BillingNonEb = &ClassRegistry::init('BillingNonEb');
                $this->BillingNonEb->contain();
                $this->BillingNonEb->bindModel(array(
                    'hasOne' => array(
                        'ProgramsDivision' => array(
                            'foreignKey' => false,
                            'conditions' => array('Program.programs_division_id = ProgramsDivision.id')
                        ),
                    )
                ));

                $billing_non_eb = $this->BillingNonEb->find('all',array(
                        'conditions' => array(
                            'BillingNonEb.program_id' => $program_id ,
                            'BillingNonEb.company_id' =>  $company_id,
                            'BillingNonEb.billing_type_id' => 2,
                            'BillingNonEb.status'     => Configure::read('status_live')
                        )
                    )
                );

                if( $billing_non_eb && count($billing_non_eb) >0 ){
                    $this->set(compact('billing_non_eb'));
                }
            }
            $this->billingDocumentDetails($oProgram,$company_id,$documentType='non_eb',$display=true);
        }else{
            $this->redirect('/');
        }
    }

    public function admin_programs_billing_regular($program_id=null,$company_id=null){
        $this->layout = 'iframe';
        Configure::write('debug',0);
        $this->Program->contain(array('ProgramsType','ProgramsSchedule','ProgramsRatePlan','ProgramsSpeaker','ProgramsParticipant','ProgramsCompanyContact'));
        $oProgram = $this->Program->findById($program_id);

        if( $oProgram ){
            $this->set(compact('program_id'));
            if( $company_id && intval($company_id)>0 ){
                $this->BillingRegular = &ClassRegistry::init('BillingRegular');
                $this->BillingRegular->contain();
                $this->BillingRegular->bindModel(array(
                    'hasOne' => array(
                        'ProgramsDivision' => array(
                            'foreignKey' => false,
                            'conditions' => array('Program.programs_division_id = ProgramsDivision.id')
                        ),
                    )
                ));

                $billing_regular = $this->BillingRegular->find('all',array(
                        'conditions' => array(
                            'BillingRegular.program_id' => $program_id ,
                            'BillingRegular.company_id' =>  $company_id,
                            'BillingRegular.billing_type_id' => 4,
                            'BillingRegular.status'     => Configure::read('status_live')
                        )
                    )
                );

                if( $billing_regular && count($billing_regular) >0 ){
                    $this->set(compact('billing_regular'));
                }

            }
            $this->billingDocumentDetails($oProgram,$company_id,$documentType='regular',$display=true);
        }else{
            $this->redirect('/');
        }
    }


    public function admin_programs_billing_on_site($program_id=null,$company_id=null){
        $this->layout = 'iframe';
        Configure::write('debug',0);
        $this->Program->contain(array('ProgramsType','ProgramsSchedule','ProgramsRatePlan','ProgramsSpeaker','ProgramsParticipant','ProgramsCompanyContact'));
        $oProgram = $this->Program->findById($program_id);

        if( $oProgram ){
            $this->set(compact('program_id'));
            if( $company_id && intval($company_id)>0 ){
                $this->BillingOsp = &ClassRegistry::init('BillingOsp');
                $this->BillingOsp->contain();
                $this->BillingOsp->bindModel(array(
                    'hasOne' => array(
                        'ProgramsDivision' => array(
                            'foreignKey' => false,
                            'conditions' => array('Program.programs_division_id = ProgramsDivision.id')
                        ),
                    )
                ));

                $billing_osp = $this->BillingOsp->find('all',array(
                        'conditions' => array(
                            'BillingOsp.program_id' => $program_id ,
                            'BillingOsp.company_id' =>  $company_id,
                            'BillingOsp.billing_type_id' => 3,
                            'BillingOsp.status'     => Configure::read('status_live')
                        )
                    )
                );

                if( $billing_osp && count($billing_osp) >0 ){
                    $this->set(compact('billing_osp'));
                }
            }
            $this->billingDocumentDetails($oProgram,$company_id,$documentType='on_site',$display=true);
        }else{
            $this->redirect('/');
        }
    }

    public function admin_programs_invoice($program_id=null,$company_id=null){
        $this->layout = 'iframe';
        Configure::write('debug',2);
        $this->Program->contain(array('ProgramsType','ProgramsSchedule','ProgramsRatePlan','ProgramsSpeaker','ProgramsParticipant','ProgramsCompanyContact'));
        $oProgram = $this->Program->findById($program_id);

        if( $oProgram ){
            $this->set(compact('program_id'));
            $participants = null;
            if( $company_id && intval($company_id)>0 ){
                $this->ProgramsParticipant = &ClassRegistry::init('ProgramsParticipant');
                $participants = $this->ProgramsParticipant->find('all',array(
                    'conditions' => array(
                        'ProgramsParticipant.program_id' => $program_id,
                        'ProgramsParticipant.status' => Configure::read('status_live'),
                        'ProgramsParticipant.company_id' => $company_id,
                        'ProgramsParticipant.programs_participant_status_id' => Configure::read('status_live')
                    ),
                    'order' => array('UPPER(TRIM(Participant.last_name))'),
                ));

                $this->Invoice = &ClassRegistry::init('Invoice');
                $invoice = $this->Invoice->find('all',array(
                        'conditions' => array(
                            'Invoice.program_id' => $program_id ,
                            'Invoice.company_id' =>  $company_id
                        )
                    )
                );

                if( $invoice ){
                    $this->set(compact('invoice'));
                }else{
                    $this->set('invoice',null);
                }
            }
            $this->set(compact('participants','company_id'));
        }else{
            $this->redirect('/');
        }
    }

    public function admin_add_attendance_sheets(){
        Configure::write('debug',0);
        $this->layout = false;
        $this->autoRender = false;
        if( isset($this->data) && $this->data['AttendanceSheetsParticipants'] ){
            $this->ProgramsAttendanceSheet = &ClassRegistry::init('ProgramsAttendanceSheet');
            $this->ProgramsAttendanceSheet->begin();
            $data = array();
            $this->ProgramsAttendanceSheet->id = null;
            $data['ProgramsAttendanceSheet']['program_id'] = $this->data['Program']['id'];
            $data['ProgramsAttendanceSheet']['source_file'] = date('Ymd-His').'-as--'. $this->Session->read('Auth.User.id').'.rtf';
            $data['ProgramsAttendanceSheet']['status'] = Configure::read('status_live');
            $data['ProgramsAttendanceSheet']['date_created'] = date("Y-m-d H:i:s");
            $data['ProgramsAttendanceSheet']['date_modified'] = date("Y-m-d H:i:s");
            $data['ProgramsAttendanceSheet']['who_created'] = $this->data['Program']['who_created'];
            $data['ProgramsAttendanceSheet']['who_modified'] = $this->data['Program']['who_created'];
            if( $this->ProgramsAttendanceSheet->save($data) ){
                $this->ProgramsAttendanceSheet->commit();
                $insertId = $this->ProgramsAttendanceSheet->getLastInsertID();
                if( $this->data['AttendanceSheetsParticipants'] ){
                    $this->AttendanceSheetsParticipants = &ClassRegistry::init('AttendanceSheetsParticipants');
                    foreach( $this->data['AttendanceSheetsParticipants']['participant_id'] as $participantCompanyId ){
                        $data = array();
                        list($participantId,$companyId ) = explode("_",$participantCompanyId);
                        $this->AttendanceSheetsParticipants->id = null;
                        $data['AttendanceSheetsParticipants']['programs_attendance_sheet_id'] = $insertId;
                        $data['AttendanceSheetsParticipants']['participant_id'] = $participantId;
                        $data['AttendanceSheetsParticipants']['company_id'] = $companyId;
                        if( $this->AttendanceSheetsParticipants->save($data) ){
                            $this->AttendanceSheetsParticipants->commit();
                        }
                    }
                }
                $this->Session->setFlash(__('Add program attendance sheets successful.', true), 'default',array('class' => 'success'));
            }
        }
        $this->redirect(array('admin'=>true,'controller'=>'programs','action'=>'programs_document',intval($this->data['Program']['id']).'/#attendance_sheets'));
        die;
    }

    public function admin_add_certificates(){
        Configure::write('debug',0);
        $this->layout = false;
        $this->autoRender = false;
        if( isset($this->data) && $this->data['CertificatesParticipants'] ){
            $this->ProgramsCertificate = &ClassRegistry::init('ProgramsCertificate');
            $this->ProgramsCertificate->begin();
            $data = array();
            $this->ProgramsCertificate->id = null;
            $data['ProgramsCertificate']['program_id'] = $this->data['Program']['id'];
            $data['ProgramsCertificate']['source_file'] = date('Ymd-His').'-crt--'. $this->Session->read('Auth.User.id').'.rtf';
            $data['ProgramsCertificate']['status'] = Configure::read('status_live');
            $data['ProgramsCertificate']['date_created'] = date("Y-m-d H:i:s");
            $data['ProgramsCertificate']['date_modified'] = date("Y-m-d H:i:s");
            $data['ProgramsCertificate']['who_created'] = $this->data['Program']['who_created'];
            $data['ProgramsCertificate']['who_modified'] = $this->data['Program']['who_created'];
            if( $this->ProgramsCertificate->save($data) ){
                $this->ProgramsCertificate->commit();
                $insertId = $this->ProgramsCertificate->getLastInsertID();
                if( $this->data['CertificatesParticipants'] ){
                    $this->CertificatesParticipants = &ClassRegistry::init('CertificatesParticipants');
                    foreach( $this->data['CertificatesParticipants']['participant_id'] as $participantCompanyId ){
                        $data = array();
                        list($participantId,$companyId ) = explode("_",$participantCompanyId);
                        $this->CertificatesParticipants->id = null;
                        $data['CertificatesParticipants']['programs_certificate_id'] = $insertId;
                        $data['CertificatesParticipants']['participant_id'] = $participantId;
                        $data['CertificatesParticipants']['company_id'] = $companyId;
                        if( $this->CertificatesParticipants->save($data) ){
                            $this->CertificatesParticipants->commit();
                        }
                    }
                }
                $this->Session->setFlash(__('Add program certificates successful.', true), 'default',array('class' => 'success'));
            }
        }
        $this->redirect(array('admin'=>true,'controller'=>'programs','action'=>'programs_document',intval($this->data['Program']['id']).'/#certificates'));
        die;
    }

    public function admin_add_dop(){
        Configure::write('debug',0);
        $this->layout = false;
        $this->autoRender = false;
        if( isset($this->data) && $this->data['DirectoriesOfParticipants'] ){
            $this->ProgramsParticipantDirectory = &ClassRegistry::init('ProgramsParticipantDirectory');
            $this->ProgramsParticipantDirectory->begin();
            $data = array();
            $this->ProgramsParticipantDirectory->id = null;
            $data['ProgramsParticipantDirectory']['program_id'] = $this->data['Program']['id'];
            $data['ProgramsParticipantDirectory']['source_file'] = date('Ymd-His').'-pd--'. $this->Session->read('Auth.User.id').'.rtf';
            $data['ProgramsParticipantDirectory']['status'] = Configure::read('status_live');
            $data['ProgramsParticipantDirectory']['date_created'] = date("Y-m-d H:i:s");
            $data['ProgramsParticipantDirectory']['date_modified'] = date("Y-m-d H:i:s");
            $data['ProgramsParticipantDirectory']['who_created'] = $this->data['Program']['who_created'];
            $data['ProgramsParticipantDirectory']['who_modified'] = $this->data['Program']['who_created'];
            if( $this->ProgramsParticipantDirectory->save($data) ){
                $this->ProgramsParticipantDirectory->commit();
                $insertId = $this->ProgramsParticipantDirectory->getLastInsertID();
                if( $this->data['DirectoriesOfParticipants'] ){
                    $this->DirectoriesOfParticipants = &ClassRegistry::init('DirectoriesOfParticipants');
                    foreach( $this->data['DirectoriesOfParticipants']['participant_id'] as $participantCompanyId ){
                        $data = array();
                        list($participantId,$companyId ) = explode("_",$participantCompanyId);
                        $this->DirectoriesOfParticipants->id = null;
                        $data['DirectoriesOfParticipants']['programs_directories_of_participant_id'] = $insertId;
                        $data['DirectoriesOfParticipants']['participant_id'] = $participantId;
                        $data['DirectoriesOfParticipants']['company_id'] = $companyId;
                        if( $this->DirectoriesOfParticipants->save($data) ){
                            $this->DirectoriesOfParticipants->commit();
                        }
                    }
                }
                $this->Session->setFlash(__('Add program directory of participants successful.', true), 'default',array('class' => 'success'));
            }
        }
        $this->redirect(array('admin'=>true,'controller'=>'programs','action'=>'programs_document',intval($this->data['Program']['id']).'/#directory_of_participants'));
        die;
    }

    public function admin_add_name_tags(){
        Configure::write('debug',0);
        $this->layout = false;
        $this->autoRender = false;
        if( isset($this->data) && $this->data['NameTagsParticipants'] ){
            $this->ProgramsNameTag = &ClassRegistry::init('ProgramsNameTag');
            $this->ProgramsNameTag->begin();
            $data = array();
            $this->ProgramsNameTag->id = null;
            $data['ProgramsNameTag']['program_id'] = $this->data['Program']['id'];
            $data['ProgramsNameTag']['source_file'] = date('Ymd-His').'-nt--'. $this->Session->read('Auth.User.id').'.rtf';
            $data['ProgramsNameTag']['status'] = Configure::read('status_live');
            $data['ProgramsNameTag']['date_created'] = date("Y-m-d H:i:s");
            $data['ProgramsNameTag']['date_modified'] = date("Y-m-d H:i:s");
            $data['ProgramsNameTag']['who_created'] = $this->data['Program']['who_created'];
            $data['ProgramsNameTag']['who_modified'] = $this->data['Program']['who_created'];
            if( $this->ProgramsNameTag->save($data) ){
                $this->ProgramsNameTag->commit();
                $insertId = $this->ProgramsNameTag->getLastInsertID();
                if( $this->data['NameTagsParticipants'] ){
                    $this->NameTagsParticipants = &ClassRegistry::init('NameTagsParticipants');
                    foreach( $this->data['NameTagsParticipants']['participant_id'] as $participantCompanyId ){
                        $data = array();
                        list($participantId,$companyId ) = explode("_",$participantCompanyId);
                        $this->NameTagsParticipants->id = null;
                        $data['NameTagsParticipants']['programs_name_tag_id'] = $insertId;
                        $data['NameTagsParticipants']['participant_id'] = $participantId;
                        $data['NameTagsParticipants']['company_id'] = $companyId;
                        if( $this->NameTagsParticipants->save($data) ){
                            $this->NameTagsParticipants->commit();
                        }
                    }
                }
                $this->Session->setFlash(__('Add program name tags successful.', true), 'default',array('class' => 'success'));
            }
        }
        $this->redirect(array('admin'=>true,'controller'=>'programs','action'=>'programs_document',intval($this->data['Program']['id']).'/#name_tags'));
        die;
    }

    public function admin_programs_schedule_add(){
        Configure::write('debug',0);
        $this->layout = false;
        $this->autoRender = false;
        if( isset($this->data) ){
            if( isset($this->data['ProgramsSchedule']) && isset($this->data['Program']) ){
                $this->ProgramsSchedule = &ClassRegistry::init('ProgramsSchedule');
                $start_date = self::getDateCriteria($this->data['ProgramsSchedule']['start_date']);

                $this->ProgramsSchedule->begin();
                $data = array();
                $this->ProgramsSchedule->id = null;
                $data['ProgramsSchedule']['program_id'] = $this->data['Program']['id'];
                $data['ProgramsSchedule']['programs_schedule_type_id'] = $this->data['ProgramsSchedule']['programs_schedule_type_id'];
                $data['ProgramsSchedule']['status'] = Configure::read('status_live');
                $data['ProgramsSchedule']['all_day_event'] = 0;
                $data['ProgramsSchedule']['start_date'] = $start_date;
                $data['ProgramsSchedule']['end_date'] = $start_date;//only 1 day add affair
                $data['ProgramsSchedule']['start_time'] = trim($this->data['ProgramsSchedule']['start_time']);
                $data['ProgramsSchedule']['end_time'] = trim($this->data['ProgramsSchedule']['end_time']);
                $data['ProgramsSchedule']['notes'] = trim($this->data['ProgramsSchedule']['notes']);
                $data['ProgramsSchedule']['address'] = trim($this->data['ProgramsSchedule']['address']);
                $data['ProgramsSchedule']['date_created'] = date("Y-m-d H:i:s");
                $data['ProgramsSchedule']['date_modified'] = date("Y-m-d H:i:s");
                if( $this->ProgramsSchedule->save($data) ){
                    $this->ProgramsSchedule->commit();
                }

            }
            $this->Session->setFlash(__('Add program schedule successful.', true), 'default',array('class' => 'success'));
            $this->redirect(array('admin'=>true,'controller'=>'programs','action'=>'programs_schedule',intval($this->data['Program']['id'])));
        }
    }

    public function admin_programs_schedule_delete(){
        $this->layout = false;
        $this->autoRender = false;
        if( isset($this->data)  ){
            if( isset($this->data['ProgramsSchedule']['id']) ){
                $this->ProgramsSchedule = &ClassRegistry::init('ProgramsSchedule');
                foreach( $this->data['ProgramsSchedule']['id'] as $key => $program_schedule_id ){
                    $sql = "DELETE FROM programs_schedules WHERE id=".intval($program_schedule_id);
                    $this->ProgramsSchedule->query($sql);
                }
            }
            $this->Session->setFlash(__('Delete successful.', true), 'default',array('class' => 'success'));
            $this->redirect(array('admin'=>true,'controller'=>'programs','action'=>'programs_schedule',intval($this->data['Program']['id'])));
        }else{
            $this->Session->setFlash(__('Delete unsuccessful.', true), 'default',array('class' => 'error'));
            $this->redirect("/");
        }
        die;
    }

    public function admin_program_participants($program_id=0,$page=0){
        Configure::write('debug',0);
        if( intval($program_id) > 0 ){
            $this->layout = 'iframe';
            if( isset($page) && intval($page)>0 && is_numeric($page) ){
                $sPage = intval($page);
            }else{
                $sPage = 1;
            }

            $this->ProgramsParticipantStatus = &ClassRegistry::init('ProgramsParticipantStatus');
            $this->ProgramsParticipant = &ClassRegistry::init('ProgramsParticipant');
            $this->ProgramsParticipant->bindModel(array(
                'hasOne' => array(
                    'ParticipantsTitle' => array(
                        'foreignKey' => false,
                        'conditions' => array('Participant.participants_title_id = ParticipantsTitle.id')
                    ),
                )
            ));
            $this->ProgramsParticipant->contain(array('Company','Participant','ParticipantsTitle'));
            $order = 'TRIM(UPPER(`Participant`.`full_name`)) ASC';
            $conditions = array(
                'ProgramsParticipant.program_id' => intval($program_id) ,
                'ProgramsParticipant.status' => Configure::read('status_live')
            );
            $this->paginate['ProgramsParticipant'] = array(
                'conditions'=> $conditions,
                'page'  => $sPage,
                'limit' => $this->paginate['Program']['limit'],
                'order' => $order
            );


            $this->set('program_id',$program_id);
            $this->set('program_participant_status', $this->ProgramsParticipantStatus->find('all'));
            $this->set('participants',$this->paginate('ProgramsParticipant'));

        }else{
            $this->redirect('/');
        }
    }

    public function paginate($object = null, $scope = array(), $whitelist = array(), $key = null) {
      	$results = parent::paginate($object, $scope, $whitelist);
      	if ($key) {
       		$this->params['paging'][$key] = $this->params['paging'][$object];
       		unset($this->params['paging'][$object]);
      	}
		return $results;
    }

    public static function getDateCriteria( $dateString ='' ){
        list( $month, $day, $year ) = explode( "/" , $dateString );

        if( strlen(trim($month)) < 2 ){
            $month = "0".$month;
        }

        if( strlen(trim($day)) < 2 ){
            $day = "0".$day;
        }

        return self::dateFormatBeforesave($year."-".$month."-".$day);
    }

    public static function dateFormatBeforeSave($dateString) {
        return date('Y-m-d', strtotime($dateString)); // Direction is from
    }
}
