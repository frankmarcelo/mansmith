<?php

class SpeakersController extends AppController {

    public $name = 'Speakers';
    public $cacheQueries = true;
/**
 * Models
 *
 * @var array
 * @access public
 */
    public $uses = array('Speaker','ProgramsDivision');
/**
 * Cache pagination results
 *
 * @var boolean
 * @access public
 */
    public $usePaginationCache = true;

/**
 * beforeFilter
 *
 * @return void
 */
    public function beforeFilter() {
        $this->Security->validatePost = false;
    	parent::beforeFilter();	
        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            $this->layout = 'ajax';
        }
    }

    public function admin_index() {
    	 if( $this->Session->read('Auth.User.role_id')!= Configure::read('status_live') ){
    	      $this->redirect("/");
    	 }
    	
    	$this->set('title_for_layout', __('Speakers', true));
        $this->Speaker->recursive = 0;
        $this->set('speakers', $this->paginate());
    }

    public function admin_add() {
    	if (!empty($this->data)) {
       	    $this->Speaker->create();
            if ($this->Speaker->save($this->data)) {
                $this->Session->setFlash(__('The Speaker has been saved', true), 'default', array('class' => 'success'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The Speaker could not be saved. Please, try again.', true), 'default', array('class' => 'error'));
            }
        } 
        $roles = $this->Speaker->ProgramsDivision->find('list');
        $this->set(compact('roles'));
    }

    public function admin_edit($id = null) {
    	if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid Speaker', true), 'default', array('class' => 'error'));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Speaker->save($this->data)) {
                $this->Session->setFlash(__('The Speaker has been saved', true), 'default', array('class' => 'success'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The Speaker could not be saved. Please, try again.', true), 'default', array('class' => 'error'));
            }
        }
        $data = null;
        if (empty($this->data)) {
            $this->data = $this->Speaker->read(null, $id);
            $data = $this->data;
        }
        $roles = $this->Speaker->ProgramsDivision->find('list');
        $this->set(compact('roles','data')); 
    }
 
    public function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Speaker', true), 'default', array('class' => 'error'));
            $this->redirect(array('action' => 'index'));
        }
        
        if ($this->Speaker->delete($id)) {
            $this->Session->setFlash(__('Speaker deleted', true), 'default', array('class' => 'success'));
            $this->redirect(array('action' => 'index'));
        }
    }
  
}
?>
