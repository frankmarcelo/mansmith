<?php

class ProgramsDivision extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'ProgramsDivision';
    public $cacheQueries = true;
    public $useTable = 'programs_division';
    public $actsAs = array('CacheQueries');

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => true,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}
/*

app/controllers/billing_eb_controller.php (line 60)

Array
(
    [BillingEb] => Array
        (
            [id] => 762
            [program_id] => 794
            [company_id] => 1756
            [program_billing_info_id] => 0
            [billing_type_id] => 1
            [company] => Waters Philippines
            [recipient] => non assigned
            [recipient_position] => 
            [recipient_fax_number] => 7224834
            [source_file] => 20091217-150351-billing-eb--6.rtf
            [billing_reference_code] => YVUQAVFXHM
            [from] => Chiqui Escareal-Go
            [billing_date] => 2009-12-17
            [seminar] => 7th Delivering Outstanding Service
            [venue] => Mandarin Oriental, Makati City
            [schedule] => November 23, 2009 to November 24, 2009
            [number_of_participant] => 2
            [early_bird_date] => 2009-11-09
            [early_bird_details] => P10,888.00 Seminar Investment |x         2 No. of pax |------------------|  21,776.00|+  2,613.12 12% VAT|------------------|P24,389.12
            [regular_details] => P11,888.00 Seminar Investment |x         2 No. of pax |------------------|  23,776.00|+  2,853.12 12% VAT|------------------|P26,629.12
            [on_site_details] => P13,888.00 Seminar Investment |x         2 No. of pax |------------------|  27,776.00|+  3,333.12 12% VAT|------------------|P31,109.12
            [early_bird_charges] => 10888.00000
            [regular_rate_charges] => 11888.00000
            [on_site_rate_charges] => 13888.00000
            [payment_policy] => PAYMENT METHODS: MANSMITH TIN: 000-128-440-000 VAT
 Seminar fees must be paid before the seminar and addressed to Mansmith and Fielders, Inc.
 Payment collection or check pick-up (within selected GMA areas only) may be done provided with
  at least 24 hr notice.
 Over-the-counter payments may also be made at any Security Bank Branch to the following account:
         Security Bank
         Timog Ave. Branch
         Savings Account # 0162-028106-001
         OR
         RCBC (New Manila Branch)
         Savings Account # 1-220-01044-5
 Please make sure to fax us the deposit slip with your company name and title of seminar to notify
  us of your payment


            [other_information] => OTHER INFORMATION:
 TIN: 000-128-440-000-VAT
 Creditable tax certificate (or Form 2307) should be attached to your payments if you
   intent to subsidize the withholding tax.  Please withhold only 2% as we are classified
   as prime or sub-contractors for training or supplier of services to top 10,000 corporation


            [cancellation_policy] => PLEASE READ -- OTHER INFORMATION:

 Once we receive registration, cancellations are accepted with full refund or no administrative charges provided 
 adviced to us EIGHT (8) working days before the seminar. Cancellations made less than 8 days before or 
 no-show during the seminar mean forfeiture of payment (or will still be charged to client in case of special credit 
 arrangement. Substitution is accepted at any point before the seminar. Only once person is expected to attend 
 each program. 

            [notes] => 
            [status] => 1
            [date_created] => 2009-12-17 15:03:51
            [date_modified] => 2012-05-06 21:59:24
            [who_created] => 1
            [who_modified] => 0
        )

    [Program] => Array
        (
            [id] => 794
            [programs_type_id] => 1
            [programs_division_id] => 2
            [title] => 7th Delivering Outstanding Service
            [seo_name] => 7th-delivering-outstanding-service
            [website] => http://www.mansmith.net
            [venue_name] => Mandarin Oriental
            [address] => 
            [suburb] => 
            [city] => Makati City
            [country] => Philippines
            [zip_code] => 
            [latitude] => 14.62021249417380
            [longitude] => 121.03447342340698
            [notes] => 2 Day Seminar by CEG
            [content] => 2 Day Seminar by CEG
            [contact_name] => Alice Torres
            [contact_email] => info@mansmith.net
            [contact_phone] => (632) 584-5858
            [post_to_facebook] => 0
            [post_to_twitter] => 0
            [status] => 1
            [discount_applies] => 1
            [total_earnings] => 0.00
            [total_money_owing] => 0.00
            [total_companies] => 6
            [total_participants] => 8
            [date_created] => 2009-08-24 10:36:26
            [date_modified] => 2012-04-15 09:21:50
            [who_created] => 1
            [who_modified] => 
        )

    [Company] => Array
        (
            [id] => 1756
            [programs_division_id] => 4
            [companies_affiliation_id] => 10
            [companies_industry_id] => 21
            [name] => Waters Philippines
            [seo_name] => waters-philippines
            [contact_person] => 
            [email] => 
            [primary_phone] => 5840651 to 53
            [secondary_phone] => 
            [mobile] => 
            [fax] => 7224834
            [website] => www.waters.com.ph
            [primary_bldg_name] => Waters Center
            [primary_street] => 14 Ilang-Ilang St. New Manila
            [primary_suburb] => 
            [primary_city] => Quezon City
            [primary_zip_code] => 
            [secondary_bldg_name] => 
            [secondary_street] => 
            [secondary_suburb] => 
            [secondary_city] => 
            [secondary_zip_code] => 
            [country] => Philippines
            [notes] => 
            [latitude] => 14.620212494174
            [longitude] => 121.034473423407
            [status] => 1
            [total_attended] => 57
            [total_employee] => 91
            [total_paid] => 0.00
            [date_created] => 2008-11-03 20:09:59
            [date_modified] => 2011-05-25 13:33:35
            [who_created] => 1
            [who_modified] => 1
        )

    [BillingType] => Array
        (
            [id] => 1
            [name] => Early Bird
            [alias] => EB
        )

    [ProgramsDivision] => Array
        (
            [id] => 2
            [title] => Mansmith
            [alias] => mansmith
            [created] => 2009-04-05 00:10:50
            [updated] => 2011-05-19 00:38:41
        )

)
* 
 * 
 */