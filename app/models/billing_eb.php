<?php

class BillingEb extends AppModel {

    public $name = 'BillingEb';
    public $cacheQueries = true;
    public $useTable = 'billing';
    public $actsAs = array('CacheQueries','Containable','Transactional');
    
    public $belongsTo = array(
	      	  'Program' => array(
                 'className'  => 'Program',
                 'dependent'  => true,
                 'conditions' => array(
                 	'Program.status'=>1,
                 	'BillingEb.billing_type_id'=>1
    			 )
              ),
              'Company' => array(
                  'type' => '',
                  'className' => 'Company',
                  'conditions' => array(
                  	'Company.status'=>1,
                  	'BillingEb.billing_type_id'=>1
              	  ) 
              ),
              'BillingType' => array(
                  'className' => 'BillingType'
              )
    ); 
    
    public function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => true,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
    
	public function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
        $conditions = compact('conditions');
        if ($recursive != $this->recursive) {
        	$conditions['recursive'] = $recursive;
        }
        unset( $extra['contain'] );
        $count = $this->find('count', array_merge($conditions, $extra));
           
        if (isset($extra['group'])) {
        	$count = $this->getAffectedRows();
        }
        return $count;
    }
    
    public function getLikeFullName($fullName =null){
        $sql  = "
    	SELECT COUNT(DISTINCT `BillingEb`.program_id) AS `count`
    	FROM `billing` AS `BillingEb`
    	JOIN `programs` AS `Program` ON (`BillingEb`.`program_id` = `Program`.`id` AND `Program`.`status` = 1)
        JOIN `companies` AS `Company` ON (`BillingEb`.`company_id` = `Company`.`id` AND `Company`.`status` = 1 AND `BillingEb`.`billing_type_id` = 1)
    	WHERE
    	(
			(`BillingEb`.`billing_type_id` = 1) AND
    		(`Program`.`status`   = ".intval(Configure::read('status_live')).") AND
    		(`BillingEb`.`status` = ".intval(Configure::read('status_live')).") AND
			(
			    (`Program`.`title` LIKE '%".addslashes($fullName)."%') OR
				(`BillingEb`.`seminar` LIKE '%".addslashes($fullName)."%') OR
				(`BillingEb`.`source_file` LIKE '%".addslashes($fullName)."%') OR
				(`BillingEb`.`billing_reference_code` LIKE '%".addslashes($fullName)."%') OR
                (`Company`.`name` LIKE '%".addslashes($fullName)."%') 
			)
    	)
    	GROUP BY program_id
    	";
    	$results = $this->query($sql);
		return count($results);
    }
}