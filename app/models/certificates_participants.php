<?php

class CertificatesParticipants extends AppModel {

    public $name = 'CertificatesParticipants';
    public $cacheQueries = true;
    public $useTable = 'certificates_participants';
    public $actsAs = array('CacheQueries','Containable','Transactional');
    
    public $belongsTo = array(
	      	 'Participant' => array(
                 'className'  => 'Participant',
                 'dependent'  => true,
    		 	 'foreignKey' => 'participant_id',
    			 'conditions' => array(
	                 	'Participant.status'=>1,
	                 	'`CertificatesParticipants`.`company_id`=`Participant`.`company_id`'
    			 )
              ),
              'ProgramsCertificate' => array(
                 'className' => 'ProgramsCertificate',
                 'dependent' => true,
                 'conditions' => array('ProgramsCertificate.status'=>1 )
              )
           ); 
    
    public function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => true,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
    
    public function getLikeFullName( $fullName =null ){
    	if( !empty($fullName) ){
  	   	   $countSearchName = $this->find('list',array('conditions'=> array("`ProgramsCertificate`.source_file LIKE '%".addslashes($fullName)."%'")));
    	   return (count($countSearchName)> 0)? true: false;		
    	}else{
    	   return false;	
    	}	
    }
}
