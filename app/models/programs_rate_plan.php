<?php

class ProgramsRatePlan extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'ProgramsRatePlan';
    public $useTable = 'programs_rate_plans';
    public $belongsTo = array(
    		  'Program' => array(
                 'className' => 'Program',
                 'dependent' => true,
                 'conditions' => array('Program.status'=>1 )
               ),
              'RatePlan' => array(
                 'className' => 'RatePlan',
                 'dependent' => true
              ),
           );

    public $validate = array(
        'cut_off_date' => array(
            'rule' => array('date', 'Y-m-d'),
            'message' => 'cut off cannot be empty.',
        ),
        'amount' => array(
            'rule' => 'notEmpty',
            'message' => 'cut off cannot be empty.',
        )
    );

    public $cacheQueries = true;
    public $actsAs = array('CacheQueries','Transactional');

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }


   private function getDateCriteria( $dateString ='' ){
       list( $month, $day, $year ) = explode( "/" , $dateString );

       if( strlen(trim($month)) < 2 ){
          $month = "0".$month;
       }

       if( strlen(trim($day)) < 2 ){
          $day = "0".$day;
       }

       return $this->dateFormatBeforesave($year."-".$month."-".$day);
    }

   private function dateFormatBeforeSave($dateString) {
       return date('Y-m-d', strtotime($dateString)); // Direction is from
   }

   private function removeExistingPlan( $program_id ){//return void;
        $sql = "DELETE FROM ".$this->useTable." WHERE program_id='".$program_id."' ";
        $this->query( $sql );
   }

   public function setCreateRatePlan( $data, $program_id ){
 
        $aRatePlanInfo = array();
        $aRatePlanIndex = array();
        $aRatePlanIds = array();
 
        foreach( $data['use_plan'] as $rateplan_key => $rateplan_value ){
            $aRatePlanIndex[] = $rateplan_key;
        }

        foreach( $data['id'] as $rateplan_key => $rateplans ){
            $aRatePlanIds[] = $rateplan_key;
        }

        $this->removeExistingPlan( $program_id );
        
        for( $i = 0; $i < count($aRatePlanIndex); $i++ ){
            foreach( $aRatePlanIds as $aRatePlanId ){

                $sql = "INSERT INTO ".$this->useTable." SET 
                           program_id ='".$program_id."',
                           rate_plan_id ='".$aRatePlanId."',
                           status ='".Configure::read('status_live')."',
                           cut_off_date ='".$this->getDateCriteria($data['cut_off_date'][$aRatePlanIndex[$i]])."',
                           num_of_adults='".$data['num_of_adults'][$aRatePlanIndex[$i]]."',
                           amount =".str_replace(",","",$data['id'][$aRatePlanId][$aRatePlanIndex[$i]]).",
                           date_created ='".date("Y-m-d H:i:s")."',
                           name ='package_".($i+1)."'
                ";
                $this->query($sql);
            }
        }
        return true;
   }
}