<?php

class UpcomingProgram extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'UpcomingProgram';
    public $cacheQueries = true;
    public $useTable  = 'upcoming_programs';
    
    public $belongsTo = array(
               'Program' => array(
		 'type' => 'INNER',
                 'className' => 'Program',
                 'dependent' => true
               )
           ); 

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => true,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}
