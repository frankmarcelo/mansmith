<?php

class ProgramsScheduleType extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'ProgramsScheduleType';
    public $useTable = 'programs_schedules_type';
    public $cacheQueries = true;
    public $actsAs = array('CacheQueries','Containable','Transactional');
    public $hasMany = array(
               'ProgramsSchedule' => array(
                 'className' => 'ProgramsSchedule',
                 'dependent' => true,
                 'conditions' => array('ProgramsSchedule.status'=>1 )
               ),

           );

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}
