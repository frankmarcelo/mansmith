<?php

class ParticipantsGrouping extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'ParticipantsGrouping';
    public $cacheQueries = true;
    public $useTable = 'participants_grouping';
    public $actsAs = array('CacheQueries','Containable');
    
    public $hasMany   = array(
       'Participant' => array(
          'type' => 'INNER',
          'className' => 'Participant',
          'dependent'=> true

       )
    );
           

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => true,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}