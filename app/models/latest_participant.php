<?php

class LatestParticipant extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'LatestParticipant';
    public $cacheQueries = true;
    public $useTable  = 'latest_participants';
    
    public $belongsTo = array(
               'Participant' => array(
    			 'type' => 'INNER',
                 'className' => 'Participant',
                 'dependent' => true
               )
           ); 

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => true,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}