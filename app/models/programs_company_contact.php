<?php

class ProgramsCompanyContact extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'ProgramsCompanyContact';
    public $cacheQueries = true;
    public $useTable = 'programs_company_contacts';
    public $actsAs = array('CacheQueries','Containable','Transactional');
    
    public $belongsTo = array(
    		   'Program' => array(
    		     'type' => 'INNER',		 
    			 'className' => 'Program',
    			 'conditions' => array('Program.status'=>1 ),
                 'dependent' => true
               ),	
               'Company' => array(
    		     'type' => 'INNER',		 
    			 'className' => 'Company',
               	 'conditions' => array('Company.status'=>1 ),  
                 'dependent' => true
               ),
               'Participant' => array(
    		     'type' => 'INNER',		 
    			 'className' => 'Participant',
               	 'conditions' => array('Participant.status'=>1 ),  
                 'dependent' => true
               ),
           ); 

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => true,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}