<?php

class ProgramBillingInfo extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'ProgramBillingInfo';
    public $cacheQueries = true;
    public $actsAs = array('CacheQueries','Containable','Transactional');
    public $useTable  = 'programs_billing_info';
    public $belongsTo = array(
                'Program' => array(
                    'className' => 'Program',
                    'dependent' => true,
                    'conditions' => array('Program.status'=>1)
                ),	
                'Company' => array(
                     'type' => '',
                     'className' => 'Company',
                     'conditions' => array('Company.status'=>1 ),
                     'dependent' => true
                ),
                'Participant'   => array(
                     'className'  => 'Participant',
                     'dependent'  => true,
                     'conditions' => array('Participant.status'=>1 )
                ), 
                'BillingType' => array(
                     'className' => 'BillingType'
                ),
                'BillingStatus' => array(
                    'className' => 'BillingStatus'
                )
           ); 
/**
 * Validation
 *
 * @var array
 * @access public
 */
    public $validate = array(
        'title' => array(
            'rule' => array('minLength', 1),
            'message' => 'Event cannot be empty.',
        ),
        'venue_name' => array(
            'rule' => array('minLength', 1),
            'message' => 'Venue cannot be empty.',
        ),
        'address' => array(
            'rule' => array('minLength', 1),
            'message' => 'Address cannot be empty.',
        ),
        'city' => array(
            'rule' => array('minLength', 1),
            'message' => 'City cannot be empty.',
        )
    );

    
    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    } 

    function convertToSeoUri($string, $separator = '-'){
        $string = trim($string);
        $string = strtolower($string); // convert to lowercase text
        $string = str_replace(" ", $separator, $string);
        $string = str_replace(".", '', $string);
        $string = str_replace(",", '', $string);
        $string = str_replace("(", '', $string);
        $string = str_replace(")", '', $string);
        $string = str_replace("'", '', $string);
        $string = str_replace(":", '', $string);
        $string = str_replace("!", '', $string);
        $string = str_replace("@", '', $string);
        $string = str_replace("/", '', $string);
        $string = str_replace("\\", '', $string);
        $string = str_replace("&", '', $string);
        $string = str_replace("*", '', $string);
        $string = str_replace("+", '', $string);
        $string = str_replace("`", '', $string);
        $string = str_replace("'", $separator, $string);
        $string = str_replace("&amp;", $separator, $string);
        $string = preg_replace("/[ -]+/", "-", $string);
        return $string;
    }
    
 	function getLikeTitleName( $title =null ){
    	if( !empty($title) ){
    		$countSearchName = $this->find('list',array('conditions'=> array("Program.title LIKE '%".addslashes(trim($title))."%'")));
    		return (count($countSearchName)> 0)? true: false;		
    	}else{
    		return false;	
    	}	
    }
}
