<?php

class RatePlan extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'RatePlan';
    public $useTable = 'rate_plans';
    public $cacheQueries = true;
    public $actsAs = array('CacheQueries');

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => true,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}