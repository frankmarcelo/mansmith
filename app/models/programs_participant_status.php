<?php

class ProgramsParticipantStatus extends AppModel {
    /**
     * Model name
     *
     * @var string
     * @access public
     */
    public $name = 'ProgramsParticipantStatus';
    public $cacheQueries = true;
    public $useTable = 'programs_participants_status';

    public function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}