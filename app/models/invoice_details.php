<?php

class InvoiceDetails extends AppModel {

    public $name = 'InvoiceDetails';
    public $cacheQueries = true;
    public $useTable = 'invoice_details';
    public $actsAs = array('CacheQueries','Containable','Transactional');
    
    public $belongsTo = array(
              'Invoice' => array(
    			 'type' => 'INNER',
                 'className'  => 'Invoice',
                 'dependent'  => true,
                 'conditions' => array(
                 	'Invoice.status'=>1
                 )
              ),
              'BillingType' => array(
                  'type' => 'INNER',
                  'className' => 'BillingType',
              )
    ); 
    
    public function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => true,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }

    public function getLikeFullName( $fullName =null ){
    	if( !empty($fullName) ){
  	   		$countSearchName = $this->find('list',array('conditions'=> array("`BillingEb`.`billing_reference_code` LIKE '%".addslashes($fullName)."%'")));
    	   	return (count($countSearchName)> 0)? true: false;
    	}else{
    	   	return false;
    	}
    }
}
