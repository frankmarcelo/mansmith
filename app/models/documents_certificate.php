<?php

class DocumentsCertificate extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'DocumentsCertificate';
    public $useTable  = 'participants';
    public $cacheQueries = true;

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
/**
 * Validation
 *
 * @var array
 * @access public
 */
}