<?php

class Speaker extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'Speaker';
    public $useTable = 'speakers';      

    public $validate = array(
        'programs_division_id' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'programs_division_id field cannot be left blank.',
            )
        ),
        'email' => array(
            'email' => array(
                'rule' => 'email',
                'message' => 'Please provide a valid email address.',
            ),
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Email field cannot be left blank.',
            )
        ),
        'first_name' => array(
            'rule' => 'notEmpty',
            'message' => 'First name cannot be left blank.',
        ),
        'last_name' => array(
            'rule' => 'notEmpty',
            'message' => 'First name cannot be left blank.',
        )
    );

    public $cacheQueries = true;
    public $actsAs = array('CacheQueries');

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => true,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => true,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }

    public $belongsTo = array('ProgramsDivision');

    public function beforeSave() {
        return true;
    }

    public function dateFormatBeforeSave($dateString) {
        return date('Y-m-d', strtotime($dateString)); // Direction is from
    }

    public function afterSave($created) {
        if (!$created) {
           
        }
    }

    public function onError(){
    }
}
