<?php

class Participant extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'Participant';
	
    public $useTable  = 'participants';
    public $actsAs    = array('Transactional'); 
    public $belongsTo = array(
               'ProgramsDivision' => array(
    			  'type' => '',
                  'className' => 'ProgramsDivision',
                  'dependent' => true
               ),
               'Company' => array(
                  'type' => '',
                  'className' => 'Company',
                  'conditions' => array('Company.status'=>1 ), 
                  'dependent' => true
               ),
               'ParticipantsTitle' => array(
                  'type' => '',
                  'className' => 'ParticipantsTitle',
                  'conditions' => array('ParticipantsTitle.status'=>1 ),
                  'dependent' => true
               ),
               'ParticipantsGrouping' => array(
                  'type' => '',
                  'className' => 'ParticipantsGrouping',
               	  'conditions' => array('ParticipantsGrouping.status'=>1 ),
                  'dependent' => true
               ),
               'ParticipantsPosition' => array(
                  'type' => '',
                  'className' => 'ParticipantsPosition',
                  'conditions' => array('ParticipantsPosition.status'=>1 ),
                  'dependent' => true
               ),
               
           );
            
	public $hasMany   = array(
    		   'ProgramsParticipant' => array(
    			 	'className' => 'ProgramsParticipant',
	                'conditions' => array('ProgramsParticipant.status'=>1 ),
                 	'order' => 'ProgramsParticipant.id DESC',
                 	'dependent'=> true
               ),
               'ProgramsCompanyContact' => array(
	              'type' => 'INNER',
	              'className' => 'ProgramsCompanyContact',
               	  'order' => 'ProgramsCompanyContact.id DESC',
               	  'dependent'=> true	
	           ),
	           'NameTagsParticipants' => array(
	              'type' => 'INNER',
	              'className' => 'NameTagsParticipants',
               	  'order' => 'NameTagsParticipants.id DESC',
               	  'dependent'=> true	
	           )
           );
           
    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    } 
    
	function convertToSeoUri($string, $separator = '-'){
        $string = trim($string);
        $string = strtolower($string); // convert to lowercase text
        $string = str_replace(" ", $separator, $string);
        $string = str_replace(".", '', $string);
        $string = str_replace(",", '', $string);
        $string = str_replace("(", '', $string);
        $string = str_replace(")", '', $string);
        $string = str_replace("'", '', $string);
        $string = str_replace(":", '', $string);
        $string = str_replace("!", '', $string);
        $string = str_replace("@", '', $string);
        $string = str_replace("/", '', $string);
        $string = str_replace("\\", '', $string);
        $string = str_replace("&", '', $string);
        $string = str_replace("*", '', $string);
        $string = str_replace("+", '', $string);
        $string = str_replace("`", '', $string);
        $string = str_replace("'", $separator, $string);
        $string = str_replace("&amp;", $separator, $string);
        $string = preg_replace("/[ -]+/", "-", $string);
        return $string;
    }
    
    function getLikeFullName( $fullName =null ){
    	if( !empty($fullName) ){
    		$countSearchName = $this->find('list',array('conditions'=> array("Participant.full_name LIKE '%".addslashes($fullName)."%'")));
    		return (count($countSearchName)> 0)? true: false;		
    	}else{
    		return false;	
    	}	
    }
}
