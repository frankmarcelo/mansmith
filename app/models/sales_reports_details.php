<?php

class SalesReportsDetails extends AppModel {

    public $name = 'SalesReportsDetails';
    public $cacheQueries = true;
    public $useTable = 'sales_reports_details';
    public $actsAs = array('CacheQueries','Containable','Transactional');

    public $belongsTo = array(
                'SalesReports' => array(
                    'type' => 'INNER',
                    'className' => 'SalesReports',
                    'dependent' => true,
                    'conditions' => array('SalesReports.status'=>1 )
                ),
                'Program' => array(
                    'type' => 'INNER',
                    'className' => 'Program',
                    'dependent' => true,
                    'conditions' => array('Program.status'=>1 )
                )
           );

    public function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => true,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}
