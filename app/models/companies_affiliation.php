<?php

class CompaniesAffiliation extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'CompaniesAffiliation';
    public $cacheQueries = true;
    public $useTable = 'companies_affiliation';
    public $actsAs = array('CacheQueries','Containable');
    public $hasMany   = array('Company');  

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => true,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => true,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}