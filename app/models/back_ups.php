<?php

class BackUps extends AppModel {

    public $name = 'BackUps';
    public $cacheQueries = true;
    public $useTable = 'back_ups';
    public $actsAs = array('CacheQueries','Containable','Transactional');

    public function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => true,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}
