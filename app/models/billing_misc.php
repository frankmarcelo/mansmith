<?php

class BillingMisc extends AppModel {

    public $name = 'BillingMisc';
    public $cacheQueries = true;
    public $useTable = 'billing_misc';
    public $actsAs = array('CacheQueries','Containable','Transactional');
    
    public $belongsTo = array(
        'ProgramsDivision' => array(
            'className' => 'ProgramsDivision',
            'dependent' => true
        ),
    );
}