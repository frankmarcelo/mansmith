<?php

class ProgramsParticipant extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
	public $name = 'ProgramsParticipant';
    public $cacheQueries = true;
    public $useTable = 'programs_participants';
    public $actsAs = array('CacheQueries','Containable','Transactional');
    
    public $belongsTo = array(
              'Company' => array(
                 'className' => 'Company',
                 'dependent' => true,
    			 'order'     => 'TRIM(UPPER(Company.name)) ASC',
    			 'conditions' => array('Company.status'=>1 )
              ),
              'Program' => array(
                 'className'  => 'Program',
                 'dependent'  => true,
              	 'order' 	  => 'TRIM(UPPER(Program.title)) ASC',
              	 'conditions' => array('Program.status'=>1 )
              ),
              'Participant'   => array(
                 'className'  => 'Participant',
                 'dependent'  => true,
                 'order'      => 'TRIM(UPPER(Participant.full_name)) ASC',
              	 'conditions' => array('Participant.status'=>1 )
              ),	 
           );

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}