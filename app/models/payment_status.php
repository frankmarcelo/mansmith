<?php

class PaymentStatus extends AppModel {

    public $name = 'PaymentStatus';
    public $cacheQueries = true;
    public $useTable = 'payment_status';
    public $actsAs = array('CacheQueries','Containable','Transactional');

    public function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => true,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}
