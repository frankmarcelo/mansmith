<?php

class Program extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'Program';
    public $cacheQueries = true;
    public $actsAs = array('CacheQueries','Containable','Transactional');
    public $useTable  = 'programs';
    public $hasMany   = array(
               'ProgramsSchedule' => array(
                     'type' => 'INNER',
	                 'className' => 'ProgramsSchedule',
	                 'order' => 'ProgramsSchedule.start_date',
                     'conditions' => array('ProgramsSchedule.status'=>1 ),
	                 'dependent'=> true
               ),
               'ProgramsRatePlan' => array(	
               		 'type' => 'INNER',
	                 'className' => 'ProgramsRatePlan',
	                 'order' => 'ProgramsRatePlan.id',
               		 'conditions' => array('ProgramsRatePlan.status'=>1 ),
	                 'dependent'=> true
               ),
               'ProgramsSpeaker' => array(
                         'type' => 'INNER',
	                 'className' => 'ProgramsSpeaker',
	                 'order' => 'ProgramsSpeaker.speaker_id',
	                 'dependent'=> true
               ),
               'ProgramsParticipant' => array(
	               	 'type' => 'INNER',
	                 'className' => 'ProgramsParticipant',
	                 'order' => 'ProgramsParticipant.company_id ASC',
               		 'conditions' => array('ProgramsParticipant.status'=>1 ),
	                 'dependent'=> true
               ),
               'ProgramsCompanyContact' => array(
	               	 'type' => 'LEFT',
	                 'className' => 'ProgramsCompanyContact',
               		 'order' => 'ProgramsCompanyContact.id DESC',
               		 'dependent'=> true	
	           ),
	           'ProgramsParticipantDirectory' => array(
	               	 'type' => 'LEFT',
	                 'className' => 'ProgramsParticipantDirectory',
               		 'order' => 'ProgramsParticipantDirectory.id DESC',
	            	 'conditions' => array('ProgramsParticipantDirectory.status'=>1 ),
               		 'dependent'=> true	
	       	   ),
               'ProgramsAttendanceSheet' => array(
	               	 'type' => 'LEFT',
	                 'className' => 'ProgramsAttendanceSheet',
               		 'order' => 'ProgramsAttendanceSheet.id DESC',
	            	 'conditions' => array('ProgramsAttendanceSheet.status'=>1 ),
               		 'dependent'=> true	
	           ),
               'ProgramsCertificate' => array(
	               	 'type' => 'LEFT',
	                 'className' => 'ProgramsCertificate',
               		 'order' => 'ProgramsCertificate.id DESC',
	            	 'conditions' => array('ProgramsCertificate.status'=>1 ),
               		 'dependent'=> true	
	       ),
    );  
    public $belongsTo = array(
                'ProgramsDivision' => array(
                    'className' => 'ProgramsDivision',
                    'dependent' => true
                ),	
                'ProgramsType' => array(
                    'className' => 'ProgramsType',
                    'dependent' => true,
                    'conditions' => array('ProgramsType.status'=>1 ),
                )
           ); 
/**
 * Validation
 *
 * @var array
 * @access public
 */
    public $validate = array(
        'title' => array(
            'rule' => array('minLength', 1),
            'message' => 'Event cannot be empty.',
        ),
        'venue_name' => array(
            'rule' => array('minLength', 1),
            'message' => 'Venue cannot be empty.',
        ),
        'address' => array(
            'rule' => array('minLength', 1),
            'message' => 'Address cannot be empty.',
        ),
        'city' => array(
            'rule' => array('minLength', 1),
            'message' => 'City cannot be empty.',
        )
    );

    
    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    } 

    public static function convertToSeoUri($string, $separator = '-'){
        $string = trim($string);
        $string = strtolower($string); // convert to lowercase text
        $string = str_replace(" ", $separator, $string);
        $string = str_replace(".", '', $string);
        $string = str_replace(",", '', $string);
        $string = str_replace("(", '', $string);
        $string = str_replace(")", '', $string);
        $string = str_replace("'", '', $string);
        $string = str_replace(":", '', $string);
        $string = str_replace("!", '', $string);
        $string = str_replace("@", '', $string);
        $string = str_replace("/", '', $string);
        $string = str_replace("\\", '', $string);
        $string = str_replace("&", '', $string);
        $string = str_replace("*", '', $string);
        $string = str_replace("+", '', $string);
        $string = str_replace("`", '', $string);
        $string = str_replace("'", $separator, $string);
        $string = str_replace("&amp;", $separator, $string);
        $string = preg_replace("/[ -]+/", "-", $string);
        return $string;
    }
    
 	function getLikeTitleName( $title =null ){
    	if( !empty($title) ){
    		$countSearchName = $this->find('list',array('conditions'=> array("Program.title LIKE '%".addslashes(trim($title))."%'")));
    		return (count($countSearchName)> 0)? true: false;		
    	}else{
    		return false;	
    	}	
    }
}
