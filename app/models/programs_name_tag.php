<?php

class ProgramsNameTag extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'ProgramsNameTag';
    public $cacheQueries = true;
    public $actsAs = array('CacheQueries','Containable','Transactional');
    public $useTable  = 'programs_name_tags';
    public $hasMany   = array(
    		 'NameTagsParticipants' => array(
                 'className' => 'NameTagsParticipants',
                 'dependent' => true,
              )	
    	   );
    public $belongsTo = array(
    		 'Program' => array(
                 'className' => 'Program',
                 'dependent' => true,
                 'conditions' => array('Program.status'=>1 )
               )
           ); 
           
	public function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
        $conditions = compact('conditions');
        if ($recursive != $this->recursive) {
        	$conditions['recursive'] = $recursive;
        }
        unset( $extra['contain'] );
        $count = $this->find('count', array_merge($conditions, $extra));
           
        if (isset($extra['group'])) {
        	$count = $this->getAffectedRows();
        }
        return $count;
    }	

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    } 
    
	function getLikeFullName($fullName=null){
        $sql  = "
    	SELECT COUNT(DISTINCT `ProgramsNameTag`.program_id) AS `count`
    	FROM `programs_name_tags` AS `ProgramsNameTag`
    	LEFT JOIN `programs` AS `Program` ON (`ProgramsNameTag`.`program_id` = `Program`.`id` AND `Program`.`status` = 1)
    	WHERE
    	(
    		(`Program`.`status` = ".intval(Configure::read('status_live')).") AND
    		(`ProgramsNameTag`.`status` = ".intval(Configure::read('status_live')).") AND
            (
                (`Program`.`title` LIKE '%".addslashes($fullName)."%') OR
                (`ProgramsNameTag`.`source_file` LIKE '%".addslashes($fullName)."%')
            )
    	)
    	GROUP BY program_id
    	";
    	$results = $this->query($sql);
		return count($results);	
    }
}