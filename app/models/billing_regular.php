<?php

class BillingRegular extends AppModel {

    public $name = 'BillingRegular';
    public $cacheQueries = true;
    public $useTable = 'billing';
    public $actsAs = array('CacheQueries','Containable','Transactional');
    
    public $belongsTo = array(
              'Program' => array(
                 'className'  => 'Program',
                 'dependent'  => true,
                 'conditions' => array('Program.status'=>1,'BillingRegular.billing_type_id'=>4)
              ),
              'Company' => array(
                  'type' => '',
                  'className' => 'Company',
                  'conditions' => array('Company.status'=>1,'BillingRegular.billing_type_id'=>4) 
              ),
              'BillingType' => array(
                  'className' => 'BillingType'
              )
    ); 
    
	public function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
        $conditions = compact('conditions');
        if ($recursive != $this->recursive) {
        	$conditions['recursive'] = $recursive;
        }
        unset( $extra['contain'] );
        $count = $this->find('count', array_merge($conditions, $extra));
           
        if (isset($extra['group'])) {
        	$count = $this->getAffectedRows();
        }
        return $count;
    }
    
    public function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => true,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
    
    public function getLikeFullName( $fullName =null ){
    	$sql  = "
    	SELECT COUNT(DISTINCT `BillingRegular`.program_id) AS `count`
    	FROM `billing` AS `BillingRegular`
    	JOIN `programs` AS `Program` ON (`BillingRegular`.`program_id` = `Program`.`id` AND `Program`.`status` = 1)
        JOIN `companies` AS `Company` ON (`BillingRegular`.`company_id` = `Company`.`id` AND `Company`.`status` = 1 AND `BillingRegular`.`billing_type_id` = 4)
    	WHERE
    	(
			(`BillingRegular`.`billing_type_id` = 4) AND
    		(`Program`.`status`   = ".intval(Configure::read('status_live')).") AND
    		(`BillingRegular`.`status` = ".intval(Configure::read('status_live')).") AND
			(
			    (`Program`.`title` LIKE '%".addslashes($fullName)."%') OR
				(`BillingRegular`.`seminar` LIKE '%".addslashes($fullName)."%') OR
				(`BillingRegular`.`source_file` LIKE '%".addslashes($fullName)."%') OR
				(`BillingRegular`.`billing_reference_code` LIKE '%".addslashes($fullName)."%') OR
                (`Company`.`name` LIKE '%".addslashes($fullName)."%') 
			)
    	)
    	GROUP BY program_id
    	";
    	$results = $this->query($sql);
		return count($results);
    }
}