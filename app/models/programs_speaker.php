<?php

class ProgramsSpeaker extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'ProgramsSpeaker';
    public $useTable = 'programs_speakers';
    public $cacheQueries = true;
    public $actsAs = array('CacheQueries','Transactional');
    public $belongsTo = array(
	               'Program' => array(
	    			  'type' => '',
	                  'className'  => 'Program',
	                  'dependent'  => true,
    				  'conditions' => array('Program.status'=>1 ) 
	               )
		);		
		
    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }

    private function removeExistingSpeaker( $program_id ){//return void;
        $sql = "DELETE FROM ".$this->useTable." WHERE program_id='".$program_id."' ";
        $this->query( $sql );
    }

    public function setCreateSpeakers( $data, $program_id ){
        $aSpeakers = array();
        foreach( $data as $speaker_key => $speaker_value ){
           if( strstr( $speaker_value,'selected' ) ){
              $aSpeakerPlaceHolder = explode( "_", $speaker_value );
              $aSpeakers[] = $aSpeakerPlaceHolder[0];
           }
        }

        $this->removeExistingSpeaker( $program_id );
        
        if( is_array($aSpeakers) && $aSpeakers ){
           foreach( $aSpeakers as $speaker_id ){
              $sql = 'INSERT INTO '.$this->useTable." SET program_id ='".$program_id."', speaker_id ='".$speaker_id."'";
              $this->query( $sql );
           }
           return true;
        } 
        return false;
    } 
}