<?php

class Financial extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'Financial';
    public $cacheQueries = true;
    public $actsAs = array('CacheQueries');
    public $useTable  = 'participants';
    public $hasMany   = array(
           );  
    public $belongsTo = array(
               'ProgramsDivision' => array(
                 'className' => 'ProgramsDivision',
                 'dependent' => true
               )
           ); 

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
/**
 * Validation
 *
 * @var array
 * @access public
 */
}