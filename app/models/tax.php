<?php
class Tax extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'Tax';
    public $useTable  = 'tax';
    public $cacheQueries = true;


    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}
