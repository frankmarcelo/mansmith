<?php

class ParticipantsTitle extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'ParticipantsTitle';
    public $cacheQueries = true;
    public $useTable = 'participants_title';
    public $actsAs = array('CacheQueries','Containable');

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => true,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}