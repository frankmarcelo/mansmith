<?php

class SalesReports extends AppModel {

    public $name = 'SalesReports';
    public $cacheQueries = true;
    public $useTable = 'sales_reports';
    public $actsAs = array('CacheQueries','Containable','Transactional');

    public $hasMany   = array(
               'SalesReportsDetails' => array(
                     'type' => 'INNER',
                     'foreignKey' => 'sales_reports_id',
                     'className' => 'SalesReportsDetails',
                     'order' => array('SalesReportsDetails.program_id'=>'ASC'),
                     'dependent' => true
	           )
           );

    public function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => true,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }

    public function getLikeFullName($fullName =null){
        $sql  = "
        SELECT COUNT(DISTINCT `SalesReports`.id) AS `count`
        FROM `sales_reports` AS `SalesReports`
        JOIN `sales_reports_details` AS `SalesReportsDetails` ON (`SalesReportsDetails`.sales_reports_id = `SalesReports`.id)
        JOIN `programs` AS `Program` ON (`SalesReportsDetails`.`program_id` = `Program`.`id` AND `Program`.`status` = 1)
        WHERE
    	(
			(`Program`.`status`   = ".intval(Configure::read('status_live')).") AND
    		(`SalesReports`.`status` = ".intval(Configure::read('status_live')).") AND
			(
			    (`Program`.`title` LIKE '%".addslashes($fullName)."%') OR
				(`SalesReports`.`source_file` LIKE '%".addslashes($fullName)."%') OR
				(`SalesReports`.`reference_code` LIKE '%".addslashes($fullName)."%')
            )
    	)
    	GROUP BY SalesReports.id
    	";
        $results = $this->query($sql);
        return count($results);
    }
}
