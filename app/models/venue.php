<?php

class Venue extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'Venue';
    public $useTable = 'programs';
    public $cacheQueries = true;
}