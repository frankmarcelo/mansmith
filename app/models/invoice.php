<?php

class Invoice extends AppModel {

    public $name = 'Invoice';
    public $cacheQueries = true;
    public $useTable = 'invoice';
    public $actsAs = array('CacheQueries','Containable','Transactional');
    public $belongsTo = array(
	      	  'Program' => array(
                 'className'  => 'Program',
                 'dependent'  => true,
                 'conditions' => array('Program.status'=>1)
              ),
              'Company' => array(
                  'type' => '',
                  'className' => 'Company',
                  'conditions' => array('Company.status'=>1) 
              )
    );

    public $hasMany   = array(
    		   'InvoiceDetails' => array(
                 'className' => 'InvoiceDetails',
                 'dependent' => true,
               )	
    	   );
    
    public function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => true,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }

    public function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
        $conditions = compact('conditions');
        if ($recursive != $this->recursive) {
        	$conditions['recursive'] = $recursive;
        }
        unset( $extra['contain'] );
        $count = $this->find('count', array_merge($conditions, $extra));

        if (isset($extra['group'])) {
        	$count = $this->getAffectedRows();
        }
        return $count;
    }
    
    public function getLikeFullName($fullName =null){
        $sql  = "
    	SELECT COUNT(DISTINCT `Invoice`.program_id) AS `count`
    	FROM `invoice` AS `Invoice`
        JOIN `invoice_details` AS InvoiceDetails ON (`InvoiceDetails`.invoice_id = `Invoice`.`id` AND `InvoiceDetails`.status=1)
    	JOIN `programs` AS `Program` ON (`Invoice`.`program_id` = `Program`.`id` AND `Program`.`status` = 1)
    	WHERE
    	(
			(`Program`.`status`   = ".intval(Configure::read('status_live')).") AND
    		(`Invoice`.`status` = ".intval(Configure::read('status_live')).") AND
			(
			    (`Program`.`title` LIKE '%".addslashes($fullName)."%') OR
                (`Invoice`.`seminar` LIKE '%".addslashes($fullName)."%') OR
              	(`InvoiceDetails`.source_file LIKE '%".addslashes($fullName)."%') OR
				(`InvoiceDetails`.`invoice_number` LIKE '%".addslashes($fullName)."%')  OR
                (`InvoiceDetails`.`company` LIKE '%".addslashes($fullName)."%')  OR
                (`InvoiceDetails`.`recipient` LIKE '%".addslashes($fullName)."%')  OR
                (`InvoiceDetails`.`recipient_address` LIKE '%".addslashes($fullName)."%')
            )
    	)
    	GROUP BY `Invoice`.`program_id`
    	";
    	$results = $this->query($sql);
		return count($results);
    }
}