<?php

class LatestCompany extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'LatestCompany';
    public $cacheQueries = true;
    public $useTable  = 'latest_companies';
    
    public $belongsTo = array(
               'Company' => array(
    			 'type' => 'INNER',
                 'className' => 'Company',
                 'dependent' => true
               )
           ); 

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => true,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}