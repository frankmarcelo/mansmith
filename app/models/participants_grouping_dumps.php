<?php

class ParticipantsGroupingDumps extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'ParticipantsGroupingDumps';
    public $cacheQueries = true;
    public $useTable = 'participants_grouping_dumps';
    public $actsAs = array('CacheQueries','Containable');
    
    public $belongsTo   = array(
        'ParticipantsGrouping' => array(
            'type' => 'INNER',
            'className' => 'ParticipantsGrouping',
            'dependent'=> true

        ),
    );

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => true,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}