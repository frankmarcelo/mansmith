<?php

class Company extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'Company';
    public $cacheQueries = true;
    public $useTable  = 'companies';
    public $actsAs   = array('Transactional','Containable');
    
    /**TODO
     * to add has many programs attended
     * has many participants
     * has many programs
     * has many payments
     * 
     */
    public $hasMany   = array(
    		   'Participant' => array(
    			 	'className' => 'Participant',
                 	'order' => 'TRIM(UPPER(Participant.full_name)) ASC',
                 	'dependent'=> true,
    				'conditions' => array('Participant.status'=>1 )
               ),
               'ProgramsParticipant' => array(
    			 	'className' => 'ProgramsParticipant',
                 	'dependent'=> true,
               		'conditions' => array('ProgramsParticipant.status'=>1 )
               ),
               'ProgramsCompanyContact' => array(
	               	 'type' => 'LEFT',
	                 'className' => 'ProgramsCompanyContact',
               		 'dependent'=> true	
	           )
           );
    
    public $belongsTo = array(
               'ProgramsDivision' => array(
    			 'type' => 'INNER',
                 'className' => 'ProgramsDivision',
                 'dependent' => true
               ),
               'CompaniesAffiliation' => array(
               	 'type' => 'INNER',
                 'className' => 'CompaniesAffiliation',
                 'dependent' => true,
               	 'conditions' => array('CompaniesAffiliation.status'=>1 )
               ),
               'CompaniesIndustry' => array(
                 'type' => 'INNER',
                 'className' => 'CompaniesIndustry',
                 'dependent' => true,
               	 'conditions' => array('CompaniesIndustry.status'=>1 )
               )
           ); 

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => true,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
	
	function convertToSeoUri($string, $separator = '-'){
		$string = trim($string);
        $string = strtolower($string); // convert to lowercase text
        $string = str_replace(" ", $separator, $string);
        $string = str_replace(".", '', $string);
        $string = str_replace(",", '', $string);
        $string = str_replace("(", '', $string);
        $string = str_replace(")", '', $string);
        $string = str_replace("'", '', $string);
        $string = str_replace(":", '', $string);
        $string = str_replace("!", '', $string);
        $string = str_replace("@", '', $string);
        $string = str_replace("/", '', $string);
        $string = str_replace("\\", '', $string);
        $string = str_replace("&", '', $string);
        $string = str_replace("*", '', $string);
        $string = str_replace("+", '', $string);
        $string = str_replace("`", '', $string);
        $string = str_replace("'", $separator, $string);
        $string = str_replace("&amp;", $separator, $string);
        $string = preg_replace("/[ -]+/", "-", $string);
        return $string;
    }
    
	function getLikeFullName( $fullName =null ){
    	if( !empty($fullName) ){
    		$countSearchName = $this->find('list',array('conditions'=> array("Company.name LIKE '%".addslashes($fullName)."%'")));
    		return (count($countSearchName)> 0)? true: false;		
    	}else{
    		return false;	
    	}	
    }
}
