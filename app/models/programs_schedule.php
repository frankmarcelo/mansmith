<?php

class ProgramsSchedule extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'ProgramsSchedule';
    public $useTable = 'programs_schedules';
    public $cacheQueries = true;
    public $actsAs = array('CacheQueries','Containable','Transactional');
    public $belongsTo = array(
               'Program' => array(
                 'className' => 'Program',
                 'dependent' => true,
                 'conditions' => array('Program.status'=>1 )
               ),
                'ProgramsScheduleType' => array(
                    'className' => 'ProgramsScheduleType',
                    'dependent' => true,
                    'conditions' => array('ProgramsSchedule.status'=>1 )
                ),
    );

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }

    function getDateCriteria( $dateString ='' ){
       list( $month, $day, $year ) = explode( "/" , $dateString );

       if( strlen(trim($month)) < 2 ){
          $month = "0".$month;
       }

       if( strlen(trim($day)) < 2 ){
          $day = "0".$day;
       }

       return $this->dateFormatBeforesave($year."-".$month."-".$day);
    }
    
    function setScheduleDeletedByProgramId( $programId = 0 ){
       if( intval($programId) > 0 ){
       	  $sSql =  "DELETE FROM ".$this->useTable." WHERE program_id ='".$programId."'";	
    	  $this->query($sSql);   	
       }	
    }

    function dateFormatBeforeSave($dateString) {
       return date('Y-m-d', strtotime($dateString)); // Direction is from
    }

    function onError(){
       //echo $this->ProgramsSchedule->queryError;die;
    }
}