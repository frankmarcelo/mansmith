<?php

class BillingType extends AppModel {

    public $name = 'BillingType';
    public $cacheQueries = true;
    public $useTable = 'billing_type';
    public $actsAs = array('CacheQueries','Containable','Transactional');


     public $hasMany   = array(
               'InvoiceDetails' => array(
                 'className' => 'InvoiceDetails',
                 'dependent' => true,
               ),
               'BillingEb' => array(
                 'className' => 'BillingEb',
                 'dependent' => true,
               ),
               'BillingNonEb' => array(
                 'className' => 'BillingNonEb',
                 'dependent' => true,
               ),
               'BillingOsp' => array(
                 'className' => 'BillingOsp',
                 'dependent' => true,
               ),
               'BillingRegular' => array(
                 'className' => 'BillingRegular',
                 'dependent' => true,
               ),
           );
    
    public function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => true,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}
