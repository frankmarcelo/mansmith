<?php

class ProgramsAttendanceSheet extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'ProgramsAttendanceSheet';
    public $cacheQueries = true;
    public $actsAs = array('CacheQueries','Containable','Transactional');
    public $useTable = 'programs_attendance_sheets';
    
    public $hasMany   = array(
    		   'AttendanceSheetsParticipants' => array(
                 'className' => 'AttendanceSheetsParticipants',
                 'dependent' => true,
               )	
    	   );
    public $belongsTo = array(
    		 'Program' => array(
                 'className' => 'Program',
                 'dependent' => true,
                 'conditions' => array('Program.status'=>1 )
               )
           ); 
	
    public function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
        $conditions = compact('conditions');
        if ($recursive != $this->recursive) {
        	$conditions['recursive'] = $recursive;
        }
        unset( $extra['contain'] );
        $count = $this->find('count', array_merge($conditions, $extra));
           
        if (isset($extra['group'])) {
        	$count = $this->getAffectedRows();
        }
        return $count;
    }
    
	public function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
    
	function getLikeFullName($fullName=null){
    	$sql  = "
    	SELECT COUNT(DISTINCT `ProgramsAttendanceSheet`.program_id) AS `count` 
    	FROM `programs_attendance_sheets` AS `ProgramsAttendanceSheet` 
    	LEFT JOIN `programs` AS `Program` ON (`ProgramsAttendanceSheet`.`program_id` = `Program`.`id` AND `Program`.`status` = 1) 
    	WHERE 
    	(
    		(`Program`.`status` = ".intval(Configure::read('status_live')).") AND 
    		(`ProgramsAttendanceSheet`.`status` = ".intval(Configure::read('status_live')).") AND 
            (
                (`Program`.`title` LIKE '%".addslashes($fullName)."%') OR
                (`ProgramsAttendanceSheet`.`source_file` LIKE '%".addslashes($fullName)."%')
            )
    	) 
    	GROUP BY program_id
    	";
    	$results = $this->query($sql);
		return count($results);	
    }
}
