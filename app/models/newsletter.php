<?php

class Newsletter extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'Newsletter';
    public $useTable  = 'participants';
    public $hasMany   = array(
           );  
    public $belongsTo = array(
               'ProgramsDivision' => array(
                 'className' => 'ProgramsDivision',
                 'dependent' => true
               )
           ); 

    public $cacheQueries = true;
    public $actsAs = array('CacheQueries');

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
/**
 * Validation
 *
 * @var array
 * @access public
 */
}