<?php

class ProgramsCertificate extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'ProgramsCertificate';
    public $cacheQueries = true;
    public $useTable = 'programs_certificates';
    public $actsAs = array('CacheQueries','Containable','Transactional');
    
    public $hasMany   = array(
    		   'CertificatesParticipants' => array(
                 'className' => 'CertificatesParticipants',
                 'dependent' => true,
               )	
    	   );
    public $belongsTo = array(
    		 'Program' => array(
                 'className' => 'Program',
                 'dependent' => true,
                 'conditions' => array('Program.status'=>1 )
               )
           );

	public function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
        $conditions = compact('conditions');
        if ($recursive != $this->recursive) {
        	$conditions['recursive'] = $recursive;
        }
        unset( $extra['contain'] );
        $count = $this->find('count', array_merge($conditions, $extra));
           
        if (isset($extra['group'])) {
        	$count = $this->getAffectedRows();
        }
        return $count;
    }
	
    public function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
    
	function getLikeFullName($fullName=null){
        $sql  = "
    	SELECT COUNT(DISTINCT `ProgramsCertificate`.program_id) AS `count`
    	FROM `programs_certificates` AS `ProgramsCertificate`
    	LEFT JOIN `programs` AS `Program` ON (`ProgramsCertificate`.`program_id` = `Program`.`id` AND `Program`.`status` = 1)
    	WHERE
    	(
    		(`Program`.`status` = ".intval(Configure::read('status_live')).") AND
    		(`ProgramsCertificate`.`status` = ".intval(Configure::read('status_live')).") AND
            (
                (`Program`.`title` LIKE '%".addslashes($fullName)."%') OR
                (`ProgramsCertificate`.`source_file` LIKE '%".addslashes($fullName)."%')
            )
    	)
    	GROUP BY program_id
    	";
    	$results = $this->query($sql);
		return count($results);	
    }
}