<?php

class ParticipantsPosition extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'ParticipantsPosition';
    public $cacheQueries = true;
    public $useTable = 'participants_position';
    public $actsAs = array('CacheQueries','Containable');

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => true,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
    
	function convertToSeoUri($string, $separator = '-'){
        $string = trim($string);
        $string = strtolower($string); // convert to lowercase text
        $string = str_replace(" ", $separator, $string);
        $string = str_replace(".", '', $string);
        $string = str_replace(",", '', $string);
        $string = str_replace("(", '', $string);
        $string = str_replace(")", '', $string);
        $string = str_replace("'", '', $string);
        $string = str_replace(":", '', $string);
        $string = str_replace("'", $separator, $string);
        $string = str_replace("&amp;", $separator, $string);
        $string = preg_replace("/[ -]+/", "-", $string);
        return $string;
    }
    
    public function createNewPosition( $sPositionName = null){
    	if( !empty($sPositionName) ){
    		$countSearchName = $this->find('list',array('conditions'=> array("ParticipantsPosition.name ='".$fullName."'")));
    		if( count($countSearchName) > 0 ){
    			return $countSearchName['ParticipantsPosition']['id'];	
    		}else{//insert a new one
    			App::import('Sanitize');
    			$sPositionName = Sanitize::clean($sPositionName);
    			$sDateTimeStamp = date("Y-m-d H:i:s");
    			$this->data['ParticipantsPosition']['name'] = $sPositionName;
    			$this->data['ParticipantsPosition']['seo_name'] = $this->convertToSeoUri($sPositionName);
				$this->data['ParticipantsPosition']['date_created'] = $sDateTimeStamp;
				$this->data['ParticipantsPosition']['date_modified'] = $sDateTimeStamp;
				$this->data['ParticipantsPosition']['who_created'] = $this->Session->read('Auth.User.id');
				$this->begin();

				if( $this->save( $this->data ) ) {
	                $this->commit(); // Persist the data
    	        	return $this->id;    
        	    }else{
        	    	return false;
        	    }
			}
    	}else{
    		return false;	
    	}	
    }
}
