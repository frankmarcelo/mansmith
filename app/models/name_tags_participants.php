<?php

class NameTagsParticipants extends AppModel {

	public $name = 'NameTagsParticipants';
    public $cacheQueries = true;
    public $useTable = 'name_tags_participants';
    public $actsAs = array('CacheQueries','Containable','Transactional');
    
    public $belongsTo = array(
	    		  'Participant' => array(
	                 'className'  => 'Participant',
	                 'dependent'  => true,
	    			 'foreignKey' => 'participant_id',
	                 'conditions' => array('Participant.status'=>1,'`NameTagsParticipants`.`company_id`=`Participant`.`company_id`')
	              ),
	              'ProgramsNameTag' => array(
	                 'className' => 'ProgramsNameTag',
	                 'dependent' => true,
	                 'conditions' => array('ProgramsNameTag.status'=>1 )
	              )
	           ); 
       
    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => true,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
    
	function getLikeFullName( $fullName =null ){
    	if( !empty($fullName) ){
    		$countSearchName = $this->find('list',array('conditions'=> array("NameTagsParticipants.source_file LIKE '%".addslashes($fullName)."%'")));
    		return (count($countSearchName)> 0)? true: false;		
    	}else{
    		return false;	
    	}	
    }
}